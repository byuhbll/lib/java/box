/** */
package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.impl.MemoryDatabase;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the {@link Box} api. This does not test all the various intricate settings and timings of
 * Box. That is done in the box-test module.
 *
 * @author Charles Draper
 */
class BoxTest extends AbstractTest {

  @BeforeEach
  void beforeEach() {
    init("principal", "unprincipal", "disabled");
  }

  /**
   * Test method for {@link edu.byu.hbll.box.Box#newBox(com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  void testNewBox() {
    assertNotNull(box);
  }

  /** Test method for {@link edu.byu.hbll.box.Box#newBuilder()}. */
  @Test
  void testNewBuilder() {
    Box.Builder builder = new Box.Builder();

    builder.config(loadYaml(BoxTest.class.getSimpleName() + ".yml"));

    SourceConfig source =
        new SourceConfig.Builder("extra")
            .processor(new TestProcessor())
            .harvester(new TestHarvester())
            .db(new MemoryDatabase())
            .build();
    builder.source(source);

    Box box = builder.build();
    assertEquals("principal", box.getSourceNames().get(0));
    assertEquals("unprincipal", box.getSourceNames().get(1));
    assertEquals("extra", box.getSourceNames().get(box.getSourceNames().size() - 1));
    box.close();
  }

  /** Test method for {@link edu.byu.hbll.box.Box#getPrincipalSource()}. */
  @Test
  void testGetPrincipalSource() {
    assertEquals("principal", box.getPrincipalSource().getName());
  }

  /** Test method for {@link edu.byu.hbll.box.Box#getHealth()}. */
  @Test
  void testGetHealth() {
    sleep(1000);
    BoxHealth health = box.getHealth();
    assertTrue(health.getSourceHealth("principal").isHealthy());
    assertTrue(health.getSourceHealth("unprincipal").isHealthy());
  }

  /** Test method for {@link edu.byu.hbll.box.Box#getSource(java.lang.String)}. */
  @Test
  void testGetSource() {
    assertEquals("unprincipal", box.getSource("unprincipal").getName());
  }

  /** Test method for {@link edu.byu.hbll.box.Box#getSources()}. */
  @Test
  void testGetSources() {
    List<Source> sources = new ArrayList<>(box.getSources().values());
    assertEquals("principal", sources.get(0).getName());
    assertEquals("unprincipal", sources.get(1).getName());
  }

  /** Test method for {@link edu.byu.hbll.box.Box#getSourceNames()}. */
  @Test
  void testGetSourceNames() {
    List<String> sourceNames = box.getSourceNames();
    assertEquals("principal", sourceNames.get(0));
    assertEquals("unprincipal", sourceNames.get(1));
  }

  /** Test method for {@link edu.byu.hbll.box.Box#verifySource(java.lang.String)}. */
  @Test
  void testVerifySource() {
    assertEquals("principal", box.verifySource("box"));
    assertEquals("unprincipal", box.verifySource("unprincipal"));
    assertThrows(IllegalArgumentException.class, () -> box.verifySource("fake"));
  }

  /** Test method for {@link edu.byu.hbll.box.Box#close()}. */
  @Test
  void testClose() {
    box.close();
  }

  /** Tests that disabled sources are not registered at all. */
  @Test
  void testGetDisabledSource() {
    assertThrows(IllegalArgumentException.class, () -> box.getSource("disabled"));
  }
}
