package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.impl.MemoryDatabase;
import java.io.IOException;
import java.util.List;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

public class DatabaseCopierTest {

  @BeforeEach
  public void beforeEach() {}

  @Test
  public void testMain() throws JsonProcessingException, IOException {
    DatabaseCopier.main(new String[] {"src/test/resources/DatabaseCopierTest.yml"});
  }

  @Test
  public void testCopy() {
    BoxDatabase from = new MemoryDatabase();
    BoxDatabase to = new MemoryDatabase();
    BoxDocument expected = null;

    for (Status status : List.of(Status.READY, Status.DELETED, Status.ERROR, Status.UNPROCESSED)) {
      for (int i = 1; i <= 10; i++) {
        BoxDocument document = new BoxDocument(status + "." + i);
        document.addDependency("source", i + "");
        document.addFacet("facet", i + "");
        document.withDocument().put("field", "value." + i);
        document.setGroupId("group." + i);
        document.setStatus(status);
        from.save(List.of(document));

        if (expected == null) {
          expected = document;
        }
      }
    }

    DatabaseCopier.copy(from, to);

    assertEquals(40, to.count(new BoxQuery().addAllStatuses()));
    assertEquals(expected, to.find(new BoxQuery("READY.1")).get(0));
  }
}
