package edu.byu.hbll.box;

import java.util.Objects;

/**
 * @author Charles Draper
 */
public class TestHarvester implements Harvester {

  public int max = 10;
  public int limit = 10;

  @Override
  public HarvestResult harvest(HarvestContext context) {
    // Tests that the contents of the context are non-null
    Objects.nonNull(context.getBox());
    Objects.nonNull(context.getSource());
    Objects.nonNull(context.getCursor());

    HarvestResult result = new HarvestResult();

    int cursor = context.getCursor().path("cursor").asInt(1);

    for (int i = cursor; i < cursor + limit; i++) {
      result.addDocuments(new BoxDocument(i + "").setAsReady());
    }

    result.setMore(Integer.parseInt(result.get(limit - 1).getId()) < max);
    result.setCursor(context.getCursor().put("cursor", cursor + result.size()));

    return result;
  }
}
