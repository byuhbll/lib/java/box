package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import org.junit.jupiter.api.Test;

public class SourceConfigTest {

  /** Tests that the default removeAge is 90 days. */
  @Test
  public void testDefaultRemoveAge() {
    assertEquals(Duration.ofDays(90), SourceConfig.builder().name("test").build().getRemoveAge());
  }

  /** Tests that setting the removeAge works. */
  @Test
  public void testSettingRemoveAge() {
    assertEquals(
        Duration.ofDays(30),
        SourceConfig.builder().name("test").removeAge(Duration.ofDays(30)).build().getRemoveAge());
  }
}
