package edu.byu.hbll.box;

/** A {@link Processor} for testing box. */
public class IdentityProcessor implements SingleProcessor {

  private Source source;

  public void postInit(InitConfig config) {
    this.source = config.getSource();
  }

  @Override
  public ProcessResult process(ProcessContext context) {
    BoxDocument document = source.collect(new BoxQuery(context.getId())).get(0);

    if (!document.isProcessed()) {
      document.setAsReady();
    }

    document = document.setCursor(null).setModified(null).setProcessed(null);
    return new ProcessResult(document);
  }
}
