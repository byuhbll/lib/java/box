package edu.byu.hbll.box.impl;

import static edu.byu.hbll.witness.WitnessUtils.sleep;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.internal.util.CursorUtils;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class MemoryDatabaseTest {

  private MemoryDatabase db = new MemoryDatabase();

  /**
   * Verifies that unprocessed documents saved or pulled from database have the sticky status bit
   * set.
   */
  @Test
  void testStickyStatuses() {
    BoxDocument doc1 = db.find(new BoxQuery("1")).get(0);
    doc1.withDocument();
    assertTrue(doc1.isUnprocessed());

    BoxDocument doc2 = new BoxDocument("2");
    db.save(List.of(new BoxDocument("2")));
    doc2 = db.find(new BoxQuery("2")).get(0);
    doc2.withDocument();
    assertTrue(doc2.isUnprocessed());
  }

  /** Verifies proper function of the offset. */
  @Test
  void testOffset() {
    for (int i = 1; i <= 5; i++) {
      db.save(List.of(new BoxDocument(i + "").setAsReady()));
    }

    QueryResult result = db.find(new BoxQuery().setOffset(2));
    assertEquals(
        List.of("3", "4", "5"), result.stream().map(d -> d.getId()).collect(Collectors.toList()));
  }

  /** Verifies proper function of order. */
  @Test
  void testOrder() {
    for (int i = 1; i <= 5; i++) {
      db.save(List.of(new BoxDocument(i + "").setAsReady()));
    }

    QueryResult result = db.find(new BoxQuery().setDescendingOrder());
    assertEquals(
        List.of("5", "4", "3", "2", "1"),
        result.stream().map(d -> d.getId()).collect(Collectors.toList()));
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MemoryDatabase#count(edu.byu.hbll.box.BoxQuery)}.
   */
  @Test
  void testCountDocuments() {
    List<BoxDocument> documents =
        List.of(
            new BoxDocument("1"),
            new BoxDocument("2").setAsReady(),
            new BoxDocument("3").setAsDeleted(),
            new BoxDocument("4").setAsError(),
            new BoxDocument("5").setAsReady().addFacet("a", "1"),
            new BoxDocument("6").setAsReady().addFacet("a", "2"),
            new BoxDocument("7").setAsReady().addFacet("a", "1").addFacet("b", "1"));

    long start = CursorUtils.nextCursor();
    db.save(documents.subList(0, 4));
    sleep(100);
    db.save(documents.subList(4, documents.size()));

    assertAll(
        () -> assertEquals(3, db.count(new BoxQuery("1", "2", "99"))),
        () -> assertEquals(5, db.count(new BoxQuery())),
        () -> assertEquals(5, db.count(new BoxQuery().setLimit(1))),
        () -> assertEquals(3, db.count(new BoxQuery().setCursor(start + 50000000))),
        () -> assertEquals(2, db.count(new BoxQuery().addFacet("a", "1"))),
        () -> assertEquals(3, db.count(new BoxQuery().addFacet("a", "1").addFacet("a", "2"))),
        () -> assertEquals(1, db.count(new BoxQuery().addFacet("a", "1").addFacet("b", "1"))));
  }

  /** Tests that the priorities are working in the process queue. */
  @Test
  void testPriorityQueue() {
    List<QueueEntry> entries = new ArrayList<>();
    entries.add(new QueueEntry("1").setPriority(1).setAttempt(Instant.MIN));
    entries.add(new QueueEntry("2").setPriority(1).setAttempt(Instant.now()));
    entries.add(new QueueEntry("3").setPriority(1).setAttempt(Instant.MAX));
    entries.add(new QueueEntry("4").setPriority(1.5).setAttempt(Instant.MIN));
    entries.add(new QueueEntry("5").setPriority(1.5).setAttempt(Instant.now()));
    entries.add(new QueueEntry("6").setPriority(1.5).setAttempt(Instant.MAX));
    entries.add(new QueueEntry("7").setPriority(2).setAttempt(Instant.MIN));
    entries.add(new QueueEntry("8").setPriority(2).setAttempt(Instant.now()));
    entries.add(new QueueEntry("9").setPriority(2).setAttempt(Instant.MAX));

    Collections.shuffle(entries);
    db.addToQueue(entries);

    assertEquals(List.of("1", "2", "4", "5", "7", "8"), db.nextFromQueue(10));
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MemoryDatabase#addToQueue(java.time.Duration)}.
   */
  @Test
  void testAddToQueueDuration() {
    db.save(List.of(new BoxDocument("1").setAsReady()));
    db.save(List.of(new BoxDocument("4").setAsDeleted()));
    sleep(400);
    db.save(List.of(new BoxDocument("2").setAsReady()));
    db.save(List.of(new BoxDocument("5").setAsDeleted()));
    sleep(400);
    db.save(List.of(new BoxDocument("3").setAsReady()));
    db.save(List.of(new BoxDocument("6").setAsDeleted()));
    sleep(400);

    assertEquals(0, db.getQueueSize());
    db.addToQueue(Duration.ofMillis(1400));
    assertEquals(0, db.getQueueSize());
    db.addToQueue(Duration.ofMillis(1000));
    assertEquals(1, db.getQueueSize());
    db.addToQueue(Duration.ofMillis(600));
    assertEquals(2, db.getQueueSize());
    db.addToQueue(Duration.ofMillis(200));
    assertEquals(3, db.getQueueSize());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MemoryDatabase#nextFromQueue(int)}. */
  @Test
  void testNextFromQueue() {
    Instant now = Instant.now();

    for (int i = 1; i <= 10; i++) {
      // first nine should be ready, tenth will not be ready
      db.addToQueue(List.of(new QueueEntry(i + "", now.plusSeconds(i - 9), 1, false)));
    }

    assertEquals(10, db.getQueueSize());
    assertEquals(List.of("1", "2", "3"), db.nextFromQueue(3));
    assertEquals(List.of("4", "5", "6", "7", "8", "9"), db.nextFromQueue(10));
  }
}
