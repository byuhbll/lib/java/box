package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 * Tests all intricacies regarding processing (ie, {@link Processor}, {@link ProcessResult}, and
 * process configuration inside {@link SourceConfig}).
 *
 * @author Charles Draper
 */
public class ProcessTest extends AbstractTest {

  /**
   * Tests that a dependency from an invalid (or no longer valid) source gets ignored with a warning
   * rather than an exception being thrown; both when saving and when reprocessing.
   */
  @Test
  public void testInvalidSourceDependency() {
    init("identity");
    source.save(new BoxDocument("1").setAsReady().addDependency("invalid", "1"));
    BoxDocument document = source.collect(new BoxQuery("1").setProcess(true)).get(0);
    assertEquals(BoxDocument.Status.READY, document.getStatus());
    assertEquals(Set.of(new DocumentId("invalid", "1")), document.getDependencies());
  }

  /**
   * Bug: Unprocessed documents were not being processed when dependencies were updated because the
   * dependents cache in the registry was not being updated for unprocessed documents.
   */
  @Test
  public void testUnprocessedDependents() {
    init("dependency", "simple");
    source.save(new BoxDocument("1").setAsUnprocessed().addDependency("simple", "1"));
    sleep(200);
    source("simple").save(new BoxDocument("1").setAsReady());
    sleep(1100);
    assertTrue(source.collect(new BoxQuery("1")).get(0).isProcessed());
  }

  /** Tests basic dependency functionality. */
  @Test
  public void testDependencyBasics() {
    init("test", "dependency1", "dependency2", "dependency3");
    source.collect(new BoxQuery("1").setProcess(true));
    sleep(2000);

    // There is a race condition in the DocumentHandler.save method where a dependent might not be
    // placed back on the queue when a dependency is ready because it was in the middle of being
    // processed when the dependency became ready. That is what is happening here with test.1. It
    // should be processed 3 times, but sometimes the second processing finishes after all three
    // dependencies are done and so it gets removed from the queue instead of added. For now we will
    // force the reprocess knowing that this is an issue that needs to be addressed. This race
    // condition is far less likely in the real world because there is usually much more latency in
    // processing dependents and dependencies.
    process("1");

    testify(source.collect(new BoxQuery()));
  }

  /** Tests auto facet fields functionality. */
  @Test
  public void testFacetFields() {
    init("facetfield", "dependency1", "dependency2", "dependency3");
    source("dependency1").collect(new BoxQuery("1").setProcess(true));
    source("dependency2").collect(new BoxQuery("1").setProcess(true));
    source("dependency3").collect(new BoxQuery("1").setProcess(true));
    source.collect(new BoxQuery("1").setProcess(true));
    sleep(2000);
    testify(source.collect(new BoxQuery()));
  }

  /**
   * Bug: when saving a value of type `long`, a complex object was being returned. TODO: This should
   * be moved to a mongo test. The bug was in the MongoDatabase.
   */
  @Test
  public void testLongValue() {
    init("simple");
    BoxDocument doc = new BoxDocument("1");
    doc.withDocument().put("long", Long.MAX_VALUE);
    source.save(doc);
    testify(source.collect(new BoxQuery()));
  }

  /**
   * Tests that sources marked as dependencyOnly do not save documents unless it is a dependency of
   * another document.
   */
  @Test
  public void testDependencyOnly() {
    init("dependencyonly", "identity");
    source.save(new BoxDocument("1").setAsReady());
    source("identity").save(new BoxDocument("2").addDependency("dependencyonly", "2"));
    source.save(new BoxDocument("2").setAsReady());
    testify(source.collect(new BoxQuery()));
  }

  /** Tests the process flag in the source config. */
  @Test
  public void testProcessFlag() {
    init("process");
    assertEquals(BoxDocument.Status.READY, source.collect(new BoxQuery("1")).get(0).getStatus());
  }

  /** Tests the wait flag in the source config. */
  @Test
  public void testWaitFlag() {
    init("wait");
    assertEquals(BoxDocument.Status.READY, source.collect(new BoxQuery("1")).get(0).getStatus());
    Instant processed = source.get("1").getProcessed().get();
    assertEquals(processed, source.collect(new BoxQuery("1")).get(0).getProcessed().get());
  }

  /** Tests that an error returns with processing error. */
  @Test
  public void testProcessError() {
    init("error");
    testify(source.collect(new BoxQuery("1")));
  }

  /** Tests that an error returns with processing error. */
  @Test
  public void testDependencyError() {
    init("identity", "error");
    source("error").collect(new BoxQuery("1"));
    source.save(new BoxDocument("1").setAsReady().addDependency("error", "1"));
    sleep(200);

    assertAll(
        () -> testify(source.collect(new BoxQuery("1")), 1),
        () -> testify(source.collect(new BoxQuery("1").setProcess(true)), 2));
  }

  /**
   * Tests that sources without a processor return deleted instead of unprocessed for non-existent
   * documents.
   */
  @Test
  public void testDeletedWhenNoProcessor() {
    init("simple");
    assertEquals(BoxDocument.Status.DELETED, source.collect(new BoxQuery("1")).get(0).getStatus());
    assertEquals(
        BoxDocument.Status.DELETED,
        source.collect(new BoxQuery("2").setProcess(true)).get(0).getStatus());
    assertEquals(
        BoxDocument.Status.DELETED,
        source.collect(new BoxQuery("3").setWait(true)).get(0).getStatus());
  }

  /**
   * Bug: Continuous and infinite processing of document when status is always unprocessed and has a
   * dependency.
   */
  @Test
  public void testNoContinousProcessing() {
    init("unprocdeps", "simple");
    source("simple").save(new BoxDocument("1").setAsReady());
    source.collect(new BoxQuery("1").setProcess(true));
    sleep(1100);
    testify(source.collect(new BoxQuery("1")));
  }

  /** Tests the quota mechanism. */
  @Test
  public void testQuota() {
    init("quota");
    source.collect(new BoxQuery("1", "2", "3"));
    sleep(1100);
    assertEquals(READY, status("1"));
    assertEquals(READY, status("2"));
    assertEquals(UNPROCESSED, status("3"));
    sleep(2000);
    assertEquals(READY, status("3"));
  }

  /** Tests the suspend mechanism. */
  @Test
  public void testSuspend() {
    init("suspend");
    source.collect(new BoxQuery("1", "2", "3"));
    sleep(1100);
    assertEquals(READY, status("1"));
    assertEquals(UNPROCESSED, status("2"));
    assertEquals(UNPROCESSED, status("3"));
    sleep(2000);
    assertEquals(READY, status("2"));
    assertEquals(UNPROCESSED, status("3"));
    sleep(2000);
    assertEquals(READY, status("3"));
  }

  /** Tests the on/off mechanism. */
  @Test
  public void testOnOff() {
    init("onoff");
    // we need to start in the off position (once the clock is on a four second boundary)
    align(4000);
    // get off the edge
    sleep(100);
    source.collect(new BoxQuery("1"));
    // queue processing can be delayed up to one second
    sleep(1100);
    assertEquals(UNPROCESSED, status("1"));
    // wait until we're in the on position, another two seconds should be plenty
    sleep(2000);
    assertEquals(READY, status("1"));
  }

  /** Tests the batching mechanism. */
  @Test
  public void testBatch() {
    init("batch");
    source.collect(new BoxQuery("1", "2", "3"));
    sleep(1100);
    assertEquals(READY, status("1"));
    assertEquals(READY, status("2"));
    assertEquals(UNPROCESSED, status("3"));
    sleep(2100);
    assertEquals(READY, status("3"));
  }

  /** Tests the automatic removal of deleted documents. */
  @Test
  public void testRemove() {
    init("remove");
    source.save(new BoxDocument("1").setAsDeleted());
    sleep(500);
    assertEquals(DELETED, status("1"));
    sleep(2000);
    assertEquals(UNPROCESSED, status("1"));
  }

  /** Tests the automatic reprocessing of old documents. */
  @Test
  public void testReprocess() {
    init("reprocess");
    source.collect(new BoxQuery("1").setProcess(true));
    sleep(500);
    assertEquals(READY, status("1"));
    // wait two seconds for reprocess (one second for age + one second for process queue cooldown)
    sleep(2000);
    assertEquals(DELETED, status("1"));
  }

  /**
   * Bug: The right timing can cause a batch of ids in the DocumentProcessor to contain duplicates.
   * This isn't so much of a bug as it is a sub-optimal situation as the ids will be processed
   * multiple times in the batch instead of just once.
   */
  @Test
  public void testRepeatedIds() {
    init("repeatedids", "dependency1", "dependency2", "dependency3");
    source.process("1");
    sleep(1500);
    // a race condition causes this to be processed either 2 or 3 times, but this is expected, we
    // just don't want it processed many more times
    assertEquals(3, source.get("1").getDocument().path("count").asInt(), 1);
  }

  /**
   * Bug: Tests that saving unprocessed documents puts them on the queue even when there are already
   * corresponding processed documents in the database.
   */
  @Test
  void testUnprocessedWithExistingProcessed() {
    init("identity");
    source.process("1");

    // verify is processed and note processed date
    BoxDocument document1 = source.get("1");
    Instant processed1 = document1.getProcessed().get();
    Instant modified1 = document1.getModified().get();
    assertTrue(document1.isProcessed());

    // move the clock ahead
    sleep(10);

    // save unprocessed document, should get placed on queue
    source.save(new BoxDocument("1"));

    // wait for processsing of queue
    sleep(1100);

    // should not overwrite processed document, but should reprocess
    BoxDocument document2 = source.get("1");
    Instant processed2 = document2.getProcessed().get();
    Instant modified2 = document2.getModified().get();
    assertTrue(document2.isProcessed());
    assertTrue(processed1.isBefore(processed2));
    assertEquals(modified1, modified2);
  }

  /**
   * Tests that saved unprocessed documents with dependencies will get processed when a dependency
   * becomes ready.
   */
  @Test
  public void testDependeciesTriggerUnprocessedDocuments() {
    init("dependency", "simple");

    source.save(new BoxDocument("1").addDependency("simple", "1"));

    // allow time for reprocessing due to new dependency
    sleep(1100);

    assertFalse(source.get("1").isProcessed());

    source("simple").save(new BoxDocument("1").setAsReady());

    // allow time for reprocessing due to new dependency
    sleep(1100);

    assertTrue(source.get("1").isProcessed());
  }

  @Test
  void testProcessContext() {
    // the {@link TestProcessor} runs this test to verify that the Box, Source, Id, and Dependency
    // objects are non-null
  }
}
