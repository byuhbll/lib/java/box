package edu.byu.hbll.box.impl;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.AbstractTest;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Tests the integration of {@link View} with the rest of Box.
 *
 * @author Charles Draper
 */
public class FacetViewIT extends AbstractTest {

  /** Tests basic functionality of a facet view. */
  @Test
  public void testFacetView() {
    init("facetview", "viewsrc");
    source("viewsrc").collect(new BoxQuery("11", "12", "22", "13", "23", "33").setProcess(true));
    witness.testify(source.collect(new BoxQuery()));
  }

  /** Tests that the cacheView template works. */
  @Test
  public void testCachedView() {
    init("cachedfacetview", "viewsrc");
    source("viewsrc").collect(new BoxQuery("1", "2", "11").setProcess(true));

    // verify harvesting
    sleep(1000);
    List<BoxDocument> docs = source.collect(new BoxQuery());
    assertEquals(2, docs.size());
    assertEquals("1", docs.get(0).getId());
    assertEquals("2", docs.get(1).getId());
    assertEquals(2, docs.get(0).getDocument().path("documents").size());
    assertEquals(1, docs.get(1).getDocument().path("documents").size());

    List<BoxDocument> sourceDocs = source("viewsrc").collect(new BoxQuery());
    assertEquals(sourceDocs.get(0).toJson(), docs.get(0).getDocument().path("documents").path(0));
    assertEquals(sourceDocs.get(1).toJson(), docs.get(1).getDocument().path("documents").path(0));
    assertEquals(sourceDocs.get(2).toJson(), docs.get(0).getDocument().path("documents").path(1));

    System.out.println(docs.get(0));
    System.out.println(docs.get(1));

    // verify that the document is cached (cursor, modified, and processed need to be reworked
    // before this check will succeed)
    // long viewCursor = getSource("viewsrc").collect(new BoxQuery("1")).get(0).getCursor().get();
    // long cachedCursor = source.collect(new BoxQuery("1")).get(0).getCursor().get();
    // assertNotEquals(viewCursor, cachedCursor);
  }

  /**
   * In response to bug where facets where being suppressed by {@link View} if limiting to other
   * configured fields causing no results to be returned. {@link XsltView} requires that facets be
   * returned.
   */
  @Test
  public void testFacetsWithFields() {
    JsonNode config = loadYaml();

    ((ObjectNode) config.path("sources").path("facetview").path("template").path("params"))
        .withArray("fields")
        .add("id");

    init(config, "facetview", "viewsrc");

    source("viewsrc").collect(new BoxQuery("1", "2", "11").setProcess(true));

    QueryResult result = source.collect(new BoxQuery());

    assertEquals(2, result.size());
    assertTrue(result.get(0).getDocument().path("documents").path(0).has("id"));
    assertFalse(result.get(0).getDocument().path("documents").path(0).path("@box").has("facets"));
  }

  /** Tests that the limit setting for documents within a facet value is working. */
  @Test
  public void testFacetDocumentLimit() {
    init("facetviewdoclimit", "viewsrc");
    source("viewsrc").collect(new BoxQuery("10", "11", "12", "21", "22", "32").setProcess(true));
    testify(source.collect(new BoxQuery()));
  }

  /** Tests that the FacetView honors the limit set in the query. */
  @Test
  public void testFacetViewLimit() {
    init("facetview", "viewsrc");
    source("viewsrc").collect(new BoxQuery("11", "12", "22", "13", "23", "33").setProcess(true));
    assertEquals(2, source.collect(new BoxQuery().setLimit(2)).size());
  }

  /**
   * Tests a facet view to make sure that we can harvest from beginning to end and see all facet
   * documents and that it will actually finish.
   */
  @Test
  public void testHarvestFacetView() {
    init("viewsrc", "facetview");

    List<String> ids = new ArrayList<>();

    for (int i = 0; i < 26; i++) {
      ids.add(((char) ('a' + i)) + "");
    }

    // add a document at the end that will be picked up by the first document's `last` facet
    // Note: the problem was that the cursor was advancing to this last document and skipping
    // all others because it got picked up by the first document.
    ids.add("bada");

    BoxQuery idQuery = new BoxQuery(ids).setProcess(true);
    source.collect(idQuery);

    BoxQuery query = new BoxQuery().setLimit(1);

    int totalQueries = 0;
    QueryResult result;

    do {
      result = source("facetview").collect(query);
      query.setCursor(result.getNextCursor());
      totalQueries++;

      int expectedDocumentsSize = 0;

      if (totalQueries == 1 || totalQueries == 27) {
        expectedDocumentsSize = 2;
      } else if (totalQueries < 27) {
        expectedDocumentsSize = 1;
      }

      if (!result.isEmpty()) {
        assertEquals(expectedDocumentsSize, result.get(0).getDocument().path("documents").size());
      }
    } while (!result.isEmpty());

    assertEquals(28, totalQueries);
  }

  /**
   * Bug: FacetViews are not correctly limiting fields. They are limiting the underlying document's
   * fields and not the outer one.
   */
  @Test
  public void testFacetViewField() {
    init("facetviewfields", "simple");

    for (int i = 1; i <= 3; i++) {
      BoxDocument document = new BoxDocument(i + "").addFacet("a", "1");
      document.withDocument().put("id", document.getId()).put("other", "field");
      source("simple").save(document);
    }

    assertAll(
        () -> witness.testify(source.get("1"), "1"),
        () -> witness.testify(source.collect(new BoxQuery("1").addField("@box")), "2"),
        () -> witness.testify(source.collect(new BoxQuery("1").addField("@doc")), "3"),
        () -> witness.testify(source.collect(new BoxQuery("1").addField("documents")), "4"),
        () -> witness.testify(source.collect(new BoxQuery("1").addField("documents.id")), "5"),
        () -> witness.testify(source.collect(new BoxQuery("1").addField("id")), "6"));
  }

  /** Tests the view builder. */
  @Test
  public void testBuilder() {
    init("viewsrc");
    source.process("11", "12", "13");
    FacetView view = FacetView.builder().baseSource(source).facetName("first").build();
    assertEquals(1, view.find(new BoxQuery()).size());
    assertEquals(3, view.find(new BoxQuery()).get(0).getDocument().path("documents").size());
  }
}
