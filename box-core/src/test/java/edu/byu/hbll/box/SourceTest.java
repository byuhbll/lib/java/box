package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the {@link Source} api. This does not test all the various intricate settings and timings
 * of the source. That is done in the box-test module.
 *
 * @author Charles Draper
 */
class SourceTest extends AbstractTest {

  /**
   * @throws java.lang.Exception
   */
  @BeforeEach
  void setUp() throws Exception {
    init("principal");

    for (int i = 1; i <= 3; i++) {
      source.save(new BoxDocument(i + "").setAsReady());
    }
  }

  /** Test method for {@link edu.byu.hbll.box.Source#clear()}. */
  @Test
  void testClear() {
    assertEquals(3, source.collect(new BoxQuery()).size());
    source.clear();
    assertEquals(0, source.collect(new BoxQuery()).size());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#collect(edu.byu.hbll.box.BoxQuery)}. */
  @Test
  void testCollect() {
    QueryResult result = source.collect(new BoxQuery());
    assertEquals(3, result.size());
    assertEquals("1", result.get(0).getId());
    assertEquals("2", result.get(1).getId());
    assertEquals("3", result.get(2).getId());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#find(edu.byu.hbll.box.BoxQuery)}. */
  @Test
  void testFind() {
    List<BoxDocument> result = new ArrayList<>();
    source.find(new BoxQuery()).forEach(d -> result.add(d));
    assertEquals(3, result.size());
    assertEquals("1", result.get(0).getId());
    assertEquals("2", result.get(1).getId());
    assertEquals("3", result.get(2).getId());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#getConfig()}. */
  @Test
  void testGetConfig() {
    assertNotNull(source.getConfig());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#getHealth()}. */
  @Test
  void testGetHealth() {
    sleep(1000);
    SourceHealth health = source.getHealth();
    assertTrue(health.isHealthy());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#getName()}. */
  @Test
  void testGetName() {
    assertEquals("principal", source.getName());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#addToQueue(java.util.Collection)}. */
  @Test
  void testAddToQueueCollection() {
    source.addToQueue(Set.of("4", "5", "6"));
    sleep(1100);
    assertEquals(6, source.collect(new BoxQuery()).size());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.Source#addToQueue(java.util.Collection,
   * java.time.Instant)}.
   */
  @Test
  void testAddToQueueCollectionInstant() {
    source.addToQueue(Set.of("4", "5", "6"), Instant.now().plusSeconds(2));
    sleep(1000);
    assertEquals(3, source.collect(new BoxQuery()).size());
    sleep(2000);
    assertEquals(6, source.collect(new BoxQuery()).size());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.Source#registerForUpdateNotifications(java.lang.Runnable)}.
   */
  @Test
  void testRegisterForUpdateNotifications() {
    AtomicBoolean updates = new AtomicBoolean(false);
    source.registerForUpdateNotifications(() -> updates.set(true));
    source.save(new BoxDocument("4").setAsReady());
    sleep(1500);
    assertTrue(updates.get());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#save(edu.byu.hbll.box.BoxDocument)}. */
  @Test
  void testSave() {
    source.save(new BoxDocument("4").setAsReady());
    source.save(List.of(new BoxDocument("5").setAsReady(), new BoxDocument("6").setAsReady()));
    assertEquals(
        List.of("1", "2", "3", "4", "5", "6"),
        source.stream(new BoxQuery()).map(BoxDocument::getId).collect(Collectors.toList()));
  }

  /** Test method for {@link edu.byu.hbll.box.Source#stream(edu.byu.hbll.box.BoxQuery)}. */
  @Test
  void testStream() {
    List<BoxDocument> result = source.stream(new BoxQuery()).collect(Collectors.toList());
    assertEquals(3, result.size());
    assertEquals("1", result.get(0).getId());
    assertEquals("2", result.get(1).getId());
    assertEquals("3", result.get(2).getId());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#triggerHarvest()}. */
  @Test
  void testTriggerHarvest() {
    source.triggerHarvest();
    sleep(100);
    assertEquals(10, source.collect(new BoxQuery().setUnlimited()).size());
  }

  /** Tests that a disabled processor is equal to no processor defined. */
  @Test
  void testDisabledProcessor() {
    init("disabledprocessor");
    source.collect(new BoxQuery("1").setProcess(true));
    assertEquals(0, source.collect(new BoxQuery()).size());
  }

  /** Tests that documents of all statuses are being saved and retrieved properly. */
  @Test
  void testSavingStatuses() {
    init("simple");
    source.save(new BoxDocument("ready").setAsReady());
    source.save(new BoxDocument("deleted").setAsDeleted());
    source.save(new BoxDocument("unprocessed").setAsUnprocessed());
    source.save(new BoxDocument("error").setAsError("error message"));

    assertAll(
        () -> witness.testify(source.collect(new BoxQuery()), "default"),
        () ->
            witness.testify(
                source.collect(new BoxQuery().setStatuses(List.of(BoxDocument.Status.READY))),
                "ready"),
        () ->
            witness.testify(
                source.collect(new BoxQuery().setStatuses(List.of(BoxDocument.Status.DELETED))),
                "deleted"),
        () ->
            witness.testify(
                source.collect(new BoxQuery().setStatuses(List.of(BoxDocument.Status.UNPROCESSED))),
                "unprocessed"),
        () ->
            witness.testify(
                source.collect(new BoxQuery().setStatuses(List.of(BoxDocument.Status.ERROR))),
                "error"));
  }

  /**
   * Tests that a request for a document comes back as deleted if it doesn't already exist and there
   * is no processor.
   */
  @Test
  void testDeletedIfNoProcessor() {
    init("simple");
    assertEquals(BoxDocument.Status.DELETED, source.collect(new BoxQuery("1")).get(0).getStatus());
  }

  /** Tests that the document will not be saved because the source is configured to not save. */
  @Test
  void testSaveFalse() {
    init("savefalse");
    source.save(new BoxDocument("1").setAsReady());
    assertEquals(
        BoxDocument.Status.UNPROCESSED, source.collect(new BoxQuery("1")).get(0).getStatus());
  }

  /** Tests that an empty document can be saved. */
  @Test
  void testEmptyDocument() {
    init("simple");
    source.save(new BoxDocument("1").setAsReady());
    assertEquals(BoxDocument.Status.READY, source.collect(new BoxQuery("1")).get(0).getStatus());
  }

  @Test
  void testFacetQueries() {
    init("principal");
    source.collect(new BoxQuery("11", "12", "21", "22").setProcess(true));
    assertAll(
        () -> witness.testify(source.collect(new BoxQuery().addFacet("first", "1")), 1),
        () ->
            witness.testify(
                source.collect(new BoxQuery().addFacet("first", "1").addFacet("first", "2")), 2),
        () ->
            witness.testify(
                source.collect(new BoxQuery().addFacet("first", "1").addFacet("last", "1")), 3),
        () ->
            witness.testify(
                source.collect(new BoxQuery().addFacet("first", "1").addFacet("last", "3")), 4),
        () ->
            witness.testify(
                source.collect(new BoxQuery().addFacet("first", "1").addFacet("fake", "1")), 5));
  }

  /** Test method for {@link edu.byu.hbll.box.Source#countDocuments()}. */
  @Test
  void testCountDocuments() {
    init("principal");
    source.process(List.of("1", "2", "3"));
    assertEquals(3, source.count(new BoxQuery()));
  }

  /** Test method for {@link edu.byu.hbll.box.Source#process(java.lang.String)}. */
  @Test
  void testProcessString() {
    init("principal");
    assertEquals(READY, source.process("1").getStatus());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#process(java.lang.String[])}. */
  @Test
  void testProcessStringArray() {
    init("principal");
    source.process("1", "2", "3");
    assertEquals(3, source.count(new BoxQuery().addStatus(READY)));
  }

  /** Test method for {@link edu.byu.hbll.box.Source#process(java.util.Collection)}. */
  @Test
  void testProcessCollection() {
    init("principal");
    source.process(List.of("1", "2", "3"));
    assertEquals(3, source.count(new BoxQuery().addStatus(READY)));
  }

  /** Test method for {@link edu.byu.hbll.box.Source#deleteFromQueue(java.lang.String[])}. */
  @Test
  void testDeleteFromQueueStringArray() {
    init("simple");
    source.getConfig().getDb().addToQueue(List.of("1", "2", "3"), Instant.MIN, true);
    source.deleteFromQueue("1", "4");
    assertEquals(2, source.getConfig().getDb().nextFromQueue(10).size());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#deleteFromQueue(java.util.Collection)}. */
  @Test
  void testDeleteFromQueueCollection() {
    init("simple");
    source.getConfig().getDb().addToQueue(List.of("1", "2", "3"), Instant.MIN, true);
    source.deleteFromQueue(List.of("1", "4"));
    assertEquals(2, source.getConfig().getDb().nextFromQueue(10).size());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#get(java.lang.String)}. */
  @Test
  void testGetString() {
    init("principal");
    source.process("1");
    assertEquals(READY, source.get("1").getStatus());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#get(java.lang.String[])}. */
  @Test
  void testGetStringArray() {
    init("principal");
    source.process("1", "2", "3");
    assertEquals(2, source.get("1", "2").stream().filter(d -> d.isReady()).count());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#get(java.util.Collection)}. */
  @Test
  void testGetCollection() {
    init("principal");
    source.process("1", "2", "3");
    assertEquals(2, source.get(List.of("1", "2")).stream().filter(d -> d.isReady()).count());
  }

  /** Test method for {@link edu.byu.hbll.box.Source#addToQueue(QueueEntry...)}. */
  @Test
  void testAddToQueueArrayQueueEntry() {
    init("principal");

    Instant now = Instant.now();
    Instant past = now.minus(1, ChronoUnit.DAYS);
    Instant future = now.plus(1, ChronoUnit.DAYS);

    List<QueueEntry> entries = new ArrayList<>();
    entries.add(new QueueEntry("1").setPriority(1).setAttempt(past));
    entries.add(new QueueEntry("2").setPriority(1).setAttempt(now));
    entries.add(new QueueEntry("3").setPriority(1).setAttempt(future));
    entries.add(new QueueEntry("4").setPriority(1.5).setAttempt(past));
    entries.add(new QueueEntry("5").setPriority(1.5).setAttempt(now));
    entries.add(new QueueEntry("6").setPriority(1.5).setAttempt(future));
    entries.add(new QueueEntry("7").setPriority(2).setAttempt(past));
    entries.add(new QueueEntry("8").setPriority(2).setAttempt(now));
    entries.add(new QueueEntry("9").setPriority(2).setAttempt(future));

    Collections.shuffle(entries);
    source.addToQueue(entries.toArray(new QueueEntry[0]));

    sleep(2000);

    List<String> idsInProcessedOrder =
        source.collect(new BoxQuery()).stream()
            .sorted(Comparator.comparing(d -> d.getProcessed().get()))
            .map(d -> d.getId())
            .collect(Collectors.toList());

    assertEquals(List.of("1", "2", "4", "5", "7", "8"), idsInProcessedOrder);
  }
}
