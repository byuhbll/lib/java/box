package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class ReadOnlyDatabaseTest {

  private ReadOnlyDatabaseImpl db = new ReadOnlyDatabaseImpl();

  /** Test method for {@link edu.byu.hbll.box.ReadOnlyDatabase#clear()}. */
  @Test
  void testClear() {
    assertThrows(UnsupportedOperationException.class, () -> db.clear());

    try {
      db.clear();
      fail();
    } catch (UnsupportedOperationException e) {
      assertEquals("unable to clear a read-only database", e.getMessage());
    }
  }

  public static class ReadOnlyDatabaseImpl implements ReadOnlyDatabase {

    @Override
    public QueryResult find(BoxQuery query) {
      return null;
    }
  }
}
