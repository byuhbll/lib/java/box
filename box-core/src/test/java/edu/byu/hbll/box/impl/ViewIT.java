package edu.byu.hbll.box.impl;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.AbstractTest;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.client.HttpBoxClient;
import java.io.UncheckedIOException;
import java.net.URI;
import org.junit.jupiter.api.Test;

/**
 * Tests the integration of {@link View} with the rest of Box.
 *
 * @author Charles Draper
 */
public class ViewIT extends AbstractTest {

  /** Tests basic functionality of a view. */
  @Test
  public void testView() {
    init("viewsrc", "view");
    source("viewsrc").collect(new BoxQuery("1").setProcess(true));

    witness.testify(source("view").collect(new BoxQuery()));

    // both the source and the view should be identical
    assertEquals(
        source("viewsrc").collect(new BoxQuery()).toString(),
        source("view").collect(new BoxQuery()).toString());
  }

  /** Tests that the cacheView template works. */
  @Test
  public void testCachedView() {
    init("cachedview", "viewsrc");
    source("viewsrc").collect(new BoxQuery("1").setProcess(true));

    // verify harvesting
    sleep(1100);
    assertEquals(BoxDocument.Status.READY, source.collect(new BoxQuery("1")).get(0).getStatus());

    // verify processing
    assertEquals(
        BoxDocument.Status.UNPROCESSED, source.collect(new BoxQuery("2")).get(0).getStatus());
    sleep(3000);
    assertEquals(BoxDocument.Status.READY, source.collect(new BoxQuery("2")).get(0).getStatus());

    // verify that the document is cached (cursor, modified, and processed need to be reworked
    // before this check will succeed)
    // long viewCursor = getSource("viewsrc").collect(new BoxQuery("2")).get(0).getCursor().get();
    // long cachedCursor = source.collect(new BoxQuery("2")).get(0).getCursor().get();
    // assertNotEquals(viewCursor, cachedCursor);
  }

  /**
   * Tests that harvests are triggered for a view's downstream source even when the downstream
   * source's harvester is not scheduled to run.
   */
  @Test
  public void testFollowView() {
    init("followview", "viewsrc", "view");
    process("viewsrc", "1");

    // verify harvesting
    sleep(3000);
    assertEquals(READY, status("view", "1"));
    assertEquals(READY, status("1"));
  }

  /** Tests that proper errors are thrown when view connected to non-existent box. */
  @Test
  public void testBadView() {
    init("badview");
    assertThrows(UncheckedIOException.class, () -> source.collect(new BoxQuery()));
  }

  /** Tests that bad view dependencies are properly handled. */
  @Test
  public void testDependentOnBadView() {
    init("viewdeperror", "badview");
    source.save(new BoxDocument("1").setAsReady().addDependency("badview", "1"));
    witness.testify(source.collect(new BoxQuery("1").setProcess(true)));
  }

  /** Tests that a view properly limits fields, by facets, and by statuses. */
  @Test
  public void testLimitedView() {
    init("limitedview", "simple");
    ObjectNode doc = mapper.createObjectNode();
    doc.putObject("obj").put("obj", 0).put("bad", 0);
    doc.putArray("arrobj").addObject().put("arrobj", 0).put("bad", 0);
    doc.putArray("arr").add(0);
    doc.putObject("sub").put("sub", 0).put("maybe", 0);
    doc.putObject("bad").put("bad", 0);

    source("simple")
        .save(new BoxDocument("10").setDocument(doc).addFacet("first", "1").addFacet("last", "0"));
    source("simple")
        .save(new BoxDocument("11").setDocument(doc).addFacet("first", "1").addFacet("last", "1"));

    assertAll(
        () -> testify(source.collect(new BoxQuery()), 1),
        () -> testify(source.collect(new BoxQuery("10")), 2),
        () -> testify(source.collect(new BoxQuery("11")), 3),
        () -> testify(source.collect(new BoxQuery("12")), 4),
        () -> testify(source.collect(new BoxQuery().addFacet("last", "0")), 5),
        () -> testify(source.collect(new BoxQuery().addFacet("last", "1")), 6),
        () -> testify(source.collect(new BoxQuery().addField("obj")), 7),
        () -> testify(source.collect(new BoxQuery().addField("bad")), 8),
        () -> testify(source.collect(new BoxQuery().addField("sub.maybe")), 9),
        () -> testify(source.collect(new BoxQuery().addFields("obj", "bad")), 10));
  }

  /** Tests that requesting an item through a view will cause the item to be processed. */
  @Test
  public void testUpstreamProcess() {
    init("view", "viewsrc");
    assertEquals(
        BoxDocument.Status.UNPROCESSED, source.collect(new BoxQuery("1")).get(0).getStatus());
    sleep(1100);
    assertEquals(BoxDocument.Status.READY, source.collect(new BoxQuery("1")).get(0).getStatus());
  }

  /** Test the saveDeleted flag on View harvesting. */
  @Test
  public void testHarvestSaveDeleted() {
    init("simple", "savedeletedtrue", "savedeletedfalse");
    source.save(new BoxDocument("1").setAsDeleted());
    source("savedeletedtrue").triggerHarvest();
    source("savedeletedfalse").triggerHarvest();
    sleep(200);

    assertAll(
        () -> testify(source("savedeletedtrue").collect(new BoxQuery()), "true"),
        () -> testify(source("savedeletedfalse").collect(new BoxQuery()), "false"));
  }

  /** Tests harvestUnprocessed flag. */
  @Test
  public void testHarvestUnprocessed() {
    init("harvestunprocessed", "simple");
    source("simple").save(new BoxDocument("1").setAsReady());
    sleep(2100);
    testify(source.collect(new BoxQuery()));
  }

  /** Tests the view builder. */
  @Test
  public void testBuilder() {
    init("viewsrc");
    source.process("1", "2", "3");
    View view = View.builder().baseSource(source).build();
    assertEquals(3, view.find(new BoxQuery().addStatus(READY)).size());
  }

  /** Bug: authentication parameters were not being set right for the HttpBoxClient. */
  @Test
  public void testHttpClientAuth() {
    init("authnone", "authfull");

    HttpBoxClient noneClient =
        (HttpBoxClient) ((View) source("authnone").getConfig().getProcessor()).getBoxClient();

    HttpBoxClient authClient =
        (HttpBoxClient) ((View) source("authfull").getConfig().getProcessor()).getBoxClient();

    assertAll(
        () -> assertEquals(URI.create("http://www.example.com/"), noneClient.getUri()),
        () -> assertNull(noneClient.getUsername()),
        () -> assertNull(noneClient.getPassword()),
        () -> assertNull(noneClient.getAccessTokenUri()),
        () -> assertNull(noneClient.getClientId()),
        () -> assertNull(noneClient.getClientSecret()),
        () -> assertEquals(URI.create("http://www.example.com/"), authClient.getUri()),
        () -> assertEquals("a", authClient.getUsername()),
        () -> assertEquals("b", authClient.getPassword()),
        () -> assertEquals(URI.create("http://www.example.com/c"), authClient.getAccessTokenUri()),
        () -> assertEquals("d", authClient.getClientId()),
        () -> assertEquals("e", authClient.getClientSecret()));
  }

  /**
   * Bug: the call to save for read only databases was throwing an UnsupportedOperationException.
   * Such calls should just quietly do nothing. This verifies that no exception is thrown.
   */
  @Test
  public void testQuietSave() {
    init("view", "viewsrc");
    source.save(new BoxDocument("1"));
  }
}
