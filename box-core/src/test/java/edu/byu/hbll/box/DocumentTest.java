package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.BoxDocument.Status;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 * Tests specific to saving and retrieving documents.
 *
 * @author Charles Draper
 */
public class DocumentTest extends AbstractTest {

  /**
   * Tests that cursor and modified do not change after saving the same document, but processed
   * does.
   */
  @Test
  void testDatesUpdated() {
    init("principal");

    long cursor = -1;
    Instant modified = null;
    Instant processed = null;

    for (int i = 0; i < 2; i++) {
      BoxDocument boxDocument = new BoxDocument("1").setAsReady();
      source.save(boxDocument);
      boxDocument = source.collect(new BoxQuery()).get(0);

      if (i == 0) {
        cursor = boxDocument.getCursor().get();
        modified = boxDocument.getModified().get();
        processed = boxDocument.getModified().get();
      } else if (i == 1) {
        assertEquals(cursor, (long) boxDocument.getCursor().get());
        assertEquals(modified, boxDocument.getModified().get());
        assertTrue(processed.isBefore(boxDocument.getProcessed().get()));
      }

      sleep(100);
    }
  }

  /** Verify that orphaned documents are marked as deleted when using groups. */
  @Test
  public void testOrphanCleanup() {
    init("simple");

    source.save(new StartGroupDocument("1"));
    source.save(new BoxDocument("1.1").setAsReady().setGroupId("1"));
    source.save(new BoxDocument("1.2").setAsReady().setGroupId("1"));
    source.save(new EndGroupDocument("1"));

    assertEquals(2, source.collect(new BoxQuery()).size());
    assertEquals(0, source.collect(new BoxQuery().addStatuses(Status.DELETED)).size());

    // orphan cleanup is timestamp dependent, need to reprocess the group after a small delay
    sleep(10);

    source.save(new StartGroupDocument("1"));
    source.save(new BoxDocument("1.2").setAsReady().setGroupId("1"));
    source.save(new BoxDocument("1.3").setAsReady().setGroupId("1"));
    source.save(new EndGroupDocument("1"));

    assertEquals(3, source.collect(new BoxQuery()).size());
    assertEquals(1, source.collect(new BoxQuery().addStatuses(Status.DELETED)).size());

    assertEquals(Status.DELETED, source.collect(new BoxQuery("1.1")).get(0).getStatus());
  }

  /**
   * Verify that simultaneous start and end group calls does not clobber the orphan detection. The
   * document should not be temporarily deleted.
   */
  @Test
  public void testOrphanCleanupRaceCondition() {
    init("simple");

    // start group (round 1)
    source.save(new StartGroupDocument("1"));
    source.save(new BoxDocument("1.1").setAsReady().setGroupId("1"));
    source.save(new BoxDocument("1.2").setAsReady().setGroupId("1"));

    // advance clock
    sleep(10);

    // start group (round 2)
    source.save(new StartGroupDocument("1"));

    // end group (round 1)
    source.save(new EndGroupDocument("1"));

    assertEquals(2, source.collect(new BoxQuery()).size());
    assertEquals(0, source.collect(new BoxQuery().addStatuses(Status.DELETED)).size());

    // advance clock
    sleep(10);

    source.save(new BoxDocument("1.1").setAsReady().setGroupId("1"));

    // end group (round 2)
    source.save(new EndGroupDocument("1"));

    // unfortunately due to the race condition, we cannot detect that "1.2" is an orphan, but we
    // error on the side of leaving orphans as ready rather than deleting non-orphaned documents
    assertEquals(2, source.collect(new BoxQuery()).size());
    assertEquals(0, source.collect(new BoxQuery().addStatuses(Status.DELETED)).size());
  }

  /**
   * Tests that a document's modified date is not changed when the documents serialize the same, but
   * individual nodes may not say they're equal. For example, the same number can be represented by
   * a DecimalNode or a DoubleNode. As json nodes they are not equal, but numerically they actually
   * are.
   */
  @Test
  void testNotModified() {
    init("simple");

    BoxDocument doc1 = new BoxDocument("0");
    BoxDocument doc2 = new BoxDocument("0");

    doc1.withDocument().put("n", new BigDecimal("51.974"));
    doc2.withDocument().put("n", 51.974);

    // verifies that the number nodes are not equal, but the strings are
    assertNotEquals(doc1.getDocument().path("n"), doc2.getDocument().path("n"));
    assertEquals(doc1.getDocument().path("n").toString(), doc2.getDocument().path("n").toString());

    source.save(doc1);
    BoxDocument savedDoc1 = source.collect(new BoxQuery("0")).get(0);
    Instant modified1 = savedDoc1.getModified().get();
    long cursor1 = savedDoc1.getCursor().get();
    Instant processed1 = savedDoc1.getProcessed().get();

    sleep(100);

    source.save(doc2);
    BoxDocument savedDoc2 = source.collect(new BoxQuery("0")).get(0);
    Instant modified2 = savedDoc2.getModified().get();
    long cursor2 = savedDoc2.getCursor().get();
    Instant processed2 = savedDoc2.getProcessed().get();

    assertEquals(modified1, modified2);
    assertEquals(cursor1, cursor2);
    assertNotEquals(processed1, processed2);
  }

  /**
   * Tests that facet fields in the configuration are processed and added to the facet list for the
   * document. This should process facet fields that appear as a string or an array of strings in
   * the config.
   */
  @Test
  public void testFacetField() {
    init("facetfield");
    BoxDocument doc = new BoxDocument("1");
    doc.withDocument().put("a", 1).put("b", 2).put("c", 3);
    source.save(doc);
    BoxDocument savedDoc = source.collect(new BoxQuery("1")).get(0);

    assertEquals(
        Set.of(new Facet("a", "1"), new Facet("b", "1"), new Facet("b", "2")),
        savedDoc.getFacets());
  }

  /**
   * In response to a bug. The cursor and modified date should not update when facet fields are
   * added to a document that has already been fully processed.
   */
  @Test
  public void testFacetFieldAndModifiedDate() {
    init("facetfield");

    List<Object> results = new ArrayList<>();

    for (int i = 0; i < 2; i++) {
      BoxDocument doc = new BoxDocument("1");
      doc.withDocument().put("a", 1);
      source.save(doc);
      BoxDocument savedDoc = source.collect(new BoxQuery("1")).get(0);
      results.add(savedDoc.getCursor().get());
      results.add(savedDoc.getModified().get());
    }

    assertEquals(results.get(0), results.get(2));
    assertEquals(results.get(1), results.get(3));
  }

  /**
   * Bug: Dependents of a view are not being put on the queue to be reprocessed when the view
   * dependency is updated (more specifically, saved).
   */
  @Test
  public void testViewDependentsPutOnQueue() {
    init("testviewdep", "simple", "simpleview");

    source.process("1");

    // allow reprocessing to finish given that a new dependency has been added
    sleep(1100);

    assertEquals(2, source.get("1").getDocument().path("count").asInt());
    source("simple").save(new BoxDocument("1").addFacet("1", "a").setAsReady());

    // wait for harvest and process to finish
    sleep(1100);

    assertEquals(3, source.get("1").getDocument().path("count").asInt());
  }
}
