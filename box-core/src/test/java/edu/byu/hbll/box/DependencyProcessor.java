package edu.byu.hbll.box;

/** A processor that is not ready until all dependencies have come in. */
public class DependencyProcessor implements SingleProcessor {

  private Source source;

  public void postInit(InitConfig config) {
    this.source = config.getSource();
  }

  @Override
  public ProcessResult process(ProcessContext context) {
    BoxDocument document = source.collect(new BoxQuery(context.getId())).get(0);

    for (DocumentId dep : document.getDependencies()) {
      if (context.getDependency(dep) == null || !context.getDependency(dep).isReady()) {
        return new ProcessResult();
      }
    }

    document.setAsReady();

    document = document.setCursor(null).setModified(null).setProcessed(null);
    return new ProcessResult(document);
  }
}
