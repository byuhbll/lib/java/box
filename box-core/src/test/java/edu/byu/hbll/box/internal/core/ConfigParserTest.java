package edu.byu.hbll.box.internal.core;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.json.ObjectMapperFactory;
import org.junit.jupiter.api.Test;

class ConfigParserTest {

  ObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /** In response to bug. Tests that a cursor db can override the default db. */
  @Test
  void testParse() {
    ObjectNode config = mapper.createObjectNode();
    config
        .putObject("sources")
        .putObject("test")
        .putObject("cursor")
        .put("type", "edu.byu.hbll.box.impl.FakeDatabase");

    // because fake database doesn't exist, an exception should be thrown
    // the bug was ignoring the override completely
    assertThrows(RuntimeException.class, () -> Box.newBox(config));
  }
}
