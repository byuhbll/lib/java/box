package edu.byu.hbll.box;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;

/**
 * Tests the {@link Source} api. This does not test all the various intricate settings and timings
 * of the source. That is done in the box-test module.
 *
 * @author Charles Draper
 */
public class BoxConfigurableTest extends AbstractTest {

  /** Tests that the postConstruct method gets called properly. */
  @Test
  public void testPostConstruct() throws Exception {
    init("initconfig");
  }

  /**
   * @author Charles Draper
   */
  public static class Implementation implements Processor, Harvester, ReadOnlyDatabase {

    @Override
    public void postConstruct(ConstructConfig config) {

      if (config == null) {
        throw new IllegalArgumentException("config is null");
      }

      if (config.getParams() == null) {
        throw new IllegalArgumentException("params is null");
      }

      ObjectNode params = config.getParams();

      if (config.getObjectType() == null) {
        throw new IllegalArgumentException("objectType is null");
      }

      ObjectType objectType = ObjectType.valueOf(params.path("objectType").asText().toUpperCase());

      switch (objectType) {
        case CURSOR_DATABASE:
          if (!config.isCursorDatabase()) {
            throw new IllegalArgumentException("objectType is not " + objectType);
          }

          break;
        case BOX_DATABASE:
          if (!config.isBoxDatabase()) {
            throw new IllegalArgumentException("objectType is not " + objectType);
          }
          break;
        case HARVESTER:
          if (!config.isHarvester()) {
            throw new IllegalArgumentException("objectType is not " + objectType);
          }
          break;
        case OTHER:
          if (!config.isOther()) {
            throw new IllegalArgumentException("objectType is not " + objectType);
          }
          break;
        case PROCESSOR:
          if (!config.isProcessor()) {
            throw new IllegalArgumentException("objectType is not " + objectType);
          }
          break;
      }
    }

    @Override
    public HarvestResult harvest(HarvestContext context) {
      return null;
    }

    @Override
    public ProcessResult process(ProcessBatch batch) {
      return null;
    }

    @Override
    public QueryResult find(BoxQuery query) {
      return null;
    }
  }
}
