package edu.byu.hbll.box.internal.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.internal.util.NumberComparator;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/**
 * Tests the {@link NumberComparator}.
 *
 * @author Charles Draper
 */
class NumberComparatorTest {

  private static final NumberComparator comp = new NumberComparator();

  /**
   * Test method for {@link
   * edu.byu.hbll.box.internal.util.NumberComparator#compare(java.lang.Number, java.lang.Number)}.
   */
  @Test
  void testCompare() {
    // basic
    assertEquals(0, comp.compare(1, 1));
    assertEquals(0, comp.compare(1, 1D));
    assertEquals(0, comp.compare(1, 1F));
    assertEquals(0, comp.compare(1, 1L));
    assertEquals(0, comp.compare(1, (short) 1));
    assertEquals(0, comp.compare(1, (byte) 1));
    assertEquals(0, comp.compare(1, new BigDecimal("1")));
    assertEquals(0, comp.compare(1, new BigInteger("1")));

    assertTrue(0 > comp.compare(0, 1));
    assertTrue(0 > comp.compare(0, 1D));
    assertTrue(0 > comp.compare(0, 1F));
    assertTrue(0 > comp.compare(0, 1L));
    assertTrue(0 > comp.compare(0, (short) 1));
    assertTrue(0 > comp.compare(0, (byte) 1));
    assertTrue(0 > comp.compare(0, new BigDecimal("1")));
    assertTrue(0 > comp.compare(0, new BigInteger("1")));

    assertTrue(0 < comp.compare(1, 0));
    assertTrue(0 < comp.compare(1D, 0));
    assertTrue(0 < comp.compare(1F, 0));
    assertTrue(0 < comp.compare(1L, 0));
    assertTrue(0 < comp.compare((short) 1, 0));
    assertTrue(0 < comp.compare((byte) 1, 0));
    assertTrue(0 < comp.compare(new BigDecimal("1"), 0));
    assertTrue(0 < comp.compare(new BigInteger("1"), 0));

    // floating point peculiarities
    assertEquals(0, comp.compare(0.53F, 0.53D));
    assertEquals(0, comp.compare(0.53F, new BigDecimal("0.53")));
    assertEquals(0, comp.compare(0.53D, new BigDecimal("0.53")));

    // special cases
    assertEquals(0, comp.compare(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY));
    assertEquals(0, comp.compare(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY));
    assertEquals(0, comp.compare(Double.NaN, Double.NaN));

    assertTrue(0 < comp.compare(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY));
    assertTrue(0 > comp.compare(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY));
    assertTrue(0 < comp.compare(Double.NaN, Double.POSITIVE_INFINITY));
    assertTrue(0 < comp.compare(Double.NaN, Double.NEGATIVE_INFINITY));
    assertTrue(0 > comp.compare(Double.POSITIVE_INFINITY, Double.NaN));
    assertTrue(0 > comp.compare(Double.NEGATIVE_INFINITY, Double.NaN));

    assertTrue(0 < comp.compare(1, Double.NEGATIVE_INFINITY));
    assertTrue(0 > comp.compare(1, Double.POSITIVE_INFINITY));
    assertTrue(0 > comp.compare(Double.NEGATIVE_INFINITY, 1));
    assertTrue(0 < comp.compare(Double.POSITIVE_INFINITY, 1));
    assertTrue(0 < comp.compare(Double.NaN, 1));
    assertTrue(0 > comp.compare(1, Double.NaN));
  }
}
