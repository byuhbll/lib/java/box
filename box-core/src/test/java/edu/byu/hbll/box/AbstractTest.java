package edu.byu.hbll.box;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.JsonUtils;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.json.YamlLoader;
import edu.byu.hbll.misc.Resources;
import edu.byu.hbll.witness.Witness;
import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.time.Instant;
import org.junit.jupiter.api.AfterEach;

/**
 * @author Charles Draper
 */
public class AbstractTest {

  public static final BoxDocument.Status READY = BoxDocument.Status.READY;
  public static final BoxDocument.Status DELETED = BoxDocument.Status.DELETED;
  public static final BoxDocument.Status UNPROCESSED = BoxDocument.Status.UNPROCESSED;
  public static final BoxDocument.Status ERROR = BoxDocument.Status.ERROR;

  protected static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /** The parent box instance. */
  protected Box box;

  /** The principal source. */
  protected Source source;

  /** The parent witness instance. */
  protected Witness witness =
      Witness.builder()
          .validate("/*/@box/cursor", x -> x.asText().matches("\\d+"))
          .validate("/*/@box/modified", x -> Instant.parse(x.asText()) != null)
          .validate("/*/@box/processed", x -> Instant.parse(x.asText()) != null)
          .validate("/*/documents/*/@box/cursor", x -> x.asText().matches("\\d+"))
          .validate("/*/documents/*/@box/modified", x -> Instant.parse(x.asText()) != null)
          .validate("/*/documents/*/@box/processed", x -> Instant.parse(x.asText()) != null)
          .build();

  /**
   * Initializes the sources as found in the corresponding class yaml files. It also includes
   * sources found in the AbstractTest.yml file. The first sourceName in the list will be the
   * principal source unless another source is explicitly set as principal.
   *
   * @param sourceNames the sources to initialize
   */
  protected void init(String... sourceNames) {
    init(loadYaml(), sourceNames);
  }

  /**
   * Same as {@link #init(String...)} except use the given config instead of the yaml files. The
   * first sourceName in the list will be the principal source unless another source is explicitly
   * set as principal.
   *
   * @param config the config to use
   * @param sourceNames the sources to initialize
   */
  protected void init(JsonNode config, String... sourceNames) {
    ObjectNode newConfig = mapper.createObjectNode();

    for (String sourceName : sourceNames) {
      newConfig.with("sources").set(sourceName, config.path("sources").path(sourceName));
    }

    newConfig.set("db", config.path("db"));

    box = Box.newBox(newConfig);

    if (sourceNames.length != 0) {
      source = source(sourceNames[0]);
    }

    // make sure primary is established (fast for memory database)
    sleep(100);
  }

  @AfterEach
  void tearDown() {
    if (box != null) {
      box.close();
    }
  }

  /**
   * Same as {@link Box#getSource(String)} using the parent box instance.
   *
   * @param sourceName the source name
   * @return the source for the given source name
   */
  protected Source source(String sourceName) {
    return box.getSource(sourceName);
  }

  /**
   * Loads yaml from the corresponding yaml file for this class.
   *
   * @return the loaded yaml
   */
  protected JsonNode loadYaml() {
    return JsonUtils.merge(
        loadYaml("AbstractTest.yml"), loadYaml(this.getClass().getSimpleName() + ".yml"));
  }

  /**
   * Loads yaml from the given resource.
   *
   * @return the loaded yaml
   */
  protected JsonNode loadYaml(String resourceName) {
    try {
      String boxConfig = Resources.readString(resourceName);
      return new YamlLoader().load(new StringReader(boxConfig));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Same as {@link Thread#sleep(long)}, but without the checked exception.
   *
   * @param millis the time to sleep in milliseconds
   */
  protected void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
    }
  }

  /**
   * Same as {@link Witness#testify(Object)} using the parent witness instance.
   *
   * @param value the value to test
   */
  protected void testify(Object value) {
    witness.testify(value);
  }

  /**
   * Same as {@link Witness#testify(Object, Object))} using the parent witness object.
   *
   * @param value the value to test
   * @param label a unique label for the test (necessary when a test method has more than one
   *     testify)
   */
  protected void testify(Object value, Object label) {
    witness.testify(value, label);
  }

  /**
   * Returns the document status for the given id of the primary source.
   *
   * @param id the id of the document
   * @return the document's status
   */
  protected BoxDocument.Status status(String id) {
    return status(source.getName(), id);
  }

  /**
   * Returns the document status for the given id of the given source.
   *
   * @param sourceName the source name
   * @param id the id of the document
   * @return the document's status
   */
  protected BoxDocument.Status status(String sourceName, String id) {
    return source(sourceName).collect(new BoxQuery(id)).get(0).getStatus();
  }

  /**
   * Sleeps until the system clock is on a boundary denoted by the unitInMillis.
   *
   * @param unitInMillis the boundary
   */
  protected void align(long unitInMillis) {
    long now = System.currentTimeMillis();
    long next = now / unitInMillis * unitInMillis + unitInMillis;
    sleep(next - now);
  }

  /**
   * Same as calling {@link #process(String, String)} with the principal source.
   *
   * @param id id of the document process
   * @return the resulting document
   */
  protected BoxDocument process(String id) {
    return process(source.getName(), id);
  }

  /**
   * Processes a document and returns it.
   *
   * @param sourceName the sourceName
   * @param id id of the document process
   * @return the resulting document
   */
  protected BoxDocument process(String sourceName, String id) {
    return source(sourceName).collect(new BoxQuery(id).setProcess(true)).get(0);
  }
}
