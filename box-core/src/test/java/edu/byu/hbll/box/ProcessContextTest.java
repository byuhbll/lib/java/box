/** */
package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author cfd2
 */
public class ProcessContextTest {

  ProcessContext context;

  @BeforeEach
  public void before() {
    Map<DocumentId, BoxDocument> dependencies = new HashMap<>();
    dependencies.put(new DocumentId("src1", "id11"), new BoxDocument("id11").setAsReady());
    dependencies.put(new DocumentId("src1", "id12"), new BoxDocument("id12").setAsReady());
    dependencies.put(new DocumentId("src2", "id2"), new BoxDocument("id2").setAsReady());
    context = new ProcessContext("id", dependencies);
  }

  /** Test method for {@link edu.byu.hbll.box.ProcessContext#ProcessContext(java.lang.String)}. */
  @Test
  public void testProcessContextString() {
    ProcessContext context = new ProcessContext("id");
    assertEquals("id", context.getId());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.ProcessContext#ProcessContext(java.lang.String,
   * java.util.Map)}.
   */
  @Test
  public void testProcessContextStringMapOfDocumentIdBoxDocument() {
    assertEquals("id", context.getId());
  }

  /** Test method for {@link edu.byu.hbll.box.ProcessContext#getId()}. */
  @Test
  public void testGetId() {
    assertEquals("id", context.getId());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.ProcessContext#getDependency(java.lang.String,
   * java.lang.String)}.
   */
  @Test
  public void testGetDependencyStringString() {
    assertEquals("id11", context.getDependency("src1", "id11").getId());
    assertNull(context.getDependency("src1", "fake"));
    assertNull(context.getDependency("fake", "fake"));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.ProcessContext#getDependency(edu.byu.hbll.box.DocumentId)}.
   */
  @Test
  public void testGetDependencyDocumentId() {
    assertEquals("id11", context.getDependency(new DocumentId("src1", "id11")).getId());
    assertNull(context.getDependency(new DocumentId("src1", "fake")));
    assertNull(context.getDependency(new DocumentId("fake", "fake")));
  }

  /** Test method for {@link edu.byu.hbll.box.ProcessContext#getDependencies()}. */
  @Test
  public void testGetDependencies() {
    assertNotNull(context.getDependencies());
  }

  /** Test method for {@link edu.byu.hbll.box.ProcessContext#getDependencies(java.lang.String)}. */
  @Test
  public void testGetDependenciesString() {
    assertEquals("id11", context.getDependencies("src1").get(0).getId());
    assertEquals("id12", context.getDependencies("src1").get(1).getId());
    assertEquals("id2", context.getDependencies("src2").get(0).getId());
    assertNotNull(context.getDependencies("fake"));
  }

  /**
   * Test method for {@link edu.byu.hbll.box.ProcessContext#getFirstDependency(java.lang.String)}.
   */
  @Test
  public void testGetFirstDependency() {
    assertEquals("id11", context.getFirstDependency("src1").getId());
    assertEquals("id2", context.getFirstDependency("src2").getId());
    assertNull(context.getFirstDependency("fake"));
  }
}
