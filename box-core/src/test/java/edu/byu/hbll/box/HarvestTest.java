package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import org.junit.jupiter.api.Test;

/**
 * Tests all intricacies regarding source harvesting (ie, {@link Harvester}, {@link HarvestResult},
 * and harvest configuration inside {@link SourceConfig}).
 *
 * @author Charles Draper
 */
public class HarvestTest extends AbstractTest {

  /** Tests basic harvesting. */
  @Test
  void testHarvest() {
    init("harvest");
    align(1000);
    sleep(500);
    source.clear();
    assertEquals(0, source.collect(new BoxQuery().setUnlimited()).size());
    sleep(1000);
    assertEquals(10, source.collect(new BoxQuery().setUnlimited()).size());
    sleep(1000);
    assertEquals(20, source.collect(new BoxQuery().setUnlimited()).size());
  }

  /**
   * Tests that the harvester is called immediately again if {@link HarvestResult#setMore(boolean)) is true.
   */
  @Test
  void testMore() {
    init("more");
    Source source = box.getSource("more");
    source.triggerHarvest();
    // wait for harvest to complete
    sleep(500);
    assertEquals(100, source.collect(new BoxQuery().setUnlimited()).size());
  }

  /** Tests reharvest functionality. */
  @Test
  void testReharvest() {
    init("reharvest");
    align(200);
    sleep(100);
    source.clear();
    assertFalse(source.get("1").isReady());
    assertFalse(source.get("11").isReady());
    sleep(200);
    Instant processed1 = source.get("1").getProcessed().get();
    assertFalse(source.get("11").isReady());
    sleep(200);
    Instant processed2 = source.get("1").getProcessed().get();
    assertTrue(processed1.isBefore(processed2));

    // verify that primary harvester still functioning
    assertTrue(source.get("11").isReady());
  }

  @Test
  void testHarvestContext() {
    // the {@link TestHarvester} runs this test to verify that the Box, Source, and Cursor objects
    // are non-null
  }
}
