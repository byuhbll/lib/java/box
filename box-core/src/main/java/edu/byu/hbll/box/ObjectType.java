package edu.byu.hbll.box;

/**
 * The type of configuration object.
 *
 * @author Charles Draper
 */
public enum ObjectType {
  /** Of type {@link Processor}. */
  PROCESSOR,
  /** Of type {@link Harvester}. */
  HARVESTER,
  /** Of type {@link BoxDatabase}. */
  BOX_DATABASE,
  /** Of type {@link BoxDatabase} used specifically for storing the cursor. */
  CURSOR_DATABASE,
  /** Any other {@link BoxConfigurable}. */
  OTHER;
}
