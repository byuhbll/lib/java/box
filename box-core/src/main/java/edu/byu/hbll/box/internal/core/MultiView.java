package edu.byu.hbll.box.internal.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.ReadOnlyDatabase;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.json.JsonField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Experimental.
 *
 * @author Charles Draper
 */
public class MultiView implements Processor, Harvester, ReadOnlyDatabase {

  private final List<ViewInfo> views = new ArrayList<>();
  private final Map<String, ViewInfo> viewMap = new LinkedHashMap<>();
  private ConstructConfig constructConfig;

  @Override
  public void postConstruct(ConstructConfig config) {
    this.constructConfig = config;
  }

  @Override
  public void postInit(InitConfig config) {
    JsonNode params = constructConfig.getParams();

    for (JsonField sourceConfig : new JsonField(params.path("sources"))) {
      Source source = config.getBox().getSource(sourceConfig.getKey());
      JsonNode sourceParams = sourceConfig.getValue();

      ViewInfo info = new ViewInfo();
      info.id = source.getName();
      info.source = source;

      info.idDownRegex = sourceParams.path("idDownRegex").asText(".+");
      info.idDownReplacement = sourceParams.path("idDownReplacement").asText(info.id + ".$0");
      info.idUpRegex = sourceParams.path("idUpRegex").asText(info.id + "\\.(.+)");
      info.idUpReplacement = sourceParams.path("idUpReplacement").asText("$1");

      views.add(info);
      viewMap.put(info.id, info);
    }
  }

  @Override
  public QueryResult find(BoxQuery query) {
    Map<String, List<String>> batches = new HashMap<>();
    QueryResult result = new QueryResult();

    if (query.isIdQuery()) {
      for (String id : query.getIds()) {
        for (ViewInfo view : views) {
          if (id.matches(view.idUpRegex)) {
            String upId = id.replaceAll(view.idUpRegex, view.idUpReplacement);
            batches.computeIfAbsent(view.id, k -> new ArrayList<>()).add(upId);
            break;
          }
        }

        for (String viewId : batches.keySet()) {
          ViewInfo view = viewMap.get(viewId);
          List<String> ids = batches.get(view.id);
          QueryResult viewResult = view.source.collect(new BoxQuery(query).clearIds().addIds(ids));
          view.downIds(viewResult);
          result.addAll(viewResult);
        }
      }
    } else {
      List<BoxDocument> documents = new ArrayList<>();

      for (ViewInfo view : views) {
        QueryResult viewResult = view.source.collect(query);
        view.downIds(viewResult);
        documents.addAll(viewResult);
      }

      Collections.sort(documents, Comparator.comparingLong(d -> d.getCursor().orElse(0L)));
      result.addAll(
          documents.subList(0, Math.min((int) query.getLimitOrDefault(), documents.size())));

      result.updateNextCursor(query);
    }

    return result;
  }

  @Override
  public HarvestResult harvest(HarvestContext context) {
    HarvestResult result = new HarvestResult();
    ObjectNode newCursor = context.getCursor().deepCopy();

    for (ViewInfo view : views) {
      long viewCursor = context.getCursor().path("cursor").path(view.id).asLong(0);
      QueryResult viewResult =
          view.source.collect(new BoxQuery().setCursor(viewCursor).setMetadataOnly());
      newCursor.with("cursor").put(view.id, viewResult.getNextCursor());
      view.downIds(viewResult);
      result.addAll(viewResult);
    }

    result.setCursor(newCursor);
    result.setMore(!result.isEmpty());

    return result;
  }

  @Override
  public ProcessResult process(ProcessBatch batch) {
    Map<String, ProcessBatch> batches = new HashMap<>();

    for (String id : batch.getIds()) {
      for (ViewInfo view : views) {
        if (id.matches(view.idUpRegex)) {
          ProcessContext context =
              new ProcessContext(view.upId(id), batch.get(id).getDependencies());
          batches.computeIfAbsent(view.id, k -> new ProcessBatch()).add(context);
          break;
        }
      }
    }

    ProcessResult result = new ProcessResult();

    for (ViewInfo view : views) {
      ProcessBatch viewBatch = batches.get(view.id);
      QueryResult viewResult =
          view.source.collect(new BoxQuery(viewBatch.getIds()).setProcess(true));
      view.downIds(viewResult);
      result.addAll(viewResult);
    }

    return result;
  }

  private static class ViewInfo {
    private String id;
    private Source source;
    private String idUpRegex;
    private String idUpReplacement;
    private String idDownRegex;
    private String idDownReplacement;

    private void downIds(Collection<? extends BoxDocument> documents) {
      documents.forEach(d -> d.setId(downId(d.getId())));
    }

    private String downId(String id) {
      return id.replaceFirst(idDownRegex, idDownReplacement);
    }

    private String upId(String id) {
      return id.replaceFirst(idUpRegex, idUpReplacement);
    }
  }
}
