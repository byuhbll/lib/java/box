package edu.byu.hbll.box;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * An entry to be added to a source's process queue.
 *
 * @author Charles Draper
 */
@Data
@AllArgsConstructor
@Accessors(chain = true)
public class QueueEntry {

  /** The id of the document to process. */
  private String id;

  /** When the processing should take place. */
  private Instant attempt = Instant.now();

  /**
   * The priority of this entry. Entries with higher priority (lower number) will be processed
   * first.
   */
  private float priority = 1;

  /** Whether or not to overwrite the entry if it already exists. */
  private boolean overwrite;

  /**
   * Creates a new {@link QueueEntry} with the given id.
   *
   * @param id the id
   */
  public QueueEntry(String id) {
    this.id = id;
  }

  /**
   * Sets the priority for the entry. Lower numbers signfiy higher priority.
   *
   * @param priority the priority
   * @return this queue entry
   */
  public QueueEntry setPriority(double priority) {
    this.priority = (float) priority;
    return this;
  }
}
