package edu.byu.hbll.box.internal.core;

import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.SourceConfig;
import edu.byu.hbll.misc.BatchExecutorService;
import edu.byu.hbll.misc.BatchRunnable;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A single background thread that processes tasks from all maintenance queues. It runs according to
 * a specified schedule. The schedule is a cron like schedule that kicks off the run of maintenance.
 * The run will only execute for the supplied
 *
 * @author Charles Draper
 */
public class QueueRunner implements BoxConfigurable {

  static final Logger logger = LoggerFactory.getLogger(QueueRunner.class);

  /** DB queue number of entry transfer size limit. */
  private static final int QUEUE_TRANSFER_LIMIT = 100;

  /** DB queue put and get size limit. */
  private static final int MEMORY_QUEUE_LIMIT = 10000;

  /** Number of seconds between DB queue checks. */
  private static final int DB_CHECK_TIMEOUT = 1;

  /** Retry again after this many milliseconds if something fails like a database connection. */
  private static final int RETRY_TIMEOUT = 60000;

  /** The registry. */
  private Registry registry;

  /** The source. */
  private SourceConfig source;

  private OnOffSemaphore onOffSemaphore;

  /**
   * Queues up submissions for the db queue and flushes them asynchronously so that submission is
   * fast.
   */
  private BatchExecutorService<QueueEntry, Object> queueService;

  /** Used solely to signal to loadFromDb() that a new entry is on the queue. */
  private BlockingQueue<Object> signal = new ArrayBlockingQueue<>(1);

  private volatile boolean shutdown;
  private CountDownLatch shutdownComplete = new CountDownLatch(1);

  /**
   * Creates a new {@link QueueRunner}.
   *
   * @param registry the registry
   * @param source the source for this runner
   * @param onOffSemaphore the on off semaphore
   */
  public QueueRunner(Registry registry, SourceConfig source, OnOffSemaphore onOffSemaphore) {
    this.source = source;
    this.registry = registry;
    this.onOffSemaphore = onOffSemaphore;
    this.queueService =
        new BatchExecutorService.Builder(new AddToDbQueue())
            .threadFactory(registry.getThreadFactory())
            .batchCapacity(QUEUE_TRANSFER_LIMIT)
            .queueCapacity(MEMORY_QUEUE_LIMIT)
            .build();

    // start up loadFromDb() thread
    registry.getThreadFactory().newThread(() -> loadFromDb()).start();
  }

  /**
   * Periodically checks the queue in the database for new entries. New available entries are placed
   * on the processing queue to be processed. Calls to submit() will trigger this check.
   */
  private void loadFromDb() {

    if (!source.isProcessEnabled()) {
      shutdownComplete.countDown();
      return;
    }

    // wait until box has been configured
    while (!registry.isConfigured() && !shutdown) {
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        shutdownComplete.countDown();
        return;
      }
    }

    boolean more = false;

    while (!shutdown) {
      try {

        if (!more) {
          signal.poll(DB_CHECK_TIMEOUT, TimeUnit.SECONDS);
        }

        // skip if processing is off
        if (!onOffSemaphore.isOn()) {
          continue;
        }

        // skip if not primary
        if (!registry.isPrimary(source.getName())) {
          continue;
        }

        List<String> ids = source.getDb().nextFromQueue(QUEUE_TRANSFER_LIMIT);

        registry.getDocumentProcessor(source.getName()).process(ids);

        more = !ids.isEmpty();

      } catch (InterruptedException e) {
        return;
      } catch (Exception e) {
        logger.error("unable to process queue at this time, trying again in one minute: " + e, e);

        try {
          // sleep and then retry
          Thread.sleep(RETRY_TIMEOUT);
        } catch (InterruptedException e1) {
          // do nothing
        }
      }
    }

    shutdownComplete.countDown();
  }

  /**
   * Submits a document ID for future asynchronous processing. The ID is first queued in memory
   * before being written to disk so this method is very fast.
   *
   * @param id the document ID to be processed
   * @param attempt when to attempt processing
   * @param resetAttempt if the ID is already in the queue, this will reset its attempt time to now
   *     if true, otherwise it will leave it as it is.
   */
  public void submit(String id, Instant attempt, boolean resetAttempt) {
    if (source.getProcessor() != null) {
      try {
        queueService
            .getQueue()
            .put(new QueueEntry(id, attempt, DocumentHandler.QUEUE_PRIORITY_DEFAULT, resetAttempt));
      } catch (InterruptedException e) {
        return;
      }
    }
  }

  @Override
  public void preDestroy() {
    logger.debug("Shutting down " + source.getName() + " " + this.getClass().getSimpleName());
    shutdown = true;

    try {
      shutdownComplete.await();
    } catch (InterruptedException e) {
      queueService.shutdown();
      return;
    }

    queueService.shutdownAndWait();
    logger.debug("Done shutting down " + source.getName() + " " + this.getClass().getSimpleName());
  }

  /** Puts queue entries onto the database queue. To be run with the BatchExecutorService. */
  private class AddToDbQueue implements BatchRunnable<QueueEntry, Object> {

    /**
     * Executes the batch.
     *
     * @param entries to be placed on the database queue
     * @return null always
     */
    @Override
    public List<Object> run(List<QueueEntry> entries) {
      source.getDb().addToQueue(entries);
      signal.offer("");
      return null;
    }
  }
}
