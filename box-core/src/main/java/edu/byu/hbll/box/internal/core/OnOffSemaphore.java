package edu.byu.hbll.box.internal.core;

import java.time.Instant;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/** A semaphore to answer whether processing is turned on or not at the moment. */
public class OnOffSemaphore {

  private Semaphore on;

  /**
   * Returns whether or not processing is on.
   *
   * @return whether or not processing is on
   */
  public boolean isOn() {
    return on == null || on.availablePermits() > 0;
  }

  /**
   * Blocks until processing is turned back on through {@link #setOn(boolean)} or until the timeout
   * occurs.
   *
   * @param timeout the time to wait
   * @param unit the unit of time
   * @return if on after wait
   * @throws InterruptedException if interrupted
   */
  public boolean awaitOn(long timeout, TimeUnit unit) throws InterruptedException {
    synchronized (this) {
      if (this.on == null) {
        return true;
      }

      boolean on = this.on.tryAcquire(timeout, unit);

      if (on) {
        this.on.release();
      }

      return on;
    }
  }

  /**
   * Sets on or off.
   *
   * @param on true or false if on
   * @return always returns false
   */
  void setOn(boolean on) {
    synchronized (this) {
      if (this.on == null) {
        this.on = new Semaphore(0);
      }

      if (on) {
        this.on.release();
      } else {
        this.on.drainPermits();
      }
    }
  }

  /**
   * Only initialize if not already initialized.
   *
   * @param nextOn the next time processing will be turned on
   * @param nextOff the next time processing will be turned off
   */
  void init(Instant nextOn, Instant nextOff) {
    synchronized (this) {
      if (this.on == null) {
        if (nextOn.isBefore(nextOff)) {
          setOn(false);
        } else {
          setOn(true);
        }
      }
    }
  }
}
