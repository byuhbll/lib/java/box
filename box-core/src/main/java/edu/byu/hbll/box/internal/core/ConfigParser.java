package edu.byu.hbll.box.internal.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.ObjectFactory;
import edu.byu.hbll.box.ObjectType;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.SourceConfig;
import edu.byu.hbll.box.SourceConfig.SourceConfigBuilder;
import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.json.JsonUtils;
import edu.byu.hbll.json.YamlLoader;
import edu.byu.hbll.scheduler.CronExpression;
import edu.byu.hbll.scheduler.CronSchedule;
import edu.byu.hbll.scheduler.PeriodicSchedule;
import edu.byu.hbll.scheduler.Schedule;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parses the yaml box configuration.
 *
 * @author Charles Draper
 */
public class ConfigParser {

  static final Logger logger = LoggerFactory.getLogger(ConfigParser.class);

  private static ObjectMapper mapper = new ObjectMapper();

  /**
   * Sets all parameters held in the {@link JsonNode}.
   *
   * @param config the config object
   * @param objectFactory the object factory
   * @return all found configurations for the sources
   */
  public static List<SourceConfig> parse(JsonNode config, ObjectFactory objectFactory) {
    logger.info("Configuring sources");
    List<SourceConfig> sources = new ArrayList<>();

    try {
      // make immediate copy to prevent modifying the actual parameter
      config = config == null ? MissingNode.getInstance() : config.deepCopy();

      // read in defaults
      JsonNode defaults =
          new YamlLoader()
              .load(
                  new InputStreamReader(
                      ConfigParser.class.getResourceAsStream("/edu/byu/hbll/box/defaults.yml"),
                      "utf8"));

      // merge default values
      config = JsonUtils.merge(defaults, config);

      Iterator<String> sourceNames = config.path("sources").fieldNames();

      while (sourceNames.hasNext()) {
        String sourceName = sourceNames.next();
        JsonNode srcCfg = config.path("sources").path(sourceName);
        SourceConfigBuilder sourceBuilder = SourceConfig.builder().name(sourceName);

        // template if exists
        if (srcCfg.has("template")) {
          JsonNode templateCfg = srcCfg.path("template");
          String templateId = templateCfg.path("id").asText(null);
          ObjectNode template = (ObjectNode) config.path("templates").path(templateId).deepCopy();

          Map<String, JsonNode> values = new HashMap<>();

          // get default param values
          for (JsonField field : new JsonField(template.path("templateParams"))) {
            values.put(field.getKey(), field.getValue());
          }

          // get set param values
          for (JsonField field : new JsonField(templateCfg.path("params"))) {
            values.put(field.getKey(), field.getValue());
          }

          template.remove("templateParams");

          edu.byu.hbll.box.internal.util.JsonUtils.bind(template, values);

          srcCfg = JsonUtils.merge(template, srcCfg);
        }

        logger.info(
            "Found source `"
                + sourceName
                + "` with the following configuration:\n"
                + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(srcCfg));

        if (!srcCfg.path("enabled").asBoolean(true)) {
          logger.warn("Source `" + sourceName + "` is disabled");
          continue;
        }

        if (srcCfg.has("principal")) {
          sourceBuilder.principal(srcCfg.path("principal").asBoolean());
        }

        if (srcCfg.has("removeAge")) {
          sourceBuilder.removeAge(Duration.parse(srcCfg.path("removeAge").asText()));
        }

        if (srcCfg.has("removeSchedule")) {
          sourceBuilder.removeSchedule(parseSchedule(srcCfg.path("removeSchedule").asText()));
        }

        if (srcCfg.has("facetField")) {
          Iterator<String> facetNames = srcCfg.path("facetField").fieldNames();

          while (facetNames.hasNext()) {
            String facetName = facetNames.next();
            JsonNode facetField = srcCfg.path("facetField").path(facetName);

            if (facetField.isArray()) {
              for (JsonNode value : facetField) {
                sourceBuilder.facetFields(facetName, value.asText());
              }
            } else {
              sourceBuilder.facetFields(facetName, facetField.asText());
            }
          }
        }

        if (srcCfg.has("save")) {
          sourceBuilder.save(srcCfg.path("save").asBoolean());
        }

        if (srcCfg.has("dependencyOnly")) {
          sourceBuilder.dependencyOnly(srcCfg.path("dependencyOnly").asBoolean());
        }

        if (srcCfg.has("defaultLimit")) {
          sourceBuilder.defaultLimit(srcCfg.path("defaultLimit").asInt());
        }

        if (srcCfg.has("postEnabled")) {
          sourceBuilder.postEnabled(srcCfg.path("postEnabled").asBoolean());
        }

        if (srcCfg.has("process") && srcCfg.path("process").path("enabled").asBoolean(true)) {

          JsonNode processCfg = srcCfg.path("process").deepCopy();

          if (!processCfg.has("type")) {
            throw new IllegalArgumentException(
                "missing 'type' attribute for source." + sourceName + ".process");
          }

          if (processCfg.has("batchDelay")) {
            sourceBuilder.batchDelay(Duration.parse(processCfg.path("batchDelay").asText()));
          }

          sourceBuilder.batchCapacity(processCfg.path("batchCapacity").asInt(1));

          if (processCfg.has("threadCount")) {
            sourceBuilder.threadCount(processCfg.path("threadCount").asInt());
          }

          if (processCfg.has("suspend")) {
            sourceBuilder.suspend(Duration.parse(processCfg.path("suspend").asText()));
          }

          if (processCfg.has("quota")) {
            sourceBuilder.quota(processCfg.path("quota").asInt());
          }

          if (processCfg.has("quotaReset")) {
            sourceBuilder.quotaReset(parseSchedule(processCfg.path("quotaReset").asText()));
          }

          if (processCfg.has("on")) {
            sourceBuilder.on(parseSchedule(processCfg.path("on").asText()));
          }

          if (processCfg.has("off")) {
            sourceBuilder.off(parseSchedule(processCfg.path("off").asText()));
          }

          if (processCfg.has("enabled")) {
            sourceBuilder.processEnabled(processCfg.path("enabled").asBoolean());
          }

          if (processCfg.has("process")) {
            sourceBuilder.process(processCfg.path("process").asBoolean());
          }

          if (processCfg.has("wait")) {
            sourceBuilder.wait(processCfg.path("wait").asBoolean());
          }

          if (processCfg.has("retryDelay")) {
            sourceBuilder.retryDelay(Duration.parse(processCfg.path("retryDelay").asText()));
          }

          if (processCfg.has("retryJitter")) {
            sourceBuilder.retryJitter(processCfg.path("retryJitter").asDouble());
          }

          if (processCfg.has("reprocessAge")) {
            sourceBuilder.reprocessAge(Duration.parse(processCfg.path("reprocessAge").asText()));
          }

          if (processCfg.has("reprocessSchedule")) {
            sourceBuilder.reprocessSchedule(
                parseSchedule(processCfg.path("reprocessSchedule").asText()));
          }

          Processor processor =
              (Processor)
                  BoxConfigurable.create(
                      sourceName, objectFactory, ObjectType.PROCESSOR, processCfg);

          sourceBuilder.processor(processor);

        } else {
          sourceBuilder.processEnabled(false);

          if (srcCfg.has("process")) {
            logger.warn("Source `" + sourceName + "` has disabled processor");
          }
        }

        if (srcCfg.has("harvest") && srcCfg.path("harvest").path("enabled").asBoolean(true)) {

          JsonNode harvestCfg = srcCfg.path("harvest").deepCopy();

          if (!harvestCfg.has("type")) {
            throw new IllegalArgumentException(
                "missing 'type' attribute for source." + sourceName + ".harvest");
          }

          if (harvestCfg.has("schedule") && !harvestCfg.path("schedule").isNull()) {
            sourceBuilder.harvestSchedule(parseSchedule(harvestCfg.path("schedule").asText()));
          }

          if (harvestCfg.has("resetSchedule")) {
            sourceBuilder.harvestResetSchedule(
                parseSchedule(harvestCfg.path("resetSchedule").asText(null)));
          }

          if (harvestCfg.has("reharvestSchedule")) {
            sourceBuilder.reharvestSchedule(
                parseSchedule(harvestCfg.path("reharvestSchedule").asText(null)));
          }

          if (harvestCfg.has("saveDeleted")) {
            sourceBuilder.saveDeleted(harvestCfg.path("saveDeleted").asBoolean());
          }

          if (harvestCfg.has("enabled") && !harvestCfg.path("enabled").isNull()) {
            sourceBuilder.harvestEnabled(harvestCfg.path("enabled").asBoolean());
          }

          Harvester harvester =
              (Harvester)
                  BoxConfigurable.create(
                      sourceName, objectFactory, ObjectType.HARVESTER, harvestCfg);

          sourceBuilder.harvester(harvester);
        } else {
          sourceBuilder.harvestEnabled(false);

          if (srcCfg.has("harvest")) {
            logger.warn("Source `" + sourceName + "` has disabled harvester");
          }
        }

        if (srcCfg.has("db") || config.has("db")) {

          JsonNode dbConfig = srcCfg.path("db");

          if (dbConfig.isMissingNode()) {
            dbConfig = config.path("db");
          }

          sourceBuilder.db(BoxDatabase.create(sourceName, objectFactory, dbConfig));
        }

        if (srcCfg.has("cursor")
            && (srcCfg.path("cursor").has("type")
                || !srcCfg.path("cursor").has("type") && config.has("db"))) {

          JsonNode cursorConfig = srcCfg.path("cursor");

          if (!cursorConfig.has("type")) {
            cursorConfig = config.path("db");
          }

          BoxDatabase cursorDb =
              (BoxDatabase)
                  BoxConfigurable.create(
                      sourceName, objectFactory, ObjectType.CURSOR_DATABASE, cursorConfig);

          sourceBuilder.cursorDb(cursorDb);
        }

        if (srcCfg.has("other")) {
          JsonNode otherConfig = srcCfg.path("other");

          sourceBuilder.other(
              BoxConfigurable.create(sourceName, objectFactory, ObjectType.OTHER, otherConfig));
        }

        SourceConfig source = sourceBuilder.build();

        sources.add(source);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return sources;
  }

  /**
   * Parses a value to a {@link Schedule}. The value may be in various formats.
   *
   * <ul>
   *   <li>millis for the period of a periodic schedule
   *   <li>{@link Duration} for the period of a periodic schedule
   *   <li>{@link CroneExpression} for the schedule of a cron schedule
   * </ul>
   *
   * @param value the value to parse
   * @return the corresponding schedule
   * @throws IllegalArgumentException if value cannot be parsed into any of those formats
   */
  private static Schedule parseSchedule(String value) {
    // first try number of millis
    try {
      long millis = Long.parseLong(value);
      return new PeriodicSchedule.Builder(Duration.ofMillis(millis)).build();
    } catch (NumberFormatException e) {
      // value not expressed as millis
    }

    // second try duration
    try {
      Duration duration = Duration.parse(value);
      return new PeriodicSchedule.Builder(duration).build();
    } catch (DateTimeParseException e) {
      // value not expressed as duration
    }

    // third try cron expression
    try {
      CronExpression cronExpression = new CronExpression(value);
      return new CronSchedule.Builder(cronExpression).build();
    } catch (IllegalArgumentException e) {
      // value not expressed as cron expression
    }

    throw new IllegalArgumentException(
        "schedule bust be number of millis, ISO-8601 duration, or cron expression");
  }
}
