package edu.byu.hbll.box;

/**
 * Creates Box objects such as processors and harvesters. The meaning of the type is implementation
 * specific and corresponds to the <code>type</code> attribute in the configuration.
 */
public interface ObjectFactory {

  /**
   * Creates a new box object according to the implementation.
   *
   * @param type the implementation specific type that corresponds to the <code>type</code>
   *     attribute in the configuration
   * @return the newly created box object
   */
  Object create(String type);
}
