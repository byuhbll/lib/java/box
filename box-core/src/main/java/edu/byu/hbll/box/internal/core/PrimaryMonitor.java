package edu.byu.hbll.box.internal.core;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.impl.MemoryDatabase;
import java.math.BigInteger;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * In a distributed environment, only one node should be allowed to run harvests. Harvests are
 * inherently singled threaded processes. Here we also only allow one node to process from the
 * queue. This class communicates with other nodes through the database to determine which node is
 * primary (ie, the node that can harvest and process the queue). It does this by reading and
 * setting an entry to the registry. The primary node must write an entry regularly to the registry
 * so that other nodes know there is an active primary. If the primary goes down, the others will
 * detect that the entry gets stale at which point at least one of them will attempt to write its
 * own entry and claim primary. If one node's entry is written and read back sequentially
 * THRESHOLD_COUNT times, then the node becomes primary.
 *
 * @author Charles Draper
 */
public class PrimaryMonitor implements Runnable {

  static final Logger logger = LoggerFactory.getLogger(PrimaryMonitor.class);

  /** The field name for this entry in the registry table. */
  public static final String REGISTRY_KEY = "primary";

  private static final int THRESHOLD_COUNT = 2;
  private static final int RANDOM_BITS = 128;

  private BoxDatabase db;

  private volatile String oldKey;
  private volatile String myOldKey;

  private volatile long staleCount = 0;
  private volatile long primaryCount = 0;

  private volatile boolean primary;

  /**
   * Creates a new {@link PrimaryMonitor} using the given database for coordination.
   *
   * @param db the underlying database
   */
  public PrimaryMonitor(BoxDatabase db) {
    this.db = db;

    // establish primary now if memory database, important for running tests more quickly
    if (db instanceof MemoryDatabase) {
      for (int i = 0; i < THRESHOLD_COUNT; i++) {
        run();
      }
    }
  }

  @Override
  public void run() {
    ObjectNode data = (ObjectNode) db.findRegistryValue(REGISTRY_KEY);
    data = data == null ? JsonNodeFactory.instance.objectNode() : data;
    String newKey = data.path("key").asText("");
    String myNewKey = new BigInteger(RANDOM_BITS, new Random()).toString(16);

    if (newKey.equals(myOldKey)) {
      staleCount = 0;
      primaryCount++;
    } else if (newKey.equals(oldKey)) {
      staleCount++;
      primaryCount = 0;
    } else {
      staleCount = 0;
      primaryCount = 0;
    }

    if (data.size() == 0 || staleCount >= THRESHOLD_COUNT || primaryCount > 0) {
      data.put("key", myNewKey);
      db.saveRegistryValue(REGISTRY_KEY, data);
    }

    primary = primaryCount >= THRESHOLD_COUNT;
    oldKey = newKey;
    myOldKey = myNewKey;
  }

  /**
   * Return whether or not this node is primary.
   *
   * @return whether or no this node is primary
   */
  public boolean isPrimary() {
    return primary;
  }
}
