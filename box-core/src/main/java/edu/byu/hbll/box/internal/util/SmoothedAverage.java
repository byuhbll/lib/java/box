package edu.byu.hbll.box.internal.util;

/**
 * Used for statistics.
 *
 * @author Charles Draper
 */
public class SmoothedAverage {

  /** The default smoothing factor. */
  public static final double DEFAULT_SMOOTHING_FACTOR = 0.1;

  private double average = 0;
  private double smoothingFactor = DEFAULT_SMOOTHING_FACTOR;
  private boolean initialized = false;

  /** Creates a new {@link SmoothedAverage} with the default smoothing factor. */
  public SmoothedAverage() {}

  /**
   * Creates a new {@link SmoothedAverage} with the given smoothing factor.
   *
   * @param smoothingFactor the smoothing factor
   */
  public SmoothedAverage(double smoothingFactor) {
    this.smoothingFactor = smoothingFactor;
  }

  /**
   * Adds a new value to this statistic.
   *
   * @param addend the value to add
   */
  public void add(double addend) {
    synchronized (this) {
      if (initialized) {
        average = smoothingFactor * addend + (1 - smoothingFactor) * average;
      } else {
        average = addend;
        initialized = true;
      }
    }
  }

  /**
   * Returns the smoothed average.
   *
   * @return the smoothed average
   */
  public double get() {
    synchronized (this) {
      return average;
    }
  }
}
