package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.ReadOnlyDatabase;
import edu.byu.hbll.box.client.BoxClient;
import edu.byu.hbll.box.client.HttpBoxClient;
import java.net.URI;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A read only database that pulls all documents from a remote Box using HTTP.
 *
 * @author Charles Draper
 */
public class RemoteDatabase implements ReadOnlyDatabase {

  static final Logger logger = LoggerFactory.getLogger(RemoteDatabase.class);

  private BoxClient boxClient;
  private ObjectNode cursor = JsonNodeFactory.instance.objectNode();
  private String sourceName;

  @Override
  public void postConstruct(ConstructConfig config) {
    ObjectNode params = config.getParams();
    String uri = Objects.requireNonNull(params.get("uri").asText(null));
    boxClient = new HttpBoxClient(URI.create(uri));
  }

  @Override
  public QueryResult find(BoxQuery query) {
    return boxClient.collect(query);
  }

  @Override
  public ObjectNode getHarvestCursor() {
    return cursor.deepCopy();
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    this.cursor = cursor.deepCopy();
  }

  /**
   * Returns the source name.
   *
   * @return the sourceName
   */
  public String getSourceName() {
    return sourceName;
  }

  /**
   * Sets the source name.
   *
   * @param sourceName the sourceName to set
   */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }
}
