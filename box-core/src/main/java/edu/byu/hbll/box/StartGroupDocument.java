package edu.byu.hbll.box;

/**
 * Special {@link BoxDocument} that signals the start of a group for identifying orphans. The id of
 * the document is the group name.
 *
 * @author Charles Draper
 */
public class StartGroupDocument extends BoxDocument {

  /**
   * Creates a new {@link StartGroupDocument}.
   *
   * @param groupId name or id of the group
   */
  public StartGroupDocument(String groupId) {
    super(groupId);
  }
}
