package edu.byu.hbll.box.internal.core;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * A counting semaphore that represents a quota. When a quota is reached, further acquisition are
 * blocked until the quota is reset.
 */
public class QuotaSemaphore implements Runnable {

  private int quota;
  private Semaphore quotaSemaphore;

  /**
   * Creates a new {@link QuotaSemaphore} with the given quota count.
   *
   * @param quota the quota
   */
  public QuotaSemaphore(int quota) {
    this.quota = quota;
    this.quotaSemaphore = new Semaphore(quota);
  }

  /** Resets the quota. */
  @Override
  public void run() {
    quotaSemaphore.drainPermits();
    quotaSemaphore.release(quota);
  }

  /**
   * Acquires semaphore if quota not reached. Once quota is reached, acquisitions will block until
   * timeout or until quota is reset by calling {@link #run()}.
   *
   * @param timeout the maximum time to wait for a permit
   * @param unit the time unit of the {@code timeout} argument
   * @return {@code true} if a permit was acquired and {@code false} if the waiting time elapsed
   *     before a permit was acquired
   * @throws InterruptedException if an interrupt occurs
   */
  public boolean tryAquire(long timeout, TimeUnit unit) throws InterruptedException {
    if (quota < 1) {
      return true;
    }

    return quotaSemaphore.tryAcquire(timeout, unit);
  }
}
