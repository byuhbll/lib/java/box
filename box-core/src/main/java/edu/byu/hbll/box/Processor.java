package edu.byu.hbll.box;

/**
 * Process ids and dependencies associated with the ids into final documents to be saved in the
 * database.
 *
 * @author Charles Draper
 */
public interface Processor extends BoxConfigurable {

  /**
   * Processes ids and dependencies associated with the ids into final documents to be saved in the
   * database.
   *
   * @param batch a batch of {@link ProcessContext}s to be processed
   * @return the result of processing the batch including newly processed documents to be saved
   */
  ProcessResult process(ProcessBatch batch);
}
