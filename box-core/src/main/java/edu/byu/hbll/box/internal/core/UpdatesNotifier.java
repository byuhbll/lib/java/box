package edu.byu.hbll.box.internal.core;

import java.io.Closeable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Gathers signals that there are updates for sources through {@link #trigger(String)}. When run is
 * called all listeners are also run in an executor.
 *
 * @author Charles Draper
 */
public class UpdatesNotifier implements Runnable, Closeable {

  private final ExecutorService executor;
  private Map<String, Set<Runnable>> listeners = new ConcurrentHashMap<>();
  private Set<String> triggered = Collections.newSetFromMap(new ConcurrentHashMap<>());

  /**
   * Creates a new {@link UpdatesNotifier} with the given thread factory.
   *
   * @param threadFactory the threadFactory to use for the internal executor
   */
  public UpdatesNotifier(ThreadFactory threadFactory) {
    this.executor = Executors.newCachedThreadPool(threadFactory);
  }

  @Override
  public void run() {
    Set<String> triggered = new HashSet<>(this.triggered);

    for (String sourceName : triggered) {
      this.triggered.remove(sourceName);

      Set<Runnable> listeners = this.listeners.get(sourceName);

      if (listeners != null) {
        listeners.forEach(l -> executor.submit(l));
      }
    }
  }

  /**
   * Registers a new updates listener.
   *
   * @param sourceName the name of the source to watch
   * @param listener the listener to register
   */
  public void register(String sourceName, Runnable listener) {
    listeners
        .computeIfAbsent(Objects.requireNonNull(sourceName), k -> new HashSet<>())
        .add(Objects.requireNonNull(listener));
  }

  /**
   * Unregisters an updates listener.
   *
   * @param sourceName name of the source to watch
   * @param listener the listener to unregister
   */
  public void unregister(String sourceName, Runnable listener) {
    listeners
        .computeIfAbsent(Objects.requireNonNull(sourceName), k -> new HashSet<>())
        .remove(Objects.requireNonNull(listener));
  }

  /**
   * Sets the source that has been updated.
   *
   * @param sourceName name of the source to trigger
   */
  public void trigger(String sourceName) {
    triggered.add(Objects.requireNonNull(sourceName));
  }

  @Override
  public void close() {
    if (executor != null) {
      executor.shutdown();
    }
  }
}
