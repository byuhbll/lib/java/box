package edu.byu.hbll.box;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Implements all write-methods as no-ops for use as a read-only database.
 *
 * @author Charles Draper
 */
public interface ReadOnlyDatabase extends BoxDatabase {

  @Override
  default int addToQueue(Duration olderThan) {
    return 0;
  }

  @Override
  default void addToQueue(Collection<String> ids, Instant attempt, boolean overwrite) {}

  @Override
  default void addToQueue(Collection<? extends QueueEntry> elements) {}

  @Override
  default void clear() {
    throw new UnsupportedOperationException("unable to clear a read-only database");
  }

  @Override
  default void deleteFromQueue(Collection<String> id) {}

  @Override
  default ObjectNode getHarvestCursor() {
    return null;
  }

  @Override
  default Set<String> listSourceDependencies() {
    return Collections.emptySet();
  }

  @Override
  default List<String> nextFromQueue(int limit) {
    return Collections.emptyList();
  }

  @Override
  default void processOrphans(String groupId, Consumer<BoxDocument> function) {}

  @Override
  default void removeDeleted(Duration olderThan) {}

  @Override
  default void save(Collection<? extends BoxDocument> resultDocument) {}

  @Override
  default void setHarvestCursor(ObjectNode cursor) {}

  @Override
  default void startGroup(String groupId) {}

  @Override
  default void updateProcessed(Collection<String> id) {}

  @Override
  default Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
    return Collections.emptyMap();
  }

  @Override
  default Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {
    return Collections.emptyMap();
  }

  @Override
  default JsonNode findRegistryValue(String id) {
    return null;
  }

  @Override
  default void saveRegistryValue(String id, JsonNode data) {}

  @Override
  default void updateDependencies(Collection<? extends BoxDocument> documents) {}
}
