package edu.byu.hbll.box;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Information for the current harvest.
 *
 * @author Charles Draper
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class HarvestContext {

  /** The singleton Box object. */
  private Box box;

  /** The source for this harvest. */
  private Source source;

  /** The saved cursor of where the harvest last left off. */
  private ObjectNode cursor = JsonNodeFactory.instance.objectNode();

  /**
   * Creates a new {@link HarvestContext} with the given cursor.
   *
   * @param cursor the cursor of where the harvest last left off
   */
  public HarvestContext(ObjectNode cursor) {
    this.cursor = Objects.requireNonNull(cursor);
  }
}
