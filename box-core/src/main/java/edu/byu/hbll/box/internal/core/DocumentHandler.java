package edu.byu.hbll.box.internal.core;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.EndGroupDocument;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.SourceConfig;
import edu.byu.hbll.box.StartGroupDocument;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/** Overloaded class for the general handling of documents. Singleton. */
@Slf4j
public class DocumentHandler {

  /** Default queue priority. */
  public static final int QUEUE_PRIORITY_DEFAULT = 1;

  /** Queue priority for background maintenance tasks. */
  public static final int QUEUE_PRIORITY_MAINTENANCE = 9;

  private Registry registry;
  private UpdatesNotifier updatesNotifier;

  /**
   * Creates a new {@link DocumentHandler}.
   *
   * @param registry the registry.
   */
  public DocumentHandler(Registry registry) {
    this.registry = registry;
    this.updatesNotifier = registry.getUpdatesNotifier();
  }

  /**
   * Retrieves documents from the database and returns them. If the process flag is set, documents
   * are processed and then returned rather than retrieving the cached versions. If the wait flag is
   * set, cached documents are retrieved, any unprocessed documents are processed, and all are
   * returned. If no flag is set, all documents are retrieved from the database. Any unprocessed
   * ones are indicated so.
   *
   * @param sourceName the source to query
   * @param query the query
   * @return documents in the same number and order as requested
   */
  public QueryResult find(String sourceName, BoxQuery query) {

    sourceName = registry.verifySource(sourceName);
    SourceConfig source = registry.getSource(sourceName);

    if (query.getLimit().orElse(BoxQuery.DEFAULT_LIMIT) < 0) {
      query = new BoxQuery(query).setLimit(source.getDefaultLimit());
    }

    QueryResult result;

    // if process is true, (re)process the documents and wait
    if (query.isIdQuery()
        && (query.isProcess().orElse(BoxQuery.DEFAULT_PROCESS) || source.isProcess())
        && source.isProcessEnabled()) {
      result = new QueryResult(process(sourceName, query.getIds()));
      result.updateNextCursor(query);
    } else {
      // pull documents from the database
      result = source.getDb().find(query);

      if (query.isIdQuery()) {

        // gather unprocessed document ids
        List<String> unprocessed =
            result.stream()
                .filter(d -> !d.isProcessed())
                .map(d -> d.getId())
                .collect(Collectors.toList());

        // if wait is true, process the unprocessed documents and wait
        if ((query.isWait().orElse(BoxQuery.DEFAULT_WAIT) || source.isWait())
            && !unprocessed.isEmpty()
            && source.isProcessEnabled()) {
          // process the unprocessed ones
          List<BoxDocument> processed = process(sourceName, unprocessed);

          // index by id for fast lookup
          Map<String, BoxDocument> indexed =
              processed.stream().collect(Collectors.toMap(d -> d.getId(), Function.identity()));

          // interleave the newly processed ones with the previously processed ones
          List<BoxDocument> latest =
              result.stream()
                  .map(d -> d.isProcessed() ? d : indexed.get(d.getId()))
                  .collect(Collectors.toList());

          result = new QueryResult(latest);
          result.updateNextCursor(query);
        } else {
          addToQueue(sourceName, unprocessed, Instant.now(), false);
        }
      }
    }

    // if there is no processor or processor is disabled
    if (!source.isProcessEnabled()) {
      // mark every unprocessed document as deleted
      result.stream().filter(d -> d.isUnprocessed()).forEach(d -> d.setAsDeleted());
    }

    return result;
  }

  /**
   * Processes and saves the documents for the given sourceName and ids. Waits for the processing to
   * finish and returns the newly processed documents.
   *
   * @param sourceName the source name
   * @param ids the ids to be processed
   * @return the resulting newly processed documents
   */
  public List<BoxDocument> process(String sourceName, Collection<String> ids) {
    DocumentProcessor processor = registry.getDocumentProcessor(sourceName);
    List<BoxDocument> documents = processor.processAndWait(new ArrayList<>(ids));
    return documents;
  }

  /**
   * Queues up the ids for the given source to be processed in the background.
   *
   * @param sourceName the source name
   * @param ids the ids to queue
   * @param attempt attempt processing at this time
   * @param overwrite whether or not to overwrite the attempt if id already exists in queue
   */
  public void addToQueue(
      String sourceName, Collection<String> ids, Instant attempt, boolean overwrite) {
    QueueRunner queueRunner = registry.getQueue(sourceName);
    ids.forEach(id -> queueRunner.submit(id, attempt, overwrite));
  }

  /**
   * Saves the given documents to the database. Depending on the current state of the documents in
   * the database and these new documents, other measures are taken. These measures include:
   *
   * <ul>
   *   <li>Start and End groups come in the form of documents therefore the appropriate start and
   *       end group calls are made to the database.
   *   <li>If the document's dependencies have changed, the document is put back on the queue to be
   *       processed.
   *   <li>If this document is a dependency for other documents, those documents are put on the
   *       queue.
   *   <li>The document is removed from the queue if processed and no dependencies have changed.
   * </ul>
   *
   * @param sourceName the source name
   * @param documents the documents to save
   */
  public void save(String sourceName, Collection<? extends BoxDocument> documents) {
    SourceConfig source = registry.getSource(sourceName);
    BoxDatabase db = source.getDb();

    List<String> startGroups = new ArrayList<>();
    List<String> endGroups = new ArrayList<>();

    List<BoxDocument> actualDocuments = new ArrayList<>();
    List<DocumentId> asDependencies = new ArrayList<>();
    List<String> ids = new ArrayList<>();

    // separate documents into start, end, and actual documents
    for (BoxDocument document : documents) {
      if (document instanceof StartGroupDocument) {
        startGroups.add(document.getId());
      } else if (document instanceof EndGroupDocument) {
        endGroups.add(document.getId());
      } else {
        actualDocuments.add(document);
        asDependencies.add(new DocumentId(sourceName, document.getId()));
        ids.add(document.getId());
      }
    }

    // first mark all start groups
    startGroups.forEach(group -> db.startGroup(group));

    // determine current state of saved documents
    final List<BoxDocument> oldState = getState(db, ids);

    // find all dependent documents (key = document id, value = dependent documents)
    Map<String, Set<DocumentId>> dependents = new HashMap<>();

    for (String dependentSourceName : registry.getDependents(sourceName)) {
      Map<DocumentId, Set<String>> sourceDependents =
          registry.getSource(dependentSourceName).getDb().findDependents(asDependencies);

      for (DocumentId documentId : sourceDependents.keySet()) {
        for (String id : sourceDependents.get(documentId)) {
          dependents
              .computeIfAbsent(documentId.getId(), k -> new HashSet<>())
              .add(new DocumentId(dependentSourceName, id));
        }
      }
    }

    List<BoxDocument> documentsToSave = new ArrayList<>();

    for (BoxDocument document : actualDocuments) {
      // make copy because we will modify it
      document = new BoxDocument(document);
      String id = document.getId();

      // log warning if not valid dependency source
      document.getDependencies().stream()
          .map(d -> d.getSourceName())
          .filter(s -> !registry.isSource(s))
          .distinct()
          .forEach(
              s -> log.warn(s + " not a valid source as dependency of " + sourceName + "." + id));

      // set facets in document according to source's facet fields
      document.addFacetsByQuery(source.getFacetFields());

      // take note of which sources this source is dependent on
      document
          .getDependencies()
          .forEach(d -> registry.addDependency(sourceName, d.getSourceName()));

      // add documents to be saved if this is a saving source and meets dependencyOnly criteria
      if (source.isSave() && (!source.isDependencyOnly() || dependents.containsKey(id))) {
        documentsToSave.add(document);
      }
    }

    db.save(documentsToSave);

    // determine current state of saved documents
    List<BoxDocument> newState = getState(db, ids);
    List<StateChange> results = getStateChanges(actualDocuments, oldState, newState);

    // notify listeners that a new document is ready if any document was modified or if save is
    // false (we can't tell if something was modified if not saved so error on over-processing)
    if (results.stream().anyMatch(r -> r.isModified()) || !source.isSave()) {
      updatesNotifier.trigger(sourceName);
    }

    // trigger dependent documents to be processed
    Map<String, Set<String>> dependentsToQueue = new HashMap<>();

    for (StateChange result : results) {
      if ((result.isModified() || !source.isSave()) && dependents.containsKey(result.getId())) {
        for (DocumentId dependent : dependents.get(result.getId())) {
          dependentsToQueue
              .computeIfAbsent(dependent.getSourceName(), k -> new HashSet<>())
              .add(dependent.getId());
        }
      }
    }

    dependentsToQueue
        .entrySet()
        .forEach(e -> addToQueue(e.getKey(), e.getValue(), Instant.now(), true));

    // delete from queue if document processed and no new dependencies
    Set<String> deleteFromQueue =
        results.stream()
            .filter(r -> r.isProcessed())
            .filter(r -> !r.dependenciesModified || !source.isSave())
            .map(r -> r.getId())
            .collect(Collectors.toSet());
    db.deleteFromQueue(deleteFromQueue);

    // add back to queue immediately if dependencies modified
    Set<String> addToQueueNow =
        results.stream()
            .filter(r -> r.isDependenciesModified())
            .filter(r -> !r.isError())
            .map(r -> r.getId())
            .filter(r -> !deleteFromQueue.contains(r))
            .collect(Collectors.toSet());
    db.addToQueue(createQueueEntries(addToQueueNow, Instant.now(), true));

    // add everything else to queue
    Set<String> addToQueue = new HashSet<>(ids);
    addToQueue.removeAll(deleteFromQueue);
    addToQueue.removeAll(addToQueueNow);
    db.addToQueue(createQueueEntries(addToQueue, Instant.now(), false));

    // process orphans
    for (String groupId : endGroups) {
      db.processOrphans(
          groupId,
          d -> save(source.getName(), List.of(new BoxDocument(d.getId(), Status.DELETED))));
    }
  }

  /**
   * Harvest the source.
   *
   * @param sourceName the source name
   * @throws Exception if something goes wrong with harvesting
   */
  boolean harvest(String sourceName) throws Exception {
    return harvest(sourceName, false);
  }

  /**
   * Harvest the source.
   *
   * @param sourceName the source name
   * @param secondary whether or not to use the secondary cursor
   * @throws Exception if something goes wrong with harvesting
   */
  boolean harvest(String sourceName, boolean secondary) throws Exception {

    // skip if not primary
    if (!registry.isPrimary(sourceName)) {
      return false;
    }

    SourceConfig source = registry.getSource(sourceName);

    ObjectNode cursor = getCursor(sourceName, secondary);

    log.info("harvesting " + source.getName() + " with cursor " + cursor);

    // IMPORTANT: the cursor is allowed to be updated by the harvest processor so we send a copy
    HarvestContext context =
        new HarvestContext(
            registry.getBox(), registry.getBox().getSource(sourceName), cursor.deepCopy());
    HarvestResult harvestResult = source.getHarvester().harvest(context);

    if (harvestResult.getCursor() == null) {
      harvestResult.setCursor(cursor);
    }

    if (source.isSaveDeleted()) {
      save(sourceName, harvestResult);
    } else {
      // determine current state of saved documents
      List<BoxDocument> oldState = getStateByDocs(source.getDb(), harvestResult);
      List<BoxDocument> documentsToSave = new ArrayList<>();

      for (int i = 0; i < harvestResult.size(); i++) {
        if (!(oldState.get(i).isUnprocessed() && harvestResult.get(i).isDeleted())) {
          documentsToSave.add(harvestResult.get(i));
        }
      }

      save(sourceName, documentsToSave);
    }

    // if cursor not updated manually during the harvest
    if (cursor.equals(getCursor(sourceName, secondary))) {
      ObjectNode newCursor = harvestResult.getCursor();

      newCursor =
          newCursor == null || !harvestResult.hasMore() && secondary
              ? JsonNodeFactory.instance.objectNode()
              : newCursor;

      setCursor(sourceName, secondary, newCursor);

      return harvestResult.hasMore();

    } else {
      return false;
    }
  }

  private ObjectNode getCursor(String sourceName, boolean secondary) {
    SourceConfig source = registry.getSource(sourceName);
    BoxDatabase cursorDb = source.getPreferredCursorDb();
    ObjectNode cursor =
        secondary
            ? (ObjectNode) cursorDb.findRegistryValue("secondaryCursor")
            : cursorDb.getHarvestCursor();

    cursor = cursor == null ? JsonNodeFactory.instance.objectNode() : cursor;
    return cursor;
  }

  private void setCursor(String sourceName, boolean secondary, ObjectNode cursor) {
    SourceConfig source = registry.getSource(sourceName);
    BoxDatabase cursorDb = source.getPreferredCursorDb();

    if (secondary) {
      cursorDb.saveRegistryValue("secondaryCursor", cursor);
    } else {
      cursorDb.setHarvestCursor(cursor);
    }
  }

  /**
   * Creates queue entries given ids.
   *
   * @param ids the ids to add
   * @param attempt the attempt
   * @param overwrite whether or not to overwrite
   * @return list of queue entries
   */
  private List<QueueEntry> createQueueEntries(
      Collection<String> ids, Instant attempt, boolean overwrite) {
    return ids.stream()
        .map(id -> new QueueEntry(id).setAttempt(attempt).setOverwrite(overwrite))
        .collect(Collectors.toList());
  }

  /**
   * Retrieves the current state for the documents denoted by the given ids. The state includes the
   * id, status, cursor, and dependencies.
   *
   * @param db the database
   * @param ids the ids for which to retrieve state
   * @return the state
   */
  private List<BoxDocument> getStateByDocs(BoxDatabase db, List<? extends BoxDocument> documents) {
    return getState(db, documents.stream().map(BoxDocument::getId).collect(Collectors.toList()));
  }

  /**
   * Retrieves the current state for the documents denoted by the given ids. The state includes the
   * id, status, cursor, and dependencies.
   *
   * @param db the database
   * @param ids the ids for which to retrieve state
   * @return the state
   */
  private List<BoxDocument> getState(BoxDatabase db, List<String> ids) {
    // an empty ids list will turn into a harvest query below
    if (ids.isEmpty()) {
      return List.of();
    }

    return db.find(
        new BoxQuery(ids)
            .addStatus(BoxDocument.Status.READY)
            .addStatus(BoxDocument.Status.DELETED)
            .addStatus(BoxDocument.Status.UNPROCESSED)
            .addField(BoxQuery.METADATA_FIELD_ID)
            .addField(BoxQuery.METADATA_FIELD_STATUS)
            .addField(BoxQuery.METADATA_FIELD_CURSOR)
            .addField(BoxQuery.METADATA_FIELD_DEPENDENCIES));
  }

  /**
   * Detects and records the state changes for the new documents vs the old documents. The oldState
   * and newState should contain the same document references and in the same order.
   *
   * @param oldState the old state of the documents
   * @param newState the new state of the documents
   * @return the detected state changes
   */
  private List<StateChange> getStateChanges(
      List<BoxDocument> saveState, List<BoxDocument> oldState, List<BoxDocument> newState) {
    List<StateChange> results = new ArrayList<>();

    for (int i = 0; i < oldState.size(); i++) {
      final BoxDocument saveDocument = saveState.get(i);
      final BoxDocument oldDocument = oldState.get(i);
      final BoxDocument newDocument = newState.get(i);
      StateChange result = new StateChange();
      result.id = saveDocument.getId();
      result.processed = saveDocument.isProcessed();
      result.error = saveDocument.isError();
      result.modified = oldDocument.getCursor().orElse(0L) < newDocument.getCursor().orElse(0L);
      result.dependenciesModified = oldDocument.hasDifferentDependencies(newDocument);
      results.add(result);
    }

    return results;
  }

  /**
   * Records what changes took place for a given document.
   *
   * @author Charles Draper
   */
  @Data
  private static class StateChange {
    /** The document id. * */
    private String id;

    /** Whether or not the document is now in a processed state. * */
    private boolean processed;

    /** Whether or not the document is now in an error state. * */
    private boolean error;

    /** Whether or not the document was modified (only checking non-volatile fields). * */
    private boolean modified;

    /** Whether or not the dependencies changed. * */
    private boolean dependenciesModified;
  }
}
