package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.internal.core.DocumentHandler;
import edu.byu.hbll.box.internal.util.BoxUtils;
import edu.byu.hbll.box.internal.util.CursorUtils;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import lombok.Data;

/** A box database that holds everything in memory. */
public class MemoryDatabase implements BoxDatabase {

  Map<String, QueueElement> queueById = new ConcurrentHashMap<>();

  Map<Float, SortedSet<QueueElement>> queue = Collections.synchronizedMap(new TreeMap<>());

  Map<String, BoxDocument> documentMap = new ConcurrentHashMap<>();

  NavigableMap<CursorEntry, CursorEntry> cursorIndex = new TreeMap<>();

  ObjectNode cursorMap = JsonNodeFactory.instance.objectNode();

  Map<DocumentId, Set<String>> dependentMap = new ConcurrentHashMap<>();

  Map<String, ObjectNode> historyMap = new ConcurrentHashMap<>();

  Map<String, JsonNode> registry = new ConcurrentHashMap<>();

  Map<String, Instant> groups = new ConcurrentHashMap<>();

  private Duration retryDelay = Duration.ofMinutes(1);
  private double retryJitter = 0.5;

  @Override
  public void postInit(InitConfig config) {
    this.retryDelay = config.getSource().getConfig().getRetryDelay();
    this.retryJitter = config.getSource().getConfig().getRetryJitter();
  }

  @Override
  public int addToQueue(Duration olderThan) {
    Instant old = Instant.now().minus(olderThan);
    int count = 0;

    for (BoxDocument document : documentMap.values()) {
      if (document.getProcessed().get().isBefore(old) && document.isReady()) {
        addToQueue(
            List.of(
                new QueueEntry(document.getId())
                    .setPriority(DocumentHandler.QUEUE_PRIORITY_MAINTENANCE)));
        count++;
      }
    }

    return count;
  }

  @Override
  public void addToQueue(Collection<? extends QueueEntry> entries) {
    for (QueueEntry entry : entries) {
      synchronized (queue) {
        QueueElement oldElement = queueById.get(entry.getId());

        if (entry.isOverwrite() || oldElement == null) {
          if (oldElement != null) {
            queue.get(oldElement.getPriority()).remove(oldElement);
          }

          SortedSet<QueueElement> bucket = queue.get(entry.getPriority());

          if (bucket == null) {
            bucket =
                Collections.synchronizedSortedSet(
                    new TreeSet<>(
                        Comparator.comparing(QueueElement::getPriority)
                            .thenComparing(QueueElement::getAttempt)
                            .thenComparing(QueueElement::getId)));
            queue.put(entry.getPriority(), bucket);
          }

          QueueElement newElement =
              new QueueElement(entry.getId(), entry.getPriority(), entry.getAttempt());
          bucket.add(newElement);
          queueById.put(entry.getId(), newElement);
        }
      }
    }
  }

  @Override
  public void deleteFromQueue(Collection<String> ids) {
    for (String id : ids) {
      synchronized (queue) {
        QueueElement element = queueById.get(id);

        if (element != null) {
          queue.get(element.priority).remove(element);
          queueById.remove(id);
        }
      }
    }
  }

  /**
   * Returns the size of the queue for testing purposes.
   *
   * @return the queue size
   */
  int getQueueSize() {
    return (int) queue.values().stream().flatMap(s -> s.stream()).count();
  }

  @Override
  public Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {
    Map<String, Set<DocumentId>> dependencyMap = new HashMap<>();

    for (String id : ids) {
      Set<DocumentId> dependencies = new HashSet<>();
      dependencyMap.put(id, dependencies);

      BoxDocument document = documentMap.get(id);

      if (document != null) {
        dependencies.addAll(document.getDependencies());
      }
    }

    return dependencyMap;
  }

  @Override
  public Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
    Map<DocumentId, Set<String>> dependentMap = new HashMap<>();

    for (DocumentId dependency : dependencies) {
      Set<String> dependents = this.dependentMap.get(dependency);
      dependents = dependents == null ? new HashSet<>() : Set.copyOf(dependents);
      dependentMap.put(dependency, dependents);
    }

    return dependentMap;
  }

  @Override
  public QueryResult find(BoxQuery query) {
    query = new BoxQuery(query);

    // set defaults if not set
    if (query.getStatuses().isEmpty()) {
      query.setStatuses(BoxQuery.DEFAULT_STATUSES);
    }

    List<BoxDocument> documents = new ArrayList<>();

    if (query.isIdQuery()) {
      for (String id : query.getIds()) {
        documents.add(documentMap.getOrDefault(id, new BoxDocument(id).setAsUnprocessed()));
      }
    } else {
      CursorEntry entryCursor = new CursorEntry(null, query.getCursorOrDefault());

      long offset = query.getOffset().orElse(0L);
      long limit = query.getLimitOrDefault();

      synchronized (cursorIndex) {
        Set<CursorEntry> sortedEntries =
            query.isAscendingOrder()
                ? cursorIndex.tailMap(entryCursor).keySet()
                : ((NavigableMap<CursorEntry, CursorEntry>) cursorIndex.headMap(entryCursor))
                    .descendingKeySet();

        for (CursorEntry entry : sortedEntries) {
          BoxDocument document = documentMap.get(entry.id);

          if (query.getStatuses().contains(document.getStatus())
              && matchesFacets(document, query)) {
            if (offset == 0) {
              documents.add(document);
            } else {
              offset--;
            }
          }

          if (documents.size() >= limit && limit != BoxQuery.UNLIMITED) {
            break;
          }
        }
      }
    }

    QueryResult result = new QueryResult();

    for (BoxDocument document : documents) {
      ObjectNode json = document.toJson(query.getFields());
      json.with("@box")
          .put("id", document.getId())
          .put("cursor", document.getCursor().orElse(null))
          .put("status", document.getStatus().toString());
      result.add(BoxDocument.parse(json));
    }

    result.updateNextCursor(query);

    return result;
  }

  @Override
  public ObjectNode getHarvestCursor() {
    return cursorMap;
  }

  @Override
  public Set<String> listSourceDependencies() {
    return Set.of();
  }

  @Override
  public List<String> nextFromQueue(int limit) {
    List<String> ids = new ArrayList<>();

    synchronized (queue) {
      if (queue.isEmpty()) {
        return ids;
      }

      Iterator<SortedSet<QueueElement>> it = queue.values().iterator();
      SortedSet<QueueElement> bucket = it.next();

      while (ids.size() < limit && !bucket.isEmpty()) {
        QueueElement entry = bucket.first();

        if (!entry.attempt.isAfter(Instant.now())) {
          ids.add(entry.id);
          Instant nextAttempt = BoxUtils.retryDateTime(entry.requested, retryDelay, retryJitter);
          addToQueue(List.of(new QueueEntry(entry.id, nextAttempt, entry.priority, true)));
        } else if (it.hasNext()) {
          bucket = it.next();
        } else {
          break;
        }
      }
    }

    return ids;
  }

  @Override
  public void processOrphans(String groupId, Consumer<BoxDocument> function) {
    Instant start = groups.remove(groupId);

    if (start != null) {
      for (BoxDocument doc : new ArrayList<>(documentMap.values())) {
        if (doc.getGroupId().isPresent()
            && doc.getGroupId().get().equals(groupId)
            && doc.getProcessed().isPresent()
            && doc.getProcessed().get().isBefore(start)) {
          function.accept(new BoxDocument(doc));
        }
      }
    }
  }

  @Override
  public void removeDeleted(Duration olderThan) {
    Instant old = Instant.now().minus(olderThan);

    for (BoxDocument document : new ArrayList<>(documentMap.values())) {
      if (document.getModified().get().isBefore(old)) {
        documentMap.remove(document.getId());
      }
    }
  }

  @Override
  public void save(Collection<? extends BoxDocument> documents) {
    for (BoxDocument document : documents) {
      String id = document.getId();

      synchronized (cursorIndex) {
        BoxDocument oldDocument = documentMap.getOrDefault(id, new BoxDocument(id));
        BoxDocument newDocument = new BoxDocument(document);

        if (Arrays.equals(oldDocument.hash(), newDocument.hash())) {
          oldDocument.setProcessed(Instant.now());
        } else if (newDocument.isProcessed() || oldDocument.isUnprocessed()) {
          // explicitly set status so that it will stick
          newDocument.setStatus(newDocument.getStatus());
          newDocument.setModified(Instant.now());
          newDocument.setProcessed(Instant.now());

          CursorEntry entry = new CursorEntry(newDocument.getId());
          newDocument.setCursor(entry.cursor);

          // remove old entry
          cursorIndex.remove(new CursorEntry(id, oldDocument.getCursor().orElse(0L)));

          // add new entries
          cursorIndex.put(entry, entry);
          documentMap.put(newDocument.getId(), newDocument);
        }
      }

      document
          .getDependencies()
          .forEach(d -> dependentMap.computeIfAbsent(d, k -> new HashSet<>()).add(id));
    }
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    cursorMap = cursor.deepCopy();
  }

  @Override
  public void startGroup(String groupId) {
    groups.computeIfAbsent(groupId, k -> Instant.now());
  }

  @Override
  public void updateProcessed(Collection<String> ids) {
    for (String id : ids) {
      if (documentMap.containsKey(id)) {
        BoxDocument document = documentMap.get(id);

        if (document != null) {
          document.setProcessed(Instant.now());
        }
      }
    }
  }

  @Data
  private static class QueueElement {
    private String id;
    private float priority;
    private Instant attempt;
    private int attempts;
    private Instant requested = Instant.now();

    public QueueElement(String id, float priority, Instant attempt) {
      this.id = id;
      this.priority = priority;
      this.attempt = attempt;
    }
  }

  private static class CursorEntry implements Comparable<CursorEntry> {

    String id;
    long cursor = CursorUtils.nextCursor();

    CursorEntry(String id) {
      this.id = id;
    }

    CursorEntry(String id, long cursor) {
      this.id = id;
      this.cursor = cursor;
    }

    @Override
    public int compareTo(CursorEntry o) {
      if (equals(o)) {
        return 0;
      } else {
        int comparison = Long.compare(cursor, o.cursor);

        if (comparison != 0) {
          return comparison;
        } else {
          return id.compareTo(o.id);
        }
      }
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      CursorEntry other = (CursorEntry) obj;
      if (id == null) {
        if (other.id != null) {
          return false;
        }
      } else if (!id.equals(other.id)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      return id.hashCode();
    }
  }

  @Override
  public JsonNode findRegistryValue(String id) {
    JsonNode value = registry.get(id);

    if (value != null) {
      return value.deepCopy();
    }

    return null;
  }

  @Override
  public void saveRegistryValue(String id, JsonNode data) {
    registry.put(id, data != null ? data.deepCopy() : null);
  }

  @Override
  public void updateDependencies(Collection<? extends BoxDocument> documents) {
    for (BoxDocument doc : documents) {
      BoxDocument dbDoc = documentMap.get(doc.getId());

      synchronized (dbDoc) {
        dbDoc.setDependencies(doc.getDependencies());
      }

      doc.getDependencies()
          .forEach(d -> dependentMap.computeIfAbsent(d, k -> new HashSet<>()).add(doc.getId()));
    }
  }

  @Override
  public long count(BoxQuery query) {
    if (query.isIdQuery()) {
      return query.getIds().size();
    }

    return find(new BoxQuery(query).setUnlimited().clearFields()).size();
  }

  /**
   * Tests whether or not the document matches the facets in the query.
   *
   * @param doc the document to test against
   * @param query the query
   * @return whether or not the document matches the facets in the query
   */
  private boolean matchesFacets(BoxDocument doc, BoxQuery query) {
    Map<String, Set<Facet>> facetMap = new HashMap<>();

    for (Facet facet : query.getFacets()) {
      facetMap.computeIfAbsent(facet.getName(), n -> new HashSet<>()).add(facet);
    }

    for (String name : facetMap.keySet()) {
      if (Collections.disjoint(doc.getFacets(), facetMap.get(name))) {
        return false;
      }
    }

    return true;
  }

  @Override
  public void clear() {
    queue.clear();
    documentMap.clear();
    cursorIndex.clear();
    cursorMap = JsonNodeFactory.instance.objectNode();
    dependentMap.clear();
    historyMap.clear();
  }
}
