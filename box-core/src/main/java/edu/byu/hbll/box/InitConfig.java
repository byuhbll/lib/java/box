package edu.byu.hbll.box;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Configuration for initializing newly instantiated Box extension classes.
 *
 * @author Charles Draper
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InitConfig {

  /** The instance of Box. */
  private Box box;

  /** The source. */
  private Source source;

  /** The type of object. */
  private ObjectType objectType;

  /**
   * Creates a new {@link InitConfig} initialized with the given box and source.
   *
   * @param box the box object
   * @param source the source
   */
  public InitConfig(Box box, Source source) {
    this(box, source, null);
  }

  /**
   * Returns the source name.
   *
   * @return the sourceName
   */
  public String getSourceName() {
    return source.getName();
  }

  /**
   * Returns whether or not this is of type processor.
   *
   * @return whether or not this is of type processor
   */
  public boolean isProcessor() {
    return objectType == ObjectType.PROCESSOR;
  }

  /**
   * Returns whether or not this is of type harvester.
   *
   * @return whether or not this is of type harvester
   */
  public boolean isHarvester() {
    return objectType == ObjectType.HARVESTER;
  }

  /**
   * Returns whether or not this is of type database.
   *
   * @return whether or not this is of type database
   */
  public boolean isBoxDatabase() {
    return objectType == ObjectType.BOX_DATABASE;
  }

  /**
   * Returns whether or not this is of type cursor database.
   *
   * @return whether or not this is of type cursor database
   */
  public boolean isCursorDatabase() {
    return objectType == ObjectType.CURSOR_DATABASE;
  }

  /**
   * Returns if type is other.
   *
   * @return if type is other
   */
  public boolean isOther() {
    return objectType == ObjectType.OTHER;
  }
}
