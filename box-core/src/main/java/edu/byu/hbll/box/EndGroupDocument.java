package edu.byu.hbll.box;

/**
 * Special {@link BoxDocument} that signals the end of a group for identifying orphans. The id of
 * the document is the group name.
 *
 * @author Charles Draper
 */
public class EndGroupDocument extends BoxDocument {

  /**
   * Creates a new {@link EndGroupDocument}.
   *
   * @param groupId name or id of the group
   */
  public EndGroupDocument(String groupId) {
    super(groupId);
  }
}
