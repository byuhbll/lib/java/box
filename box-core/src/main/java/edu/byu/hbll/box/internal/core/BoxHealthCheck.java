package edu.byu.hbll.box.internal.core;

import edu.byu.hbll.box.BoxHealth;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.SourceConfig;
import edu.byu.hbll.box.SourceHealth;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Checks the health of all sources by attempting to query the source. If the query throws an error,
 * then it is considered not healthy.
 *
 * @author Charles Draper
 */
public class BoxHealthCheck implements Runnable {

  private Registry registry;
  private BoxHealth boxHealth = new BoxHealth(false, Collections.unmodifiableMap(new HashMap<>()));

  /**
   * Creates a new {@link BoxHealthCheck}.
   *
   * @param registry the registry
   */
  public BoxHealthCheck(Registry registry) {
    this.registry = registry;
  }

  @Override
  public void run() {
    boolean healthy = true;
    Map<String, SourceHealth> sourceHealthMap = new HashMap<>();

    for (SourceConfig source : registry.getSources()) {
      try {
        registry.getDocumentHandler().find(source.getName(), new BoxQuery().setLimit(1));
        sourceHealthMap.put(source.getName(), new SourceHealth(source.getName(), true, null));
      } catch (Exception e) {
        sourceHealthMap.put(
            source.getName(), new SourceHealth(source.getName(), false, e.getMessage()));
        healthy = false;
      }
    }

    sourceHealthMap = Collections.unmodifiableMap(sourceHealthMap);

    this.boxHealth = new BoxHealth(healthy, sourceHealthMap);
  }

  /**
   * Returns the health of box.
   *
   * @return the health of box
   */
  public BoxHealth getHealth() {
    return boxHealth;
  }
}
