package edu.byu.hbll.box.internal.core;

import edu.byu.hbll.box.ObjectFactory;
import java.lang.reflect.Constructor;

/**
 * This object factory instantiates a new object of the given class. The given class must have a no
 * args constructor.
 */
public class DefaultObjectFactory implements ObjectFactory {

  @Override
  public Object create(String type) {
    try {
      Class<?> c = Class.forName(type);
      Constructor<?> constructor = c.getDeclaredConstructor();
      constructor.setAccessible(true);
      return constructor.newInstance();
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
