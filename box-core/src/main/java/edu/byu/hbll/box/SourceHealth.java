package edu.byu.hbll.box;

/**
 * A snapshot of the health of a particular source.
 *
 * @author Charles Draper
 */
public class SourceHealth {

  private String sourceName;
  private boolean healthy;
  private String message;

  /**
   * Creates a new {@link SourceHealth}.
   *
   * @param sourceName the source name
   * @param healthy whether the source is healthy or not
   * @param message a status message (generally null if healthy)
   */
  public SourceHealth(String sourceName, boolean healthy, String message) {
    this.sourceName = sourceName;
    this.healthy = healthy;
    this.message = message;
  }

  /**
   * Returns the source name.
   *
   * @return the source name
   */
  public String getSourceName() {
    return sourceName;
  }

  /**
   * Returns whether the source is healthy or not.
   *
   * @return whether the source is healthy or not
   */
  public boolean isHealthy() {
    return healthy;
  }

  /**
   * Returns a status message (generally null if healthy).
   *
   * @return a status message (generally null if healthy)
   */
  public String getMessage() {
    return message;
  }
}
