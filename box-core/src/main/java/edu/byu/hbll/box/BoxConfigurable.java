package edu.byu.hbll.box;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.internal.util.JsonUtils;

/**
 * Any extension classes (ie, classes that implement {@link Processor}, {@link Harvester}, {@link
 * BoxDatabase}, etc) must implement this. This is very similar to the PostConstruct and PreDestroy
 * annotations in Java EE managed classes. When Box instantiates an extension class, it calls
 * postConstruct and passes it configuration information. By default this method binds the values in
 * params to the fields in the newly constructed object. The binding is handled by Jackson.
 *
 * <p>When the application is undeployed, preDestroy() is called so the client can perform any
 * cleanup necessary. By default this method does nothing.
 *
 * <p>Note: postConstruct and preDestroy are only called if Box constructs the instance.
 *
 * @author Charles Draper
 */
public interface BoxConfigurable {

  /**
   * Creates a {@link BoxConfigurable} given the configuration.
   *
   * @param sourceName the source name
   * @param objectFactory the object factory to use
   * @param objectType the object type
   * @param config the box object configuration
   * @return the newly created box configurable
   */
  public static BoxConfigurable create(
      String sourceName, ObjectFactory objectFactory, ObjectType objectType, JsonNode config) {
    if (!config.has("type")) {
      throw new IllegalArgumentException(
          "missing 'type' attribute for " + sourceName + " " + objectType);
    }

    BoxConfigurable configurable =
        (BoxConfigurable) objectFactory.create(config.path("type").asText());

    ObjectNode params =
        config.has("params")
            ? (ObjectNode) config.path("params")
            : JsonNodeFactory.instance.objectNode();

    ConstructConfig constructConfig = new ConstructConfig(sourceName, params);
    constructConfig.setObjectType(objectType);
    configurable.postConstruct(constructConfig);

    return configurable;
  }

  /**
   * Called when Box constructs a new instance of the class.
   *
   * @param config configuration for this client.
   */
  default void postConstruct(ConstructConfig config) {
    JsonUtils.bind(config.getParams(), this);
  }

  /**
   * Called after Box is completely initialized.
   *
   * @param config configuration for this client.
   */
  default void postInit(InitConfig config) {}

  /**
   * Called when the application is undeployed to allow the client to cleanup resources. Default is
   * to do nothing.
   */
  default void preDestroy() {}
}
