package edu.byu.hbll.box.internal.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Comparator;

/**
 * Compares {@link Number}s numerically. For example, an {@link Integer} and a {@link BigDecimal}
 * with the same numeric value will be considered equal even though they are of two different number
 * types.
 *
 * <p>Derived from
 * https://stackoverflow.com/questions/2683202/comparing-the-values-of-two-generic-numbers/2683438.
 *
 * @author Charles Draper
 */
public class NumberComparator implements Comparator<Number> {

  private static final BigDecimal POSITIVE_INFINITY = new BigDecimal(1);
  private static final BigDecimal NEGATIVE_INFINITY = new BigDecimal(-1);
  private static final BigDecimal NAN = new BigDecimal(0);

  @Override
  public int compare(Number x, Number y) {
    BigDecimal bx = toBigDecimal(x);
    BigDecimal by = toBigDecimal(y);

    if (bx == by) {
      return 0;
    } else if (bx == NAN) {
      return 1;
    } else if (by == NAN) {
      return -1;
    } else if (bx == POSITIVE_INFINITY || by == NEGATIVE_INFINITY) {
      return 1;
    } else if (bx == NEGATIVE_INFINITY || by == POSITIVE_INFINITY) {
      return -1;
    } else {
      return toBigDecimal(x).compareTo(toBigDecimal(y));
    }
  }

  /**
   * Converts the given number to a {@link BigDecimal}. Special floating point numbers such as
   * infinity and nan get converted into a special instance of a {@link BigDecimal}.
   *
   * @param x the number to convert
   * @return the conversion to {@link BigDecimal}
   */
  private static BigDecimal toBigDecimal(Number x) {
    if (x instanceof BigDecimal) {
      return (BigDecimal) x;
    } else if (x instanceof BigInteger) {
      return new BigDecimal((BigInteger) x);
    } else if (x instanceof Byte
        || x instanceof Short
        || x instanceof Integer
        || x instanceof Long) {
      return new BigDecimal(x.longValue());
    } else if (x instanceof Double) {
      if (((Double) x).isNaN()) {
        return NAN;
      } else if (((Double) x).isInfinite()) {
        return ((double) x) < 0 ? NEGATIVE_INFINITY : POSITIVE_INFINITY;
      } else {
        return BigDecimal.valueOf(x.doubleValue());
      }
    } else if (x instanceof Float) {
      if (((Float) x).isNaN()) {
        return NAN;
      } else if (((Float) x).isInfinite()) {
        return ((float) x) < 0 ? NEGATIVE_INFINITY : POSITIVE_INFINITY;
      } else {
        return new BigDecimal(x.toString());
      }
    }

    try {
      return new BigDecimal(x.toString());
    } catch (final NumberFormatException e) {
      throw new RuntimeException(
          "The given number (\""
              + x
              + "\" of class "
              + x.getClass().getName()
              + ") does not have a parsable string representation",
          e);
    }
  }
}
