package edu.byu.hbll.box.internal.core;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.SourceConfig;
import edu.byu.hbll.scheduler.CronSchedule;
import edu.byu.hbll.scheduler.PeriodicSchedule;
import edu.byu.hbll.scheduler.Schedule;
import edu.byu.hbll.scheduler.ScheduledTask;
import edu.byu.hbll.scheduler.Scheduler;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class holding the keys to box and tying it all together. This is a remnant of the Java EE
 * days an much that is stored here could be eventually moved into {@link Source}.
 */
public class Registry {

  static final Logger logger = LoggerFactory.getLogger(Registry.class);

  /** Alias for the principal source. */
  public static final String PRINCIPAL_SOURCE_ALIAS = "box";

  private volatile boolean configured = false;
  @Getter private Box box;
  private SourceConfig principalSource;
  private Map<String, SourceConfig> sources = new LinkedHashMap<>();
  private DocumentHandler documentHandler;
  private ThreadFactory threadFactory;
  private Map<String, QueueRunner> queues = new HashMap<>();
  private List<Object> closeables = new ArrayList<>();
  private Map<String, DocumentProcessor> documentProcessors = new HashMap<>();
  private Map<String, PrimaryMonitor> primaryMonitors = new HashMap<>();
  private UpdatesNotifier updatesNotifier;
  private Map<String, Set<String>> dependents = new ConcurrentHashMap<>();
  private Scheduler scheduler;
  private BoxHealthCheck boxHealthCheck;
  private AtomicBoolean closed = new AtomicBoolean();
  private List<Schedulable> schedulables = new ArrayList<>();

  private Map<String, ScheduledTask> scheduledTasks = new LinkedHashMap<>();

  /**
   * Creates and initializes the registry given the source configs and thread factory.
   *
   * @param box the singleton Box object
   * @param sources the source configs to register
   * @param threadFactory the thread factory to use
   */
  public Registry(Box box, List<SourceConfig> sources, ThreadFactory threadFactory) {

    this.box = box;
    this.threadFactory = threadFactory;

    // set up sources
    for (SourceConfig source : sources) {

      if (!source.isEnabled()) {
        continue;
      }

      if (source.isPrincipal() && principalSource == null) {
        principalSource = source;
      }

      this.sources.put(source.getName(), source);

      OnOffSemaphore onOffSemaphore = new OnOffSemaphore();

      if (source.getOn() != null && source.getOff() != null) {
        schedule(source.getName(), "on", source.getOn(), () -> onOffSemaphore.setOn(true));
        schedule(source.getName(), "off", source.getOff(), () -> onOffSemaphore.setOn(false));

        Instant nextOn = Instant.MAX;
        Instant nextOff = Instant.MIN;

        if (source.getOn() instanceof CronSchedule) {
          nextOn = ((CronSchedule) source.getOn()).getExpression().prev();
        }

        if (source.getOff() instanceof CronSchedule) {
          nextOff = ((CronSchedule) source.getOff()).getExpression().prev();
        }

        onOffSemaphore.init(nextOn, nextOff);
      }

      QuotaSemaphore quotaSemaphore = new QuotaSemaphore(source.getQuota());
      schedule(source.getName(), "quotaReset", source.getQuotaReset(), () -> quotaSemaphore.run());
      QueueRunner queueRunner = new QueueRunner(this, source, onOffSemaphore);
      queues.put(source.getName(), queueRunner);
      closeables.add(queueRunner);

      if (source.isProcessEnabled()) {
        DocumentProcessor documentProcessor = new DocumentProcessor(this, source, quotaSemaphore);
        closeables.add(documentProcessor);
        documentProcessors.put(source.getName(), documentProcessor);
      }

      // set up empty dependents per source
      dependents.computeIfAbsent(
          source.getName(), k -> Collections.newSetFromMap(new ConcurrentHashMap<>()));

      closeables.add(source.getProcessor());
      closeables.add(source.getHarvester());
      closeables.add(source.getDb());
      closeables.add(source.getOthers());
    }

    if (principalSource == null && !sources.isEmpty()) {
      principalSource = sources.get(0);
    }

    updatesNotifier = new UpdatesNotifier(threadFactory);
    schedule("all", "updatesNotifier", PeriodicSchedule.ofSeconds(1), updatesNotifier);
    closeables.add(updatesNotifier);

    // set up document handler
    documentHandler = new DocumentHandler(this);

    // init source schedules
    for (SourceConfig source : sources) {
      if (source.getHarvester() != null && source.isHarvestEnabled()) {

        schedule(
            source.getName(),
            "harvest",
            source.getHarvestSchedule(),
            () -> {
              try {
                while (!closed.get() && documentHandler.harvest(source.getName())) {}
                ;
              } catch (Exception e) {
                logger.error(e.toString(), e);
              }
            });
      }

      if (source.getReprocessAge() != null) {
        schedule(
            source.getName(),
            "reprocessAge",
            source.getReprocessSchedule(),
            () -> {
              if (isPrimary(source.getName())) {
                source.getDb().addToQueue(source.getReprocessAge(), true);
              }
            });
      }

      if (source.getRemoveAge() != null) {
        schedule(
            source.getName(),
            "removeAge",
            source.getRemoveSchedule(),
            () -> {
              if (isPrimary(source.getName())) {
                source.getDb().removeDeleted(source.getRemoveAge());
              }
            });
      }

      if (source.getHarvestResetSchedule() != null) {
        schedule(
            source.getName(),
            "harvestReset",
            source.getHarvestResetSchedule(),
            () -> {
              if (isPrimary(source.getName())) {
                source
                    .getPreferredCursorDb()
                    .setHarvestCursor(JsonNodeFactory.instance.objectNode());
              }
            });
      }

      if (source.getHarvester() != null
          && source.isHarvestEnabled()
          && source.getReharvestSchedule() != null) {

        schedule(
            source.getName(),
            "harvest",
            source.getReharvestSchedule(),
            () -> {
              try {
                while (!closed.get() && documentHandler.harvest(source.getName(), true)) {}
                ;
              } catch (Exception e) {
                logger.error(e.toString(), e);
              }
            });
      }

      PrimaryMonitor primaryMonitor = new PrimaryMonitor(source.getPreferredCursorDb());
      this.primaryMonitors.put(source.getName(), primaryMonitor);
      closeables.add(primaryMonitor);

      schedule(
          source.getName(),
          "primaryMonitor",
          PeriodicSchedule.ofSeconds(2),
          () -> primaryMonitor.run());

      // init dependents
      for (String dependency : source.getDb().listSourceDependencies()) {
        if (dependents.containsKey(dependency)) {
          dependents.get(dependency).add(source.getName());
        }
      }
    }

    boxHealthCheck = new BoxHealthCheck(this);
    schedule("all", "healthCheck", PeriodicSchedule.ofMinutes(1), boxHealthCheck);
    closeables.add(boxHealthCheck);

    configured = true;
  }

  /** Starts all the schedulables. */
  public void startSchedules() {
    scheduler = new Scheduler(threadFactory);
    closeables.add(scheduler);
    schedulables.forEach(s -> schedule(s));
  }

  /**
   * Adds a dependent of a dependency.
   *
   * @param dependent the dependent source
   * @param dependency the source as a dependency
   */
  public void addDependency(String dependent, String dependency) {
    if (dependents.containsKey(dependency)) {
      dependents.get(dependency).add(dependent);
    }
  }

  /**
   * Returns the box health check.
   *
   * @return the box health check
   */
  public BoxHealthCheck getBoxHealthCheck() {
    return boxHealthCheck;
  }

  /**
   * Returns the dependents of the given dependency.
   *
   * @param sourceName the dependency
   * @return the dependents
   */
  public Set<String> getDependents(String sourceName) {
    return dependents.get(sourceName);
  }

  /**
   * Return the document handler.
   *
   * @return the documentHandler
   */
  public DocumentHandler getDocumentHandler() {
    return documentHandler;
  }

  /**
   * Get the document processor for the given source name.
   *
   * @param sourceName the source name
   * @return the document processor
   */
  public DocumentProcessor getDocumentProcessor(String sourceName) {
    return documentProcessors.get(sourceName);
  }

  /**
   * Returns the principal source config.
   *
   * @return the principal source config
   */
  public SourceConfig getPrincipalSource() {
    return principalSource;
  }

  /**
   * Get queue for the given source.
   *
   * @param name the source name
   * @return the queue for the given source
   */
  public QueueRunner getQueue(String name) {
    return queues.get(name);
  }

  /**
   * Returns the source config for the given source name.
   *
   * @param name the source name
   * @return the source config
   */
  public SourceConfig getSource(String name) {
    return sources.get(verifySource(name));
  }

  /**
   * Return the sources.
   *
   * @return the sources
   */
  public List<SourceConfig> getSources() {
    return new ArrayList<>(sources.values());
  }

  private ScheduledTask getTask(String sourceName, String type) {
    return scheduledTasks.get(sourceName + "\t" + type);
  }

  /**
   * Returns the thread factory.
   *
   * @return the threadFactory
   */
  public ThreadFactory getThreadFactory() {
    return threadFactory;
  }

  /**
   * Returns the updates notifier.
   *
   * @return the updates notifier
   */
  public UpdatesNotifier getUpdatesNotifier() {
    return updatesNotifier;
  }

  /**
   * Returns whether or no the registry is closed.
   *
   * @return if is closed
   */
  public boolean isClosed() {
    return closed.get();
  }

  /**
   * Returns whether or not this registry is configured.
   *
   * @return whether or not this registry is configured
   */
  public boolean isConfigured() {
    return configured;
  }

  /**
   * Whether or not the given source is primary.
   *
   * @param sourceName the source name
   * @return whether it is primary or not
   */
  public boolean isPrimary(String sourceName) {
    try {
      return primaryMonitors.get(verifySource(sourceName)).isPrimary();
    } catch (RuntimeException e) {
      logger.error(sourceName + ": " + e);
      throw e;
    }
  }

  /**
   * Returns true if the given name represents a registered source.
   *
   * @param name source name
   * @return if source exists
   */
  public boolean isSource(String name) {
    return sources.containsKey(name);
  }

  /** Shuts down the registry. */
  public void preDestroy() {

    closed.set(true);

    for (Object closeable : closeables) {
      if (closeable != null) {
        try {
          if (closeable instanceof BoxConfigurable) {
            ((BoxConfigurable) closeable).preDestroy();
          } else if (closeable instanceof AutoCloseable) {
            ((AutoCloseable) closeable).close();
          } else if (closeable instanceof Scheduler) {
            ((Scheduler) closeable).shutdown();
            ((Scheduler) closeable).awaitTermination(Duration.ofDays(1));
          }
        } catch (Exception e) {
          logger.error(e.toString(), e);
        }
      }
    }
  }

  private ScheduledTask schedule(Schedulable schedulable) {
    return scheduledTasks.put(
        schedulable.sourceName + "\t" + schedulable.type,
        scheduler.scheduleTask(schedulable.schedule, schedulable.task));
  }

  private void schedule(String sourceName, String type, Schedule schedule, Runnable task) {
    schedulables.add(new Schedulable(sourceName, type, schedule, task));
  }

  /**
   * Triggers a harvest for the given source.
   *
   * @param sourceName the source name
   */
  public void triggerHarvest(String sourceName) {
    ScheduledTask task = getTask(sourceName, "harvest");

    if (task != null) {
      task.schedule(Instant.now());
    } else {
      throw new UnsupportedOperationException("harvest is disabled for " + sourceName);
    }
  }

  /**
   * Verify this source name and return it if valid.
   *
   * @param sourceName the source name to verify
   * @return the canonical source name
   * @throws IllegalArgumentException if source is not recognized
   */
  public String verifySource(String sourceName) {
    sourceName =
        sourceName == null
                || sourceName.trim().isEmpty()
                || sourceName.equals(PRINCIPAL_SOURCE_ALIAS)
            ? getPrincipalSource().getName()
            : sourceName;

    if (!isSource(sourceName)) {
      throw new IllegalArgumentException("source " + sourceName + " not recognized");
    }

    return sourceName;
  }

  /**
   * A task to be scheduled.
   *
   * @author Charles Draper
   */
  private class Schedulable {

    private String sourceName;
    private String type;
    private Schedule schedule;
    private Runnable task;

    /**
     * Creates a new {@link Schedulable}.
     *
     * @param sourceName the source for the task
     * @param type the type of task
     * @param schedule the schedule for the task
     * @param task the task
     */
    private Schedulable(String sourceName, String type, Schedule schedule, Runnable task) {
      this.sourceName = sourceName;
      this.type = type;
      this.schedule = schedule;
      this.task = task;
    }
  }
}
