DO $$ BEGIN
  -- there is no IF NOT EXISTS for types
  CREATE TYPE box_status AS ENUM ('UNPROCESSED', 'READY', 'DELETED', 'ERROR');
EXCEPTION
  WHEN duplicate_object THEN NULL;
END $$;

CREATE TABLE IF NOT EXISTS documents (
  source TEXT NOT NULL,
  id TEXT NOT NULL,
  status box_status NOT NULL DEFAULT 'UNPROCESSED',
  cursor BIGSERIAL NOT NULL,
  modified TIMESTAMPTZ DEFAULT NOW(),
  processed TIMESTAMPTZ DEFAULT NOW(),
  hash BIGINT NOT NULL DEFAULT 0,
  document JSON NOT NULL DEFAULT '{}'::JSON,
  facets JSONB,
  dependencies JSONB,
  group_id TEXT,
  PRIMARY KEY(source, id)
);

-- findByHarvest (alternative)
CREATE INDEX IF NOT EXISTS documents_source_cursor_idx ON documents (source, cursor);

-- findByHarvest (alternative)
CREATE INDEX IF NOT EXISTS documents_source_status_cursor_idx ON documents (source, status, cursor);

-- findByHarvest (alternative)
CREATE INDEX IF NOT EXISTS documents_facets_idx ON documents USING gin (facets);

-- addToQueue
CREATE INDEX IF NOT EXISTS documents_source_processed_idx ON documents (source, processed);

-- findDependents, listSourceDependencies
CREATE INDEX IF NOT EXISTS documents_dependencies_idx ON documents USING gin (dependencies);

-- processOrphans
CREATE INDEX IF NOT EXISTS documents_source_group_id_processed_idx ON documents (source, group_id, processed);

-- removeDeleted
CREATE INDEX IF NOT EXISTS documents_source_status_modified_idx ON documents (source, status, modified);

CREATE TABLE IF NOT EXISTS queue (
  source TEXT NOT NULL,
  id TEXT NOT NULL,
  priority REAL NOT NULL DEFAULT 1,
  requested TIMESTAMPTZ DEFAULT NOW(),
  attempt TIMESTAMPTZ DEFAULT NOW(),
  attempted TIMESTAMPTZ,
  attempts INTEGER DEFAULT 0,
  PRIMARY KEY(source, id)
);

-- nextFromQueue, addToQueue
CREATE INDEX IF NOT EXISTS queue_priority_attempt_idx ON queue (source, priority, attempt);

CREATE TABLE IF NOT EXISTS registry (
  source TEXT NOT NULL,
  id TEXT NOT NULL,
  data JSONB NOT NULL DEFAULT '{}'::JSONB,
  PRIMARY KEY(source, id)
);

CREATE TABLE IF NOT EXISTS groups (
  source TEXT NOT NULL,
  id TEXT NOT NULL,
  start TIMESTAMPTZ DEFAULT NOW(),
  PRIMARY KEY(source, id)
);
