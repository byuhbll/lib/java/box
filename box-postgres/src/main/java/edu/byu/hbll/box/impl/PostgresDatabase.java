package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.internal.core.DocumentHandler;
import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.misc.Resources;
import edu.byu.hbll.stats.time.Times;
import edu.byu.hbll.stats.time.Times.Marker;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.postgresql.util.PGobject;

/** A box database that persists to Postgres. */
@NoArgsConstructor
@Slf4j
public class PostgresDatabase implements BoxDatabase, AutoCloseable {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private static final int SLOW_QUERY_WARNING_MILLIS = 500;
  private static final double MILLIS_TO_SECONDS_FACTOR = 0.001;

  private static final String SOURCE_FACET_NAME = "@source";
  private static final String STATUS_FACET_NAME = "@status";

  private String sourceName;
  private BasicDataSource ds;
  private QueryRunner db;
  private Duration retryDelay = Duration.ofMinutes(1);
  private double retryJitter = 0.5;

  @Override
  public void postConstruct(ConstructConfig config) {
    this.sourceName = config.getSourceName();
    String uri =
        Objects.requireNonNull(
            config.getParams().path("uri").asText(null),
            "uri must not be null (eg. jdbc:postgresql://localhost/mydb)");
    this.ds = new BasicDataSource();
    this.ds.setUrl(uri);
    this.ds.setInitialSize(1);
    this.db = new QueryRunner(ds);

    try {
      update(Resources.readString("box.sql"));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void postInit(InitConfig config) {
    this.retryDelay = config.getSource().getConfig().getRetryDelay();
    this.retryJitter = config.getSource().getConfig().getRetryJitter();
  }

  @Override
  public void preDestroy() {
    try {
      close();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void close() throws Exception {
    if (this.ds != null) {
      ds.close();
    }
  }

  @Override
  public int addToQueue(Duration olderThan) {
    return addToQueue(olderThan, false);
  }

  @Override
  public int addToQueue(Duration olderThan, boolean useHeuristics) {
    // if there is still an item on the queue ready to be processed, placed there by this
    // maintenance method, skip for now

    return update(
        "INSERT INTO queue (source, id, attempt, priority) "
            + " SELECT source, id, NOW(), ? FROM documents "
            + " WHERE source = ? "
            + " AND processed < NOW() - INTERVAL '"
            + olderThan.toMillis()
            + " MILLISECONDS'"
            + (useHeuristics
                ? " AND NOT EXISTS (SELECT 1 FROM queue WHERE source = ? AND priority = ? AND"
                    + " attempt < NOW())"
                : ""),
        sourceName,
        sourceName,
        DocumentHandler.QUEUE_PRIORITY_MAINTENANCE,
        DocumentHandler.QUEUE_PRIORITY_MAINTENANCE);
  }

  @Override
  public void addToQueue(Collection<? extends QueueEntry> entries) {
    final Marker m = Times.single();
    Map<Boolean, List<QueueEntry>> map =
        entries.stream().collect(Collectors.groupingBy(QueueEntry::isOverwrite));

    for (boolean overwrite : map.keySet()) {
      List<QueueEntry> deduped = dedup(map.get(overwrite), QueueEntry::getId);
      List<Object> params = new ArrayList<>();

      for (QueueEntry entry : deduped) {
        params.add(sourceName);
        params.add(entry.getId());
        params.add(entry.getAttempt().atOffset(ZoneOffset.UTC));
        params.add(entry.getPriority());
      }

      final int columnCount = 4;

      update(
          "INSERT INTO queue (source, id, attempt, priority) "
              + "VALUES "
              + placeholders(deduped.size(), columnCount)
              + " ON CONFLICT (source, id)"
              + (overwrite
                  ? " DO UPDATE SET attempt = EXCLUDED.attempt, priority = EXCLUDED.priority,"
                      + " requested = NOW()"
                  : " DO NOTHING"),
          params);
    }

    m.mark();
    log.debug("addToQueue: %s: %s", sourceName, m);
  }

  @Override
  public void deleteFromQueue(Collection<String> ids) {
    final Marker m = Times.single();

    if (ids.isEmpty()) {
      return;
    }

    update(
        "DELETE FROM queue WHERE source = ? AND id IN ("
            + String.join(", ", Collections.nCopies(ids.size(), "?"))
            + ")",
        sourceName,
        ids);

    m.mark();
    log.debug("deleteFromQueue: %s: %s", sourceName, m);
  }

  @Override
  public Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
    final Marker m = Times.single();

    if (dependencies.isEmpty()) {
      return Map.of();
    }

    List<String> dependencyQueries = new ArrayList<>();

    for (DocumentId dependency : dependencies) {
      ObjectNode dependencyQuery = mapper.createObjectNode();
      dependencyQuery.withArray(dependency.getSourceName()).add(dependency.getId());
      dependencyQueries.add(dependencyQuery.toString());
    }

    String orQuery = repeat("dependencies @> ?::jsonb", dependencyQueries.size(), " OR ");

    List<BoxDocumentData> documentDatas =
        query(
            "SELECT id, dependencies FROM documents WHERE (" + orQuery + ")",
            new BeanListHandler<>(BoxDocumentData.class),
            dependencyQueries);

    Map<DocumentId, Set<String>> dependents = new HashMap<>();

    for (BoxDocumentData data : documentDatas) {
      BoxDocument document = data.toBoxDocument();
      document.getDependencies().stream()
          .forEach(d -> dependents.computeIfAbsent(d, k -> new HashSet<>()).add(document.getId()));
    }

    dependents.keySet().retainAll(dependencies);

    m.mark();
    log.debug("findDependents: %s: %s", sourceName, m);

    return dependents;
  }

  @Override
  public QueryResult find(BoxQuery query) {
    final Marker m = Times.single();
    List<BoxDocumentData> dataDocs = query.isIdQuery() ? findById(query) : findByHarvest(query);
    List<BoxDocument> documents =
        dataDocs.stream().map(d -> d.toBoxDocument()).collect(Collectors.toList());
    documents = projectFields(query, documents);
    QueryResult result = new QueryResult(documents).updateNextCursor(query);
    m.mark();
    log.debug("find: %s: %s", sourceName, m);
    return result;
  }

  /**
   * Projects fields (ie, only keeps fields) per the box query.
   *
   * @param query the box query
   * @param documents the documents for projection
   * @return the projected docments
   */
  private List<BoxDocument> projectFields(BoxQuery query, List<BoxDocument> documents) {
    if (query.getFields().isEmpty()) {
      return documents;
    } else {
      List<BoxDocument> projected = new ArrayList<>();

      for (BoxDocument document : documents) {
        ObjectNode json = document.toJson(query.getFields());
        // add required fields
        json.with("@box")
            .put("id", document.getId())
            .put("cursor", document.getCursor().orElse(null))
            .put("status", document.getStatus().toString());
        projected.add(BoxDocument.parse(json));
      }

      return projected;
    }
  }

  /**
   * Finds documents according to the given ID query.
   *
   * @param query the query to use
   * @return matching documents
   */
  private List<BoxDocumentData> findById(BoxQuery query) {
    if (!query.isIdQuery()) {
      return List.of();
    }

    String placeholders = placeholders(query.getIds().size());

    List<BoxDocumentData> documents =
        query(
            "SELECT *, group_id AS groupId FROM documents WHERE source = ? AND id IN "
                + placeholders,
            new BeanListHandler<>(BoxDocumentData.class),
            sourceName,
            query.getIds());

    Map<String, BoxDocumentData> documentMap =
        documents.stream().collect(Collectors.toMap(d -> d.getId(), d -> d));

    List<BoxDocumentData> orderedDocuments =
        query.getIds().stream()
            .map(id -> documentMap.computeIfAbsent(id, k -> new BoxDocumentData(k)))
            .collect(Collectors.toList());

    return orderedDocuments;
  }

  /**
   * Finds documents according to the given harvest query.
   *
   * @param query the query to use
   * @return matching documents
   */
  private List<BoxDocumentData> findByHarvest(BoxQuery query) {
    if (!query.isHarvestQuery()) {
      return List.of();
    }

    String comparison = query.isAscendingOrder() ? ">=" : "<=";
    String statusPlaceholders =
        String.join(
            ", ", Collections.nCopies(query.getStatusesOrDefault().size(), "?::box_status"));
    QueryAndParams facetQuery = generateFacetQuery(query);

    // note that range queries or order by are not supported by gin indexes, therefore the cursor
    // and facets fields cannot create a composite index, the engine will have to choose between the
    // cursor index or the facets index

    List<BoxDocumentData> documents =
        query(
            "SELECT *, group_id AS groupId FROM documents WHERE source = ?"
                + " AND cursor "
                + comparison
                + " ?"
                + " AND status IN ("
                + statusPlaceholders
                + ") AND "
                + facetQuery.getQuery()
                + " ORDER BY cursor "
                + query.getOrderOrDefault()
                + " LIMIT ?"
                + " OFFSET ?",
            new BeanListHandler<>(BoxDocumentData.class),
            sourceName,
            query.getCursorOrDefault(),
            query.getStatusValuesOrDefault(),
            facetQuery.getParams(),
            query.getLimitOrDefault() < 0 ? null : query.getLimitOrDefault(),
            query.getOffset().orElse(0L));

    return documents;
  }

  /**
   * Generates the facet where clause given the box query.
   *
   * @param query the box query
   * @return the where clauses for facets
   */
  private QueryAndParams generateFacetQuery(BoxQuery query) {
    if (query.getFacets().isEmpty()) {
      return new QueryAndParams("TRUE", List.of());
    }

    // add source and statuses for gin index coverage
    List<FacetPair> facets = new ArrayList<>();
    facets.add(new FacetPair(SOURCE_FACET_NAME, sourceName));
    query.getStatusValuesOrDefault().stream()
        .map(s -> new FacetPair(STATUS_FACET_NAME, s))
        .forEach(f -> facets.add(f));
    query.getFacets().stream()
        .map(f -> new FacetPair(f.getName(), f.getValue()))
        .forEach(f -> facets.add(f));

    Map<String, List<FacetPair>> groupings =
        facets.stream().collect(Collectors.groupingBy(FacetPair::getName));

    List<String> andQueries = new ArrayList<>();
    List<Object> params = new ArrayList<>();

    for (String name : groupings.keySet()) {
      List<String> orQueries = new ArrayList<>();

      for (FacetPair facet : groupings.get(name)) {
        ObjectNode facetQuery = mapper.createObjectNode();
        facetQuery.withArray(facet.getName()).add(facet.getValue());
        orQueries.add(facetQuery.toString());
      }

      params.addAll(orQueries);
      String orQuery = "(" + repeat("facets @> ?::jsonb", orQueries.size(), " OR ") + ")";
      andQueries.add(orQuery);
    }

    String andQuery = String.join(" AND ", andQueries);
    return new QueryAndParams(andQuery, params);
  }

  @Override
  public ObjectNode getHarvestCursor() {
    final Marker m = Times.single();
    ObjectNode cursor = (ObjectNode) findRegistryValue("cursor");

    if (cursor == null) {
      cursor = mapper.createObjectNode();
    }

    m.mark();
    log.debug("getHarvestCursor: %s: %s", sourceName, m);
    return cursor;
  }

  @Override
  public Set<String> listSourceDependencies() {
    final Marker m = Times.single();
    List<String> sourceDependencies =
        query(
            "SELECT DISTINCT jsonb_object_keys(dependencies) FROM documents",
            new ColumnListHandler<String>());
    m.mark();
    log.debug("listSourceDependencies: %s: %s", sourceName, m);
    return Set.copyOf(sourceDependencies);
  }

  @Override
  public List<String> nextFromQueue(int limit) {
    final Marker m = Times.single();
    List<String> ids =
        query(
            "WITH updated AS (UPDATE queue SET attempt = "
                + "   NOW() + MAKE_INTERVAL(secs => ? * power(2, attempts) * (random() - ? + 1)),"
                + " attempts = attempts + 1,"
                + " attempted = NOW()"
                + " WHERE source = ? AND id IN ("
                + "   SELECT id FROM queue WHERE source = ? AND attempt <= NOW()"
                + "   ORDER BY priority, attempt LIMIT ?"
                + "   FOR UPDATE SKIP LOCKED"
                + ") RETURNING id, priority, attempt)"
                + " SELECT id FROM updated ORDER BY priority, attempt",
            new ColumnListHandler<String>(),
            retryDelay.toMillis() * MILLIS_TO_SECONDS_FACTOR,
            retryJitter,
            sourceName,
            sourceName,
            limit);
    m.mark();
    log.trace("nextFromQueue: %s: %s", sourceName, m);
    return ids;
  }

  @Override
  public void processOrphans(String groupId, Consumer<BoxDocument> function) {
    final Marker m = Times.single();
    Timestamp start =
        query(
            "SELECT start FROM groups WHERE source = ? AND id = ?",
            new ScalarHandler<>(),
            sourceName,
            groupId);

    if (start != null) {
      String query = "SELECT id FROM documents WHERE source = ? AND group_id = ? AND processed < ?";

      // use pure jdbc so we can stream the results
      try (Connection connection = ds.getConnection();
          PreparedStatement statement = connection.prepareStatement(query)) {

        statement.setString(1, sourceName);
        statement.setString(2, groupId);
        statement.setTimestamp(3, start);

        try (ResultSet resultSet = statement.executeQuery()) {
          List<String> batch = new ArrayList<>();

          while (resultSet.next()) {
            batch.add(resultSet.getString(1));

            if (batch.size() == 10) {
              processOrphans(batch, function);
              clear();
            }
          }

          processOrphans(batch, function);
        }

      } catch (SQLException e) {
        throw new RuntimeException(e);
      }
    }

    m.mark();
    log.debug("processOrphans: %s: %s", sourceName, m);
  }

  /**
   * Process orphans for the given box document ids.
   *
   * @param ids the ids to process
   * @param function the function to perform
   */
  private void processOrphans(List<String> ids, Consumer<BoxDocument> function) {
    find(new BoxQuery(ids)).stream().forEach(function);
  }

  @Override
  public void removeDeleted(Duration olderThan) {
    final Marker m = Times.single();
    update(
        "DELETE FROM documents WHERE source = ? AND status = 'DELETED' AND modified < NOW() -"
            + " INTERVAL ?",
        olderThan.toMillis() + " ms",
        sourceName);
    m.mark();
    log.debug("removeDeleted: %s: %s", sourceName, m);
  }

  @Override
  public void save(Collection<? extends BoxDocument> documents) {
    final Marker m = Times.single();
    if (documents.isEmpty()) {
      return;
    }

    List<? extends BoxDocument> deduped = dedup(documents, BoxDocument::getId);

    List<Object> ps = new ArrayList<>();
    ps.add(sourceName);
    deduped.stream().forEach(d -> ps.add(d.getId()));

    Map<String, Long> dbHashes =
        query(
                "SELECT id, hash FROM documents WHERE source = ? AND id IN "
                    + placeholders(deduped.size()),
                new ArrayListHandler(),
                sourceName,
                deduped.stream().map(d -> d.getId()))
            .stream()
            .collect(Collectors.toMap(r -> (String) r[0], r -> (Long) r[1]));

    Map<String, Long> hashes =
        deduped.stream().collect(Collectors.toMap(d -> d.getId(), d -> hashToLong(d)));

    Map<Boolean, List<BoxDocument>> documentGroups =
        deduped.stream()
            .collect(
                Collectors.groupingBy(d -> !hashes.get(d.getId()).equals(dbHashes.get(d.getId()))));

    List<BoxDocument> documentsToSave = documentGroups.getOrDefault(true, List.of());

    if (!documentsToSave.isEmpty()) {
      List<Object> params = new ArrayList<>();
      List<String> placeholders = new ArrayList<>();

      for (BoxDocument document : documentsToSave) {
        params.add(sourceName);
        params.add(document.getId());
        params.add(document.getStatus().toString());
        params.add(hashToLong(document));
        params.add(wrap(document.toJson()));
        params.add(wrap(facetsToJson(document)));
        params.add(wrap(dependenciesToJson(document)));
        params.add(document.getGroupId().orElse(null));
        placeholders.add("(?, ?, ?::box_status, ?, ?, ?, ?, ?)");
      }

      update(
          "INSERT INTO documents"
              + " (source, id, status, hash, document, facets, dependencies, group_id)"
              + " VALUES "
              + String.join(", ", placeholders)
              + " ON CONFLICT (source, id) DO UPDATE SET status = EXCLUDED.status, cursor ="
              + " DEFAULT, modified = DEFAULT, processed = DEFAULT, hash = EXCLUDED.hash, document"
              + " = EXCLUDED.document, facets = EXCLUDED.facets, dependencies ="
              + " EXCLUDED.dependencies, group_id = EXCLUDED.group_id",
          params);
    }

    List<BoxDocument> documentsToTouch = documentGroups.getOrDefault(false, List.of());

    if (!documentsToTouch.isEmpty()) {
      update(
          "UPDATE documents SET processed = NOW() WHERE source = ? AND id IN "
              + placeholders(documentsToTouch.size()),
          sourceName,
          documentsToTouch.stream().map(d -> d.getId()).toArray());
    }

    m.mark();
    log.debug("save: %s: %s", sourceName, m);
  }

  @Override
  public void updateDependencies(Collection<? extends BoxDocument> documents) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    final Marker m = Times.single();
    saveRegistryValue("cursor", cursor);
    m.mark();
    log.debug("setHarvestCursor: %s: %s", sourceName, m);
  }

  @Override
  public void startGroup(String groupId) {
    final Marker m = Times.single();
    update(
        "INSERT INTO groups (source, id, start)"
            + " VALUES (?, ?, NOW())"
            + " ON CONFLICT (source, id) DO UPDATE SET start = NOW()",
        sourceName,
        groupId);
    m.mark();
    log.debug("startGroup: %s: %s", sourceName, m);
  }

  @Override
  public void updateProcessed(Collection<String> ids) {
    final Marker m = Times.single();
    update(
        "UPDATE documents SET processed = NOW() WHERE source = ? AND id IN ("
            + placeholders(ids.size())
            + ")",
        sourceName,
        ids);
    m.mark();
    log.debug("updateProcessed: %s: %s", sourceName, m);
  }

  @Override
  public Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {
    final Marker m = Times.single();
    if (ids.isEmpty()) {
      return Map.of();
    }

    List<BoxDocumentData> results =
        query(
            "SELECT * FROM documents WHERE source = ? AND id IN " + placeholders(ids.size()),
            new BeanListHandler<>(BoxDocumentData.class),
            sourceName,
            ids);

    Map<String, Set<DocumentId>> dependencyMap =
        results.stream()
            .map(d -> d.toBoxDocument())
            .collect(Collectors.toMap(BoxDocument::getId, BoxDocument::getDependencies));
    m.mark();
    log.debug("findDependencies: %s: %s", sourceName, m);
    return dependencyMap;
  }

  @Override
  public JsonNode findRegistryValue(String id) {
    final Marker m = Times.single();
    PGobject data =
        query(
            "SELECT data FROM registry WHERE source = ? AND id = ?",
            new ScalarHandler<>(),
            sourceName,
            id);

    m.mark();
    log.trace("findRegistryValue: %s: %s", sourceName, m);

    if (data == null) {
      return null;
    } else {
      return mapper.readTree(data.getValue());
    }
  }

  @Override
  public void saveRegistryValue(String id, JsonNode data) {
    final Marker m = Times.single();
    PGobject dataObject = new PGobject();
    dataObject.setType("json");

    try {
      dataObject.setValue(data.toString());
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }

    update(
        "INSERT INTO registry VALUES (?, ?, ?) ON CONFLICT (source, id) DO UPDATE SET data = ?",
        sourceName,
        id,
        dataObject,
        dataObject);

    m.mark();
    log.trace("saveRegistryValue: %s: %s", sourceName, m);
  }

  @Override
  public void clear() {
    final Marker m = Times.single();
    update(
        "DELETE FROM documents WHERE source = ?; DELETE FROM queue WHERE source = ?; DELETE FROM"
            + " registry WHERE source = ?;",
        sourceName,
        sourceName,
        sourceName);
    m.mark();
    log.debug("clear: %s: %s", sourceName, m);
  }

  // Helper Methods

  /**
   * Helper method equivalent to {@link QueryRunner#query(String, ResultSetHandler, Object...)}.
   *
   * @param <T> the type of object that the handler returns
   * @param sql the SQL statement to execute.
   * @param rsh the handler used to create the result object from the <code>ResultSet</code>
   * @param params initialize the PreparedStatement's IN parameters with this array
   * @return an object generated by the handler
   */
  private <T> T query(String sql, ResultSetHandler<T> rsh, Object... params) {
    try {
      long start = System.currentTimeMillis();
      T response = db.query(sql, rsh, flattenParams(params));
      long end = System.currentTimeMillis();
      long duration = end - start;

      if (duration > SLOW_QUERY_WARNING_MILLIS) {
        log.warn("slow query: {}ms : {}", duration, sql);
      }

      if (log.isTraceEnabled()) {
        log.trace(bind(sql, flattenParams(params)));
      }

      return response;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Helper method equivalent to {@link QueryRunner#update(String, Object...)}.
   *
   * @param sql the SQL statement to execute
   * @param params initializes the PreparedStatement's IN (i.e. '?') parameters
   * @return the number of rows updated
   * @throws RuntimeException if a database access error occurs
   */
  private int update(String sql, Object... params) {
    try {
      return db.update(sql, flattenParams(params));
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Binds the parameters to the sql for debugging purposes.
   *
   * @param sql the sql
   * @param params the parameters
   * @return the final sql
   */
  private String bind(String sql, Object... params) {
    try (Connection connection = ds.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql)) {

      for (int i = 0; i < params.length; i++) {
        statement.setObject(i + 1, params[i]);
      }

      return statement.toString();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Flattens params into a single array of params. Params could include collections, arrays,
   * streams, or single items.
   *
   * @param params the params to flatten
   * @return the flatten params
   */
  private Object[] flattenParams(Object... params) {
    List<Object> flattenedParams = new ArrayList<>();

    for (Object param : params) {
      if (param instanceof Iterable) {
        ((Iterable<?>) param).forEach(p -> flattenedParams.add(p));
      } else if (param instanceof Object[]) {
        Arrays.stream((Object[]) param).forEach(p -> flattenedParams.add(p));
      } else if (param instanceof Stream) {
        ((Stream<?>) param).forEach(p -> flattenedParams.add(p));
      } else {
        flattenedParams.add(param);
      }
    }

    return flattenedParams.toArray();
  }

  /**
   * Repeats the given text a number of times separated by the delimiter.
   *
   * @param text the text to repeat
   * @param copies the number of times to repeat the text
   * @param delimiter the delimiter between repitions
   * @return the repeated text
   */
  private String repeat(String text, int copies, String delimiter) {
    return String.join(delimiter, Collections.nCopies(copies, text));
  }

  /**
   * Same as {@link #placeholders(int)} with args 1, columns.
   *
   * @param columns number of columns
   * @return the placeholder string
   */
  private String placeholders(int columns) {
    return placeholders(1, columns);
  }

  /**
   * Same as {@link #placeholders(int, int, String)} with args rows, columns, ", ".
   *
   * @param rows number of rows
   * @param columns number of columns
   * @return the placeholder string
   */
  private String placeholders(int rows, int columns) {
    return placeholders(rows, columns, ", ");
  }

  /**
   * Creates a placeholder string to be used in a {@link PreparedStatement}. The placeholder string
   * is in the form of <code>(?, ?, ?), (?, ?, ?)</code>. The rows indicates the number of groups.
   * The columns indicates the number within each group. And the delimiter is how each group is
   * separated.
   *
   * @param rows number of groups
   * @param columns number of items per group
   * @param delimiter text separating each group
   * @return the placeholder string
   */
  private String placeholders(int rows, int columns, String delimiter) {
    StringBuilder builder = new StringBuilder();

    for (int i = 0; i < rows; i++) {
      if (i != 0) {
        builder.append(delimiter);
      }

      builder.append("(");

      for (int j = 0; j < columns; j++) {
        if (j != 0) {
          builder.append(", ");
        }

        builder.append("?");
      }

      builder.append(")");
    }

    return builder.toString();
  }

  /**
   * Converts the result of {@link BoxDocument#hash()} to a Long.
   *
   * @param document the document to hash
   * @return the long representation of the hash
   */
  private Long hashToLong(BoxDocument document) {
    return ByteBuffer.wrap(document.hash()).getLong();
  }

  /**
   * Converts a document's dependencies to a json document.
   *
   * @param document the box document
   * @return the dependencies document
   */
  private ObjectNode dependenciesToJson(BoxDocument document) {
    ObjectNode dependencies = mapper.createObjectNode();

    for (DocumentId dependency : document.getDependencies()) {
      dependencies.withArray(dependency.getSourceName()).add(dependency.getId());
    }

    return dependencies;
  }

  /**
   * Converts a document's facets to a json document.
   *
   * @param document the box document
   * @return the facets document
   */
  private ObjectNode facetsToJson(BoxDocument document) {
    ObjectNode facets = mapper.createObjectNode();

    if (!document.getFacets().isEmpty()) {
      // include source and status for gin index coverage
      facets.withArray(SOURCE_FACET_NAME).add(sourceName);
      facets.withArray(STATUS_FACET_NAME).add(document.getStatus().toString());
    }

    for (Facet facet : document.getFacets()) {
      facets.withArray(facet.getName()).add(facet.getValue());
    }

    return facets;
  }

  /**
   * Wraps a {@link JsonNode} into a {@link PGobject} for inserting into postgres.
   *
   * @param node the json node to wrap
   * @return the wrapped json node
   */
  private PGobject wrap(JsonNode node) {
    try {
      PGobject jsonWrapper = new PGobject();
      jsonWrapper.setType("json");
      jsonWrapper.setValue(mapper.writeValueAsString(node));
      return jsonWrapper;
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Deduplicates the given collection.
   *
   * @param <T> type of items in the collection
   * @param <U> the type of key for identifying each item
   * @param items the collection
   * @param keyMapper the function for extracting the key
   * @return the deduped list
   */
  private <T, U> List<T> dedup(Collection<T> items, Function<T, U> keyMapper) {
    Map<U, T> dedupMap = new LinkedHashMap<>();

    for (T item : items) {
      U key = keyMapper.apply(item);
      dedupMap.remove(key);
      dedupMap.put(key, item);
    }

    return List.copyOf(dedupMap.values());
  }

  /** Bean for binding metadata for query result. */
  @Data
  public static class MetadataData {
    private PGobject metadata;
    private BoxDocument document;

    /**
     * Sets the metadata and creates a {@link BoxDocument from it}.
     *
     * @param metadata the metadata to set
     */
    public void setMetadata(PGobject metadata) {
      this.metadata = metadata;
      ObjectNode node = (ObjectNode) mapper.readTree(metadata.getValue());
      document = new BoxDocument(node.path("id").asText());
      document.setStatus(Status.valueOf(node.path("status").asText().toUpperCase()));

      for (JsonField facet : new JsonField(node.path("facets"))) {
        for (JsonNode facetValue : facet.getValue()) {
          document.addFacet(facet.getKey(), facetValue.asText());
        }
      }

      for (JsonField dependency : new JsonField(node.path("dependencies"))) {
        for (JsonNode id : dependency.getValue()) {
          document.addDependency(dependency.getKey(), id.asText());
        }
      }
    }
  }

  /** Bean for binding a document for query result. */
  @Data
  @NoArgsConstructor
  public static class BoxDocumentData {

    private String source;
    private String id;
    private String status;
    private Long cursor;
    private Long hash;
    private Timestamp modified;
    private Timestamp processed;
    private PGobject document;
    private PGobject facets;
    private PGobject dependencies;
    private String groupId;

    /**
     * Create an empty object with just id.
     *
     * @param id the id
     */
    public BoxDocumentData(String id) {
      this.id = id;
    }

    /**
     * Converts this database representation to a {@link BoxDocument}.
     *
     * @return a new box document
     */
    public BoxDocument toBoxDocument() {
      BoxDocument document;

      if (this.document != null) {
        document = BoxDocument.parse(this.document.getValue());
      } else {
        document = new BoxDocument(id);

        if (status != null) {
          document.setStatus(BoxDocument.Status.valueOf(status));
        }

        if (facets != null) {
          JsonNode facetsNode = mapper.readTree(facets.getValue());

          for (JsonField facet : new JsonField(facetsNode)) {
            for (JsonNode facetValue : facet.getValue()) {
              document.addFacet(facet.getKey(), facetValue.asText());
            }
          }
        }

        if (dependencies != null) {
          JsonNode dependenciesNode = mapper.readTree(dependencies.getValue());

          for (JsonField dependency : new JsonField(dependenciesNode)) {
            for (JsonNode id : dependency.getValue()) {
              document.addDependency(dependency.getKey(), id.asText());
            }
          }
        }

        document.setGroupId(groupId);
      }

      if (cursor != null) {
        document.setCursor(cursor);
      }

      if (modified != null) {
        document.setModified(modified.toInstant());
      }

      if (processed != null) {
        document.setProcessed(processed.toInstant());
      }

      return document;
    }
  }

  /** Object containing the sql and parameters for a query. */
  @Data
  @AllArgsConstructor
  public static class QueryAndParams {
    private String query;
    private List<Object> params;
  }

  /** Same as {@link Facet}, but without constraints. */
  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class FacetPair {
    private String name;
    private String value;
  }
}
