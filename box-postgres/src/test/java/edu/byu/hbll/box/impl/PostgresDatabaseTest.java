package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDatabaseTest;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.ObjectType;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * @author Charles Draper
 */
@Testcontainers
public class PostgresDatabaseTest extends BoxDatabaseTest {

  @Container
  private static GenericContainer<?> postgresContainer =
      new GenericContainer<>("postgres:10")
          .withExposedPorts(5432)
          .withEnv("POSTGRES_USER", "root")
          .withEnv("POSTGRES_PASSWORD", "asdf");

  private static PostgresDatabase db;

  @BeforeAll
  public static void beforeAll() throws Exception {
    String pgHost = postgresContainer.getContainerIpAddress();
    int pgPort = postgresContainer.getMappedPort(5432);
    String postgresUri =
        "jdbc:postgresql://" + pgHost + ":" + pgPort + "/test?user=root&password=asdf";

    BasicDataSource ds = new BasicDataSource();
    ds.setUrl(postgresUri.replace("/test", "/"));
    ds.setInitialSize(1);
    new QueryRunner(ds).update("CREATE DATABASE test");

    db = new PostgresDatabase();
    ConstructConfig config = new ConstructConfig();
    config.setSourceName("test");
    config.setObjectType(ObjectType.BOX_DATABASE);
    config.setParams(JsonNodeFactory.instance.objectNode().put("uri", postgresUri));
    db.postConstruct(config);
  }

  @Override
  public BoxDatabase getDatabase() {
    return db;
  }
}
