package edu.byu.hbll.box;

import static edu.byu.hbll.box.BoxDocument.Status.DELETED;
import static edu.byu.hbll.box.BoxDocument.Status.ERROR;
import static edu.byu.hbll.box.BoxDocument.Status.READY;
import static edu.byu.hbll.box.BoxDocument.Status.UNPROCESSED;
import static edu.byu.hbll.box.BoxQuery.DOCUMENT_FIELD;
import static edu.byu.hbll.box.BoxQuery.METADATA_FIELD;
import static edu.byu.hbll.box.BoxQuery.METADATA_FIELD_ID;
import static edu.byu.hbll.box.BoxQuery.METADATA_FIELD_STATUS;
import static edu.byu.hbll.box.BoxTestUtils.demodoc;
import static edu.byu.hbll.box.BoxTestUtils.demoobj;
import static edu.byu.hbll.box.BoxTestUtils.doc;
import static edu.byu.hbll.box.BoxTestUtils.find;
import static edu.byu.hbll.box.BoxTestUtils.ids;
import static edu.byu.hbll.box.BoxTestUtils.obj;
import static edu.byu.hbll.box.BoxTestUtils.query;
import static edu.byu.hbll.box.BoxTestUtils.save;
import static edu.byu.hbll.box.BoxTestUtils.sleep;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * An abstract test class for testing implementations of {@link BoxDatabase}. Extend this class per
 * implementation to be tested and provide an instance of the {@link BoxDatabase} by implementing
 * {@link #getDatabase()}. This class will test the basic functionality of a {@link BoxDatabase}.
 * Because implementations may have edge cases not covered by these tests, additional tests should
 * be added to the test subclass.
 */
public abstract class BoxDatabaseTest {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private BoxDatabase db = getDatabase();

  @BeforeEach
  public void beforeEach() {
    db.clear();
  }

  @Test
  public void testAddToQueueCollection() {
    QueueEntry entry1 = new QueueEntry("1", Instant.now(), 3F, false);
    QueueEntry entry2 = new QueueEntry("2", Instant.now(), 1F, false);
    QueueEntry entry3 = new QueueEntry("3", Instant.now(), 2F, false);
    QueueEntry entry4 = new QueueEntry("4", Instant.now().plus(100, ChronoUnit.MILLIS), 3F, true);
    QueueEntry entry5 = new QueueEntry("5", Instant.now().plus(100, ChronoUnit.MILLIS), 1F, true);
    QueueEntry entry6 = new QueueEntry("6", Instant.now().plus(100, ChronoUnit.MILLIS), 2F, true);
    List<QueueEntry> entries = List.of(entry1, entry2, entry3, entry4, entry5, entry6);

    assertAll(
        () -> assertEquals(List.of(), db.nextFromQueue(10)),
        () -> db.addToQueue(entries),
        () -> assertEquals(List.of("2", "3", "1"), db.nextFromQueue(10)),
        () -> assertEquals(List.of(), db.nextFromQueue(10)),
        () -> sleep(200),
        () -> assertEquals(List.of("5", "6", "4"), db.nextFromQueue(10)),
        () -> assertEquals(List.of(), db.nextFromQueue(10)),
        () -> db.addToQueue(entries),
        () -> sleep(200),
        () -> assertEquals(List.of("5", "6", "4"), db.nextFromQueue(10)),
        // verify can handle the same thing twice
        () -> db.addToQueue(List.of(entry4, entry4)));
  }

  @Test
  public void testDeleteFromQueue() {
    QueueEntry entry1 = new QueueEntry("1");
    QueueEntry entry2 = new QueueEntry("2");
    QueueEntry entry3 = new QueueEntry("3");
    assertAll(
        () -> assertEquals(List.of(), db.nextFromQueue(10)),
        () -> db.deleteFromQueue(List.of()),
        () -> db.addToQueue(List.of(entry1, entry2, entry3)),
        () -> db.deleteFromQueue(List.of("1", "3")),
        () -> assertEquals(List.of("2"), db.nextFromQueue(10)));
  }

  @Test
  public void testFindDependents() {
    BoxDocument doc1 = new BoxDocument("1");
    BoxDocument doc2 = new BoxDocument("2").addDependency("src1", "2");
    BoxDocument doc3 = new BoxDocument("3").addDependency("src1", "3").addDependency("src2", "3");
    BoxDocument doc4 =
        new BoxDocument("4").addDependency("src1", "4.1").addDependency("src1", "4.2");
    BoxDocument doc51 = new BoxDocument("51").addDependency("src1", "5");
    BoxDocument doc52 = new BoxDocument("52").addDependency("src1", "5");
    save(db, doc1, doc2, doc3, doc4, doc51, doc52);

    assertAll(
        () -> assertEquals(Map.of(), db.findDependents(List.of())),
        () -> assertEquals(Map.of(), db.findDependents(List.of(new DocumentId("src1", "1")))),
        () ->
            assertEquals(
                Map.of(new DocumentId("src1", "2"), Set.of("2")),
                db.findDependents(List.of(new DocumentId("src1", "2")))),
        () ->
            assertEquals(
                Map.of(new DocumentId("src1", "3"), Set.of("3")),
                db.findDependents(List.of(new DocumentId("src1", "3")))),
        () ->
            assertEquals(
                Map.of(new DocumentId("src1", "4.1"), Set.of("4")),
                db.findDependents(List.of(new DocumentId("src1", "4.1")))),
        () ->
            assertEquals(
                Map.of(new DocumentId("src1", "5"), Set.of("51", "52")),
                db.findDependents(List.of(new DocumentId("src1", "5")))),
        () ->
            assertEquals(
                Map.of(
                    new DocumentId("src1", "2"),
                    Set.of("2"),
                    new DocumentId("src1", "3"),
                    Set.of("3"),
                    new DocumentId("src1", "4.1"),
                    Set.of("4"),
                    new DocumentId("src1", "5"),
                    Set.of("51", "52")),
                db.findDependents(
                    List.of(
                        new DocumentId("src1", "2"),
                        new DocumentId("src1", "3"),
                        new DocumentId("src1", "4.1"),
                        new DocumentId("src1", "5")))));
  }

  @Test
  public void testListSourceDependencies() {
    BoxDocument doc1 = new BoxDocument("1");
    BoxDocument doc2 = new BoxDocument("2").addDependency("src1", "2");
    BoxDocument doc3 = new BoxDocument("3").addDependency("src1", "3").addDependency("src2", "3");

    assertAll(
        () -> assertEquals(Set.of(), db.listSourceDependencies()),
        () -> save(db, doc1),
        () -> assertEquals(Set.of(), db.listSourceDependencies()),
        () -> save(db, doc2),
        () -> assertEquals(Set.of("src1"), db.listSourceDependencies()),
        () -> save(db, doc3),
        () -> assertEquals(Set.of("src1", "src2"), db.listSourceDependencies()));
  }

  @Test
  public void testSaveAndFindRegistryValue() {

    assertAll(
        () -> assertNull(db.findRegistryValue("a")),
        () -> db.saveRegistryValue("a", obj().put("a", "1")),
        () -> assertEquals(obj().put("a", "1"), db.findRegistryValue("a")),
        () -> db.saveRegistryValue("b", mapper.createArrayNode().add("1").add("2").add("3")),
        () ->
            assertEquals(
                mapper.createArrayNode().add("1").add("2").add("3"), db.findRegistryValue("b")),
        () -> db.saveRegistryValue("a", obj().put("a", "2")),
        () -> assertEquals(obj().put("a", "2"), db.findRegistryValue("a")));
  }

  @Test
  public void testSetAndGetHarvestCursor() {
    assertAll(
        () -> assertEquals(obj(), db.getHarvestCursor()),
        () -> db.setHarvestCursor(obj().put("a", "1")),
        () -> assertEquals(obj().put("a", "1"), db.getHarvestCursor()),
        () -> db.setHarvestCursor(obj().put("b", "2")),
        () -> assertEquals(obj().put("b", "2"), db.getHarvestCursor()));
  }

  @Test
  public void testFindForHarvestQueries() {
    BoxDocument r1 = new BoxDocument("r1").setAsReady();
    BoxDocument r2 = new BoxDocument("r2").setAsReady();
    BoxDocument r3 = new BoxDocument("r3").setAsReady();

    assertAll(
        () -> assertEquals(ids(), find(db, new BoxQuery())),
        () -> save(db, r1, r2, r3),
        () -> assertEquals(ids(r1, r2, r3), ids(find(db, new BoxQuery()))));
  }

  @Test
  public void testFindForHarvestQueriesByStatuses() {
    BoxDocument u1 = new BoxDocument("u1");
    BoxDocument r1 = new BoxDocument("r1").setAsReady();
    BoxDocument d1 = new BoxDocument("d1").setAsDeleted();
    BoxDocument e1 = new BoxDocument("e1").setAsError("error");
    save(db, u1, r1, d1, e1);

    assertAll(
        // default statuses
        () -> assertEquals(ids(r1, d1), ids(find(db, new BoxQuery()))),
        // all statuses
        () ->
            assertEquals(
                ids(u1, r1, d1, e1),
                ids(find(db, new BoxQuery().addStatuses(UNPROCESSED, READY, DELETED, ERROR)))),
        // all individually
        () -> assertEquals(ids(u1), ids(find(db, new BoxQuery().addStatus(UNPROCESSED)))),
        () -> assertEquals(ids(r1), ids(find(db, new BoxQuery().addStatus(READY)))),
        () -> assertEquals(ids(d1), ids(find(db, new BoxQuery().addStatus(DELETED)))),
        () -> assertEquals(ids(e1), ids(find(db, new BoxQuery().addStatus(ERROR)))));
  }

  @Test
  public void testSave() {
    assertAll(
        // test can save same document multiple times
        () -> db.save(List.of(doc(1), doc(1))));
  }

  @Test
  public void testFindForHarvestQueriesByFacets() {
    BoxDocument r1 = new BoxDocument("r1").setAsReady();
    BoxDocument r2 = new BoxDocument("r2").setAsReady().addFacet("a", "1");
    BoxDocument r3 = new BoxDocument("r3").setAsReady().addFacet("a", "1").addFacet("a", "2");
    BoxDocument r4 = new BoxDocument("r4").setAsReady().addFacet("a", "1").addFacet("b", "1");
    save(db, r1, r2, r3, r4);

    assertAll(
        () -> assertEquals(ids(r2, r3, r4), ids(find(db, new BoxQuery().addFacet("a", "1")))),
        () ->
            assertEquals(
                ids(r2, r3, r4),
                ids(find(db, new BoxQuery().addFacet("a", "1").addFacet("a", "2")))),
        () ->
            assertEquals(
                ids(r3), ids(find(db, new BoxQuery().addFacet("a", "2").addFacet("a", "3")))),
        () ->
            assertEquals(
                ids(r4),
                ids(
                    find(
                        db,
                        new BoxQuery()
                            .addFacet("a", "1")
                            .addFacet("a", "2")
                            .addFacet("b", "1")
                            .addFacet("b", "2")))));
  }

  @Test
  public void testFindForHarvestQueriesWithLimits() {
    save(db, "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11");

    assertAll(
        () -> assertEquals(10, find(db).size()),
        () -> assertEquals(0, find(db, query().setLimit(0)).size()),
        () -> assertEquals(3, find(db, query().setLimit(3)).size()),
        () -> assertEquals(11, find(db, query().setUnlimited()).size()));
  }

  @Test
  public void testFindForHarvestQueriesWithOffsets() {
    save(db, "1", "2", "3");

    assertAll(
        () -> assertEquals(List.of("1", "2", "3"), ids(db, query().setOffset(0))),
        () -> assertEquals(List.of("2", "3"), ids(db, query().setOffset(1))),
        () -> assertEquals(List.of("3"), ids(db, query().setOffset(2))),
        () -> assertEquals(List.of(), ids(db, query().setOffset(3))));
  }

  @Test
  public void testFindForHarvestQueriesWithOrder() {
    save(db, "1", "2", "3");

    assertAll(
        () -> assertEquals(List.of("1", "2", "3"), ids(db, query())),
        () -> assertEquals(List.of("3", "2", "1"), ids(db, query().setDescendingOrder())));
  }

  @Test
  public void testFindForHarvestQueriesWithCursor() {
    save(db, "1", "2", "3");
    List<BoxDocument> results = find(db, new BoxQuery());
    long cursor1 = results.get(0).getCursor().get();
    long cursor2 = results.get(1).getCursor().get();
    long cursor3 = results.get(2).getCursor().get();

    assertAll(
        () -> assertEquals(List.of("1", "2", "3"), ids(db, query())),
        () -> assertEquals(List.of("1", "2", "3"), ids(db, query().setCursor(cursor1))),
        () -> assertEquals(List.of("3"), ids(db, query().setCursor(cursor3))),
        () -> assertEquals(List.of(), ids(db, query().setCursor(cursor3 + 1))),
        () ->
            assertEquals(
                List.of("2", "1"), ids(db, query().setCursor(cursor2).setDescendingOrder())));
  }

  @Test
  public void testFindForIdQueries() {
    BoxDocument r1 = new BoxDocument("r1").setAsReady();
    BoxDocument r2 = new BoxDocument("r2").setAsReady();
    BoxDocument r3 = new BoxDocument("r3").setAsReady();
    save(db, r1, r2, r3);

    assertAll(
        // id query, verify ordering
        () -> assertEquals(ids(r2, r3, r1), ids(find(db, new BoxQuery("r2", "r3", "r1")))),
        // id query, verify repeated
        () -> assertEquals(ids(r1, r1), ids(find(db, new BoxQuery("r1", "r1")))),
        // id query, verify nonexistant
        () -> assertEquals(List.of("n1"), ids(find(db, new BoxQuery("n1")))));
  }

  @Test
  public void testFindForDocumentIntegrity() {
    BoxDocument c1 = new BoxDocument("c1", demoobj());
    save(db, c1);
    BoxDocument result = find(db, new BoxQuery("c1")).get(0);

    assertAll(
        () -> assertEquals(c1, result),
        () -> assertTrue(result.getCursor().get() > 0),
        () -> assertTrue(result.getModified().get().isAfter(Instant.now().minusSeconds(1))),
        () -> assertTrue(result.getModified().get().isBefore(Instant.now().plusSeconds(1))),
        () -> assertTrue(result.getProcessed().get().isAfter(Instant.now().minusSeconds(1))),
        () -> assertTrue(result.getProcessed().get().isBefore(Instant.now().plusSeconds(1))));
  }

  @Test
  public void testFindForFieldProjection() {
    BoxDocument d1 = demodoc("1").clearDependencies();
    save(db, d1);
    String idField = METADATA_FIELD_ID;
    String statusField = METADATA_FIELD_STATUS;
    assertAll(
        () -> assertEquals(d1, find(db).get(0)),
        () ->
            assertEquals(
                BoxDocument.parse(d1.toString(List.of(DOCUMENT_FIELD, idField, statusField))),
                find(db, query().addField(DOCUMENT_FIELD)).get(0)),
        () ->
            assertEquals(
                BoxDocument.parse(d1.toString(List.of(METADATA_FIELD))),
                find(db, query().addField(METADATA_FIELD)).get(0)),
        () ->
            assertEquals(
                BoxDocument.parse(d1.toString(List.of("a", "b", idField, statusField))),
                find(db, query().addFields("a", "b")).get(0)),
        () ->
            assertEquals(
                BoxDocument.parse(d1.toString(List.of("c", "f.a", idField, statusField))),
                find(db, query().addFields("c", "f.a")).get(0)));
  }

  @Test
  public void testProcessOrphans() {
    save(db, doc(1));
    save(db, doc(2).setGroupId("1"));
    // use a complex doc to verify the entire doc is returned
    save(db, demodoc(3).setGroupId("2"));
    save(db, demodoc(4).setGroupId("2"));
    sleep(100);
    db.startGroup("2");
    save(db, doc(5).setGroupId("2"));
    save(db, doc(6).setGroupId("2"));

    List<BoxDocument> actual = new ArrayList<>();
    db.processOrphans("2", d -> actual.add(d));

    List<BoxDocument> expected = List.of(demodoc(3).setGroupId("2"), demodoc(4).setGroupId("2"));
    assertEquals(expected, actual);
  }

  @Test
  public void testFindDependencies() {
    List<BoxDocument> docs = List.of(doc(1), demodoc(2), demodoc(3));
    save(db, docs);
    Map<String, Set<DocumentId>> expected =
        docs.stream().collect(Collectors.toMap(BoxDocument::getId, BoxDocument::getDependencies));
    assertAll(
        () -> assertEquals(Map.of(), db.findDependencies(List.of())),
        () -> assertEquals(Map.of("1", Set.of()), db.findDependencies(List.of("1"))),
        () -> assertEquals(expected, db.findDependencies(List.of("1", "2", "3"))));
  }

  /**
   * Returns the {@link BoxDatabase} to use for the test.
   *
   * @return the box database
   */
  public abstract BoxDatabase getDatabase();
}
