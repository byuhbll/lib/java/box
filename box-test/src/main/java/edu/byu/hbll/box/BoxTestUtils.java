package edu.byu.hbll.box;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.misc.Resources;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;

/** Utilities to simplify Box testing. */
@UtilityClass
public class BoxTestUtils {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /**
   * Creates a new empty json array.
   *
   * @return a new json array
   */
  public static ArrayNode arr() {
    return mapper.createArrayNode();
  }

  /**
   * Creates an empty, but ready {@link BoxDocument} with the given id.
   *
   * @param id the document id
   * @return the new box document
   */
  public static BoxDocument doc(int id) {
    return doc(id + "");
  }

  /**
   * Creates an empty, but ready {@link BoxDocument} with the given id.
   *
   * @param id the document id
   * @return the new box document
   */
  public static BoxDocument doc(String id) {
    return new BoxDocument(id).setAsReady();
  }

  /**
   * Creates a ready {@link BoxDocument} with the given id, a demo document, facets, and
   * dependencies.
   *
   * @param id the document id
   * @return the new box document
   */
  public static BoxDocument demodoc(int id) {
    return demodoc(id + "");
  }

  /**
   * Creates a ready {@link BoxDocument} with the given id, a demo document, facets, and
   * dependencies.
   *
   * @param id the document id
   * @return the new box document
   */
  public static BoxDocument demodoc(String id) {
    try {
      return BoxDocument.parse(Resources.readString("demodoc.json")).setId(id);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Same as {@link BoxDatabase#find} with an empty query.
   *
   * @param db the database to use
   * @return the query results
   */
  public static QueryResult find(BoxDatabase db) {
    return find(db, new BoxQuery());
  }

  /**
   * Same as {@link BoxDatabase#find} with the given query.
   *
   * @param db the database to use
   * @param query the query
   * @return the query results
   */
  public static QueryResult find(BoxDatabase db, BoxQuery query) {
    return db.find(query);
  }

  /**
   * Same as {@link BoxDatabase#find} with the given query, but only returns the ids of the query
   * results.
   *
   * @param db the database to use
   * @param query the query
   * @return the box document ids of the query results
   */
  public static List<String> ids(BoxDatabase db, BoxQuery query) {
    return ids(find(db, query));
  }

  /**
   * Returns only the ids of the given box documents.
   *
   * @param documents the documents
   * @return the corresponding ids
   */
  public static List<String> ids(BoxDocument... documents) {
    return ids(List.of(documents));
  }

  /**
   * Returns only the ids of the given box documents.
   *
   * @param documents the documents
   * @return the corresponding ids
   */
  public static List<String> ids(List<BoxDocument> documents) {
    return documents.stream().map(d -> d.getId()).collect(Collectors.toList());
  }

  /**
   * Creates a new empty json object.
   *
   * @return a new json object
   */
  public static ObjectNode obj() {
    return mapper.createObjectNode();
  }

  /**
   * Creates a demo json object.
   *
   * @return a demo json object
   */
  public static ObjectNode demoobj() {
    return demodoc(1).getDocument();
  }

  /**
   * Same as <code>new BoxQuery()</code>.
   *
   * @return a new empty box query
   */
  public static BoxQuery query() {
    return new BoxQuery();
  }

  /**
   * Same as {@link BoxDatabase#save}.
   *
   * @param db the database to use
   * @param documents the documents to save
   */
  public static void save(BoxDatabase db, List<BoxDocument> documents) {
    db.save(documents);
  }

  /**
   * Same as {@link BoxDatabase#save}.
   *
   * @param db the database to use
   * @param documents the documents to save
   */
  public static void save(BoxDatabase db, BoxDocument... documents) {
    db.save(List.of(documents));
  }

  /**
   * Same as {@link BoxDatabase#save}, but creates ready, but empty documents from the given ids.
   *
   * @param db the database to use
   * @param ids the ids to create documents from
   */
  public static void save(BoxDatabase db, String... ids) {
    db.save(Arrays.stream(ids).map(id -> doc(id)).collect(Collectors.toList()));
  }

  /**
   * Same as {@link Thread#sleep(long)}, but without checked exceptions.
   *
   * @param millis the length of time to sleep in milliseconds
   */
  public static void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
