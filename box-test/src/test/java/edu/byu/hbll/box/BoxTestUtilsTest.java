package edu.byu.hbll.box;

import static edu.byu.hbll.box.BoxTestUtils.*;
import static edu.byu.hbll.box.BoxTestUtils.arr;
import static edu.byu.hbll.box.BoxTestUtils.demodoc;
import static edu.byu.hbll.box.BoxTestUtils.demoobj;
import static edu.byu.hbll.box.BoxTestUtils.doc;
import static edu.byu.hbll.box.BoxTestUtils.find;
import static edu.byu.hbll.box.BoxTestUtils.obj;
import static edu.byu.hbll.box.BoxTestUtils.sleep;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.impl.MemoryDatabase;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.misc.Resources;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BoxTestUtilsTest {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private BoxDocument demodoc;
  private MemoryDatabase db = new MemoryDatabase();

  @BeforeEach
  public void beforeEach() throws Exception {
    demodoc = BoxDocument.parse(Resources.readString("demodoc.json"));
  }

  @Test
  public void testArr() {
    assertEquals(mapper.createArrayNode(), arr());
  }

  @Test
  public void testDocInt() {
    assertEquals(new BoxDocument("1").setAsReady(), doc(1));
  }

  @Test
  public void testDocString() {
    assertEquals(new BoxDocument("1").setAsReady(), doc("1"));
  }

  @Test
  public void testDemodocInt() {
    assertEquals(demodoc, demodoc(1));
  }

  @Test
  public void testDemodocString() {
    assertEquals(demodoc, demodoc("1"));
  }

  @Test
  public void testFindDefault() {
    QueryResult result = new QueryResult(List.of(doc(1), doc(2), doc(3)));
    db.save(result);
    assertEquals(result, find(db));
  }

  @Test
  public void testFindQuery() {
    QueryResult result = new QueryResult(List.of(doc(1), doc(2), doc(3)));
    db.save(result);
    assertEquals(result, find(db, new BoxQuery()));
  }

  @Test
  public void testIdsQuery() {
    db.save(List.of(doc(1), doc(2), doc(3)));
    assertEquals(List.of("1", "2", "3"), ids(db, new BoxQuery()));
  }

  @Test
  public void testIdsDocArray() {
    assertEquals(List.of("1", "2", "3"), ids(doc(1), doc(2), doc(3)));
  }

  @Test
  public void testIdsDocList() {
    assertEquals(List.of("1", "2", "3"), ids(List.of(doc(1), doc(2), doc(3))));
  }

  @Test
  public void testObj() {
    assertEquals(mapper.createObjectNode(), obj());
  }

  @Test
  public void testDemoobj() {
    assertEquals(demodoc.getDocument(), demoobj());
  }

  @Test
  public void testQuery() {
    assertTrue(query() instanceof BoxQuery);
  }

  @Test
  public void testSaveDocList() {
    QueryResult result = new QueryResult(List.of(doc(1), doc(2), doc(3)));
    save(db, result);
    assertEquals(result, db.find(new BoxQuery()));
  }

  @Test
  public void testSaveDocArray() {
    save(db, doc(1), doc(2), doc(3));
    assertEquals(List.of(doc(1), doc(2), doc(3)), db.find(new BoxQuery()));
  }

  @Test
  public void testSaveIdArray() {
    save(db, "1", "2", "3");
    assertEquals(List.of(doc(1), doc(2), doc(3)), db.find(new BoxQuery()));
  }

  @Test
  public void testSleep() {
    long sleepTime = 200;
    long startTime = System.currentTimeMillis();

    sleep(sleepTime);

    long endTime = System.currentTimeMillis();
    long actualSleepTime = endTime - startTime;

    long buffer = 50;
    assertTrue(actualSleepTime >= sleepTime - buffer);
  }
}
