# Box

Box is an add-on framework for Java applications that performs asynchronous gathering of source data with subsequent processing or transformation of that data into output JSON documents and exposing those documents through a web API. Box excels at maintaining existing documents including keeping them up-to-date in real time and deleting them when required. Box supports aggregating data from multiple sources as well as splitting data into multiple documents. How documents are processed is left up to each application. The web api makes the documents available by ID or by harvesting all documents. Harvests can be filtered using facets and documents can be pared down by requesting only needed fields.

## Usage

To include a typical Box setup with your application, add the following dependencies to your project's pom.xml.

```xml
<dependency>
  <groupId>edu.byu.hbll.box</groupId>
  <artifactId>box-mongo</artifactId>
  <version>2.7.0</version>
</dependency>
<dependency>
  <groupId>edu.byu.hbll.box</groupId>
  <artifactId>box-spring-boot</artifactId>
  <version>2.7.0</version>
</dependency>
```

Find additional Tutorials at [Tutorials](https://bitbucket.org/byuhbll/box/wiki/Tutorials)

Now create a new Box instance (see Configuration).

```java
JsonNode config = ...;
Box box = Box.newBox(config);
```

or

```java
Box box = Box.newBuilder()...build();
```

A Box instance is made up of processing units called sources. For Box to do anything, you need to configure at least one source. Sources within a single Box instance can interact with each other through a dependency hierarchy.

## Configuration

There are multiple ways to configure Box. One is using a YAML or JSON configuration file and passing it to box in the form of a Jackson `JsonNode`. You may also programmatically configure Box.

### Yaml File

You can configure a new instance of Box with a YAML file. Use the `config` library to load YAML as a JsonNode and pass that node into `box`.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>config</artifactId>
  <version>3.2.0</version>
</dependency>
```

```java
JsonNode config = new YamlLoader().load(Paths.get("/path/to/config"));
Box box = Box.newBox(config.path("box"));
```

The YAML has the following form. IMPORTANT: In the sample YAML below, the root field `box` is not necessary, but it helps keep Box configurations separate from other configurations the app requires. You must pass the box object, not the root object (ie, use `Box.newBox(config.path("box"))` not `Box.newBox(config)`).

```yaml
# Box configuration section
box:
  # Source definitions
  sources:
    # Source 1 (required, any name matching [0-9A-Za-z_]+)
    src1:
      # true|false to access this source through web service without specifying source name
      # ie, /box/document vs /src1/document
      # if no principal is defined, the first source listed is automatically made principal
      principal: false
      # enables this source, a disabled source is skipped completely (default: true);
      enabled: true
      # when to go looking for deleted documents to remove (default: PT1M)
      removeSchedule: PT1M
      # removes documents that were deleted more than ISO 8601 duration ago (default: P90D)
      # note if set to null, removing of deleted documents will not happen
      removeAge: P90D
      # queries resulting documents to generate facets (default: none)
      facetField:
        # facetname: [path.to.field]
      # whether to save the resulting documents (default: true)
      save: true
      # handle and save document only if marked as dependency of other document (default: false)
      dependencyOnly: false
      # enable posting of documents to web endpoints (default: false)
      postEnabled: false
      process:
        # Class that implements edu.byu.hbll.box.Processor (required)
        type: edu.byu.hbll.box.impl.View
        # process documents in batches of size up to batchCapacity (default: 1)
        batchCapacity: 1
        # time to wait while batch fills up, use Duration ISO 8601 (default: PT0S)
        batchDelay: PT0S
        # number of threads running the batches (default: 1)
        threadCount: 1
        # limit number of batches processed to this (default: 0)
        quota: 0
        # reset the used-up quota on this schedule (default: 0 0 0 * * * *)
        quotaReset: "0 0 0 * * * *"
        # suspend each thread after processed batch, helps with throttling (default: PT0S)
        suspend: PT0S
        # when to enable processing (default: null)
        # note if either on or off is null, processing will always be on
        "on": "0 0 0 * * * *"
        # when to disable processing (default: null)
        # note if either on or off is null, processing will always be on
        "off": "0 0 7 * * * *"
        # enables this processor, a disabled processor never gets created (default: true);
        enabled: true
        # delay before retrying after failed process (default: PT1M)
        # note that the delay for each consecutive failure doubles
        retryDelay: PT1M
        # additional variance for retryDelay as a +/- percentage (default: 0.5)
        retryJitter: 0.5
        # reprocess ready documents older than this age in ISO 8601 duration (default: null)
        # note if set to null, reprocessing will not happen
        reprocessAge: P7D
        # schedule for looking for documents that need to be reprocessed (default: PT1M)
        reprocessSchedule: PT1M
        # whether or not to always process requested documents (default: false)
        # often used in conjunction with save: false
        process: false
        # whether to wait for requested documents to be processed if not already (default: false)
        wait: false
        # any additional parameters needed to configure the client
        params:
          # ...
      harvest:
        # Class that implements edu.byu.hbll.box.Harvester (required)
        type: edu.byu.hbll.box.impl.View
        # enable the harvester, a disabled harvester never gets created (default: true)
        enabled: true
        # when to run the harvest (default: PT1M)
        schedule: PT1M
        # schedule for running a reharvest (default: null)
        reharvestSchedule: null
        # save new deleted documents (default: false)
        saveDeleted: false
        # any additional parameters needed to configure the harvester
        params:
          # ...
      db: # same as global database definition below, but specific to this source
        ...
      cursor: # overrides the database config for the harvest cursor only, if type is left blank or null, it will use the global database config, otherwise configuration is the same as the definition below
        ...
      web: # web specific settings
        endpoints:
          documents:
            # documents to be posted
            post:
              # enable documents to be posted (default: false)
              enabled: false
    # Source 2:
    # src2:
    # ...
  # Database definition (can be overridden per source)
  db:
    # class that implements edu.byu.hbll.box.BoxDatabase (default: edu.byu.hbll.box.impl.InMemoryDatabase)
    # other pre-implemented options: edu.byu.hbll.box.impl.MongoDatabase
    type: edu.byu.hbll.box.impl.InMemoryDatabase
    # any additional parameters needed to configure the database client
    params:
      # ...
```

Schedules can be periodic or cron in nature. For periodic, you may specify the period by number of millis or by an ISO 8601 duration. A cron schedule follows the specifications found at http://javadoc.io/doc/edu.byu.hbll/scheduler.

### JsonNode

You can configure Box by passing a JsonNode to the `Box` object just like when using the YAML file. IMPORTANT: Box expects the children of the `box` field in the YAML file example above to be at the root, not the `box` field.

```java
ObjectNode config = JsonNodeFactory.instance.objectNode();
...
Box box = Box.newBox(config);
```

### Programmatically

You can also configure Box programmatically using the builder.

```java
Box box = Box.newBuilder()...build();
```

## Vocabulary

### Source

Initial or seed data come from somewhere and in Box we refer to those as sources. A source exists of a processor or harvester or both. These two processors differ in the way data is queried and brought into the system. Each source in an application should have a unique name matching the pattern [0-9A-Za-z_].

#### Processor

A processor that only executes for a given ID when requested by another application.

#### Harvester

A harvester that processes new and updated documents on a schedule perhaps by processing updated documents from other sources.

## Implementations

Box comes pre-baked with several common implementations of its Processor, Harvester, and BoxDatabase interfaces.

### View

A Processor, Harvester, and Database implementation that acts as a read-only view into another source inside or outside the current box. Box sees a view as nothing more than a local source. Configuring a view properly is somewhat complicated, so Box supports templates for easily setting them up. It is also possible to use the View as separate components. For instance, you may only want the View for harvesting from another source in order to follow that source and fill the process queue.

```yaml
box:
  sources:
    mysrc:
      template:
        id: view
        params:
          uri: https://www.example.com/path/to/box

          # instead of hitting local sources through the web interface, you can specify the name
          # of the source here. This overrides anything in the uri field.
          local: mysrc

          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY, DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
```

#### Source Follower

View can be used as a source follower. Every document created, updated, or deleted at the upstream source results in a parallel document being processed in this source. For this you will not use the view template, but rather define View as just a Harvester alongside your Processor.

```yaml
box:
  sources:
    mysrc:
      process: ...
      harvest:
        type: edu.byu.hbll.box.impl.View
        params:
          uri: https://www.example.com/path/to/box
          harvestUnprocessed: true
```

### FacetView

Built on top of a View, the FacetView targets documents by facet instead of id. When querying the upstream source as a View would do, it translates the requested id query into a facet query. The documents in the response are are then a collection of documents representing the requested facet value.

```yaml
box:
  sources:
    mysrc:
      template:
        id: facetView
        params:
          uri: https://www.example.com/path/to/box
          facetName: the_facet_group_to_target

          # instead of hitting local sources through the web interface, you can specify the name
          # of the source here. This overrides anything in the uri field.
          local: mysrc

          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY, DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
          # limit number of documents included with each facet. Default: 10, No Limit: -1
          limit: 10
```

### Cached Views

Both the View and FacetViews can be used to cache upstream documents rather than viewing them live. Again templates are used for this:

```yaml
box:
  sources:
    mysrc:
      template:
        id: cachedView
        params:
          uri: https://www.example.com/path/to/box

          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY, DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
```

```yaml
box:
  sources:
    mysrc:
      template:
        id: cachedFacetView
        params:
          uri: https://www.example.com/path/to/box
          facetName: the_facet_group_to_target

          # optional params
          # fields to return for each document. Default: [null]
          fields: [field1, field2]
          # only include documents with these statuses. Default: [READY,DELETED]
          statuses: [READY, DELETED]
          # filter documents by facets; key value pairs in the form of "name:value". Default: [null]
          facets: []
          # whether or not to harvest documents from view as unprocessed so they'll get put on the process queue
          harvestUnprocessed: false
          # username for basic authentication. Default: null
          username: myusername
          # password to use with basic authentication. Default: null
          password: mypassword
```

### OAI

A Processor and Harvester that requests documents from OAI responders are implemented for you as the OaiProcessor and OaiHarvester. These processors take the following params:

```yaml
box:
  sources:
    process:
      type: edu.byu.hbll.box.impl.OaiProcessor
      params:
        # the URI of the OAI responder (required)
        uri: "https://www.example.com/oai"
        # The metadataPrefix to use (default: oai_dc)
        metadataPrefix: oai_dc
        # regex to convert the Box ID to the OAI ID
        idRegex: "^ojs\\."
        # regex to convert the Box ID to the OAI ID
        idReplacement: "oai:ojsspc.lib.byu.edu:"

    harvest:
      type: edu.byu.hbll.box.impl.OaiHarvester
      params:
        # the URI of the OAI responder (required)
        uri: "https://www.example.com/oai"
        # The metadataPrefix to use (default: oai_dc)
        metadataPrefix: oai_dc
        # Whether or not to harvest ids only instead of full records (default: false)
        idsOnly: false
        # Whether or not to query one month at a time (default: false)
        chunked: false
        # regex to convert the Box ID to the OAI ID (default: null)
        idRegex: "^ojs\\."
        # regex to convert the Box ID to the OAI ID (default: null)
        idReplacement: "oai:ojsspc.lib.byu.edu:"
```

### MongoDB

MongoDB implementation of BoxDatabase.

```yaml
box:
  db:
    type: edu.byu.hbll.box.impl.MongoDatabase
    params:
      # MongoDB Connection String URI (default: mongodb://localhost/) See https://docs.mongodb.com/manual/reference/connection-string/
      uri: mongodb://localhost/
      # Database name for this instance of box (required)
      database: box
```

## In Memory Database

An implementation of BoxDatabase that holds all data in memory. This is the default.

```yaml
box:
  db:
    type: edu.byu.hbll.box.impl.InMemoryDatabase
```

## PostgreSQL

PostgreSQL implementation of BoxDatabase.

```yaml
box:
  db:
    type: edu.byu.hbll.box.impl.PostgresDatabase
    params:
      # [PostgreSQL Connection URI](https://jdbc.postgresql.org/documentation/use/#connecting-to-the-database) (default: none)
      uri: jdbc:postgresql://localhost/db?user=username&password=password
```

### Spring Boot

As described in the API section, a `box-spring-boot` module exists. This allows Box to be accessed via the web and Box to exist in the Spring ecosystem.

The `box-spring-boot` module comes with auto-configuration for Box. The Box configuration as shown in the YAML configuration above must be found at `byuhbll.box` in the Spring properties.

The module also allows Spring Beans to be used for Box objects. To tell Box to use a bean rather than instantiate a new object of the given type, the prefix `bean:` must be added to the `type` field in the configuration.

## API

To enable the box web service API, the `box-spring-boot` can be included as dependency in the POM. Note: The application must be a spring-boot application for this to run. The box API path begins after the application's context root.

### Multiple Documents and Harvesting

Path

    GET /box/documents
    GET /{source}/documents

Path parameters

source : the source name (type: string, default: none)

Query parameters for multiple documents

    id             : filter by document id (type: string, default: null, multiple: true)
    wait           : wait for documents to process (type: boolean, default: false)
    process        : (re)process the documents (type: boolean, default: false)
    field          : only return these document fields (type: string, default: null, multiple: true)

Query parameters for harvesting

    from           : modified since (type: ISO Date, default: null)
    cursor         : modified since (type: long, default: 0)
    offset         : start from this offset (type: long, default: 0)
    order          : order documents by asc or desc (type: enum, default: asc)
    limit          : page size (type: int, default: 10, no limit: -1)
    status         : include only documents with these statuses (type: string, default: ready,deleted)

    facet          : filter by facet, OR logic within a facet group, AND logic between groups (type: string in the form `key:value`, default: null, multiple: true)

Common query parameters

    field          : only return these fields in dot notation, `@doc` is an implicit field which excludes the metadata (type: string, default: null, multiple: true)

### Single Documents

Path

    GET /box/documents/{id}
    GET /{source}/documents/{id}

Path parameters

source : the source name (type: string, default: none)
id : the document id (type: string, default: none)

Query parameters

    wait           : wait for documents to process (type: boolean, default: false)
    process        : (re)process the documents (type: boolean, default: false)
    field          : only return these fields in dot notation, `@doc` is an implicit field which excludes the metadata (type: string, default: null, multiple: true)

### Administration

Administration is done through web endpoints when the `box-spring-boot` module is present. Box uses the Spring Boot Actuator framework for all of its admin endpoints. The paths below will need to be appended to the actuator base uri.

#### Reprocess ready documents

Path

    GET /box/{source}/reprocess

Path parameters

source : the source name (type: string, default: none)

Query parameters

    age    : min age of documents (type: ISO 8601 duration, default: PT0S)

#### Trigger harvest

Path

    GET /box/{source}/harvest

Path parameters

source : the source name (type: string, default: none)

#### Clear Database

Path

    GET /box/{source}/clear

Path parameters

source : the source name; `all` is a reserved source name referring to all sources (type: string, default: none)

### Stats (experimental)

Path

    GET /box/{source}/stats

Path parameters

source : the source name (type: string, default: none)

## Security

Box itself does not provide any security for its web services; however, you can add your own if needed. You can setup security in your web.xml and application server. To do basic authentication, for example, create a src/main/webapp/WEB-INF/web.xml file with the following components:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.jcp.org/xml/ns/javaee"
  xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd" id="WebApp_ID"
  version="3.1">
  <security-constraint>
    <web-resource-collection>
      <web-resource-name>box</web-resource-name>
      <url-pattern>/box/admin/*</url-pattern>
    </web-resource-collection>
    <auth-constraint>
      <role-name>boxAdmin</role-name>
    </auth-constraint>
  </security-constraint>
  <login-config>
    <auth-method>BASIC</auth-method>
    <realm-name>jdbcRealm</realm-name>
  </login-config>
</web-app>
```

## ID Only Harvest

At times a harvester is only able to indicate the IDs of the documents, but not process the documents itself. In this case, a direct processor must exist to actually process the documents according the gathered IDs. To indicate that the harvester is only sending IDs and not documents, do not set the document.

```java
new ResultDocument(id);
```

## Orphaned Documents

Generally documents that should be deleted are explicitly marked so in the `BoxDocument`. There are times; however, that the source alone cannot determine when a document should be deleted. This can happen for `Processor` in which case the `reprocessSchedule` and `reprocessAge` directives become vital. When those are set, Box will attempt to reprocess ready documents on a schedule allowing the `Processor` to mark the document deleted when appropriate.

This can also occur for a `Harvester` that only knows about new or updated documents, but not deletes. If the `Harvester` handled this itself, it would have to perform a diff of the documents that SHOULD exist against the documents that DO exist. Thankfully, Box facilitates this diff and will clean up orphan documents for you. This is done by using the `startGroup` and `endGroup` directives. At the beginning of a fresh harvest, mark the start of the group. When the harvest completes, mark the end of the group. Box will then delete any documents belonging to that group that were not processed during that time.

Orphaned documents can also occur when an input document splits into multiple output documents. The child documents that get created can change over time leaving orphaned documents. Box can automatically clean these up by using the group mechanism. Basically the processor that splits the document will signal the beginning of the group and then later signal the end of the group. Any existing documents belonging to the same group that were not processed during the window will be deleted. This is how it's typically done:

```java
// code to split a parent document into multiple child documents
ProcessResult result = new ProcessResult();
result.startGroup(parentId);
result.add(childDocument1.groupId(parentId));
result.add(childDocument2.groupId(parentId));
result.add(childDocument3.groupId(parentId));
result.endGroup(parentId);
// mark the parent document as deleted so it's removed from the process queue
result.add(new BoxDocument(parentId).setAsDeleted());
```

Note that the start and end group signals do not need to be in the same ProcessResult or HarvestResult. They can traverse long periods of time and even restarts.

Note that scheduled reprocessing or direct processing of these child documents can occur. The Processor will need to be able to handle this when a request is received to (re)process a child document.

## Dependencies

Box manages dependencies for you when a document is dependent on documents from other sources. These dependencies are set during process.

```java
new BoxDocument(id).addDependency(dependencySourceName, dependencyId);
```

Box will asynchronously gather these dependencies and hand them to the Processor along with the document to be processed. It reprocesses the document each time a dependency is made available or updated.

Box also allows dependencies to be collected before processing. Sometimes it's necessary to gather dependencies before the document can be processed. For this case, set the id of the document and its dependencies, but don't set the document itself. It needs to remain in an UNPROCESSED state.

## Important

A circular dependency involving only views will lock up the applications involved. So don't create a circular dependency circle of just views. Even one non-view in there would be fine. It's only if they're ALL views.

Requested documents that do not exist at a harvest only source (ie, do not have a processor or processor is disabled) will automatically be marked as deleted in the response.

An unprocessed document cannot overwrite a processed document. This aligns with the philosophy of not having documents regress. A processed document's dependencies however can be updated by an unprocessed document.

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)