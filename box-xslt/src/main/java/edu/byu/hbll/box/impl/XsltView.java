package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.misc.Resources;
import edu.byu.hbll.xml.XmlJsonConverter;
import edu.byu.hbll.xml.XmlJsonSchema;
import edu.byu.hbll.xslt.XslTransformer;
import edu.byu.hbll.xslt.plugins.Functions;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * A view that retrieves documents from another Box and transforms them using XSLT. An optional XSD
 * may be used for the source and resulting XML in order to help in the translation of json to xml
 * and back. It orders, filters, and renames fields (see
 * https://javadoc.io/doc/edu.byu.hbll/xml/latest/edu/byu/hbll/xml/XmlJsonConverter.html).
 *
 * <p>Configuring the XsltView using the config file. The XsltView extends {@link View} so all
 * parameters used for {@link View} are also used for the {@link XsltView}. Additional ones include:
 *
 * <ul>
 *   <li>xsltFilePath: path to the xsl file to compile (relative to xsltDirectoryResourceName if
 *       specified)
 *   <li>xsltDirectoryPath: path to the xsl directory to watch for changes
 *   <li>xsltDirectoryResourceName: takes the place of xsltDirectoryPath if null, extracts this
 *       directory from the classpath to be used for the xsl
 *   <li>sourceXsdFilePath: path to an XSD file for the source
 *   <li>sourceXsdResourceName: classpath resource to be used as the source XSD
 *   <li>resultXsdFilePath: path to an XSD file for the result
 *   <li>resultXsdResourceName: classpath resource to be used as the result XSD
 *   <li>newMethod: use new method of batching if true
 * </ul>
 *
 * <p>The source and result XML will follow the form and behavior of {@link
 * View#transform(ProcessBatch)} very closely. This is a new method of handling the XSLT transform
 * therefore you must specify <code>newMethod : true</code> in order to use this form rather than
 * the deprecated one.
 *
 * <p>The source document will be of the form:
 *
 * <pre>
 * &lt;batch&gt;
 *   &lt;context&gt; &lt;-- repeatable --&gt;
 *     &lt;sourceName&gt;&lt;-- name of the source --&gt;&lt;/sourceName&gt;
 *     &lt;id&gt;&lt;-- id of the document --&gt;&lt;/id&gt;
 *     &lt;document&gt;
 *       &lt;!-- the upstream box document in serialized form --&gt;
 *     &lt;/document&gt;
 *     &lt;dependency&gt; &lt;-- repeatable --&gt;
 *       &lt;sourceName&gt;&lt;-- name of the dependency's source --&gt;&lt;/sourceName&gt;
 *       &lt;id&gt;&lt;-- id of the dependency --&gt;&lt;/id&gt;
 *       &lt;document&gt;&lt;-- dependency box document in serialized form --&gt;&lt;/document&gt;
 *     &lt;/dependency&gt;
 *   &lt;/context&gt;
 * &lt;/batch&gt;
 * </pre>
 *
 * <p>The resulting document should be of the form:
 *
 * <pre>
 * &lt;documents&gt;
 *   &lt;document&gt; &lt;-- repeatable --&gt;
 *     &lt;!--
 *       the resulting document in serialized form
 *       note that <code>@box</code> was changed to <code>_box</code> for XML
 *       also plural metadata fields should be in their singular form
 *       (ie, facet instead of facets, dependency instead of dependencies)
 *     --&gt;
 *   &lt;/document&gt;
 * &lt;/documents&gt;
 * </pre>
 *
 * <p>Below is the older form. In order to start using the newer form, you must set <code>
 * newMethod : true</code>
 *
 * <p>DEPRECATED: The source document will be of the form:
 *
 * <pre>
 * &lt;source&gt;
 *   &lt;id&gt;&lt;-- id of the document --&gt;&lt;/id&gt;
 *   &lt;document&gt;
 *     &lt;!-- the source document --&gt;
 *   &lt;/document&gt;
 *   &lt;facet&gt;
 *     &lt;-- optional Box facet to accompany the document (repeatable) --&gt;
 *     &lt;name&gt;&lt;-- name of the facet --&gt;&lt;/name&gt;
 *     &lt;value&gt;&lt;-- value of the facet --&gt;&lt;/value&gt;
 *   &lt;/facet&gt;
 *   &lt;dependency&gt;
 *     &lt;-- optional Box dependency to accompany the document (repeatable) --&gt;
 *     &lt;sourceName&gt;&lt;-- sourceName of the dependency --&gt;&lt;/sourceName&gt;
 *     &lt;id&gt;&lt;-- id of the dependency --&gt;&lt;/id&gt;
 *     &lt;document&gt;&lt;-- the dependency document --&gt;&lt;/document&gt;
 *   &lt;/dependency&gt;
 * &lt;/source&gt;
 * </pre>
 *
 * <p>DEPRECATED: The resulting document should be of the form:
 *
 * <pre>
 * &lt;result&gt;
 *   &lt;document&gt;
 *     &lt;!-- the resulting document --&gt;
 *   &lt;/document&gt;
 *   &lt;facet&gt;
 *     &lt;-- optional Box facet to accompany the document (repeatable) --&gt;
 *     &lt;name&gt;&lt;-- name of the facet group --&gt;&lt;/name&gt;
 *     &lt;value&gt;&lt;-- value of the facet --&gt;&lt;/value&gt;
 *   &lt;/facet&gt;
 *   &lt;dependency&gt;
 *     &lt;-- optional Box dependency to accompany the document (repeatable) --&gt;
 *     &lt;sourceName&gt;&lt;-- sourceName of the dependency --&gt;&lt;/sourceName&gt;
 *     &lt;id&gt;&lt;-- id of the dependency --&gt;&lt;/id&gt;
 *   &lt;/dependency&gt;
 * &lt;/result&gt;
 * </pre>
 *
 * @author Charles Draper
 */
@SuperBuilder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
public class XsltView extends View {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /** The XSLT Transformer used to transform the documents. */
  private XslTransformer transformer;

  /** The source schema, describes how to convert the JSON to XML pre-transformation. */
  private XmlJsonSchema sourceDocumentSchema;

  /** The result schema, describes how to convert the XML to JSON post-transformation. */
  private XmlJsonSchema resultDocumentSchema;

  /** The source schema, describes how to convert the JSON to XML pre-transformation. */
  private XmlJsonSchema sourceSchema;

  @Deprecated private XmlJsonSchema sourceSchemaOld;

  /** The result schema, describes how to convert the XML to JSON post-transformation. */
  private XmlJsonSchema resultSchema;

  @Deprecated private XmlJsonSchema resultSchemaOld;

  /** The XSLT to use. */
  private File xsltFile;

  /** Watch this directory for changes and reload the XSLT. */
  private File xsltDir;

  /** Transform the JSON document to XML using these rules. */
  private File sourceXsdFile;

  /** Transform the resulting XML document to JSON using these rules. */
  private File resultXsdFile;

  /** Whether or not to use the new method of batching documents. */
  private boolean newMethod;

  /** Saxon extension functions. */
  @Builder.Default private Collection<? extends ExtensionFunctionDefinition> functions = List.of();

  /**
   * Creates a new {@link XsltView}.
   *
   * @param baseUri base Box URI for the source documents
   * @param xsltFile the XSLT to use
   * @deprecated user builder
   */
  @Deprecated
  public XsltView(URI baseUri, File xsltFile) {
    this(baseUri, xsltFile, null, null, null);
  }

  /**
   * Creates a new {@link XsltView}.
   *
   * @param baseUri base Box URI for the source documents
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @deprecated user builder
   */
  @Deprecated
  public XsltView(
      URI baseUri, File xsltFile, File xsltDir, File sourceXsdFile, File resultXsdFile) {
    super(baseUri);
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, Collections.emptyList());
  }

  /**
   * Creates a new {@link XsltView}.
   *
   * @param baseUri base Box URI for the source documents
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @param functions additional extension functions to include
   * @deprecated user builder
   */
  @Deprecated
  public XsltView(
      URI baseUri,
      File xsltFile,
      File xsltDir,
      File sourceXsdFile,
      File resultXsdFile,
      Collection<? extends ExtensionFunctionDefinition> functions) {
    super(baseUri);
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, functions);
  }

  /**
   * Creates a new {@link XsltView}.
   *
   * @param source this source
   * @param localSource upstream source
   * @param xsltFile the XSLT to use
   * @deprecated user builder
   */
  @Deprecated
  public XsltView(Source source, Source localSource, File xsltFile) {
    this(source, localSource, xsltFile, null, null, null);
  }

  /**
   * Creates a new {@link XsltView}.
   *
   * @param source this source
   * @param localSource upstream source
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @deprecated user builder
   */
  @Deprecated
  public XsltView(
      Source source,
      Source localSource,
      File xsltFile,
      File xsltDir,
      File sourceXsdFile,
      File resultXsdFile) {
    super(source, localSource);
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, Collections.emptyList());
  }

  /**
   * Creates a new {@link XsltView}.
   *
   * @param source this source
   * @param localSource upstream source
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @param functions additional extension functions to include
   * @deprecated user builder
   */
  @Deprecated
  public XsltView(
      Source source,
      Source localSource,
      File xsltFile,
      File xsltDir,
      File sourceXsdFile,
      File resultXsdFile,
      Collection<? extends ExtensionFunctionDefinition> functions) {
    super(source, localSource);
    init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, functions);
  }

  @Override
  public void postConstruct(ConstructConfig config) {
    super.postConstruct(config);

    BoxParams params = new BoxParams();
    config.bind(params);

    try {
      File xsltFile = params.getXsltFilePath();
      File xsltDir = params.getXsltDirectoryPath();

      if (xsltDir == null && params.getXsltDirectoryResourceName() != null) {
        xsltDir = Resources.extract(params.getXsltDirectoryResourceName()).toFile();
      }

      if (xsltDir != null && params.getXsltDirectoryPath() == null) {
        // if xsltDir is an extracted resource, then xslt file is relative to it
        xsltFile = xsltDir.toPath().resolve(xsltFile.toPath()).toFile();
      }

      File sourceXsdFile = params.getSourceXsdFilePath();

      if (sourceXsdFile == null && params.getSourceXsdResourceName() != null) {
        sourceXsdFile = Resources.extract(params.getSourceXsdResourceName()).toFile();
      }

      File resultXsdFile = params.getResultXsdFilePath();

      if (resultXsdFile == null && params.getResultXsdResourceName() != null) {
        resultXsdFile = Resources.extract(params.getResultXsdResourceName()).toFile();
      }

      newMethod = params.newMethod;

      init(xsltFile, xsltDir, sourceXsdFile, resultXsdFile, Collections.emptyList());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * Sets parameters and initializes the transformer and schema.
   *
   * @param xsltFile the XSLT to use
   * @param xsltDir optionally watch this directory for changes and reload the XSLT (null allowed)
   * @param sourceXsdFile optionally transform resulting JSON to XML pre-transformation (null
   *     allowed)
   * @param resultXsdFile optionally transform resulting XML to JSON using these rules (null
   *     allowed)
   * @param functions additional extension functions to include
   * @return this
   */
  private XsltView init(
      File xsltFile,
      File xsltDir,
      File sourceXsdFile,
      File resultXsdFile,
      Collection<? extends ExtensionFunctionDefinition> functions) {
    this.xsltFile = xsltFile;
    this.xsltDir = xsltDir;
    this.sourceXsdFile = sourceXsdFile;
    this.resultXsdFile = resultXsdFile;
    this.functions = functions;
    init();
    return this;
  }

  /**
   * Initializes the transformer and schema.
   *
   * @return this
   */
  private XsltView init() {
    Collection<ExtensionFunctionDefinition> allFunctions = new ArrayList<>();
    allFunctions.addAll(Functions.newFunctions());
    allFunctions.addAll(functions());
    allFunctions.addAll(functions);

    if (transformer == null) {
      transformer =
          XslTransformer.builder()
              .file(xsltFile.toPath())
              .watchDirs(xsltDir == null ? List.of() : List.of(xsltDir.toPath()))
              .functions(allFunctions)
              .build();
    }

    sourceSchema = loadSchema("source.xsd");
    sourceSchemaOld = new XmlJsonSchema("source");

    if (sourceDocumentSchema == null) {
      if (sourceXsdFile != null) {
        sourceDocumentSchema = XmlJsonSchema.parseXsd(new StreamSource(sourceXsdFile));
      } else {
        sourceDocumentSchema = new XmlJsonSchema();
      }
    }

    sourceDocumentSchema.setName("document");
    sourceDocumentSchema.setXmlName("document");
    sourceSchemaOld.add(sourceDocumentSchema);

    sourceDocumentSchema
        .getChildSchemas()
        .forEach(s -> sourceSchema.getChildSchema("documents").add(s));

    resultSchema = loadSchema("result.xsd");
    resultSchemaOld = new XmlJsonSchema("result");

    if (resultDocumentSchema == null) {
      if (resultXsdFile != null) {
        resultDocumentSchema = XmlJsonSchema.parseXsd(new StreamSource(resultXsdFile));
      } else {
        resultDocumentSchema = new XmlJsonSchema();
      }
    }

    resultDocumentSchema.setName("document");
    resultDocumentSchema.setXmlName("document");
    resultSchemaOld.add(resultDocumentSchema);

    resultSchemaOld.add(new XmlJsonSchema("facet").array());
    resultSchemaOld.add(new XmlJsonSchema("dependency").array());

    resultDocumentSchema
        .getChildSchemas()
        .forEach(s -> resultSchema.getChildSchema("documents").add(s));

    return this;
  }

  @Override
  protected List<BoxDocument> transform(ProcessBatch batch) {
    List<BoxDocument> documents = new ArrayList<>();

    if (this.newMethod) {
      ObjectNode batchNode = mapper.createObjectNode();

      for (ProcessContext processContext : batch) {
        String id = processContext.getId();
        BoxDocument sourceDocument = processContext.getDependency(getSourceName(), id);

        ObjectNode contextNode = batchNode.withArray("contexts").addObject();
        contextNode.put("sourceName", getSourceName());
        contextNode.put("id", id);
        contextNode.set("document", sourceDocument.toJson());

        for (DocumentId documentId : processContext.getDependencies().keySet()) {
          BoxDocument dependency = processContext.getDependency(documentId);

          // do not include the source document as a dependency
          if (dependency != sourceDocument) {
            ObjectNode dependencyNode = contextNode.withArray("dependencies").addObject();
            dependencyNode.put("sourceName", documentId.getSourceName());
            dependencyNode.put("id", documentId.getId());
            dependencyNode.set("document", dependency.toJson());
          }
        }
      }

      Document sourceXml = XmlJsonConverter.toXmlDoc(batchNode, sourceSchema);

      // transform the source according to the XSLT rules
      Document transformedXml;

      try {
        transformedXml = transform(sourceXml, Map.of());

        // convert the transformed result back to a JSON document
        ObjectNode results = XmlJsonConverter.toJsonDoc(transformedXml, resultSchema);

        for (JsonNode document : results.path("documents")) {
          documents.add(BoxDocument.parse((ObjectNode) document));
        }
      } catch (TransformerException e) {
        // if batch fails, process each one individually
        // we don't want one bad apple ruining the whole bushel
        if (batch.size() > 1) {
          for (ProcessContext processContext : batch) {
            documents.addAll(transform(new ProcessBatch(List.of(processContext))));
          }
        } else {
          documents.add(new BoxDocument(batch.get(0).getId()).setAsError(e.getMessage()));
        }
      }
    } else {
      // deprecated method
      for (ProcessContext context : batch) {
        documents.add(transform(context));
      }
    }

    return documents;
  }

  /**
   * Transforms one document at a time.
   *
   * @param processContext the source document and metadata
   * @return the resulting transformed document
   * @deprecated use {@link #transform(ProcessBatch)} instead
   */
  @Deprecated
  protected BoxDocument transform(ProcessContext processContext) {
    String id = processContext.getId();
    BoxDocument sourceDocument = processContext.getDependency(getSourceName(), id);
    BoxDocument resultDocument = new BoxDocument(id);

    if (sourceDocument.isDeleted()) {
      resultDocument.setAsDeleted();
    } else if (sourceDocument.isReady()) {
      try {

        // create source wrapper document
        ObjectNode source = JsonNodeFactory.instance.objectNode().put("id", id);
        source.set("document", sourceDocument.getDocument());

        // add facets to source wrapper document
        for (Facet facet : sourceDocument.getFacets()) {
          source
              .withArray("facet")
              .addObject()
              .put("name", facet.getName())
              .put("value", facet.getValue());
        }

        for (DocumentId documentId : processContext.getDependencies().keySet()) {
          BoxDocument dependency = processContext.getDependency(documentId);

          // do not include the source document as a dependency
          if (dependency != sourceDocument && !dependency.isDeleted()) {
            source
                .withArray("dependency")
                .addObject()
                .put("sourceName", documentId.getSourceName())
                .put("id", documentId.getId())
                .set("document", dependency.getDocument());
          }
        }

        // convert source wrapper document to XML
        Document sourceXml = XmlJsonConverter.toXmlDoc(source, sourceSchemaOld);

        // add default parameters
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("id", id);
        params.put("sourceName", getSourceName());

        // transform the source according to the XSLT rules
        Document transformedXml = transform(sourceXml, params);

        Element rootElement = transformedXml.getDocumentElement();

        if (rootElement == null) {
          throw new IllegalArgumentException(
              "transformed result is blank, it must contain a root element of <result>");
        }

        if (!rootElement.getLocalName().equals("result")) {
          throw new IllegalArgumentException(
              "transformed result must contain a root element of <result>");
        }

        // convert the transformed result back to a JSON document
        ObjectNode document = XmlJsonConverter.toJsonDoc(rootElement, resultSchemaOld);

        if (document.path("document").isObject()) {
          resultDocument.setDocument((ObjectNode) document.path("document"));
        } else {
          throw new IllegalArgumentException(
              "root element <result> must contain a <document> element even if empty");
        }

        addDependencies(resultDocument, document.path("dependency"));
        addFacets(resultDocument, document.path("facet"));
      } catch (Exception e) {
        log.error("Error transforming " + id + ": " + e, e);
        resultDocument.setAsError(e.toString());
      }
    }

    return resultDocument;
  }

  /**
   * Transforms the source xml into the result xml.
   *
   * @param sourceXml the source xml
   * @param params any parameters to send to the Transformer
   * @return the transformed document
   * @throws TransformerException if an error occurs with the transformation
   */
  protected Document transform(Document sourceXml, Map<String, ? extends Object> params)
      throws TransformerException {
    return transformer.transform(sourceXml, params);
  }

  /**
   * Adds all dependencies from the transformed document to the given resultDocument.
   *
   * @param resultDocument the resultDocument
   * @param dependencies the dependencies to add
   */
  private void addDependencies(BoxDocument resultDocument, JsonNode dependencies) {
    for (JsonNode dependency : dependencies) {
      resultDocument.addDependency(
          dependency.path("sourceName").asText(null), dependency.path("id").asText(null));
    }
  }

  /**
   * Adds all facets from the transformed document to the given resultDocument.
   *
   * @param resultDocument the resultDocument
   * @param facets the facets to add
   */
  private void addFacets(BoxDocument resultDocument, JsonNode facets) {
    for (JsonNode facet : facets) {
      resultDocument.addFacet(facet.path("name").asText(null), facet.path("value").asText(null));
    }
  }

  /**
   * Child classes can specify additional extension functions to include with the Saxon Transformer.
   * This is called exactly once during the initialization of the class.
   *
   * @return a collection of additional extensions functions to be included with the transformer
   */
  protected Collection<? extends ExtensionFunctionDefinition> functions() {
    return Collections.emptyList();
  }

  private XmlJsonSchema loadSchema(String resourceName) {
    try {
      return XmlJsonSchema.parseXsd(
          new StreamSource(Resources.getResourceUrl(resourceName).openStream()));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  @Override
  public void preDestroy() {
    try {
      super.preDestroy();
    } catch (Exception e) {
      log.warn(e.toString());
    }

    transformer.close();
  }

  @Data
  private static class BoxParams {
    private File xsltFilePath;
    private File xsltDirectoryPath;
    private String xsltDirectoryResourceName;
    private File sourceXsdFilePath;
    private String sourceXsdResourceName;
    private File resultXsdFilePath;
    private String resultXsdResourceName;
    private boolean newMethod;
  }

  /** Needed for javadoc to succeed (https://stackoverflow.com/a/58809436/1530184). */
  @java.lang.SuppressWarnings("all")
  @lombok.Generated
  public abstract static class XsltViewBuilder<C extends XsltView, B extends XsltViewBuilder<C, B>>
      extends View.ViewBuilder<C, B> {

    @java.lang.SuppressWarnings("all")
    @lombok.Generated
    private B sourceSchema(final XmlJsonSchema sourceSchema) {
      return self();
    }

    @java.lang.SuppressWarnings("all")
    @lombok.Generated
    private B resultSchema(final XmlJsonSchema resultSchema) {
      return self();
    }
  }

  /** Override of the lombok builder impl. */
  @java.lang.SuppressWarnings("all")
  @lombok.Generated
  private static final class XsltViewBuilderImpl
      extends XsltView.XsltViewBuilder<XsltView, XsltView.XsltViewBuilderImpl> {
    @java.lang.Override
    @java.lang.SuppressWarnings("all")
    @lombok.Generated
    public XsltView build() {
      return new XsltView(this).init();
    }
  }
}
