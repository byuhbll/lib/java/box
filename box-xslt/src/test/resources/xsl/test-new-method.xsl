<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:output
    indent="yes"
    method="xml"
    encoding="UTF-8" />
    
  <xsl:template match="/">
    <documents>
      <xsl:apply-templates />    
    </documents>
  </xsl:template>
  
  <xsl:template match="context">
    <document>
      <xsl:copy-of select="/" />
      <_box>
        <xsl:copy-of select="document/_box/id|document/_box/status"/>
        <facet>
          <name>facet1</name>
          <value>value1</value>
        </facet>
        <dependency>
          <sourceName>principal</sourceName>
          <id>dep1</id>
        </dependency>
        <groupId>groupId1</groupId>
      </_box>
    </document>
    <xsl:if test="id='bad'"><xsl:message terminate="yes"/></xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
