<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:output
    indent="yes"
    method="xml"
    encoding="UTF-8" />
  
  <xsl:template match="source">
    <result>
      <document>
        <xsl:apply-templates select="document" />
      </document>
      <xsl:if test="id='testSourceFacets'">
        <xsl:copy-of select="facet" />
      </xsl:if>
      <xsl:if test="document/id='2'">
        <dependency><sourceName>xsltview</sourceName><id>1</id></dependency>
      </xsl:if>
    </result>
  </xsl:template>
  
  <xsl:template match="document">
    <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="id">
    <xsl:copy-of select="." />
  </xsl:template>
  
  <xsl:template match="a">
    <b><xsl:value-of select="replace(.,'^a','b')" /></b>
  </xsl:template>
  
</xsl:stylesheet>
