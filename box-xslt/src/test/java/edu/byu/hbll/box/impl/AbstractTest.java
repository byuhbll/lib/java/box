package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.json.YamlLoader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;

/**
 * @author Charles Draper
 */
public class AbstractTest {

  protected static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  protected Box box;
  protected Source source;

  void init(String... sourceNames) {
    Set<String> enabled = Set.of(sourceNames);
    ObjectNode boxConfig = (ObjectNode) loadYaml();

    for (JsonField source : new JsonField(boxConfig.path("sources"))) {
      if (!enabled.contains(source.getKey())) {
        ((ObjectNode) source.getValue()).put("enabled", false);
      }
    }

    box = Box.newBox(boxConfig);

    if (sourceNames.length != 0) {
      source = getSource(sourceNames[0]);
    }

    // make sure primary is established (fast for memory database)
    sleep(100);
  }

  @AfterEach
  void tearDown() {
    box.close();
  }

  protected Source getSource(String sourceName) {
    return box.getSource(sourceName);
  }

  protected JsonNode loadYaml() {
    try {
      return new YamlLoader()
          .load(new InputStreamReader(this.getClass().getResourceAsStream("/box.yml")));
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  protected void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
    }
  }
}
