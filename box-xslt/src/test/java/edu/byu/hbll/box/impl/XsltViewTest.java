package edu.byu.hbll.box.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Facet;
import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the {@link Box} api. This does not test all the various intricate settings and timings of
 * Box. That is done in the box-test module.
 *
 * @author Charles Draper
 */
class XsltViewTest extends AbstractTest {

  @BeforeEach
  void beforeEach() {
    init("xsltview", "principal");
  }

  /**
   * This test is a result of a bug where the XsltView was not transforming the correct document if
   * that document was dependent on another document in the same source. Instead it was actually
   * transforming the dependency document. This is because it was looking up the document it needed
   * to transform using processContext.getFirstDependency(sourceName). Sometimes the dependency
   * showed up as the first dependency.
   */
  @Test
  public void testCorrectDependencyIsTransformed() {
    for (int i = 1; i <= 2; i++) {
      BoxDocument doc = new BoxDocument(i + "");
      doc.withDocument().put("id", i);
      box.getSource("principal").save(doc);
    }

    // verify document is fully processed
    source.collect(new BoxQuery("2").setProcess(true));
    source.collect(new BoxQuery("2").setProcess(true));

    // verify document no longer changes after being fully processed (document will toggle if bug
    // still present)
    Instant start = Instant.now();
    source.collect(new BoxQuery("2").setProcess(true));
    BoxDocument doc2 = source.collect(new BoxQuery("2")).get(0);
    assertTrue(doc2.getModified().get().compareTo(start) < 0);
  }

  /** Tests that facets are being passed as part of the source document. */
  @Test
  public void testSourceFacets() {
    BoxDocument doc =
        new BoxDocument("testSourceFacets").addFacet("name1", "value1").addFacet("name2", "value2");
    doc.withDocument().put("id", "testSourceFacets");
    box.getSource("principal").save(doc);

    // verify facets were passed from source to result
    BoxDocument result = source.collect(new BoxQuery("testSourceFacets").setProcess(true)).get(0);
    List<Facet> facets = new ArrayList<>(result.getFacets());
    assertEquals("name1", facets.get(0).getName());
    assertEquals("value1", facets.get(0).getValue());
    assertEquals("name2", facets.get(1).getName());
    assertEquals("value2", facets.get(1).getValue());
  }

  /** Tests the view builder. */
  @Test
  public void testBuilder() {
    for (int i = 1; i <= 2; i++) {
      BoxDocument doc = new BoxDocument(i + "");
      doc.withDocument().put("id", i);
      box.getSource("principal").save(doc);
    }

    XsltView view =
        XsltView.builder()
            .baseSource(box.getSource("principal"))
            .xsltFile(new File("src/test/resources/test.xsl"))
            .build();

    assertEquals(2, view.find(new BoxQuery()).size());
  }

  /** Bug: Verify that the sourceXsdFile and resultXsdFile are being loaded properly. */
  @Test
  public void testXsdFiles() {
    BoxDocument doc = new BoxDocument("1");
    doc.withDocument().put("id", "1").withArray("as").add("a1");
    getSource("principal").save(doc);

    String expected = "{\"id\":\"1\",\"bs\":[\"b1\"]}";
    ObjectNode result = source.process("1").getDocument();
    assertEquals(expected, result.toString());
  }

  /** Tests that the XSL and XSD can be classpath resources. */
  @Test
  public void testXslXsdAsResources() {
    init("xsltviewresources", "principal");
    BoxDocument doc = new BoxDocument("1");
    doc.withDocument().put("id", "1").withArray("as").add("a1");
    getSource("principal").save(doc);

    String expected = "{\"id\":\"1\",\"bs\":[\"b1\"]}";
    ObjectNode result = source.process("1").getDocument();
    assertEquals(expected, result.toString());
  }

  /** Tests the new batching method. */
  @Test
  public void testNewMethod() {
    init("xsltviewnewmethod", "principal");

    BoxDocument doc = new BoxDocument("1");
    doc.withDocument().put("id", "1");
    getSource("principal").save(doc);

    getSource("principal").save(new BoxDocument("dep1").setAsReady());
    getSource("principal").save(new BoxDocument("dep2").setAsReady());

    source.process("1");
    source.process("1");

    String expected =
        "{\"batch\":{\"context\":{\"sourceName\":\"xsltviewnewmethod\",\"id\":\"1\",\"document\":{\"id\":\"1\",\"_box\":{\"id\":\"1\",\"status\":\"READY\",\"cursor\":\"REMOVED\",\"modified\":\"REMOVED\",\"processed\":\"REMOVED\"}},\"dependency\":{\"sourceName\":\"principal\",\"id\":\"dep1\",\"document\":{\"_box\":{\"id\":\"dep1\",\"status\":\"READY\",\"cursor\":\"REMOVED\",\"modified\":\"REMOVED\",\"processed\":\"REMOVED\"}}}}},\"@box\":{\"id\":\"1\",\"status\":\"READY\",\"cursor\":\"REMOVED\",\"modified\":\"REMOVED\",\"processed\":\"REMOVED\",\"facets\":[{\"name\":\"facet1\",\"value\":\"value1\"}],\"dependencies\":[{\"sourceName\":\"principal\",\"id\":\"dep1\"}],\"groupId\":\"groupId1\"}}";
    String actual =
        source
            .get("1")
            .toString()
            .replaceAll("\"(cursor|modified|processed)\":\"[^\"]+\"", "\"$1\":\"REMOVED\"");

    assertEquals(expected, actual);
  }

  /**
   * Tests the new batching method will reprocess documents individually if one causes the batch to
   * fail.
   */
  @Test
  public void testNewMethodReprocessesIndividually() {
    init("xsltviewnewmethod", "principal");

    getSource("principal").save(new BoxDocument("1").setAsReady());
    getSource("principal").save(new BoxDocument("bad").setAsReady());

    List<BoxDocument> result = source.process("1", "bad");

    assertTrue(result.get(0).isReady());
    assertTrue(result.get(1).isError());
  }
}
