package edu.byu.hbll.box.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.box.BoxQuery;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import java.net.URI;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.web.client.RestTemplate;

/**
 * @author Charles Draper
 */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class SpringBoxClientIT {

  @Test
  public void testSpringClient() {
    URI uri = URI.create("http://www.example.com/box");

    BoxClient client = new SpringBoxClient(uri);
    assertEquals(10, client.collect(new BoxQuery()).size());

    client = new SpringBoxClient(uri, new RestTemplate());
    assertEquals(10, client.collect(new BoxQuery()).size());
  }
}
