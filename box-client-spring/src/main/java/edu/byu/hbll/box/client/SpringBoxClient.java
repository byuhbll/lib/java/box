package edu.byu.hbll.box.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * An HTTP client for communicating with Box through it's web api using the {@link RestTemplate} for
 * the underlying communication.
 *
 * @author Charles Draper
 */
public class SpringBoxClient extends AbstractHttpBoxClient {

  private RestTemplate client;

  /**
   * Constructs a new {@link SpringBoxClient} with given base uri.
   *
   * @param uri the base uri of the box source (eg, http://example.com/app/box)
   */
  public SpringBoxClient(URI uri) {
    this(uri, new RestTemplate());
  }

  /**
   * Constructs a new {@link SpringBoxClient} with given base uri and {@link RestTemplate}.
   *
   * @param uri the base uri of the box source (eg, http://example.com/app/box)
   * @param client http client to use
   */
  public SpringBoxClient(URI uri, RestTemplate client) {
    super(uri);
    this.client = client;
  }

  @Override
  protected InputStream send(URI uri) {
    ResponseEntity<Resource> responseEntity =
        client.exchange(uri, HttpMethod.GET, null, Resource.class);

    try {
      return responseEntity.getBody().getInputStream();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
