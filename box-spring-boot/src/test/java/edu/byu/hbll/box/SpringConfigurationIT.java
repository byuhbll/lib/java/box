package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.scheduler.PeriodicSchedule;
import edu.byu.hbll.scheduler.Schedule;
import java.time.Duration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;

/**
 * Sets properties in environment variables (ahem, system properties, because environment variables
 * are difficult to set from here) to test that they are properly set in the Box configuration by
 * Spring.
 */
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
@TestPropertySource("/SpringConfigurationIT.properties")
public class SpringConfigurationIT {

  @Autowired Box box;

  /**
   * Tests that the spring configuration works properly for Box configuration. This is in response
   * to an issue where environment variables were generally unusable because of the camel case
   * fields in Box configuration. Environment variables could not be mapped to camel case fields.
   * But the spring configuration approach allows this.
   */
  @Test
  public void testSpringConfiguration() {
    SourceConfig config = box.getSource("MYSRC").getConfig();
    Duration testDuration = Duration.parse("PT2M");

    // source properties
    assertEquals(testDuration, getDuration(config.getRemoveSchedule()));
    assertEquals(testDuration, config.getRemoveAge());
    assertEquals("myvalue", config.getFacetFields().get("myfacet").toArray()[0]);
    assertEquals(false, config.isSave());
    assertEquals(true, config.isDependencyOnly());
    assertEquals(true, config.isPostEnabled());

    // process properties
    assertEquals(TestObject.class, config.getProcessor().getClass());
    assertEquals(2, config.getBatchCapacity());
    assertEquals(testDuration, config.getBatchDelay());
    assertEquals(2, config.getThreadCount());
    assertEquals(2, config.getQuota());
    assertEquals(testDuration, getDuration(config.getQuotaReset()));
    assertEquals(testDuration, config.getSuspend());
    assertEquals(testDuration, getDuration(config.getOn()));
    assertEquals(testDuration, getDuration(config.getOff()));
    assertEquals(true, config.isProcessEnabled());
    assertEquals(testDuration, config.getRetryDelay());
    assertEquals(0.2, config.getRetryJitter());
    assertEquals(testDuration, config.getReprocessAge());
    assertEquals(testDuration, getDuration(config.getReprocessSchedule()));
    assertEquals(true, config.isProcess());
    assertEquals(true, config.isWait());
    assertEquals("process", getId(config.getProcessor()));

    // harvest properties
    assertEquals(TestObject.class, config.getHarvester().getClass());
    assertEquals(true, config.isHarvestEnabled());
    assertEquals(testDuration, getDuration(config.getHarvestSchedule()));
    assertEquals(testDuration, getDuration(config.getReharvestSchedule()));
    assertEquals(true, config.isSaveDeleted());
    assertEquals("harvest", getId(config.getHarvester()));

    // db properties
    assertEquals(TestObject.class, config.getDb().getClass());
    assertEquals("db", getId(config.getDb()));

    // cursor properties
    assertEquals(TestObject.class, config.getCursorDb().getClass());
    assertEquals("cursor", getId(config.getCursorDb()));

    // other properties
    assertEquals(TestObject.class, config.getOthers().get(0).getClass());
    assertEquals("other", getId(config.getOthers().get(0)));

    // web properties
    assertEquals(true, config.isPostEnabled());

    // global db properties
    assertEquals("global", getId(box.getSource("MYSRCDB").getConfig().getDb()));
  }

  private Duration getDuration(Schedule schedule) {
    return ((PeriodicSchedule) schedule).getPeriod();
  }

  private String getId(Object object) {
    return ((TestObject) object).getConfig().getParams().path("ID").asText();
  }
}
