package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.internal.spring.SpringBootObjectFactory.BoxConfigurableWrapper;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class SpringBootObjectFactoryIT {

  @Autowired Box box;

  /** Tests that spring beans are successfully used as Box objects. */
  @Test
  public void testSpringBootBeansAsBoxObjects() {
    for (String sourceName : List.of("springbeanbytype", "springbeanbyname")) {
      SourceConfig config = box.getSource(sourceName).getConfig();

      assertTrue(
          ((SpringBeanObject) ((BoxConfigurableWrapper) config.getProcessor()).getObject())
              .isBean());
      assertTrue(
          ((SpringBeanObject) ((BoxConfigurableWrapper) config.getHarvester()).getObject())
              .isBean());
      assertTrue(
          ((SpringBeanObject) ((BoxConfigurableWrapper) config.getDb()).getObject()).isBean());
      assertTrue(
          ((SpringBeanObject) ((BoxConfigurableWrapper) config.getCursorDb()).getObject())
              .isBean());
      assertTrue(
          ((SpringBeanObject) ((BoxConfigurableWrapper) config.getOthers().get(0)).getObject())
              .isBean());
    }
  }
}
