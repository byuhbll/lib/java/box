package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.box.internal.spring.SpringBootObjectFactory.BeanWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

/** Integration tests for the management/actuator endpoints. */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class ManagementIT {

  @Autowired private TestRestTemplate rest;
  @Autowired Box box;

  @BeforeEach
  public void beforeEach() {
    box.getSources().values().stream().filter(s -> !isReadOnly(s)).forEach(s -> s.clear());
  }

  /** Verifies the health endpoint is working. */
  @Test
  public void testHealth() {
    // Wrap in try-catch if needing to assert 4xx or 5xx responses.
    ResponseEntity<JsonNode> response = rest.getForEntity("/actuator/health", JsonNode.class);

    // Verify that the correct response code was sent.
    assertEquals(HttpStatus.OK, response.getStatusCode());

    // Verify that the correct response body was sent.
    JsonNode body = response.getBody();
    assertNotNull(body);
    assertEquals("UP", body.path("status").asText());
  }

  /** Verifies that clear wipes out the database and stops any current harvests. */
  @Test
  public void testClear() {
    sleep(100);

    Source source = box.getPrincipalSource();

    assertEquals(0, source.collect(new BoxQuery()).size());

    rest.getForEntity("/actuator/box/box/harvest", JsonNode.class);

    // allow time for harvest to complete
    sleep(2000);

    assertEquals(10, source.collect(new BoxQuery()).size());
    assertTrue(
        source.getConfig().getPreferredCursorDb().getHarvestCursor().path("cursor").asLong(0) > 0);

    rest.getForEntity("/actuator/box/box/clear", JsonNode.class);

    assertEquals(0, source.collect(new BoxQuery()).size());
    assertEquals(
        0, source.getConfig().getPreferredCursorDb().getHarvestCursor().path("cursor").asLong(0));
  }

  /** Verifies that clear all wipes out the database for all sources. */
  @Test
  public void testClearAll() {
    sleep(100);

    for (Source source : box.getSources().values()) {
      if (isReadOnly(source)) {
        continue;
      }

      source.save(new BoxDocument("1").setAsReady());
      assertEquals(1, source.collect(new BoxQuery()).size());
    }

    JsonNode response = rest.getForObject("/actuator/box/all/clear", JsonNode.class);

    assertTrue(response.path("clear").has("principal"));
    assertTrue(response.path("clear").has("one"));

    for (Source source : box.getSources().values()) {
      if (isReadOnly(source)) {
        continue;
      }

      assertEquals(0, source.collect(new BoxQuery()).size());
    }
  }

  private void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
    }
  }

  private static boolean isReadOnly(Source source) {
    BoxDatabase db = source.getConfig().getDb();
    return db instanceof ReadOnlyDatabase
        || db instanceof BeanWrapper && ((BeanWrapper) db).getObject() instanceof ReadOnlyDatabase;
  }
}
