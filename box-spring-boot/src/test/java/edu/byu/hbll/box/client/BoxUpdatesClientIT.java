/** */
package edu.byu.hbll.box.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.box.Application;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument;
import java.net.URI;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

/**
 * @author Charles Draper
 */
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class BoxUpdatesClientIT {

  @Autowired private TestRestTemplate httpClient;

  @Autowired Box box;

  @Test
  public void testUpdates() throws Exception {
    URI uri = URI.create(httpClient.getRootUri() + "box");
    AtomicInteger counter = new AtomicInteger();
    Runnable runnable = () -> counter.incrementAndGet();

    new BoxUpdatesClient(uri, runnable);

    Thread.sleep(1000);

    // make sure it does not count on start, only after it receives a notification
    assertEquals(0, counter.get());

    box.getPrincipalSource().save(new BoxDocument("1").setAsReady());

    // wait at least a second because notifications are sent at most once per second
    Thread.sleep(1100);

    assertEquals(1, counter.get());
  }
}
