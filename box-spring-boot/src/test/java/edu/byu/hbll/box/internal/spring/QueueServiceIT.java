package edu.byu.hbll.box.internal.spring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.box.Application;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Source;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * @author Charles Draper
 */
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class QueueServiceIT {

  @Autowired private TestRestTemplate client;
  @Autowired private Box box;

  @BeforeEach
  public void beforeEach() {
    for (Source source : box.getSources().values()) {
      try {
        source.clear();
      } catch (UnsupportedOperationException e) {
        // expected for readonly databases
      }
    }
  }

  @Test
  public void testPost() throws InterruptedException {
    // body
    client.postForEntity("/one/queue", "1\n2\n3", String.class);

    // file
    MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
    params.add("file", new ClassPathResource("ids"));
    client.postForEntity("/one/queue", params, String.class);

    Thread.sleep(1000);
    List<BoxDocument> results =
        box.getSource("one").collect(new BoxQuery().addStatus(BoxDocument.Status.READY));

    assertEquals(6, results.size());
    assertEquals("1", results.get(0).getId());
    assertEquals("2", results.get(1).getId());
    assertEquals("3", results.get(2).getId());
    assertEquals("4", results.get(3).getId());
    assertEquals("5", results.get(4).getId());
    assertEquals("6", results.get(5).getId());
  }
}
