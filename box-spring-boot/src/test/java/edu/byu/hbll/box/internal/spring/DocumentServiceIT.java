package edu.byu.hbll.box.internal.spring;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.Application;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.witness.HttpWitness;
import java.time.Instant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

/**
 * @author Charles Draper
 */
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class DocumentServiceIT {

  private static final String postDocument = "{\"id\":\"100\",\"@box\":{\"id\":\"100\"}}";

  @BeforeAll
  public static void beforeAll() throws Exception {
    System.setProperty("config.box.sources.principal.postEnabled", "true");
  }

  @Autowired private TestRestTemplate client;
  @Autowired private Box box;

  private HttpWitness witness;

  @BeforeEach
  public void beforeEach() {
    witness =
        HttpWitness.builder()
            .uriPrefix(client.getRootUri())
            .replace("/body/nextUri", ":\\d+/", "/")
            .replace("/body/nextUri", "cursor=\\d+", "cursor=0")
            .validate("/body/nextCursor", x -> x.isTextual() && x.asText().matches("\\d+"))
            .validate(
                "/body/documents/*/@box/cursor", x -> x.isTextual() && x.asText().matches("\\d+"))
            .validate("/body/documents/*/@box/modified", x -> Instant.parse(x.asText()) != null)
            .validate("/body/documents/*/@box/processed", x -> Instant.parse(x.asText()) != null)
            .validate("/body/@box/cursor", x -> x.isTextual() && x.asText().matches("\\d+"))
            .validate("/body/@box/modified", x -> Instant.parse(x.asText()) != null)
            .validate("/body/@box/processed", x -> Instant.parse(x.asText()) != null)
            .ignore("/body/timestamp")
            //            .overwrite(true)
            .build();

    for (Source source : box.getSources().values()) {
      try {
        source.clear();
      } catch (UnsupportedOperationException e) {
        // expected for readonly databases
      }
    }
  }

  /** Posting a document should succeed because postEnabled: true for `principal` source. */
  @Test
  public void testPostDocument() {
    ResponseEntity<String> response =
        client.postForEntity("/box/documents", postDocument, String.class);
    assertEquals(200, response.getStatusCodeValue());
  }

  /** Posting a document should fail because postEnabled: false for `one` source. */
  @Test
  public void testPostDocumentFail() {
    assertEquals(
        HttpStatus.METHOD_NOT_ALLOWED,
        client.postForEntity("/one/documents", postDocument, String.class).getStatusCode());
  }

  /** General test of the document service. */
  @Test
  public void testGetDocuments() {
    witness.testifyGet("principal/documents", "empty");
    box.getPrincipalSource().collect(new BoxQuery("1", "2", "3").setProcess(true));
    witness.testifyGet("principal/documents", "three");
  }

  /** Tests the response of a source that doesn't exist. */
  @Test
  public void testFakeSource() {
    witness.testifyGet("fake/documents");
  }

  /** Tests the response of a deleted item. */
  @Test
  public void testDeleted() {
    box.getSource("simple").save(new BoxDocument("deleted").setAsDeleted());
    witness.testifyGet("simple/documents", "documents");
    witness.testifyGet("simple/documents/deleted", "document");
  }

  /** Tests the response of an error. */
  @Test
  public void testError() {
    box.getSource("simple").save(new BoxDocument("error").setAsError("error message"));
    witness.testifyGet("simple/documents/error");
  }

  /** Tests various statuses. */
  @Test
  public void testStatuses() {
    Source source = box.getSource("simple");
    source.save(new BoxDocument("ready").setAsReady());
    source.save(new BoxDocument("deleted").setAsDeleted());
    source.save(new BoxDocument("unprocessed").setAsUnprocessed());
    source.save(new BoxDocument("error").setAsError("error message"));

    assertAll(
        () -> witness.testifyGet("simple/documents", "default"),
        () -> witness.testifyGet("simple/documents?status=ready", "ready"),
        () -> witness.testifyGet("simple/documents?status=deleted", "deleted"),
        () -> witness.testifyGet("simple/documents?status=unprocessed", "unprocessed"),
        () -> witness.testifyGet("simple/documents?status=error", "error"),
        () -> witness.testifyGet("simple/documents?status=ready&status=error", "combo"));
  }

  /** Bug: deleted documents were showing up as unprocessed when specifying fields */
  @Test
  public void testDeletedWithFields() {
    box.getSource("simple").save(new BoxDocument("deleted").setAsDeleted());
    witness.testifyGet("simple/documents?field=id&field=@box.id&field=@box.status");
  }

  /** Test process flag. */
  @Test
  public void testProcessFlag() {
    assertAll(
        () -> witness.testifyGet("principal/documents/1", 1),
        () -> witness.testifyGet("principal/documents/2?process=false", 2),
        () -> witness.testifyGet("principal/documents/3?process", 3),
        () -> witness.testifyGet("principal/documents/4?process=true", 4));
  }

  /** Test wait flag. */
  @Test
  public void testWaitFlag() {
    assertAll(
        () -> witness.testifyGet("principal/documents/1", 1),
        () -> witness.testifyGet("principal/documents/2?wait=false", 2),
        () -> witness.testifyGet("principal/documents/3?wait", 3),
        () -> witness.testifyGet("principal/documents/4?wait=true", 4));

    Instant processed1 =
        Instant.parse(
            witness.get("principal/documents/4?wait").at("/body/@box/processed").asText());
    Instant processed2 =
        Instant.parse(
            witness.get("principal/documents/4?wait").at("/body/@box/processed").asText());
    assertTrue(processed1.equals(processed2));
  }

  /** Tests facet parameters. */
  @Test
  public void testFacetParameters() {
    witness.get("principal/documents?id=11&id=12&id=21&id=22&process");
    assertAll(
        () -> witness.testifyGet("principal/documents?facet=first:1", 1),
        () -> witness.testifyGet("principal/documents?facet=first:1&facet=first:2", 2),
        () -> witness.testifyGet("principal/documents?facet=first:1&facet=last:1", 3),
        () -> witness.testifyGet("principal/documents?facet=first:1&facet=last:3", 4),
        () -> witness.testifyGet("principal/documents?facet=first:1&facet=fake:1", 5));
  }

  /** Tests facet parameters. */
  @Test
  public void testMultipleDocuments() {
    witness.get("principal/documents?id=1&id=2&id=3&process");
    witness.testifyGet("principal/documents?id=1&id=2&id=3");
  }

  /** Tests the offset parameter. */
  @Test
  public void testOffset() {
    witness.get("principal/documents?id=1&id=2&id=3&id=4&id=5&process");
    witness.testifyGet("principal/documents?offset=3");
  }

  /** Tests the order parameter. */
  @Test
  public void testOrder() {
    witness.get("principal/documents?id=1&id=2&id=3&id=4&id=5&process");
    witness.testifyGet("principal/documents?order=asc", "asc");
    witness.testifyGet("principal/documents?order=desc", "desc");
  }
}
