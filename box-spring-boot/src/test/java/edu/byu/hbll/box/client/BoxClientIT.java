package edu.byu.hbll.box.client;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.node.ArrayNode;
import edu.byu.hbll.box.Application;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.witness.HttpWitness;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

/**
 * @author Charles Draper
 */
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class BoxClientIT {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  @Autowired private TestRestTemplate httpClient;

  private HttpWitness witness =
      HttpWitness.builder()
          .validate("/*/@box/cursor", n -> n.asText().matches("\\d+"))
          .ignore("/*/@box/modified")
          .ignore("/*/@box/processed")
          //          .overwrite(true)
          .build();

  private BoxClient client;

  @Autowired Box box;

  @BeforeEach
  public void setUp() throws Exception {
    client = new HttpBoxClient(URI.create(httpClient.getRootUri() + "box"));

    box.getPrincipalSource().clear();
    Thread.sleep(100);
    box.getPrincipalSource().clear();

    List<String> ids = new ArrayList<>();

    for (int i = 1; i <= 20; i++) {
      ids.add(i + "");
    }

    client.collect(new BoxQuery(ids).setProcess(true));
  }

  @Test
  public void testLimits() {
    assertAll(
        () -> assertEquals(10, client.collect(new BoxQuery()).size()),
        () -> assertEquals(0, client.collect(new BoxQuery().setLimit(0)).size()),
        () -> assertEquals(20, client.collect(new BoxQuery().setUnlimited()).size()),
        () -> assertEquals(7, client.collect(new BoxQuery().setLimit(7)).size()),
        () -> assertEquals(2, client.collect(new BoxQuery("1", "2").setLimit(7)).size()));
  }

  @Test
  public void testFields() {
    assertAll(
        () -> testify(new BoxQuery().setLimit(1), "default"),
        () -> testify(new BoxQuery().setLimit(1).addFields("id"), "id"),
        () -> testify(new BoxQuery().setLimit(1).addFields("id", "@box.id"), "ids"),
        () -> testify(new BoxQuery("1").addFields("id", "@box.id"), "ids1"),
        () -> testify(new BoxQuery().setLimit(1).addFields("@doc"), "doc"),
        () -> testify(new BoxQuery("1").addFields("@doc"), "doc1"),
        () -> testify(new BoxQuery().setLimit(1).addFields("@box"), "box"),
        () -> testify(new BoxQuery("1").addFields("@box"), "box1"));
  }

  @Test
  public void testFacets() {
    assertAll(
        () -> testify(new BoxQuery().addFacet("first", "1").addFacet("last", "1"), "multiple"),
        () -> testify(new BoxQuery().addFacet("first", "1"), "one"));
  }

  @Test
  public void testStatuses() {
    assertAll(
        () -> testify(new BoxQuery(), "default"),
        () -> testify(new BoxQuery().addStatuses(Status.READY), "ready"),
        () -> testify(new BoxQuery().addStatuses(Status.DELETED), "deleted"));
  }

  @Test
  public void testIds() {
    assertAll(
        () -> testify(new BoxQuery("1"), "one"),
        () -> testify(new BoxQuery("1", "3", "2", "new"), "multiple"));
  }

  @Test
  public void testProcess() {
    assertAll(
        () -> testify(new BoxQuery("new1"), "unprocessed"),
        () -> testify(new BoxQuery("new2").setProcess(true), "processed"));
  }

  @Test
  public void testWait() {
    assertAll(
        () -> testify(new BoxQuery("new1").setWait(true), "new"),
        () -> testify(new BoxQuery("1").setWait(true), "old"));
  }

  @Test
  public void testCursor() {
    long cursor = client.collect(new BoxQuery().setLimit(1)).get(0).getCursor().get() + 1;

    assertAll(
        () ->
            assertEquals(
                19, client.collect(new BoxQuery().setCursor(cursor).setUnlimited()).size()),
        () -> assertEquals(0, client.collect(new BoxQuery().setCursor(Long.MAX_VALUE)).size()));
  }

  @Test
  public void testFind() {
    AtomicInteger total = new AtomicInteger();
    client
        .find(new BoxQuery().setUnlimited())
        .forEach(
            d -> {
              total.incrementAndGet();
              System.out.println(d.getId());
            });
    assertEquals(20, total.get());
  }

  @Test
  public void testStream() {
    long total = client.stream(new BoxQuery().setUnlimited()).count();
    assertEquals(20, total);
  }

  @Test
  public void testOffset() {
    assertAll(
        () -> testify(new BoxQuery().setOffset(0), "zero"),
        () -> testify(new BoxQuery().setOffset(10), "ten"));
  }

  @Test
  public void testOrder() {
    assertAll(
        () -> testify(new BoxQuery(), "default"),
        () -> testify(new BoxQuery().setAscendingOrder(), "asc"),
        () -> testify(new BoxQuery().setDescendingOrder(), "desc"));
  }

  /**
   * Many IDs can be added to a {@link BoxQuery}, so many that it makes the request uri too long for
   * the upstream Box server to handle. Batching was added to the box client and so this tests that
   * batching.
   */
  @Test
  public void testManyIdsInUri() throws Exception {
    BoxQuery query = new BoxQuery();

    for (int i = 0; i < 10000; i++) {
      query.addId(i + "");
    }

    assertEquals(10000, client.collect(query).size());

    // give extra time to clear such a large number of documents
    box.getPrincipalSource().clear();
    Thread.sleep(1000);
  }

  private void testify(BoxQuery query, String label) {
    QueryResult docs = client.collect(query);
    ArrayNode list = mapper.createArrayNode();
    docs.forEach(d -> list.add(d.toJson()));
    witness.testify(list, label);
  }
}
