package edu.byu.hbll.box;

import jakarta.annotation.PostConstruct;
import lombok.Getter;

/**
 * @author Charles Draper
 */
public class TestObject implements Processor, Harvester, ReadOnlyDatabase {

  @Getter private ConstructConfig config;

  @PostConstruct
  public void postConstruct(ConstructConfig config) {
    this.config = config;
  }

  @Override
  public QueryResult find(BoxQuery query) {
    return new QueryResult();
  }

  @Override
  public HarvestResult harvest(HarvestContext context) {
    return new HarvestResult();
  }

  @Override
  public ProcessResult process(ProcessBatch batch) {
    return new ProcessResult();
  }
}
