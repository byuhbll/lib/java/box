package edu.byu.hbll.box;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("mySpringBean")
public class SpringBeanObject implements Processor, Harvester, ReadOnlyDatabase {

  @Autowired private BeanFactory beanFactory;

  @Override
  public QueryResult find(BoxQuery query) {
    return new QueryResult();
  }

  @Override
  public HarvestResult harvest(HarvestContext context) {
    return new HarvestResult();
  }

  @Override
  public ProcessResult process(ProcessBatch batch) {
    return new ProcessResult();
  }

  public boolean isBean() {
    return beanFactory != null;
  }
}
