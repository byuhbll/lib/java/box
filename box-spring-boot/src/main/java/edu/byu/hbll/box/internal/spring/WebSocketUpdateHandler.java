package edu.byu.hbll.box.internal.spring;

import edu.byu.hbll.box.Source;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

/**
 * Web socket server for sending signals to clients that there are new document updates.
 *
 * @author Charles Draper
 */
public class WebSocketUpdateHandler extends AbstractWebSocketHandler {

  static final Logger logger = LoggerFactory.getLogger(WebSocketUpdateHandler.class);

  private Source source;
  private Set<WebSocketSession> sessions = ConcurrentHashMap.newKeySet();

  /**
   * Creates a new {@link WebSocketUpdateHandler} for the given source.
   *
   * @param source the box source
   */
  public WebSocketUpdateHandler(Source source) {
    this.source = source;
    this.source.registerForUpdateNotifications(() -> sendUpdateNotification());
  }

  @Override
  public void afterConnectionEstablished(WebSocketSession session) throws Exception {
    sessions.add(session);
  }

  @Override
  public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus)
      throws Exception {
    sessions.remove(session);
  }

  /** Sends a simple signal that there are updates to all listening. */
  public void sendUpdateNotification() {
    for (WebSocketSession session : sessions) {
      try {
        session.sendMessage(new TextMessage(source.getName() + " updated"));
      } catch (Exception e) {
        logger.error("Unable to send update notification over websockets, closing session: " + e);

        try {
          session.close(CloseStatus.SESSION_NOT_RELIABLE);
        } catch (IOException e1) {
          logger.error("Error while closing websocket connection: " + e);
        }

        sessions.remove(session);
      }
    }
  }
}
