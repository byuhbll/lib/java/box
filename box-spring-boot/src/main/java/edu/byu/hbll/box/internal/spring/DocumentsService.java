package edu.byu.hbll.box.internal.spring;

import com.fasterxml.jackson.core.io.JsonStringEncoder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.internal.util.CursorUtils;
import edu.byu.hbll.box.internal.util.FlexDateParser;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerErrorException;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * The `/documents` and `/documents/{id}` web services. These are the endpoints for querying box of
 * http.
 *
 * <p>NOTE: The {@link RequestMapping} endpoints in this class are designed with extra unspecific
 * templates in order to push them down in matching precedence so as to not hijack all of the
 * application's other paths.
 */
@RestController
@RequestMapping("{sourceName:.+}/{x:documents}")
public class DocumentsService {

  static final Logger logger = LoggerFactory.getLogger(DocumentsService.class);
  private static final ObjectMapper mapper = new ObjectMapper();

  @Autowired private Box box;
  @Autowired HttpServletRequest request;
  @Autowired HttpServletResponse response;

  /**
   * Posts a document to a box source. The document should be in proper box document serialized
   * form.
   *
   * @param sourceName the name of the source where the document will be saved
   * @param json the document to post including metadata
   * @return the status of the post
   */
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> postDocument(@PathVariable String sourceName, @RequestBody String json) {

    try {
      box.verifySource(sourceName);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "source not recognized");
    }

    Source source = box.getSource(sourceName);

    if (!source.getConfig().isPostEnabled()) {
      throw new ResponseStatusException(
          HttpStatus.METHOD_NOT_ALLOWED, "post is not allowed for this source");
    }

    BoxDocument boxDocument = null;

    try {
      boxDocument = BoxDocument.parse(json);
    } catch (Exception e) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST, "cannot parse json into box document: " + e.getMessage(), e);
    }

    source.save(boxDocument);
    return ResponseEntity.ok().body(boxDocument);
  }

  /**
   * `/documents/{id}` endpoint. Given the id, queries box and returns the corresponding document in
   * json format.
   *
   * @param params the document endpoint parameters
   * @return the document in json format
   */
  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getDocument(DocumentParams params) {

    logger.debug("{}", UriUtils.getRequestUri(request));

    Source source;

    try {
      source = box.getSource(params.getSourceName());
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "source not recognized");
    }

    List<BoxDocument> docs;
    BoxQuery query = new DocumentsParams(params).toQuery();

    try {
      docs = source.collect(query);

    } catch (Exception e) {
      logger.error(e.toString(), e);
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR, "unable to retrieve document");
    }

    BoxDocument doc = docs.isEmpty() ? null : docs.get(0);
    ObjectNode node = doc.toJson();

    // core metadata fields always present within Box, but can be removed for the web interface
    // remove box.id, box.status, or box.cursor?
    List<String> removes = prepareMetadata(query);
    applyMetadataRemoves(node, removes);

    if (doc.getMessage().isPresent()) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(node);
    } else if (doc.isDeleted()) {
      return ResponseEntity.status(HttpStatus.GONE).body(node);
    } else if (!doc.isProcessed()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(node);
    }

    return ResponseEntity.ok(node);
  }

  /**
   * `/documents` endpoint. Given the query, queries box and returns all documents found. The
   * documents are streamed one at a time. The response is an object with three fields: documents
   * (array of box documents), nextUri (the next uri when end reached), nextCursor (the next cursor
   * when end reached).
   *
   * @param params the documents endpoint parameters
   * @return the documents response
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getDocuments(DocumentsParams params) {

    logger.debug("{}", UriUtils.getRequestUri(request));

    String sourceName = params.getSourceName();

    try {
      sourceName = box.verifySource(sourceName);
    } catch (Exception e) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST, "source `" + sourceName + "` not recognized");
    }

    String finalSourceName = sourceName;

    if (params.getFrom() != null) {
      try {
        Instant from = FlexDateParser.parse(params.getFrom());
        long cursor = CursorUtils.getCursor(from);
        params.setCursor(cursor);
      } catch (DateTimeParseException e) {
        throw new ResponseStatusException(
            HttpStatus.BAD_REQUEST, "from parameter must be a ISO 8601 date: " + e);
      }
    }

    Source source = box.getSource(finalSourceName);

    response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    try {
      OutputStream out = response.getOutputStream();

      BufferedOutputStream bout = new BufferedOutputStream(out);
      PrintWriter writer = new PrintWriter(new OutputStreamWriter(bout, StandardCharsets.UTF_8));

      BoxQuery query = params.toQuery();

      long cursor = query.getCursorOrDefault();
      boolean first = true;

      // core metadata fields always present within Box, but can be removed for the web interface
      // remove box.id, box.status, or box.cursor?
      List<String> removes = prepareMetadata(query);

      for (BoxDocument doc : source.find(query)) {
        if (first) {
          // we write the documents field inside the loop so that if box.find() throws an exception
          // we can send back a proper error response
          writer.print("{\"documents\":[");
        } else {
          writer.print(',');
        }

        ObjectNode node = doc.toJson();

        applyMetadataRemoves(node, removes);

        // I tried using the mapper to write, but the stream is getting closed each time
        writer.print(mapper.writeValueAsString(node));
        first = false;
        cursor = doc.getCursor().orElse(BoxQuery.DEFAULT_CURSOR);
      }

      if (first) {
        // if no documents written
        writer.print("{\"documents\":[");
      }

      writer.print("]");

      if (query.isHarvestQuery()) {
        long nextCursor =
            query.getCursorOrDefault() == cursor
                ? cursor
                : cursor + (query.isAscendingOrder() ? 1 : -1);

        UriBuilder uriBuilder =
            UriComponentsBuilder.fromUri(UriUtils.getRequestUri(request))
                .replaceQueryParam("cursor", nextCursor)
                .replaceQueryParam("from")
                .replaceQueryParam("offset");

        if (request.getHeader("X-Forwarded-Proto") != null) {
          uriBuilder.scheme(request.getHeader("X-Forwarded-Proto"));
        }

        String nextUri = uriBuilder.build().toString();

        JsonStringEncoder e = JsonStringEncoder.getInstance();

        writer.print(",\"nextUri\":\"" + new String(e.quoteAsString(nextUri)) + "\",");
        writer.print("\"nextCursor\":\"" + nextCursor + "\"");
      }

      writer.print("}");
      writer.flush();
      writer.close();
      return null;
    } catch (Exception e) {
      logger.error(e.toString(), e);
      throw new ServerErrorException("unable to retrieve documents", e);
    }
  }

  /**
   * Returns true or false given the representing text value.
   *
   * @param value boolean text value
   * @return matching boolean
   */
  static boolean booleanValueOf(String value) {
    if (value == null) {
      return false;
    }

    switch (value.toLowerCase()) {
      case "true":
      case "yes":
      case "y":
      case "":
      case "1":
        return true;
      default:
        return false;
    }
  }

  private List<String> prepareMetadata(BoxQuery query) {
    // core metadata fields always present within Box, but can be removed for the web interface
    // internal metadata such as dependencies and groupId is always removed in web interface

    List<String> removes = new ArrayList<>();
    removes.add(BoxQuery.METADATA_FIELD_DEPENDENCIES);
    removes.add(BoxQuery.METADATA_FIELD_GROUP_ID);

    // remove box.id, box.status, or box.cursor?
    if (query.getFields().isEmpty()) {
      return removes;
    }

    boolean boxField = query.getFields().contains("@box");
    boolean boxFields = query.getFields().stream().anyMatch(f -> f.startsWith("@box."));

    if (boxField && !boxFields) {
      return removes;
    } else if (!boxField && !boxFields) {
      return Collections.singletonList("@box");
    }

    Arrays.asList(
            BoxQuery.METADATA_FIELD_ID,
            BoxQuery.METADATA_FIELD_STATUS,
            BoxQuery.METADATA_FIELD_CURSOR)
        .stream()
        .filter(f -> !query.getFields().contains(f))
        .forEach(f -> removes.add(f));

    return removes;
  }

  private ObjectNode applyMetadataRemoves(ObjectNode node, List<String> removes) {
    for (String metadataField : removes) {
      String[] path = metadataField.split("\\.");
      JsonNode parent = node;

      for (int i = 0; i < path.length - 1; i++) {
        parent = parent.path(path[i]);
      }

      if (parent.isObject()) {
        ((ObjectNode) parent).remove(path[path.length - 1]);
      }
    }

    return node;
  }
}
