package edu.byu.hbll.box.internal.spring;

import java.util.ArrayList;
import java.util.List;

/** Represents the parameters for a document request. */
public class DocumentParams {

  private String sourceName;
  private String id;
  private boolean wait;
  private boolean process;
  private List<String> fields = new ArrayList<>();

  /**
   * Returns the source name for this request.
   *
   * @return the sourceName
   */
  public String getSourceName() {
    return sourceName;
  }

  /**
   * Sets the source name for this request.
   *
   * @param sourceName the sourceName to set
   */
  public void setSourceName(String sourceName) {
    this.sourceName = sourceName;
  }

  /**
   * Returns the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the ids for this request.
   *
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Returns the wait flag for this request.
   *
   * @return the wait
   */
  public boolean isWait() {
    return wait;
  }

  /**
   * Sets the wait flag for this request.
   *
   * @param wait the wait to set
   */
  public void setWait(String wait) {
    this.wait = DocumentsService.booleanValueOf(wait);
  }

  /**
   * Returns the process flag.
   *
   * @return the process
   */
  public boolean isProcess() {
    return process;
  }

  /**
   * Sets the process flag for this request.
   *
   * @param process the process to set
   */
  public void setProcess(String process) {
    this.process = DocumentsService.booleanValueOf(process);
  }

  /**
   * Returns the fields.
   *
   * @return the fields
   */
  public List<String> getFields() {
    return fields;
  }

  /**
   * Sets the fields for this request.
   *
   * @param fields the fields to set
   */
  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  /**
   * Alias for {@link #setFields(List)}.
   *
   * @param fields the fields to set
   */
  public void setField(List<String> fields) {
    this.fields = fields;
  }
}
