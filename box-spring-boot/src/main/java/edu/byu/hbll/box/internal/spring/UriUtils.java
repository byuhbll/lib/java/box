package edu.byu.hbll.box.internal.spring;

import jakarta.servlet.http.HttpServletRequest;
import java.net.URI;

/**
 * Status URI utilities.
 *
 * @author Charles Draper
 */
public class UriUtils {

  /**
   * Get the original request uri given the servlet request.
   *
   * @param request the servlet request
   * @return the original request uri
   */
  public static URI getRequestUri(HttpServletRequest request) {

    String requestUrl = request.getRequestURL().toString();
    String queryString = request.getQueryString();

    if (queryString == null || queryString.isEmpty()) {
      return URI.create(requestUrl);
    } else {
      return URI.create(requestUrl + "?" + queryString);
    }
  }
}
