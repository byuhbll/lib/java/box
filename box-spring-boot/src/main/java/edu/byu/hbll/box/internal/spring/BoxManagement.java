package edu.byu.hbll.box.internal.spring;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.SourceConfig;
import java.time.Duration;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/**
 * Management web service for box.
 *
 * @author Charles Draper
 */
@Component
@Endpoint(id = "box")
public class BoxManagement {

  @Autowired private Box box;

  /**
   * Returns proper usage.
   *
   * @return the proper usage
   */
  @ReadOperation(produces = MediaType.APPLICATION_JSON_VALUE)
  @WriteOperation(produces = MediaType.APPLICATION_JSON_VALUE)
  public JsonNode boxUsage1() {
    ObjectNode response = JsonNodeFactory.instance.objectNode();
    response.put(
        "error",
        "usage: /box/{sourceName}/{operation} where operation is one of "
            + "(reprocess|harvest|clear|stats)");
    return response;
  }

  /**
   * Returns proper usage.
   *
   * @param sourceName name of the source on which to perform the operation
   * @return the proper usage
   */
  @ReadOperation(produces = MediaType.APPLICATION_JSON_VALUE)
  @WriteOperation(produces = MediaType.APPLICATION_JSON_VALUE)
  public JsonNode boxUsage2(@Selector String sourceName) {
    return boxUsage1();
  }

  /**
   * Management endpoint for box.
   *
   * @param sourceName name of the source on which to perform the operation
   * @param operation the operation to perform (reprocess, harvest, clear, stats)
   * @return the resulting information
   */
  @ReadOperation(produces = MediaType.APPLICATION_JSON_VALUE)
  @WriteOperation(produces = MediaType.APPLICATION_JSON_VALUE)
  public JsonNode box(@Selector String sourceName, @Selector String operation) {
    ObjectNode response = JsonNodeFactory.instance.objectNode();
    ObjectNode container = response.putObject(operation);

    Set<Source> sources = new LinkedHashSet<>();

    if (sourceName.toLowerCase().equals(SourceConfig.ALL_SOURCES_ALIAS)) {
      sources.addAll(box.getSources().values());
    } else {
      try {
        sources.add(box.getSource(sourceName));
      } catch (Exception e) {
        container.put(sourceName, e.toString());
        return response;
      }
    }

    for (Source source : sources) {
      JsonNode message;

      try {
        switch (operation) {
          case "reprocess":
            message = reprocess(source, Duration.ZERO.toString());
            break;
          case "harvest":
            message = harvest(source);
            break;
          case "clear":
            message = clear(source);
            break;
          case "stats":
            message = stats(source);
            break;
          default:
            response.removeAll();
            response.put(
                "error",
                "unrecognized operation "
                    + operation
                    + "; should be one of reprocess, harvest, clear, stats");
            return response;
        }
      } catch (Exception e) {
        message = text(e.getMessage() != null ? e.getMessage() : e);
      }

      container.set(source.getName(), message);
    }

    return response;
  }

  /**
   * Puts all existing documents back on the queue for reprocess that meet the age requirement.
   *
   * @param source the source to reprocess
   * @param age minimum age for triggering a reprocess
   * @return information regarding the operation
   */
  public JsonNode reprocess(Source source, String age) {
    Duration olderThan = Duration.parse(age);
    new Thread(() -> source.getConfig().getDb().addToQueue(olderThan)).start();
    return text("adding documents older than " + age + " to the process queue");
  }

  /**
   * Triggers a harvest on the given source.
   *
   * @param source the source to trigger harvest
   * @return information regarding the operation
   */
  public JsonNode harvest(Source source) {
    source.triggerHarvest();
    return text("triggered harvest");
  }

  /**
   * Completely clears the database for the given source.
   *
   * @param source the source to clear
   * @return information regarding the operation
   */
  public JsonNode clear(Source source) {
    source.clear();
    return text("cleared database");
  }

  /**
   * Returns stats about the source. TODO
   *
   * @param source the source
   * @return stats a string
   */
  private JsonNode stats(Source source) {
    //    DocumentProcessor documentProcessor = registry.getDocumentProcessor(source.getName());
    //    Map<String, Benchmark.Data> stats = documentProcessor.getStats();
    //    ObjectNode response = mapper.createObjectNode();
    //    response.put("WARNING", "This API call is experimental and is subject to change.");
    //    Benchmark.Data overallStats = stats.get("processBatch");
    //
    //    if (overallStats != null) {
    //      response.with("process").put("numBatches", overallStats.getCount());
    //      response.with("process").put("numDocuments", documentProcessor.getNumDocuments());
    //      response.with("process").put("averageBatchSize",
    // documentProcessor.getAverageBatchSize());
    //      stats
    //          .entrySet()
    //          .stream()
    //          .forEach(
    //              e ->
    //                  response
    //                      .with("process")
    //                      .put(
    //                          e.getKey() + "Time",
    //                          ((float) e.getValue().getAverageRecent() / 1000000)));
    //      response.with("process").put("throughput", overallStats.getThroughput());
    //      response.with("process").put("throughputRecent", overallStats.getThroughputRecent());
    //    }
    //
    //    return response.toString();
    return text("not implemented yet");
  }

  /**
   * Converts a text message into a text node.
   *
   * @param message the message
   * @return the text node containing the message
   */
  private TextNode text(Object message) {
    return JsonNodeFactory.instance.textNode(message == null ? "" : message.toString());
  }
}
