package edu.byu.hbll.box.internal.spring;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.box.internal.core.ConfigParser;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.util.List;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties for the application. Any fields read in {@link ConfigParser} should appear here. This
 * exists to take advantage of the spring boot configuration features including allowing environment
 * variables to be bound when Box requires camel-case field names.
 */
@ConfigurationProperties("byuhbll.box")
@Data
public class BoxProperties {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private Map<String, Source> sources;
  private Other db;

  /**
   * Serializes this object to json.
   *
   * @return the json representation
   */
  public JsonNode toJson() {
    return mapper.valueToTree(this);
  }

  /** Properties class for a source object. */
  @Data
  public static class Source {
    private Template template;
    private Boolean principal;
    private Boolean enabled;
    private String removeSchedule;
    private String removeAge;
    private Map<String, List<String>> facetField;
    private Boolean save;
    private Boolean dependencyOnly;
    private Boolean defaultLimit;
    private Boolean postEnabled;
    private Process process;
    private Harvest harvest;
    private Other db;
    private Other cursor;
    private Other other;
    private Web web;
  }

  /** Properties class for a template object. */
  @Data
  public static class Template {
    private String id;
    private Map<String, Object> params;
  }

  /** Properties class for the process section. */
  @Data
  public static class Process {
    private String type;
    private Integer batchCapacity;
    private String batchDelay;
    private Integer threadCount;
    private Integer quota;
    private String quotaReset;
    private String suspend;
    private String on;
    private String off;
    private Boolean enabled;
    private String retryDelay;
    private Double retryJitter;
    private String reprocessAge;
    private String reprocessSchedule;
    private Boolean process;
    private Boolean wait;
    private Map<String, Object> params;
  }

  /** Properties class for the harvest section. */
  @Data
  public static class Harvest {
    private String type;
    private Boolean enabled;
    private String schedule;
    @Deprecated private String resetSchedule;
    private String reharvestSchedule;
    private Boolean saveDeleted;
    private Map<String, Object> params;
  }

  /** Properties class for the other section. */
  @Data
  public static class Other {
    private String type;
    private Map<String, Object> params;
  }

  /** Properties class for the web section. */
  @Data
  public static class Web {
    private Endpoints endpoints;
  }

  /** Properties class for the endpoints section. */
  @Data
  public static class Endpoints {
    private Documents documents;
  }

  /** Properties class for the documents section. */
  @Data
  public static class Documents {
    private Post post;
  }

  /** Properties class for the post section. */
  @Data
  public static class Post {
    private Boolean enabled;
  }
}
