package edu.byu.hbll.box.internal.spring;

import edu.byu.hbll.box.Box;
import jakarta.annotation.PreDestroy;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Auto configuration for {@link Box}.
 *
 * @author Charles Draper
 */
@Configuration
@EnableConfigurationProperties(BoxProperties.class)
public class BoxConfiguration {

  private BoxProperties properties;
  private SpringBootObjectFactory objectFactory;
  private Box box;

  /**
   * Creates a new configuration with the given properties and object factory.
   *
   * @param properties the properties
   * @param objectFactory the object factory for spring boot beans
   */
  public BoxConfiguration(BoxProperties properties, SpringBootObjectFactory objectFactory) {
    this.properties = properties;
    this.objectFactory = objectFactory;
  }

  /**
   * Returns the {@link Box} object.
   *
   * @return box
   */
  @Bean
  @ConditionalOnMissingBean
  public Box box() {
    return box = Box.newBuilder().config(properties.toJson()).objectFactory(objectFactory).build();
  }

  /** Shuts down Box. */
  @PreDestroy
  public void preDestroy() {
    if (box != null) {
      box.close();
    }
  }
}
