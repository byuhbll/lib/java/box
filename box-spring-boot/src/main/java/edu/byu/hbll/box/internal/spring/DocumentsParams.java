package edu.byu.hbll.box.internal.spring;

import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.Facet;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Represents the parameters for a documents request. */
@NoArgsConstructor
@Getter
@Setter
public class DocumentsParams {

  private String sourceName;
  private List<String> ids = new ArrayList<>();
  private boolean wait;
  private boolean process;
  private String from;
  private Long cursor;
  private Long offset;
  private BoxQuery.Order order;
  private long limit = BoxQuery.DEFAULT_LIMIT;
  private List<BoxDocument.Status> statuses = new ArrayList<>();
  private List<String> fields = new ArrayList<>();
  private List<String> facets = new ArrayList<>();

  /**
   * Creates a {@link DocumentsParams} from a {@link DocumentParams}.
   *
   * @param params the params to transfer
   */
  public DocumentsParams(DocumentParams params) {
    this.sourceName = params.getSourceName();
    this.ids.add(params.getId());
    this.wait = params.isWait() || params.isProcess();
    this.process = params.isProcess();
    this.fields = params.getFields();
  }

  /**
   * Alias for setIds(List).
   *
   * @param ids the ids to set
   */
  public void setId(List<String> ids) {
    setIds(ids);
  }

  /**
   * Sets whether or not to wait for processing if unprocessed.
   *
   * @param wait the wait to set
   */
  public void setWait(String wait) {
    this.wait = DocumentsService.booleanValueOf(wait);
  }

  /**
   * Sets whether or not the documents should be force processed.
   *
   * @param process the process to set
   */
  public void setProcess(String process) {
    this.process = DocumentsService.booleanValueOf(process);
  }

  /**
   * Alias for setFields(List).
   *
   * @param fields the fields to set
   */
  public void setField(List<String> fields) {
    setFields(fields);
  }

  /**
   * Alias for setFacets(List).
   *
   * @param facets the facets to set
   */
  public void setFacet(List<String> facets) {
    setFacets(facets);
  }

  /**
   * Sets the statuses for this request.
   *
   * @param statuses the statuses to set
   */
  public void setStatuses(List<String> statuses) {
    this.statuses = new ArrayList<>();

    if (statuses.isEmpty()) {
      this.statuses.add(BoxDocument.Status.READY);
      this.statuses.add(BoxDocument.Status.DELETED);
    } else {
      for (String status : statuses) {
        this.statuses.add(BoxDocument.Status.valueOf(status.toUpperCase()));
      }
    }
  }

  /**
   * Alias for {@link #setStatuses(List)}.
   *
   * @param statuses the statuses to set
   */
  public void setStatus(List<String> statuses) {
    setStatuses(statuses);
  }

  /**
   * Sets the order for this request.
   *
   * @param order the order to set
   */
  public void setOrder(String order) {
    this.order = BoxQuery.Order.valueOf(order.toUpperCase());
  }

  /**
   * Converts the query parameters into a proper {@link BoxQuery}.
   *
   * @return the resulting box query
   */
  public BoxQuery toQuery() {
    BoxQuery query =
        new BoxQuery()
            .addIds(ids)
            .setProcess(isProcess())
            .setWait(isWait())
            .setCursor(cursor)
            .setLimit(limit)
            .setOffset(offset)
            .setOrder(order)
            .addStatuses(statuses)
            .addFields(fields)
            .addFacets(Facet.parse(facets));

    return query;
  }
}
