package edu.byu.hbll.box.internal.spring;

import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.SourceConfig;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Configuration for updates notifications over web sockets.
 *
 * @author Charles Draper
 */
@Configuration
@EnableWebSocket
public class WebSocketUpdateConfiguration implements WebSocketConfigurer {

  @Autowired private Box box;

  @Override
  public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    for (Source source : box.getSources().values()) {
      List<String> names = new ArrayList<>();
      names.add(source.getName());

      if (source.isPrincipal()) {
        names.add(SourceConfig.PRINCIPAL_SOURCE_ALIAS);
      }

      for (String name : names) {
        registry
            .addHandler(new WebSocketUpdateHandler(source), "/" + name + "/updates")
            .setAllowedOrigins("*");
      }
    }
  }
}
