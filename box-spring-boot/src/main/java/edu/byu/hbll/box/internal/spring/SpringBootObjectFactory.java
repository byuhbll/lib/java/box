package edu.byu.hbll.box.internal.spring;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxConfigurable;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ObjectFactory;
import edu.byu.hbll.box.ProcessBatch;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.Processor;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.internal.core.DefaultObjectFactory;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import lombok.Getter;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Allows Spring Beans to be used as Box objects. A <code>bean:</code> prefix must be set on the
 * <code>type</code> value otherwise the {@link DefaultObjectFactory} is used which tries to
 * instantiate the class denoted by the given type. The <code>bean:</code> prefix is needed to
 * ensure backwards compatibility.
 *
 * <p>When marked with <code>bean:</code>, the remainder of the type value is first assumed to be a
 * bean name. If there is no bean by that name, it then assumes the given type is a class name and
 * tries to retrieve a bean of that class type.
 *
 * <p>Because Box only has an implicit dependency on these beans and not explicit, there is no
 * guarantee that these beans will initialize before Box. Instead of attempting to return a bean
 * that may or may not exist yet, a bean wrapper is returned to satisfy Box and the Bean is
 * initialized later. The {@link BoxConfigurable#postConstruct(ConstructConfig)} and {@link
 * BoxConfigurable#postInit(InitConfig)} methods are called the {@link ContextRefreshedEvent}
 * listener.
 */
@Component
public class SpringBootObjectFactory implements ObjectFactory {

  private BeanFactory beanFactory;
  private List<BeanWrapper> beanWrappers = new ArrayList<>();

  /**
   * Creates a new {@link SpringBootObjectFactory} with the given {@link BeanFactory}.
   *
   * @param beanFactory the bean factory
   */
  public SpringBootObjectFactory(BeanFactory beanFactory) {
    this.beanFactory = beanFactory;
  }

  @Override
  public Object create(String type) {
    if (!type.toLowerCase().startsWith("bean:")) {
      try {
        return new DefaultObjectFactory().create(type);
      } catch (RuntimeException e) {
        throw new IllegalArgumentException("Unable instantiate class of type " + type);
      }
    }

    BeanWrapper beanWrapper = new BeanWrapper(type.substring("bean:".length()));
    beanWrappers.add(beanWrapper);
    return beanWrapper;
  }

  /** Find beans once all have been initialized. */
  @EventListener(ContextRefreshedEvent.class)
  public void findBeans() {
    beanWrappers.forEach(b -> b.init());
  }

  /** Wrapper for Box Bean objects. */
  public class BeanWrapper extends BoxConfigurableWrapper
      implements Processor, Harvester, BoxDatabase {

    private String type;

    private Processor processor;
    private Harvester harvester;
    private BoxDatabase db;

    private boolean initialized;

    /**
     * Initializes a new bean wrapper with the class type from the Box configuration.
     *
     * @param type the type field from the Box configuration
     */
    private BeanWrapper(String type) {
      this.type = type;
    }

    /**
     * Retrieves the bean throwing an {@link IllegalArgumentException} if bean doesn't exist.
     *
     * @throws IllegalArgumentException if bean doesn't exist
     */
    private void init() {
      if (initialized) {
        return;
      }

      synchronized (this) {
        if (initialized) {
          return;
        }

        BoxConfigurable object = null;

        try {
          // first try to retrieve bean by name
          object = (BoxConfigurable) beanFactory.getBean(type);
        } catch (NoSuchBeanDefinitionException e) {
          // do nothing
        }

        if (object == null) {
          Class<?> c;

          try {
            c = Class.forName(type);
          } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Unable to find bean or class named " + type + ".");
          }

          try {
            // second try to retrieve bean by class
            object = (BoxConfigurable) beanFactory.getBean(c);
          } catch (NoSuchBeanDefinitionException e) {
            throw new IllegalArgumentException("Unable to find bean with name or class of " + type);
          }
        }

        // call the post methods
        object.postConstruct(constructConfig);
        object.postInit(initConfig);

        if (object instanceof Processor) {
          this.processor = (Processor) object;
        }

        if (object instanceof Harvester) {
          this.harvester = (Harvester) object;
        }

        if (object instanceof BoxDatabase) {
          this.db = (BoxDatabase) object;
        }

        this.object = object;
        this.initialized = true;
      }
    }

    @Override
    public int addToQueue(Duration olderThan) {
      return db.addToQueue(olderThan);
    }

    public int addToQueue(Duration olderThan, boolean useHeuristics) {
      return db.addToQueue(olderThan, useHeuristics);
    }

    public void addToQueue(Collection<? extends QueueEntry> elements) {
      db.addToQueue(elements);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void addToQueue(Collection<String> ids, Instant attempt, boolean overwrite) {
      db.addToQueue(ids, attempt, overwrite);
    }

    @Override
    public void deleteFromQueue(Collection<String> ids) {
      db.deleteFromQueue(ids);
    }

    @Override
    public Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
      return db.findDependents(dependencies);
    }

    @Override
    public QueryResult find(BoxQuery query) {
      return db.find(query);
    }

    @Override
    public ObjectNode getHarvestCursor() {
      return db.getHarvestCursor();
    }

    @Override
    public Set<String> listSourceDependencies() {
      // this is called by Box before bean is initialized, so may need to skip
      if (!initialized) {
        return Set.of();
      }

      return db.listSourceDependencies();
    }

    @Override
    public List<String> nextFromQueue(int limit) {
      return db.nextFromQueue(limit);
    }

    @Override
    public void processOrphans(String groupId, Consumer<BoxDocument> function) {
      db.processOrphans(groupId, function);
    }

    @Override
    public void removeDeleted(Duration olderThan) {
      db.removeDeleted(olderThan);
    }

    @Override
    public void save(Collection<? extends BoxDocument> documents) {
      db.save(documents);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void updateDependencies(Collection<? extends BoxDocument> documents) {
      db.updateDependencies(documents);
    }

    @Override
    public void setHarvestCursor(ObjectNode cursor) {
      db.setHarvestCursor(cursor);
    }

    @Override
    public void startGroup(String groupId) {
      db.startGroup(groupId);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void updateProcessed(Collection<String> ids) {
      db.updateProcessed(ids);
    }

    @Override
    public Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {
      return db.findDependencies(ids);
    }

    @Override
    public JsonNode findRegistryValue(String id) {
      return db.findRegistryValue(id);
    }

    @Override
    public void saveRegistryValue(String id, JsonNode value) {
      db.saveRegistryValue(id, value);
    }

    public void clear() {
      db.clear();
    }

    @Override
    public long count(BoxQuery query) {
      return db.count(query);
    }

    @Override
    public HarvestResult harvest(HarvestContext context) {
      return harvester.harvest(context);
    }

    @Override
    public ProcessResult process(ProcessBatch batch) {
      return processor.process(batch);
    }
  }

  /**
   * A simple implementation of a {@link BoxConfigurable} so that {@link BeanWrapper} can gain
   * future access to the construct and init configs.
   */
  public class BoxConfigurableWrapper implements BoxConfigurable {

    /** The underlying Box object. */
    @Getter protected BoxConfigurable object;

    /** The construct config from Box. */
    protected ConstructConfig constructConfig;

    /** The init config from Box. */
    protected InitConfig initConfig;

    @Override
    public void postConstruct(ConstructConfig config) {
      this.constructConfig = config;
    }

    @Override
    public void postInit(InitConfig config) {
      this.initConfig = config;
    }

    @Override
    public void preDestroy() {
      if (object != null) {
        object.preDestroy();
      }
    }
  }
}
