package edu.byu.hbll.box.internal.spring;

import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.Source;
import jakarta.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

/**
 * Service endpoint for the queue.
 *
 * @author Charles Draper
 */
@RestController
@RequestMapping("{sourceName:.+}/{x:queue}")
public class QueueService {

  private static final int BATCH_SIZE = 1000;

  @Autowired private Box box;
  @Autowired HttpServletRequest request;

  /**
   * Posts a file or body of new line separated ids to add to the queue.
   *
   * @param sourceName the Box source to use
   * @param file the file containing the ids if not null
   * @param body the body containing the ids if not null
   * @return a simple response
   * @throws ResponseStatusException if can't read file
   */
  @RequestMapping(
      method = RequestMethod.POST,
      consumes = {MediaType.TEXT_PLAIN_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
  public ResponseEntity<?> post(
      @PathVariable String sourceName,
      @RequestParam(name = "file", required = false) MultipartFile file,
      @RequestBody(required = false) String body) {

    try {
      box.verifySource(sourceName);
    } catch (Exception e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "source not recognized");
    }

    Source source = box.getSource(sourceName);

    List<String> ids = new ArrayList<>();

    try (BufferedReader reader =
        new BufferedReader(
            body != null
                ? new StringReader(body)
                : new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {
      String id;

      while ((id = reader.readLine()) != null) {
        ids.add(id);

        if (ids.size() >= BATCH_SIZE) {
          source.addToQueue(ids);
          ids.clear();
        }
      }

      source.addToQueue(ids);
    } catch (IOException e) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    return ResponseEntity.ok().build();
  }
}
