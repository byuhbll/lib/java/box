package edu.byu.hbll.box.impl;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.setOnInsert;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.DeleteManyModel;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.model.UpdateManyModel;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDatabase;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ObjectType;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.Source;
import edu.byu.hbll.box.internal.core.DocumentHandler;
import edu.byu.hbll.box.internal.util.BoxUtils;
import edu.byu.hbll.box.internal.util.CursorUtils;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.conversions.Bson;

/** A box database that persists to MongoDB. */
@Slf4j
public class MongoDatabase implements BoxDatabase, AutoCloseable {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();
  private static final int BATCH_SIZE = 1000;
  private static final int ADD_TO_QUEUE_BATCH_SIZE = 1000;
  private static final int UPGRADE_BATCH_SIZE = 100;

  private MongoClient mongo;

  private String database;

  @JsonProperty private String sourceName;

  private String documentsName;

  private MongoCollection<Document> queue;
  private MongoCollection<Document> cursor;
  private MongoCollection<Document> metadata;
  private MongoCollection<Document> groups;
  private MongoCollection<Document> documents;
  private MongoCollection<Document> registry;

  private Duration retryDelay = Duration.ofMinutes(1);
  private double retryJitter = 0.5;

  private Document facetsStatusCursorIndex;

  /** Constructor for serializers/deserializers. */
  MongoDatabase() {}

  /**
   * Constructs a new {@link MongoDatabase} that uses the given database and source. Uses the given
   * mongo client for communication.
   *
   * @param mongo the mongo client
   * @param database the name of the database to use
   * @param sourceName the source name
   */
  public MongoDatabase(MongoClient mongo, String database, String sourceName) {
    this.mongo = mongo;
    this.database = database;
    this.sourceName = sourceName;
    init();
  }

  /**
   * Constructs a new {@link MongoDatabase} that uses the given database and source. Connects to
   * localhost with default connection parameters.
   *
   * @param database the name of the database to use
   * @param sourceName the source name
   */
  public MongoDatabase(String database, String sourceName) {
    this(MongoClients.create(), database, sourceName);
  }

  /**
   * Constructs a new {@link MongoDatabase} that uses the given database and source. Uses the given
   * connection string to connect to mongo.
   *
   * @param uri the mongo connection string
   * @param database the name of the database to use
   * @param sourceName the source name
   */
  public MongoDatabase(String uri, String database, String sourceName) {
    this(MongoClients.create(uri), database, sourceName);
  }

  @Override
  public void postInit(InitConfig config) {
    if (config.getObjectType() == ObjectType.BOX_DATABASE) {
      upgradeAsync(config.getBox(), config.getSource());
    }

    this.retryDelay = config.getSource().getConfig().getRetryDelay();
    this.retryJitter = config.getSource().getConfig().getRetryJitter();
  }

  @Override
  public void addToQueue(Collection<? extends QueueEntry> entries) {
    List<UpdateOneModel<Document>> updates = new ArrayList<>();
    int count = 0;

    for (QueueEntry entry : entries) {
      Document updateDoc = new Document("_id", entry.getId()).append("requested", new Date());
      Document insertDoc = new Document();

      Document attemptDoc = entry.isOverwrite() ? updateDoc : insertDoc;
      attemptDoc
          .append("priority", entry.getPriority())
          .append("attempt", Date.from(entry.getAttempt()));

      insertDoc.append("attempts", 0);

      Document element = new Document("$set", updateDoc).append("$setOnInsert", insertDoc);

      updates.add(
          new UpdateOneModel<>(
              eq("_id", entry.getId()), element, new UpdateOptions().upsert(true)));
      count++;

      if (updates.size() == ADD_TO_QUEUE_BATCH_SIZE || count == entries.size()) {
        queue.bulkWrite(updates);
        updates.clear();
      }
    }
  }

  @Override
  public int addToQueue(Duration olderThan) {
    return addToQueue(olderThan, false);
  }

  @Override
  public int addToQueue(Duration olderThan, boolean useHeuristics) {

    int priority = DocumentHandler.QUEUE_PRIORITY_MAINTENANCE;

    if (useHeuristics) {
      // if there is still an item on the queue ready to be processed, placed there by this
      // maintenance method, skip for now
      Bson query = and(lt("attempt", new Date()), eq("priority", priority));

      if (queue.find(query).limit(1).first() != null) {
        return 0;
      }
    }

    Date oldDate = Date.from(Instant.now().minus(olderThan));
    Document query =
        new Document("processed", new Document("$lt", oldDate)).append("status", "READY");

    List<QueueEntry> batch = new ArrayList<>();
    int count = 0;

    for (Document document : metadata.find(query)) {
      batch.add(new QueueEntry(document.getString("_id")).setPriority(priority));

      if (batch.size() == ADD_TO_QUEUE_BATCH_SIZE) {
        addToQueue(batch);
        batch.clear();
      }

      count++;
    }

    if (!batch.isEmpty()) {
      addToQueue(batch);
    }

    return count;
  }

  @Override
  public void clear() {
    // drop all collections
    queue.drop();
    cursor.drop();
    metadata.drop();
    groups.drop();
    documents.drop();
    registry.drop();

    // recreate collections with proper indexes
    init();
  }

  @Override
  public void close() {
    if (mongo != null) {
      mongo.close();
    }
  }

  /**
   * Converts a mongo document to a box document.
   *
   * @param result the mongo document to convert
   * @return the resulting box document
   */
  private BoxDocument convert(Document result) {
    if (result == null) {
      return null;
    }

    Document metadata = result;
    Document document;

    if (result.containsKey("document")) {
      try {
        @SuppressWarnings("unchecked")
        List<Document> docs = ((List<Document>) result.get("document"));

        if (docs.isEmpty()) {
          document = new Document();
        } else {
          document = docs.get(0);
          document.remove("_id");
        }
      } catch (Exception e) {
        log.warn(e.toString(), e);
        return null;
      }
    } else {
      document = new Document();
    }

    metadata.remove("document");
    metadata.put("id", metadata.remove("_id"));
    document.put("@box", metadata);

    if (metadata.containsKey("modified")) {
      try {
        metadata.put("modified", ((Date) metadata.get("modified")).toInstant().toString());
      } catch (Exception e) {
        log.warn(e.toString(), e);
      }
    }

    if (metadata.containsKey("processed")) {
      try {
        metadata.put("processed", ((Date) metadata.get("processed")).toInstant().toString());
      } catch (Exception e) {
        log.warn(e.toString(), e);
      }
    }

    if (metadata.containsKey("cursor")) {
      try {
        metadata.put("cursor", metadata.getLong("cursor").toString());
      } catch (Exception e) {
        log.warn(e.toString(), e);
      }
    }

    BoxDocument boxDocument = mapper.readValue(document.toJson(), BoxDocument.class);

    // for some reason jackson deserializes the facets into a HashSet instead of a LinkedHashSet, so
    // the facets get out of order, this corrects that
    @SuppressWarnings("unchecked")
    List<Document> facets = (List<Document>) metadata.get("facets");

    if (facets != null) {
      boxDocument.clearFacets();

      for (Document facet : facets) {
        boxDocument.addFacets(mapper.readValue(facet.toJson(), Facet.class));
      }
    }

    return boxDocument;
  }

  /**
   * Creates an index on the mongo collection.
   *
   * @param collection the collection to be indexed
   * @param path the path to be indexed
   */
  private Document createIndex(MongoCollection<Document> collection, String... path) {
    Document index = new Document();
    IndexOptions options = new IndexOptions().background(true);

    for (String field : path) {
      index.append(field, 1);
    }

    collection.createIndex(index, options);
    return index;
  }

  @Override
  public void deleteFromQueue(Collection<String> ids) {
    queue.deleteMany(eq("_id", new Document("$in", ids)));
  }

  @Override
  public QueryResult find(BoxQuery query) {

    List<Document> pipeline = new ArrayList<>();
    Document indexHint = null;

    if (query.isIdQuery()) {
      pipeline.add(
          new Document("$match", new Document("_id", new Document("$in", query.getIds()))));
    } else {
      String comparison = query.isAscendingOrder() ? "$gte" : "$lte";
      pipeline.add(
          new Document(
              "$match",
              new Document("cursor", new Document(comparison, query.getCursorOrDefault()))));
    }

    List<String> statuses = new ArrayList<>();
    query.getStatusesOrDefault().forEach(s -> statuses.add(s.toString()));
    pipeline.add(new Document("$match", new Document("status", new Document("$in", statuses))));

    if (!query.getFacets().isEmpty()) {
      // group facets by facet name
      Map<String, List<Document>> facetMap = new HashMap<>();
      query.getFacets().stream()
          .forEach(
              f ->
                  facetMap
                      .computeIfAbsent(f.getName(), k -> new ArrayList<>())
                      .add(new Document("name", f.getName()).append("value", f.getValue())));

      // query facets so that OR logic is used within a facet group, but AND logic is used between
      // groups
      facetMap.entrySet().stream()
          .forEach(
              f ->
                  pipeline.add(
                      new Document(
                          "$match", new Document("facets", new Document("$in", f.getValue())))));

      // Mongo sometimes chooses a suboptimal index for a facet query so this forces the facet index
      // this is generally considered a hack solution, but not sure of another way to fix it
      // maybe future releases of Mongo will do better at choosing indexes
      indexHint = facetsStatusCursorIndex;
    }

    if (query.isHarvestQuery()) {
      int order = query.isAscendingOrder() ? 1 : -1;
      pipeline.add(new Document("$sort", new Document("cursor", order)));

      query.getOffset().ifPresent(it -> pipeline.add(new Document("$skip", it)));

      if (query.getLimitOrDefault() >= 0) {
        pipeline.add(new Document("$limit", query.getLimitOrDefault()));
      }
    }

    if (!isMetadataOnly(query)) {
      Document lookup =
          new Document("from", documentsName)
              .append("localField", "_id")
              .append("foreignField", "_id")
              .append("as", "document");

      pipeline.add(new Document("$lookup", lookup));
    }

    Document projection = projectFields(query);

    if (projection != null) {
      pipeline.add(new Document("$project", projection));
    }

    Map<String, BoxDocument> documentMap = new LinkedHashMap<>();

    var aggregatePipeline = metadata.aggregate(pipeline);

    if (indexHint != null) {
      aggregatePipeline.hint(indexHint);
    }

    for (Document document : aggregatePipeline) {
      BoxDocument processDocument = convert(document);

      if (processDocument != null) {
        documentMap.put(processDocument.getId(), processDocument);
      }
    }

    QueryResult result = new QueryResult();

    if (query.isIdQuery()) {
      for (String id : query.getIds()) {
        BoxDocument processDocument = documentMap.get(id);

        if (processDocument == null) {
          processDocument = new BoxDocument(id);
        }

        result.add(processDocument);
      }
    } else {
      result.addAll(documentMap.values());
    }

    result.updateNextCursor(query);

    return result;
  }

  @Override
  public Map<String, Set<DocumentId>> findDependencies(Collection<String> ids) {

    Map<String, Set<DocumentId>> dependencies = new HashMap<>();

    for (Document registry : metadata.find(new Document("_id", new Document("$in", ids)))) {
      BoxDocument document = convert(registry);
      dependencies.put(document.getId(), document.getDependencies());
    }

    return dependencies;
  }

  @Override
  public Map<DocumentId, Set<String>> findDependents(Collection<DocumentId> dependencies) {
    Map<DocumentId, Set<String>> map = new HashMap<>();

    for (DocumentId dependency : dependencies) {
      Set<String> ids = new HashSet<>();
      String id = dependency.getId();
      String sourceName = dependency.getSourceName();

      Document query =
          new Document("dependencies", new Document("sourceName", sourceName).append("id", id));

      for (Document document : metadata.find(query)) {
        ids.add(document.getString("_id"));
      }

      map.put(dependency, ids);
    }

    return map;
  }

  @Override
  public JsonNode findRegistryValue(String id) {
    Document document = registry.find(eq("_id", id)).first();

    if (document != null) {
      Document data = (Document) document.get("value");

      if (data != null) {
        JsonNode dataNode = mapper.readTree(data.toJson());
        return dataNode;
      }
    }

    return null;
  }

  /**
   * Returns the mongo collection given the name.
   *
   * @param name name of the collection
   * @return the mongo collection
   */
  private MongoCollection<Document> getCollection(String name) {
    return mongo.getDatabase(database).getCollection(getCollectionName(name));
  }

  /**
   * Returns the full collection name by prepending the source name.
   *
   * @param name the name of type of collection
   * @return the full collection name
   */
  private String getCollectionName(String name) {
    return sourceName + "_" + name;
  }

  @Override
  public ObjectNode getHarvestCursor() {
    ObjectNode cursor = (ObjectNode) findRegistryValue("cursor");

    if (cursor == null) {
      cursor = mapper.createObjectNode();
    }

    return cursor;
  }

  /** Initializes the database. */
  private void init() {

    documentsName = getCollectionName("documents");

    queue = getCollection("queue");
    cursor = getCollection("cursor");
    metadata = getCollection("metadata");
    documents = getCollection("documents");
    groups = getCollection("groups");
    registry = getCollection("registry");

    // find documents dependent on a source document
    createIndex(metadata, "dependencies");

    // find dependent sources (findDependentSources)
    createIndex(metadata, "dependencies.sourceName");

    // harvest documents
    createIndex(metadata, "status", "cursor");

    // reprocess old documents
    createIndex(metadata, "processed");

    // remove old deleted documents
    createIndex(metadata, "modified", "status");

    // harvest documents by facet
    facetsStatusCursorIndex = createIndex(metadata, "facets", "status", "cursor");

    // for finding orphans
    createIndex(metadata, "groupId", "processed");

    // for the upgrade to 2.2
    createIndex(metadata, "hash");

    // poll the next tasks from the queue
    createIndex(queue, "priority", "attempt");
  }

  private boolean isMetadataOnly(BoxQuery query) {

    if (query.getFields().isEmpty()) {
      return false;
    }

    for (String field : query.getFields()) {
      if (!field.equals("@box") && !field.startsWith("@box.")) {
        return false;
      }
    }

    return true;
  }

  @Override
  public Set<String> listSourceDependencies() {
    Set<String> sourceDependencies = new HashSet<>();

    for (String dependency : metadata.distinct("dependencies.sourceName", String.class)) {
      // mongo 3.6 returns null when no matches are found
      if (dependency != null) {
        sourceDependencies.add(dependency);
      }
    }

    return sourceDependencies;
  }

  @Override
  public List<String> nextFromQueue(int limit) {

    FindIterable<Document> tasks =
        queue
            .find(new Document("attempt", new Document("$lte", new Date())))
            .sort(new Document("priority", 1).append("attempt", 1))
            .limit(limit);

    List<String> ids = new ArrayList<>();
    List<UpdateOneModel<Document>> updates = new ArrayList<>();

    for (Document task : tasks) {
      String id = task.getString("_id");
      int attempts = task.getInteger("attempts");
      Date requested = task.getDate("requested");
      Instant nextAttempt = BoxUtils.retryDateTime(requested.toInstant(), retryDelay, retryJitter);

      ids.add(id);

      updates.add(
          new UpdateOneModel<>(
              eq("_id", id),
              new Document(
                  "$set",
                  new Document("attempt", Date.from(nextAttempt))
                      .append("attempts", attempts + 1))));
    }

    if (!updates.isEmpty()) {
      queue.bulkWrite(updates);
    }

    return ids;
  }

  @Override
  public void postConstruct(ConstructConfig config) {

    this.sourceName = config.getSourceName();
    ObjectNode params = config.getParams();

    if (!params.has("database")) {
      throw new IllegalArgumentException(
          "The parameter `database` is required. This should be the name of the database to use.");
    }

    String uri = params.path("uri").asText("mongodb://localhost/");
    database = params.path("database").asText();

    mongo = MongoClients.create(uri);

    init();
  }

  @Override
  public void processOrphans(String groupId, Consumer<BoxDocument> function) {
    Document groupDocument = groups.findOneAndDelete(eq("_id", groupId));

    if (groupDocument != null) {
      Date start = groupDocument.getDate("start");

      for (Document document : metadata.find(and(eq("groupId", groupId), lt("processed", start)))) {
        function.accept(convert(document));
      }
    }
  }

  private Document projectFields(BoxQuery query) {
    if (query.getFields().isEmpty()) {
      // no projection
      return null;
    }

    boolean boxField = query.getFields().contains("@box");
    boolean boxFields = query.getFields().stream().anyMatch(f -> f.startsWith("@box."));

    Document projection = new Document("_id", 1).append("cursor", 1).append("status", 1);

    if (boxField && !boxFields) {
      projection.append("modified", 1);
      projection.append("processed", 1);
      projection.append("message", 1);
      projection.append("facets", 1);
      projection.append("dependencies", 1);
      projection.append("groupId", 1);
    }

    for (String field : query.getFields()) {
      if (field.equals("@doc")) {
        projection.append("document", 1);
      } else if (field.startsWith("@box.")) {
        projection.append(field.substring(5), 1);
      } else if (field.startsWith("@doc.")) {
        projection.append("document." + field.substring(5), 1);
      } else {
        projection.append("document." + field, 1);
      }
    }

    return projection;
  }

  @Override
  public void removeDeleted(Duration olderThan) {
    Date oldDate = Date.from(Instant.now().minus(olderThan));
    Document query =
        new Document("modified", new Document("$lt", oldDate))
            .append("status", BoxDocument.Status.DELETED.toString());

    List<String> ids;

    do {
      ids = new ArrayList<>();

      for (Document document : metadata.find(query).limit(BATCH_SIZE)) {
        ids.add(document.getString("_id"));
      }

      if (!ids.isEmpty()) {
        queue.deleteMany(in("_id", ids));
        documents.deleteMany(in("_id", ids));
        metadata.deleteMany(in("_id", ids));
      }

    } while (ids.size() == BATCH_SIZE);
  }

  private Long hashToLong(BoxDocument document) {
    return ByteBuffer.wrap(document.hash()).getLong();
  }

  @Override
  public void save(Collection<? extends BoxDocument> documents) {
    List<WriteModel<Document>> documentsWrites = new ArrayList<>();
    List<WriteModel<Document>> metadataWrites = new ArrayList<>();

    // first gather all the hashes
    List<String> ids = documents.stream().map(d -> d.getId()).collect(Collectors.toList());
    Map<String, Long> documentHashes = new HashMap<>();
    Set<String> processed = new HashSet<>();

    for (Document doc :
        metadata.find(in("_id", ids)).projection(new Document("hash", 1).append("status", 1))) {
      String id = doc.getString("_id");
      Number hash = (Number) doc.get("hash");
      String status = doc.getString("status");

      if (hash != null) {
        documentHashes.put(id, hash.longValue());
      }

      if (status != null && (status.equals("READY") || status.equals("DELETED"))) {
        processed.add(id);
      }
    }

    List<String> unchanged = new ArrayList<>();
    List<String> deleted = new ArrayList<>();

    for (BoxDocument boxDocument : documents) {
      String id = boxDocument.getId();
      long hash = hashToLong(boxDocument);

      if (documentHashes.containsKey(id) && documentHashes.get(id) == hash) {
        unchanged.add(id);
      } else {
        ObjectNode document = boxDocument.toJson();
        ObjectNode metadata = (ObjectNode) document.remove("@box");

        document.put("_id", id);
        metadata.put("_id", id);
        metadata.remove("id");
        metadata.put("hash", hash);

        // handle the document
        if (boxDocument.isDeleted()) {
          deleted.add(id);
        } else if (boxDocument.isReady()) {
          documentsWrites.add(
              new ReplaceOneModel<>(
                  eq("_id", id),
                  Document.parse(document.toString()),
                  new ReplaceOptions().upsert(true)));
        }

        // handle the metadata
        if (boxDocument.isProcessed() || !processed.contains(id)) {
          Document metadataDocument = Document.parse(metadata.toString());
          Instant now = Instant.now();
          metadataDocument.put("cursor", CursorUtils.nextCursor());
          metadataDocument.put("modified", Date.from(now));
          metadataDocument.put("processed", Date.from(now));

          metadataWrites.add(
              new ReplaceOneModel<>(
                  eq("_id", id), metadataDocument, new ReplaceOptions().upsert(true)));
        }
      }
    }

    documentsWrites.add(new DeleteManyModel<>(in("_id", deleted)));
    metadataWrites.add(new UpdateManyModel<>(in("_id", unchanged), set("processed", new Date())));

    this.documents.bulkWrite(documentsWrites);
    this.metadata.bulkWrite(metadataWrites);
  }

  @Override
  public void saveRegistryValue(String id, JsonNode data) {
    Document document = new Document("_id", id);
    document.append("value", Document.parse(data.toString()));
    registry.replaceOne(eq("_id", id), document, new ReplaceOptions().upsert(true));
  }

  @Override
  public void setHarvestCursor(ObjectNode cursor) {
    saveRegistryValue("cursor", cursor);
  }

  @Override
  public void startGroup(String groupId) {
    groups.updateOne(
        eq("_id", groupId), setOnInsert("start", new Date()), new UpdateOptions().upsert(true));
  }

  @Override
  public void updateDependencies(Collection<? extends BoxDocument> documents) {
    for (BoxDocument boxDocument : documents) {
      ObjectNode document = boxDocument.toJson();
      Object dependencies = Document.parse(document.path("@box").toString()).get("dependencies");
      String operation = dependencies == null ? "$unset" : "$set";

      this.metadata.updateOne(
          eq("_id", boxDocument.getId()),
          new Document(operation, new Document("dependencies", dependencies)));
    }
  }

  @Override
  public void updateProcessed(Collection<String> ids) {
    metadata.updateMany(
        eq("_id", new Document("$in", ids)),
        new Document("$set", new Document("processed", new Date())));
  }

  @Override
  public long count(BoxQuery query) {
    if (query.isIdQuery()) {
      return query.getIds().size();
    }

    List<Bson> pipeline = new ArrayList<>();

    pipeline.add(gte("cursor", query.getCursorOrDefault()));

    Set<BoxDocument.Status> statuses = query.getStatusesOrDefault();

    if (!statuses.isEmpty()) {
      pipeline.add(in("status", statuses.stream().map(s -> s.toString()).collect(toList())));
    }

    query.getFacets().stream()
        .collect(
            groupingBy(
                Facet::getName,
                mapping(
                    f -> new Document("name", f.getName()).append("value", f.getValue()),
                    toList())))
        .entrySet()
        .forEach(f -> pipeline.add(in("facets", f.getValue())));

    long count = metadata.countDocuments(and(pipeline));
    return count;
  }

  /** Upgrades the database from earlier versions asynchronously. */
  private void upgradeAsync(Box box, Source source) {
    box.getThreadFactory().newThread(() -> upgrade(source)).start();
  }

  /** Upgrades the database from earlier versions. */
  private void upgrade(Source source) {
    Instant giveUp = Instant.now().plus(10, ChronoUnit.MINUTES);

    while (Instant.now().isBefore(giveUp)) {
      if (source.isPrimary()) {
        upgradeTo2Dot3();
        upgradeTo2Dot2();
        break;
      }

      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        break;
      }
    }
  }

  /** Upgrading to version 2.2. This adds the hash if not present. */
  private void upgradeTo2Dot2() {
    boolean more = true;

    while (more) {
      List<String> batch = new ArrayList<>();

      for (Document doc :
          metadata
              .find(exists("hash", false))
              .projection(include("_id"))
              .limit(UPGRADE_BATCH_SIZE)) {
        batch.add(doc.getString("_id"));
      }

      if (batch.isEmpty()) {
        more = false;
      } else {
        List<UpdateOneModel<Document>> hashUpdates = new ArrayList<>();

        for (BoxDocument document : find(new BoxQuery(batch))) {
          long hash = hashToLong(document);
          hashUpdates.add(
              new UpdateOneModel<>(
                  and(eq("_id", document.getId()), exists("hash", false)), set("hash", hash)));
        }

        metadata.bulkWrite(hashUpdates);
      }
    }
  }

  /** Upgrading to version 2.3. This moves the cursor into the registry. */
  private void upgradeTo2Dot3() {
    if (cursor.countDocuments() > 0) {
      Document document = cursor.find().first();
      registry.replaceOne(
          eq("_id", document.get("_id")), document, new ReplaceOptions().upsert(true));
      cursor.drop();
    }
  }
}
