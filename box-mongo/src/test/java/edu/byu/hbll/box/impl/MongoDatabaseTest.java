package edu.byu.hbll.box.impl;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Updates.unset;
import static edu.byu.hbll.witness.WitnessUtils.sleep;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import edu.byu.hbll.box.Box;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.DocumentId;
import edu.byu.hbll.box.InitConfig;
import edu.byu.hbll.box.ObjectType;
import edu.byu.hbll.box.QueryResult;
import edu.byu.hbll.box.QueueEntry;
import edu.byu.hbll.box.SourceConfig;
import edu.byu.hbll.box.internal.util.CursorUtils;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.witness.MongoWitness;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * @author Charles Draper
 */
@Testcontainers
class MongoDatabaseTest {

  public static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();
  static final BoxDocument.Status READY = BoxDocument.Status.READY;
  static final BoxDocument.Status DELETED = BoxDocument.Status.DELETED;
  static final BoxDocument.Status ERROR = BoxDocument.Status.ERROR;
  static final BoxDocument.Status UNPROCESSED = BoxDocument.Status.UNPROCESSED;

  @Container
  private static GenericContainer<?> mongoContainer =
      new GenericContainer<>("mongo:4.2").withExposedPorts(27017);

  private static String host;
  private static int port;
  private static String uri;
  private static MongoClient client;
  private static MongoDatabase db;
  private static MongoWitness witness;

  private static MongoCollection<Document> queue;
  private static MongoCollection<Document> metadata;
  private static MongoCollection<Document> documents;
  private static MongoCollection<Document> groups;
  private static MongoCollection<Document> registry;

  @BeforeAll
  public static void beforeAll() {
    host = mongoContainer.getContainerIpAddress();
    port = mongoContainer.getFirstMappedPort();
    uri = "mongodb://" + host + ":" + port + "/";
    client = MongoClients.create(uri);
    db = new MongoDatabase(client, "test", "test");
    witness =
        MongoWitness.builder()
            .uri(uri)
            .databaseName("test")
            .validate("/documents/*/requested/$date", n -> Instant.parse(n.asText()) != null)
            .validate("/documents/*/cursor", n -> n.asLong() > 0)
            .validate("/documents/*/modified/$date", n -> n.asLong() >= 0)
            .validate("/documents/*/processed/$date", n -> n.asLong() >= 0)
            .validate("/test_metadata/documents/*/cursor", n -> n.asLong() > 0)
            .validate("/test_metadata/documents/*/modified/$date", n -> n.asLong() >= 0)
            .validate("/test_metadata/documents/*/processed/$date", n -> n.asLong() >= 0)
            .validate("/cursor", n -> n.asLong() > 0)
            .validate("/modified/$date", n -> n.asLong() >= 0)
            .validate("/processed/$date", n -> n.asLong() >= 0)
            .validate("/*/@box/cursor", n -> n.asLong() > 0)
            .validate("/*/@box/modified", n -> Instant.parse(n.asText()) != null)
            .validate("/*/@box/processed", n -> Instant.parse(n.asText()) != null)
            .saveOriginal(true)
            .build();

    queue = collection("queue");
    metadata = collection("metadata");
    documents = collection("documents");
    groups = collection("groups");
    registry = collection("registry");

    // initialize collections not already initialized
    db.setHarvestCursor(mapper.createObjectNode());
    db.save(List.of(new BoxDocument("1").setAsReady()));
  }

  private static MongoCollection<Document> collection(String name) {
    return client.getDatabase("test").getCollection("test_" + name);
  }

  @AfterAll
  public static void afterAll() throws IOException {
    db.close();
  }

  @BeforeEach
  public void beforeEach() {
    witness.clearDatabase();
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#MongoDatabase()}.
   *
   * @throws IOException
   */
  @Test
  void testMongoDatabase() throws IOException {
    try (MongoDatabase db = new MongoDatabase()) {}
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#MongoDatabase(com.mongodb.MongoClient, java.lang.String,
   * java.lang.String)}.
   *
   * @throws IOException
   */
  @Test
  void testMongoDatabaseMongoClientStringString() throws IOException {
    try (MongoDatabase db = new MongoDatabase(MongoClients.create(uri), "test", "test")) {
      // tests that the indexes are set up properly
      witness.testifyDatabase();
    }
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#MongoDatabase(java.lang.String,
   * java.lang.String)}.
   *
   * @throws IOException
   */
  @Test
  void testMongoDatabaseStringString() throws IOException {
    // unable to reliably test in CI/CD environment
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#MongoDatabase(java.lang.String,
   * java.lang.String, java.lang.String)}.
   *
   * @throws IOException
   */
  @Test
  void testMongoDatabaseStringStringString() throws IOException {
    try (MongoDatabase db = new MongoDatabase(uri, "test", "test")) {
      // tests that the indexes are set up properly
      witness.testifyDatabase();
    }
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#addToQueue(java.util.Collection,
   * java.time.Instant, boolean)}.
   */
  @Test
  @SuppressWarnings("deprecation")
  void testAddToQueueCollectionOfStringInstantBoolean() {
    Instant attempt1 = Instant.parse("2000-01-01T00:00:00Z");
    Instant attempt2 = Instant.parse("2001-01-01T00:00:00Z");
    db.addToQueue(List.of("1", "2", "3"), attempt1, false);
    witness.testifyCollection("test_queue", 1);
    db.addToQueue(List.of("1", "2", "3"), attempt2, false);
    witness.testifyCollection("test_queue", 2);
    db.addToQueue(List.of("1", "2", "3"), attempt2, true);
    witness.testifyCollection("test_queue", 3);
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#addToQueue(java.util.Collection,
   * java.time.Instant, boolean)}.
   *
   * <p>Tests the add to queue batching.
   */
  @Test
  @SuppressWarnings("deprecation")
  void testAddToQueueBatching() {
    List<String> ids = new ArrayList<>();
    int total = 7431;

    for (int i = 0; i < total; i++) {
      ids.add(i + "");
    }

    db.addToQueue(ids, Instant.now(), false);
    assertEquals(total, queue.countDocuments());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#addToQueue(java.time.Duration)}. */
  @Test
  void testAddToQueueDuration() {
    db.save(List.of(new BoxDocument("1").setAsReady()));
    db.save(List.of(new BoxDocument("4").setAsDeleted()));
    sleep(600);
    db.save(List.of(new BoxDocument("2").setAsReady()));
    db.save(List.of(new BoxDocument("5").setAsDeleted()));
    sleep(600);
    db.save(List.of(new BoxDocument("3").setAsReady()));
    db.save(List.of(new BoxDocument("6").setAsDeleted()));
    sleep(600);

    assertEquals(0, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(2100));
    assertEquals(0, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(1500));
    assertEquals(1, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(900));
    assertEquals(2, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(300));
    assertEquals(3, queue.countDocuments());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#addToQueue(java.time.Duration,
   * boolean)}.
   */
  @Test
  void testAddToQueueDurationBoolean() {
    db.save(List.of(new BoxDocument("1").setAsReady()));
    db.save(List.of(new BoxDocument("4").setAsDeleted()));
    sleep(600);
    db.save(List.of(new BoxDocument("2").setAsReady()));
    db.save(List.of(new BoxDocument("5").setAsDeleted()));
    sleep(600);
    db.save(List.of(new BoxDocument("3").setAsReady()));
    db.save(List.of(new BoxDocument("6").setAsDeleted()));
    sleep(600);

    assertEquals(0, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(2100), true);
    assertEquals(0, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(1500), true);
    assertEquals(1, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(900), true);
    assertEquals(1, queue.countDocuments());
    db.addToQueue(Duration.ofMillis(300), true);
    assertEquals(1, queue.countDocuments());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#clear()}. */
  @Test
  @SuppressWarnings("deprecation")
  void testClear() {
    db.save(List.of(new BoxDocument("1").setAsReady()));
    db.addToQueue(List.of("1"), Instant.now(), false);
    assertEquals(1, metadata.countDocuments());
    assertEquals(1, documents.countDocuments());
    assertEquals(1, queue.countDocuments());
    db.clear();
    assertEquals(0, metadata.countDocuments());
    assertEquals(0, documents.countDocuments());
    assertEquals(0, queue.countDocuments());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#close()}.
   *
   * @throws IOException
   */
  @Test
  void testClose() throws IOException {
    try (MongoDatabase db = new MongoDatabase()) {
      db.close();
    }
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#deleteFromQueue(java.util.Collection)}.
   */
  @Test
  @SuppressWarnings("deprecation")
  void testDeleteFromQueue() {
    db.addToQueue(List.of("1", "2", "3"), Instant.now(), false);
    assertEquals(3, queue.countDocuments());
    db.deleteFromQueue(List.of("1"));
    assertEquals(2, queue.countDocuments());
    db.deleteFromQueue(List.of("1", "4"));
    assertEquals(2, queue.countDocuments());
    db.deleteFromQueue(List.of("2", "3"));
    assertEquals(0, queue.countDocuments());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#find(edu.byu.hbll.box.BoxQuery)}.
   */
  @Test
  void testFind() {
    ObjectNode node = mapper.createObjectNode().put("a", "1");
    node.putObject("b").put("b1", "1").put("b2", "2");

    db.save(List.of(new BoxDocument("5").setAsReady()));
    long cursor5 = db.find(new BoxQuery("5")).get(0).getCursor().orElse(0L);

    List<BoxDocument> documents =
        List.of(
            new BoxDocument("1"),
            new BoxDocument("2").setAsReady(),
            new BoxDocument("3").setAsDeleted(),
            new BoxDocument("4").setAsError("Error in processing"),
            new BoxDocument("6")
                .setDocument(node)
                .addDependency("a", "1")
                .addDependency("a", "2")
                .addDependency("b", "1")
                .setGroupId("a"),
            new BoxDocument("7")
                .setAsReady()
                .addFacet("a", "1")
                .addFacet("a", "2")
                .addFacet("b", "1"),
            new BoxDocument("8").setAsReady().addFacet("a", "2").addFacet("b", "1"));

    db.save(documents);

    assertAll(
        () -> witness.testify(db.find(new BoxQuery()), 1),
        () -> witness.testify(db.find(new BoxQuery("2", "3", "99")), 2),
        () -> witness.testify(db.find(new BoxQuery().addStatus(DELETED).addStatus(ERROR)), 3),
        () -> witness.testify(db.find(new BoxQuery().setCursor(cursor5 + 1)), 4),
        () -> witness.testify(db.find(new BoxQuery().setLimit(2)), 5),
        () -> witness.testify(db.find(new BoxQuery().setUnlimited()), 6),
        () -> witness.testify(db.find(new BoxQuery("6").addField("@box")), 7),
        () -> witness.testify(db.find(new BoxQuery("6").addField("@doc")), 8),
        () -> witness.testify(db.find(new BoxQuery("6").addField("b.b1")), 9),
        () -> witness.testify(db.find(new BoxQuery("6").addFields("a", "b.b2")), 10),
        () -> witness.testify(db.find(new BoxQuery("6").addFields("a", "@box.processed")), 11),
        () -> witness.testify(db.find(new BoxQuery().addFacet("a", "1")), 12),
        () -> witness.testify(db.find(new BoxQuery().addFacet("a", "1").addFacet("a", "2")), 13),
        () -> witness.testify(db.find(new BoxQuery().addFacet("a", "1").addFacet("b", "1")), 14),
        () -> witness.testify(db.find(new BoxQuery().setOffset(2)), 15),
        () -> witness.testify(db.find(new BoxQuery().setDescendingOrder()), 16));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#findDependencies(java.util.Collection)}.
   */
  @Test
  void testFindDependencies() {
    db.save(List.of(new BoxDocument("1").addDependency("dep1", "1")));
    db.save(List.of(new BoxDocument("2").addDependency("dep1", "1").addDependency("dep2", "2")));
    witness.testify(db.findDependencies(List.of("1", "2", "3")));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#findDependents(java.util.Collection)}.
   */
  @Test
  void testFindDependents() {
    db.save(List.of(new BoxDocument("1").addDependency("dep1", "1")));
    db.save(List.of(new BoxDocument("2").addDependency("dep1", "1").addDependency("dep2", "2")));
    witness.testify(
        db.findDependents(
            List.of(
                new DocumentId("dep1", "1"),
                new DocumentId("dep2", "2"),
                new DocumentId("dep3", "3"))));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#findRegistryValue(java.lang.String)}.
   */
  @Test
  void testFindRegistryValue() {
    db.saveRegistryValue("test", JsonNodeFactory.instance.objectNode().put("a", "b"));
    assertEquals("b", db.findRegistryValue("test").path("a").asText());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#getHarvestCursor()}. */
  @Test
  void testGetHarvestCursor() {
    db.setHarvestCursor(JsonNodeFactory.instance.objectNode().put("a", "b"));
    assertEquals("b", db.getHarvestCursor().path("a").asText());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#listSourceDependencies()}. */
  @Test
  void testListSourceDependencies() {
    db.save(List.of(new BoxDocument("1").addDependency("dep1", "1")));
    db.save(List.of(new BoxDocument("2").addDependency("dep1", "1").addDependency("dep2", "2")));
    assertEquals(Set.of("dep1", "dep2"), db.listSourceDependencies());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#nextFromQueue(int)}. */
  @Test
  void testNextFromQueue() {
    Instant now = Instant.now();

    for (int i = 1; i <= 10; i++) {
      // first nine should be ready, tenth will not be ready
      db.addToQueue(List.of(new QueueEntry(i + "", now.plusSeconds(i - 9), 1, false)));
    }

    assertEquals(10, queue.countDocuments());
    assertEquals(10, queue.countDocuments(eq("attempts", 0)));
    assertEquals(List.of("1", "2", "3"), db.nextFromQueue(3));
    assertEquals(List.of("4", "5", "6", "7", "8", "9"), db.nextFromQueue(10));
    assertEquals(10, queue.countDocuments(gt("attempt", new Date())));
    assertEquals(1, queue.countDocuments(lt("attempt", Date.from(Instant.now().plusSeconds(1)))));
    assertEquals(
        9,
        queue.countDocuments(
            and(
                gt("attempt", Date.from(Instant.now().plusSeconds(25))),
                lt("attempt", Date.from(Instant.now().plusSeconds(95))))));
    assertEquals(9, queue.countDocuments(eq("attempts", 1)));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#postConstruct(edu.byu.hbll.box.ConstructConfig)}.
   *
   * @throws IOException
   */
  @Test
  void testPostConstruct() throws IOException {
    try (MongoDatabase db = new MongoDatabase()) {
      ConstructConfig config = new ConstructConfig();
      config.setObjectType(ObjectType.BOX_DATABASE);
      config.setSourceName("test");

      ObjectNode params = mapper.createObjectNode();
      config.setParams(params);

      assertThrows(IllegalArgumentException.class, () -> db.postConstruct(config));

      params.put("uri", uri);
      params.put("database", "test");

      db.postConstruct(config);
      witness.testifyDatabase();
    }
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#processOrphans(java.lang.String,
   * java.util.function.Consumer)}.
   */
  @Test
  void testProcessOrphans() {
    db.save(List.of(new BoxDocument("1").setAsReady().setProcessed(Instant.now()).setGroupId("a")));
    db.save(List.of(new BoxDocument("2").setAsReady().setProcessed(Instant.now()).setGroupId("a")));
    db.save(List.of(new BoxDocument("3").setAsReady().setProcessed(Instant.now()).setGroupId("b")));
    sleep(100);
    db.startGroup("a");
    db.save(List.of(new BoxDocument("4").setAsReady().setProcessed(Instant.now()).setGroupId("a")));
    db.save(List.of(new BoxDocument("5").setAsReady().setProcessed(Instant.now()).setGroupId("a")));
    db.save(List.of(new BoxDocument("6").setAsReady().setProcessed(Instant.now()).setGroupId("a")));
    db.save(List.of(new BoxDocument("7").setAsReady().setProcessed(Instant.now()).setGroupId("b")));

    assertEquals(1, groups.countDocuments());

    AtomicInteger counter = new AtomicInteger();
    db.processOrphans("a", d -> counter.getAndIncrement());
    assertEquals(2, counter.get());

    // verify that the group is removed
    assertEquals(0, groups.countDocuments());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#removeDeleted(java.time.Duration)}.
   */
  @Test
  void testRemoveDeleted() {
    db.save(List.of(new BoxDocument("1").setAsDeleted().setModified(Instant.now())));
    db.save(List.of(new BoxDocument("2").setAsDeleted().setModified(Instant.now())));
    db.save(List.of(new BoxDocument("3").setAsReady().setModified(Instant.now())));
    assertEquals(3, metadata.countDocuments());
    sleep(200);
    db.save(List.of(new BoxDocument("4").setAsDeleted().setModified(Instant.now())));
    db.save(List.of(new BoxDocument("5").setAsReady().setModified(Instant.now())));
    assertEquals(5, metadata.countDocuments());
    db.removeDeleted(Duration.ofMillis(100));
    assertEquals(3, metadata.countDocuments());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#save(java.util.Collection)}. */
  @Test
  void testSave() {
    ObjectNode node = mapper.createObjectNode().put("a", "1");
    Instant date = Instant.EPOCH;

    List<BoxDocument> documents =
        List.of(
            new BoxDocument("1"),
            new BoxDocument("2").setAsDeleted(),
            new BoxDocument("3").setAsReady(),
            new BoxDocument("4").setDocument(node),
            new BoxDocument("5").setAsReady().setModified(date),
            new BoxDocument("6").setAsReady().setProcessed(date),
            new BoxDocument("7").setAsReady().setCursor(100L));

    db.save(documents);
    witness.testifyDatabase("database");

    db.save(List.of(new BoxDocument("4").setAsDeleted()));
    witness.testifyDocument("test_metadata", "4", "metadata.4");
    witness.testifyDocument("test_documents", "4", "documents.4");

    // verify that an unprocessed document cannot overwrite a processed one
    db.save(List.of(new BoxDocument("2").setAsError(), new BoxDocument("3")));
    assertFalse(db.find(new BoxQuery("2")).get(0).isError());
    assertFalse(db.find(new BoxQuery("3")).get(0).isUnprocessed());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#saveRegistryValue(java.lang.String,
   * com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  void testSaveRegistryValue() {
    db.saveRegistryValue("test", JsonNodeFactory.instance.objectNode().put("a", "b"));
    assertEquals("b", db.findRegistryValue("test").path("a").asText());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#setHarvestCursor(com.fasterxml.jackson.databind.node.ObjectNode)}.
   */
  @Test
  void testSetHarvestCursor() {
    db.setHarvestCursor(JsonNodeFactory.instance.objectNode().put("a", "b"));
    assertEquals("b", db.getHarvestCursor().path("a").asText());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#startGroup(java.lang.String)}. */
  @Test
  void testStartGroup() {
    Instant now = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    db.startGroup("a");
    Instant actual = groups.find(eq("_id", "a")).first().getDate("start").toInstant();
    assertTrue(actual.equals(now) || actual.isAfter(now));

    // verify that starting group "a" again does not overwrite existing start
    sleep(100);
    db.startGroup("a");
    Instant actual2 = groups.find(eq("_id", "a")).first().getDate("start").toInstant();
    assertEquals(actual, actual2);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#updateDependencies(java.util.Collection)}.
   */
  @Test
  void testUpdateDependencies() {
    db.save(List.of(new BoxDocument("1")));
    db.save(List.of(new BoxDocument("2").addDependency("a", "2")));
    db.save(List.of(new BoxDocument("3").addDependency("a", "3").addDependency("b", "3")));
    db.save(List.of(new BoxDocument("4").setAsReady()));

    List<BoxDocument> documents =
        List.of(
            new BoxDocument("1").addDependency("a", "1"),
            new BoxDocument("2"),
            new BoxDocument("3").addDependency("c", "3").addDependency("d", "3"),
            new BoxDocument("4").addDependency("a", "4"));

    db.updateDependencies(documents);
    witness.testifyCollection("test_metadata");
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.MongoDatabase#updateProcessed(java.util.Collection)}.
   */
  @Test
  void testUpdateProcessed() {
    db.save(List.of(new BoxDocument("1").setProcessed(Instant.now()).setAsReady()));
    db.save(List.of(new BoxDocument("2").setProcessed(Instant.now()).setAsReady()));
    db.save(List.of(new BoxDocument("3").setProcessed(Instant.now()).setAsReady()));
    sleep(10);
    Instant mark1 = Instant.now();
    sleep(100);
    QueryResult result = db.find(new BoxQuery());
    result.forEach(d -> assertTrue(d.getProcessed().get().isBefore(mark1)));
    Instant mark2 = Instant.now();
    sleep(10);
    db.updateProcessed(List.of("1", "2"));
    result = db.find(new BoxQuery());
    assertTrue(result.get(0).getProcessed().get().isAfter(mark2));
    assertTrue(result.get(1).getProcessed().get().isAfter(mark2));
    assertTrue(result.get(2).getProcessed().get().isBefore(mark2));
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.MongoDatabase#count(edu.byu.hbll.box.BoxQuery)}.
   */
  @Test
  void testCountDocuments() {
    List<BoxDocument> documents =
        List.of(
            new BoxDocument("1"),
            new BoxDocument("2").setAsReady(),
            new BoxDocument("3").setAsDeleted(),
            new BoxDocument("4").setAsError(),
            new BoxDocument("5").setAsReady().addFacet("a", "1"),
            new BoxDocument("6").setAsReady().addFacet("a", "2"),
            new BoxDocument("7").setAsReady().addFacet("a", "1").addFacet("b", "1"));

    long start = CursorUtils.nextCursor();
    db.save(documents.subList(0, 4));
    sleep(100);
    db.save(documents.subList(4, documents.size()));

    assertAll(
        () -> assertEquals(3, db.count(new BoxQuery("1", "2", "99"))),
        () -> assertEquals(5, db.count(new BoxQuery())),
        () -> assertEquals(5, db.count(new BoxQuery().setLimit(1))),
        () -> assertEquals(3, db.count(new BoxQuery().setCursor(start + 50000000))),
        () -> assertEquals(2, db.count(new BoxQuery().addFacet("a", "1"))),
        () -> assertEquals(3, db.count(new BoxQuery().addFacet("a", "1").addFacet("a", "2"))),
        () -> assertEquals(1, db.count(new BoxQuery().addFacet("a", "1").addFacet("b", "1"))));
  }

  /** Tests that the priorities are working in the process queue. */
  @Test
  void testPriorityQueue() {
    Instant now = Instant.now();
    Instant past = now.minus(1, ChronoUnit.DAYS);
    Instant future = now.plus(1, ChronoUnit.DAYS);

    List<QueueEntry> entries = new ArrayList<>();
    entries.add(new QueueEntry("1").setPriority(1).setAttempt(past));
    entries.add(new QueueEntry("2").setPriority(1).setAttempt(now));
    entries.add(new QueueEntry("3").setPriority(1).setAttempt(future));
    entries.add(new QueueEntry("4").setPriority(1.5).setAttempt(past));
    entries.add(new QueueEntry("5").setPriority(1.5).setAttempt(now));
    entries.add(new QueueEntry("6").setPriority(1.5).setAttempt(future));
    entries.add(new QueueEntry("7").setPriority(2).setAttempt(past));
    entries.add(new QueueEntry("8").setPriority(2).setAttempt(now));
    entries.add(new QueueEntry("9").setPriority(2).setAttempt(future));

    Collections.shuffle(entries);
    db.addToQueue(entries);

    assertEquals(List.of("1", "2", "4", "5", "7", "8"), db.nextFromQueue(10));
  }

  @Test
  void testUpgradeTo2Dot2() {
    Box box = Box.newBuilder().source(SourceConfig.builder().name("test").build()).build();

    @SuppressWarnings("resource")
    MongoDatabase db = new MongoDatabase(client, "test", "test");
    db.save(List.of(new BoxDocument("1").setAsReady()));
    Instant modified1 = db.find(new BoxQuery()).get(0).getModified().get();
    Instant processed1 = db.find(new BoxQuery()).get(0).getProcessed().get();

    // erase hash
    client
        .getDatabase("test")
        .getCollection("test_metadata")
        .updateOne(new Document(), unset("hash"));

    // pause to allow time for calls to Instant.now() to vary
    sleep(10);

    // make sure modified gets updated with erased hash
    db.save(List.of(new BoxDocument("1").setAsReady()));
    Instant modified2 = db.find(new BoxQuery()).get(0).getModified().get();
    Instant processed2 = db.find(new BoxQuery()).get(0).getProcessed().get();

    assertTrue(modified1.isBefore(modified2));
    assertTrue(processed1.isBefore(processed2));

    // erase hash again
    client
        .getDatabase("test")
        .getCollection("test_metadata")
        .updateOne(new Document(), unset("hash"));

    // pause to allow time for calls to Instant.now() to vary
    sleep(10);

    // now reinstate hash with upgrade
    InitConfig config = new InitConfig();
    config.setBox(box);
    config.setSource(box.getPrincipalSource());
    config.setObjectType(ObjectType.BOX_DATABASE);
    db.postInit(config);

    // pause to allow upgrade to run
    sleep(100);

    // make sure modified does not update
    db.save(List.of(new BoxDocument("1").setAsReady()));
    Instant modified3 = db.find(new BoxQuery()).get(0).getModified().get();
    Instant processed3 = db.find(new BoxQuery()).get(0).getProcessed().get();

    assertEquals(modified2, modified3);
    assertTrue(processed2.isBefore(processed3));
  }

  @Test
  void testUpgradeTo2Dot3() {

    // populate cursor table
    MongoCollection<Document> cursor = collection("cursor");
    cursor.insertOne(new Document("_id", "cursor").append("a", "1"));

    // verify counts
    assertEquals(1, cursor.countDocuments());
    assertEquals(0, registry.countDocuments());

    // setup in order to run upgrade
    Box box = Box.newBuilder().source(SourceConfig.builder().name("test").build()).build();
    @SuppressWarnings("resource")
    MongoDatabase db = new MongoDatabase(client, "test", "test");

    InitConfig config = new InitConfig();
    config.setBox(box);
    config.setSource(box.getPrincipalSource());
    config.setObjectType(ObjectType.BOX_DATABASE);

    // run upgrade
    db.postInit(config);

    // allow async upgrade to finish
    sleep(1000);

    // make sure cursor was moved to registry
    assertEquals(0, cursor.countDocuments());
    assertEquals("1", registry.find(eq("_id", "cursor")).first().get("a"));
  }

  /**
   * Bug: When a hash fits in a 32-bit integer rather than 64-bit, Mongo stores it as a 32-bit
   * integer. This was causing a class cast exception when trying to read the hash field as a long.
   */
  @Test
  void testIntSizedHashes() {
    // an unprocessed document with this id will give a "long" hash that fits in an integer
    String id = "2675409586";
    BoxDocument doc = new BoxDocument(id);

    // verify that the hash fits in an integer
    // if the hash no longer fits in an integer, then something's changed with the hashing algorithm
    // and you'll need to find another id that causes an integer sized hash
    long hash = ByteBuffer.wrap(doc.hash()).getLong();
    assertTrue(hash <= Integer.MAX_VALUE && hash >= Integer.MIN_VALUE);

    // first save it to the database
    db.save(List.of(doc));

    // save it again which causes a comparison on the existing "long" hash
    // this second call should not throw a ClassCastException
    db.save(List.of(doc));
  }
}
