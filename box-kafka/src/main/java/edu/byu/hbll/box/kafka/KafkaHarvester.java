package edu.byu.hbll.box.kafka;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.impl.View;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.time.Duration;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

/**
 * Implementation of {@link Harvester} for ingesting a kafka topic into box.
 *
 * <p>The default behavior of this class is to convert a kafka {@link ConsumerRecord} into a ready
 * {@link BoxDocument}. The record's key is used as the box document's id. See {@link
 * #convertValueToObjectNode(String)} for details on how the value is handled.
 *
 * <p>The default behavior of this class can be overridden by extending the class and overriding
 * {@link #convertRecordToDocument(ConsumerRecord)}, {@link #convertKeyToId(String)}, and {@link
 * #convertValueToObjectNode(String)}.
 *
 * <h2>Configuration</h2>
 *
 * <p>When using this class, you must provide the necessary configuration for connecting to kafka.
 * The desired kafka consumer configs (http://kafka.apache.org/documentation/#consumerconfigs)
 * should be provided in the {@code params} field of the harvester configuration. While most of the
 * kafka consumer configs are passed straight to the {@link KafkaConsumer} as they are provided, the
 * following properties are always set, overriding them if they are provided:
 *
 * <pre>
 * enable.auto.commit=false
 * key.deserializer=org.apache.kafka.common.serialization.StringDeserializer
 * value.deserializer=org.apache.kafka.common.serialization.StringDeserializer
 * </pre>
 *
 * <p>In addition to all the valid kafka consumer configs, the kafka topic to consume should be
 * provided in the {@code topic} param field. Similar to a box {@link View}, this class also
 * supports the {@code harvestUnprocessed} parameter.
 */
public class KafkaHarvester implements Harvester {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  /** The Kafka consumer. */
  protected KafkaConsumer<String, String> consumer;

  /** Add items to queue only if true. */
  protected boolean harvestUnprocessed;

  /** Topics to follow. */
  protected Collection<String> topics;

  @Override
  public void postConstruct(ConstructConfig config) {
    Harvester.super.postConstruct(config);
    this.topics =
        StreamSupport.stream(config.getParams().path("topics").spliterator(), false)
            .map(node -> node.asText())
            .collect(Collectors.toList());
    if (this.topics.isEmpty()) {
      throw new IllegalArgumentException("topics configuration must be provided");
    }

    this.harvestUnprocessed = config.getParams().path("harvestUnprocessed").asBoolean(false);

    config.getParams().remove("topics");
    config.getParams().remove("harvestUnprocessed");

    Properties props = new Properties();
    this.flattenJson("", config.getParams(), props);
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
    props.put(
        ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getCanonicalName());
    props.put(
        ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        StringDeserializer.class.getCanonicalName());

    consumer = new KafkaConsumer<>(props);
  }

  /**
   * Flattens a json object into properties with dot-separated key names.
   *
   * <p>Adapted from https://stackoverflow.com/a/24150263/6137043
   *
   * @param currentPath The current path in the json object.
   * @param json The current json node.
   * @param properties The Properties object to store flattened values in.
   */
  private void flattenJson(String currentPath, JsonNode json, Properties properties) {
    if (json.isObject()) {
      ObjectNode objectNode = (ObjectNode) json;
      Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
      String pathPrefix = currentPath.isEmpty() ? "" : currentPath + ".";

      while (iter.hasNext()) {
        Map.Entry<String, JsonNode> entry = iter.next();
        flattenJson(pathPrefix + entry.getKey(), entry.getValue(), properties);
      }
    } else if (json.isArray()) {
      throw new IllegalArgumentException("Arrays are not supported in kafka properties");
    } else if (json.isValueNode()) {
      properties.put(currentPath, json.asText());
    }
  }

  @Override
  public void preDestroy() {
    this.consumer.close(Duration.ofSeconds(20));
  }

  /**
   * Implementation of {@link Harvester#harvest(HarvestContext)} for consuming records from kafka
   * into a {@link HarvestResult}. For each incoming record, {@link
   * #convertRecordToDocument(ConsumerRecord)} is used to convert the record to a box document. The
   * record is skipped of the returned optional is empty.
   */
  @Override
  public HarvestResult harvest(HarvestContext context) {
    if (this.consumer.subscription().isEmpty()) {
      this.consumer.subscribe(this.topics);
    }
    this.consumer.commitAsync();
    HarvestResult result = new HarvestResult().setMore(true);
    ConsumerRecords<String, String> records = this.consumer.poll(Duration.ofSeconds(10));
    for (ConsumerRecord<String, String> record : records) {
      this.convertRecordToDocument(record).ifPresent(result::add);
    }
    return result;
  }

  /**
   * Converts a kafka {@link ConsumerRecord} to a {@link BoxDocument}. The document id is obtained
   * from {@link #convertKeyToId(String)}, and the document itself is obtained from {@link
   * #convertValueToObjectNode(String)}. If the record's key is empty, the record is skipped
   * entirely. If the record's value is empty, it results in a deleted document.
   *
   * @param record The record to convert
   * @return The resulting BoxDocument, or an empty optional.
   */
  protected Optional<BoxDocument> convertRecordToDocument(ConsumerRecord<String, String> record) {
    Optional<String> id = this.convertKeyToId(record.key());
    if (id.isEmpty()) {
      return Optional.empty();
    }
    BoxDocument document = new BoxDocument(convertKeyToId(record.key()).get());
    if (!harvestUnprocessed) {
      Optional<ObjectNode> value = this.convertValueToObjectNode(record.value());
      if (value.isEmpty()) {
        document.setAsDeleted();
      } else {
        document.setDocument(value.get());
      }
    }
    return Optional.of(document);
  }

  /**
   * Converts a kafka record's key to a box identifier. The default implementation simply uses the
   * key as the id with no conversion.
   *
   * @param key The key to convert
   * @return The box document id, or an empty optional if {@code key} is {@code null}
   */
  protected Optional<String> convertKeyToId(String key) {
    return Optional.ofNullable(key);
  }

  /**
   * Converts a kafka record's value to an ObjectNode for box. The default implementation attempts
   * to parse the value into a JsonNode. If the result is an ObjectNode, that is returned.
   * Otherwise, the resulting JsonNode is returned in an ObjectNode in the {@code value} field. If
   * the json parsing fails altogether, The entire value string is stored in the {@code value} field
   * of an ObjectNode and returned.
   *
   * @param value The value to convert
   * @return The resulting ObjectNode, or an empty optional if {@code value} is {@code null}
   */
  protected Optional<ObjectNode> convertValueToObjectNode(String value) {
    if (value == null) {
      return Optional.empty();
    }
    try {
      JsonNode node = mapper.readTree(value);
      if (node.isObject()) {
        return Optional.of((ObjectNode) node);
      }
      ObjectNode obj = mapper.createObjectNode();
      obj.set("value", node);
      return Optional.of(obj);
    } catch (Exception e) {
      // Do Nothing
    }
    ObjectNode node = mapper.createObjectNode();
    node.put("value", value);
    return Optional.of(node);
  }
}
