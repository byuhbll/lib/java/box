package edu.byu.hbll.box.kafka;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public class KafkaHarvesterTest {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  @Container private static KafkaContainer kafkaContainer = new KafkaContainer();

  private static KafkaProducer<String, String> producer;
  private String topic;
  private KafkaHarvester harvester;

  @BeforeAll
  public static void beforeAll() {
    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaContainer.getBootstrapServers());
    props.put(
        ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
    props.put(
        ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getCanonicalName());
    props.put(ProducerConfig.ACKS_CONFIG, "-1");
    producer = new KafkaProducer<>(props);
  }

  @AfterAll
  public static void afterAll() {
    producer.close();
  }

  @BeforeEach
  public void beforeEach() {
    this.topic = UUID.randomUUID().toString();

    // Because of how spring does properties, this is how the kafka properties arrive at runtime.
    String paramsString =
        "{\"bootstrap\": {\"servers\": \""
            + kafkaContainer.getBootstrapServers()
            + "\"}, \"group\": {\"id\": \"group1\"}, \"auto\": {\"offset\": {\"reset\":"
            + " \"earliest\"}}, \"topics\": [\""
            + topic
            + "\"]}";

    ObjectNode params = (ObjectNode) mapper.readTree(paramsString);
    this.harvester = new KafkaHarvester();
    this.harvester.postConstruct(new ConstructConfig("sourceName", params));
  }

  @AfterEach
  public void afterEach() {
    this.harvester.preDestroy();
  }

  @Test
  public void testHarvest() throws InterruptedException, ExecutionException {
    ProducerRecord<String, String> record =
        new ProducerRecord<>(this.topic, "1234", "{\"key1\": \"value1\"}");

    producer.send(record).get();
    HarvestResult result = harvester.harvest(new HarvestContext());
    assertEquals(1, result.size());
    assertEquals("1234", result.get(0).getId());
    assertEquals("value1", result.get(0).getDocument().path("key1").asText());

    result = harvester.harvest(new HarvestContext());
    assertEquals(0, result.size());

    record = new ProducerRecord<>(this.topic, "5678", "[1, 2, 3, 4]");
    producer.send(record).get();
    result = harvester.harvest(new HarvestContext());
    assertEquals(1, result.size());
    assertEquals("5678", result.get(0).getId());
    assertEquals(4, result.get(0).getDocument().path("value").size());

    record = new ProducerRecord<>(this.topic, "3456", "This is just text");
    producer.send(record).get();
    result = harvester.harvest(new HarvestContext());
    assertEquals(1, result.size());
    assertEquals("3456", result.get(0).getId());
    assertEquals("This is just text", result.get(0).getDocument().path("value").asText());
  }

  @Test
  public void testHarvestNullKey() throws InterruptedException, ExecutionException {
    ProducerRecord<String, String> record =
        new ProducerRecord<>(this.topic, null, "{\"key1\": \"value1\"}");

    producer.send(record).get();
    HarvestResult result = harvester.harvest(new HarvestContext());
    assertEquals(0, result.size());
  }

  @Test
  public void testHarvestNullValue() throws InterruptedException, ExecutionException {
    ProducerRecord<String, String> record = new ProducerRecord<>(this.topic, "1234", null);

    producer.send(record).get();
    HarvestResult result = harvester.harvest(new HarvestContext());
    assertEquals(1, result.size());
    assertEquals(Status.DELETED, result.get(0).getStatus());
  }
}
