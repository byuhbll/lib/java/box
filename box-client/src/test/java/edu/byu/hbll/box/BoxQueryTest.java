/** */
package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * @author cfd2
 */
public class BoxQueryTest {

  BoxQuery query = new BoxQuery();

  /** Test method for {@link edu.byu.hbll.box.BoxQuery#isIdQuery()}. */
  @Test
  public void testIsId() {
    assertTrue(query.addIds("").isIdQuery());
  }

  /** Test method for {@link edu.byu.hbll.box.BoxQuery#isHarvestQuery()}. */
  @Test
  public void testIsHarvest() {
    assertTrue(query.isHarvestQuery());
  }
}
