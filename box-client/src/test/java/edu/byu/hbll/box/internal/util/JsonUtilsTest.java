package edu.byu.hbll.box.internal.util;

import static edu.byu.hbll.box.internal.util.JsonUtils.project;
import static org.junit.jupiter.api.Assertions.assertAll;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import edu.byu.hbll.witness.Witness;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class JsonUtilsTest {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  protected Witness witness = Witness.builder().build();

  /**
   * Test method for {@link
   * edu.byu.hbll.box.internal.util.JsonUtils#project(com.fasterxml.jackson.databind.JsonNode,
   * java.util.Collection)}.
   */
  @Test
  void testProject() {
    ObjectNode node = mapper.createObjectNode();
    node.with("a").put("b", 0).put("c", 0);
    node.with("a").with("d").put("e", 0);
    node.withArray("f").addObject().put("g", 0).put("h", 0);
    node.withArray("f").addObject().put("g", 0).put("h", 0);
    node.withArray("f").add(0);

    assertAll(
        () -> witness.testify(project(node, null), "1"),
        () -> witness.testify(project(node, List.of()), "2"),
        () -> witness.testify(project(node, List.of("a")), "3"),
        () -> witness.testify(project(node, List.of("a.b")), "4"),
        () -> witness.testify(project(node, List.of("f.g")), "5"),
        () -> witness.testify(project(node, List.of("a.b", "f.g")), "6"),
        () -> witness.testify(project(node, List.of("f.i")), "7"));
  }
}
