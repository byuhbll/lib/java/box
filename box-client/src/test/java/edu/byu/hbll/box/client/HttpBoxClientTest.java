package edu.byu.hbll.box.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.byu.hbll.box.BoxQuery;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflyConfig;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * Most of the {@link HttpBoxClient} tests are found in the box-spring-boot module where it is much
 * easier to test the client.
 *
 * @author Charles Draper
 */
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(config = @HoverflyConfig(captureAllHeaders = true), enableAutoCapture = true)
class HttpBoxClientTest {

  private static final String expected =
      "[{\"@box\":{\"id\":\"1\",\"status\":\"READY\",\"cursor\":\"1572635850032670188\",\"modified\":\"2019-11-01T19:17:30.031Z\",\"processed\":\"2020-03-12T16:59:34.555Z\"}}]";

  /** Tests that OAuth2 authorization works properly. */
  @Test
  void testOauth2() {
    BoxClient client =
        HttpBoxClient.builder()
            .uri("http://box.example.com/box/")
            .accessTokenUri(
                "http://keycloak.example.com/auth/realms/example/protocol/openid-connect/token")
            .clientId("myClientId")
            .clientSecret("mySecret")
            .build();

    assertEquals(expected, client.collect(new BoxQuery()).toString());
  }

  /** Tests that Basic Authentication authorization works properly. */
  @Test
  void testBasicAuth() {
    BoxClient client =
        HttpBoxClient.builder()
            .uri("http://box.example.com/box")
            .username("myUsername")
            .password("myPassword")
            .build();

    assertEquals(expected, client.collect(new BoxQuery()).toString());
  }
}
