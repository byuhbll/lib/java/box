package edu.byu.hbll.box.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.byu.hbll.box.internal.util.CursorUtils;
import java.time.Instant;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
public class CursorUtilsTest {

  /**
   * Test method for {@link
   * edu.byu.hbll.box.internal.util.CursorUtils#getCursor(java.time.Instant)}.
   */
  @Test
  public void testGetCursor() {
    assertEquals(0, CursorUtils.getCursor(Instant.EPOCH));
    assertEquals(123000000123l, CursorUtils.getCursor(Instant.ofEpochSecond(123, 123)));
  }

  /** Test method for {@link edu.byu.hbll.box.internal.util.CursorUtils#nextCursor()}. */
  @Test
  public void testNextCursor() {
    // because calls to the clock can return the same value multiple times (especially if the time
    // resolution is low), we make sure the next cursor is always greater than the previous
    long cursor0 = CursorUtils.nextCursor();
    long cursor1 = CursorUtils.nextCursor();
    long cursor2 = CursorUtils.nextCursor();
    long cursor3 = CursorUtils.nextCursor();
    long cursor4 = CursorUtils.nextCursor();
    long cursor5 = CursorUtils.nextCursor();
    long cursor6 = CursorUtils.nextCursor();
    long cursor7 = CursorUtils.nextCursor();
    long cursor8 = CursorUtils.nextCursor();
    long cursor9 = CursorUtils.nextCursor();

    assertTrue(cursor0 < cursor1);
    assertTrue(cursor1 < cursor2);
    assertTrue(cursor2 < cursor3);
    assertTrue(cursor3 < cursor4);
    assertTrue(cursor4 < cursor5);
    assertTrue(cursor5 < cursor6);
    assertTrue(cursor6 < cursor7);
    assertTrue(cursor7 < cursor8);
    assertTrue(cursor8 < cursor9);
  }
}
