package edu.byu.hbll.box;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.witness.Witness;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
public class BoxDocumentTest {

  protected Witness witness =
      Witness.builder()
          .validate("/@box/cursor", x -> x.asText().matches("\\d+"))
          .validate("/@box/modified", x -> Instant.parse(x.asText()) != null)
          .validate("/@box/processed", x -> Instant.parse(x.asText()) != null)
          .build();

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#matches(java.util.Collection)}. */
  @Test
  public void testMatches() {
    BoxDocument doc = new BoxDocument();
    Collection<Facet> facets = new ArrayList<>();

    assertTrue(doc.matches(facets));

    doc.addFacets("1", "1");

    assertTrue(doc.matches(facets));

    doc = new BoxDocument();
    facets.add(new Facet("1", "1"));

    assertFalse(doc.matches(facets));

    doc.addFacets("1", "1");
    facets.add(new Facet("1", "2"));

    assertTrue(doc.matches(facets));

    facets.add(new Facet("2", "1"));

    assertFalse(doc.matches(facets));

    doc.addFacets("2", "2");

    assertFalse(doc.matches(facets));

    doc.addFacets("2", "1");

    assertTrue(doc.matches(facets));
  }

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#getFacetValues(java.lang.String)}. */
  @Test
  public void testGetFacetValues() {
    BoxDocument doc = new BoxDocument().addFacet("a", "1").addFacet("b", "1").addFacet("b", "2");
    assertEquals(Set.of("1"), doc.getFacetValues("a"));
    assertEquals(List.of("1", "2"), List.copyOf(doc.getFacetValues("b")));
    assertEquals(Set.of(), doc.getFacetValues("c"));
  }

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#getFacetValue(java.lang.String)}. */
  @Test
  public void testGetFacetValue() {
    BoxDocument doc = new BoxDocument().addFacet("a", "1").addFacet("b", "1").addFacet("b", "2");
    assertEquals("1", doc.getFacetValue("a").get());
    assertEquals("1", doc.getFacetValue("b").get());
    assertNull(doc.getFacetValue("c").orElse(null));
  }

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#toString(Collection)}. */
  @Test
  public void testToStringCollection() {
    BoxDocument document = new BoxDocument("1");
    document.setAsReady();
    document.setCursor(1L);
    document.setModified(Instant.EPOCH);
    document.setProcessed(Instant.EPOCH);
    document.addFacet("a", "1");

    ObjectNode doc = document.getDocument();
    doc.with("a").put("b", 0).put("c", 0);
    doc.with("a").with("d").put("e", 0);
    doc.withArray("f").addObject().put("g", 0).put("h", 0);
    doc.withArray("f").addObject().put("g", 0).put("h", 0);
    doc.withArray("f").add(0);

    assertAll(
        () -> witness.testify(document.toString(null), "1"),
        () -> witness.testify(document.toString(List.of()), "2"),
        () -> witness.testify(document.toString(List.of("@doc")), "3"),
        () -> witness.testify(document.toString(List.of("@box")), "4"),
        () -> witness.testify(document.toString(List.of("a")), "5"),
        () -> witness.testify(document.toString(List.of("a.b")), "6"),
        () -> witness.testify(document.toString(List.of("f.g")), "7"),
        () -> witness.testify(document.toString(List.of("a.b", "f.g")), "8"),
        () -> witness.testify(document.toString(List.of("@box.id", "@box.status")), "9"),
        () -> witness.testify(document.toString(List.of("a", "@box.facets")), "10"),
        () -> witness.testify(document.toString(List.of("f.i")), "11"));
  }

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#hash()}. */
  @Test
  public void testHash() {
    BoxDocument document = new BoxDocument("1");
    assertEquals("pKchbwP++/IwQfsYUX0A6TMIrFB1z02tsKCd4OuUmjs=", hashToString(document));

    // ensure the hash changes with non-volatile field updates
    document.setId("2");
    assertEquals("XZ8VLR2Xo2Wo8VDreIONBQwXjF9G5CQPT6mc2+pUdTo=", hashToString(document));
    document.setAsReady();
    assertEquals("dT3wIyKa+5NKYwoGiD3NFiPZQa9JIdzfXsfRv7l71V0=", hashToString(document));
    document.addFacet("a", "b");
    assertEquals("GYW7bKvaU5ngt9q7452A8Wz8I18wIpw1IPk35NpQ6IY=", hashToString(document));
    document.addDependency("a", "b");
    assertEquals("HZ3U8lStoFY4wbPbms8Bi4RzPTiOpIwDVFllJtM4ftw=", hashToString(document));
    document.setGroupId("a");
    assertEquals("JpNdf5I/6C+10Qa34R0/Ju2On3ixCKP1hekNyy493j8=", hashToString(document));
    document.setMessage("a");
    assertEquals("V0l45GNjeLerTmWmbfH5P/+noN2HROK3a0h2QN6NV6g=", hashToString(document));
    document.withDocument().put("a", "b");
    assertEquals("O4QzUeDBRPWVBbY8uEmrokqkogeuRM5UNgZGawanC7s=", hashToString(document));

    // ensure the hash does NOT change with volatile field updates
    document.setProcessed(Instant.now());
    assertEquals("O4QzUeDBRPWVBbY8uEmrokqkogeuRM5UNgZGawanC7s=", hashToString(document));
    document.setModified(Instant.now());
    assertEquals("O4QzUeDBRPWVBbY8uEmrokqkogeuRM5UNgZGawanC7s=", hashToString(document));
    document.setCursor(new Random().nextLong());
    assertEquals("O4QzUeDBRPWVBbY8uEmrokqkogeuRM5UNgZGawanC7s=", hashToString(document));
  }

  private String hashToString(BoxDocument document) {
    return Base64.getEncoder().encodeToString(document.hash());
  }

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#equals(Object)}. */
  @Test
  public void testEquals() {
    BoxDocument document1 = new BoxDocument("1");
    BoxDocument document2 = new BoxDocument("1");
    assertEquals(document1, document2);

    document1.withDocument().put("a", 1);
    assertNotEquals(document1, document2);
    document2.withDocument().put("a", 1);
    assertEquals(document1, document2);

    document1.setMessage("a");
    assertNotEquals(document1, document2);
    document2.setMessage("a");
    assertEquals(document1, document2);

    document1.addDependency("a", "1");
    assertNotEquals(document1, document2);
    document2.addDependency("a", "1");
    assertEquals(document1, document2);

    document1.addFacet("a", "1");
    assertNotEquals(document1, document2);
    document2.addFacet("a", "1");
    assertEquals(document1, document2);

    document1.setGroupId("a");
    assertNotEquals(document1, document2);
    document2.setGroupId("a");
    assertEquals(document1, document2);

    document1.setCursor(1L);
    assertEquals(document1, document2);
    document2.setCursor(2L);
    assertEquals(document1, document2);

    document1.setModified(Instant.now());
    assertEquals(document1, document2);
    document2.setModified(Instant.now());
    assertEquals(document1, document2);

    document1.setProcessed(Instant.now());
    assertEquals(document1, document2);
    document2.setProcessed(Instant.now());
    assertEquals(document1, document2);
  }

  /** Test method for {@link edu.byu.hbll.box.BoxDocument#hashCode()}. */
  @Test
  public void testHashCode() {
    BoxDocument document = new BoxDocument("1");
    assertEquals(522049079, document.hashCode());
    document.withDocument().put("a", 1);
    assertEquals(1387382005, document.hashCode());
  }
}
