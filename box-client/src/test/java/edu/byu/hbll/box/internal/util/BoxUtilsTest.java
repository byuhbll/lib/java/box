package edu.byu.hbll.box.internal.util;

import static edu.byu.hbll.box.internal.util.BoxUtils.canonicalizeFields;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.Instant;
import java.util.Set;
import org.junit.jupiter.api.Test;

/**
 * @author Charles Draper
 */
class BoxUtilsTest {

  /**
   * Test method for {@link edu.byu.hbll.box.internal.util.BoxUtils#retryDateTime(Instant, Duration,
   * double)}.
   */
  @Test
  void testRetryDateTime() {
    Duration delay = Duration.ofMinutes(1);
    Duration base0 = Duration.ofMinutes(0);
    Duration base1 = Duration.ofMinutes(1);
    Duration base2 = Duration.ofMinutes(2);
    Duration base4 = Duration.ofMinutes(4);
    Duration base8 = Duration.ofMinutes(8);

    assertAttempt(base0, delay, 0.5, 30, 90);
    assertAttempt(base1, delay, 0.5, 30, 90);
    assertAttempt(base2, delay, 0.5, 60, 180);
    assertAttempt(base4, delay, 0.5, 120, 360);
    assertAttempt(base8, delay, 0.5, 240, 720);

    delay = Duration.ofMinutes(2);

    assertAttempt(base0, delay, 0.1, 108, 132);
    assertAttempt(base1, delay, 0.1, 108, 132);
    assertAttempt(base2, delay, 0.1, 108, 132);
    assertAttempt(base4, delay, 0.1, 216, 264);
    assertAttempt(base8, delay, 0.1, 432, 528);
  }

  /**
   * Asserts that the attempt date time is in the proper range.
   *
   * @param ago the base is this duration ago
   * @param delay the retryDelay
   * @param jitter the retryJitter
   * @param lower the lower bound in seconds in the future
   * @param upper the upper bound in seconds in the future
   */
  private void assertAttempt(Duration ago, Duration delay, double jitter, int lower, int upper) {
    Instant now = Instant.now();
    long retryDateTime = BoxUtils.retryDateTime(now.minus(ago), delay, jitter).getEpochSecond();
    long lowerBound = now.plusSeconds(lower).getEpochSecond();
    long upperBound = now.plusSeconds(upper).getEpochSecond() + 1;

    // add or subtract one for a timing buffer
    assertTrue(lowerBound <= retryDateTime + 1);
    assertTrue(upperBound >= retryDateTime - 1);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.internal.util.BoxUtils#canonicalizeFields(java.util.Collection)}.
   */
  @Test
  void testCanonicalizeFields() {
    assertEquals(Set.of(), canonicalizeFields(Set.of()));
    assertEquals(Set.of("@doc.a"), canonicalizeFields(Set.of("a")));
    assertEquals(Set.of("@doc.a", "@doc.b"), canonicalizeFields(Set.of("a", "b")));
    assertEquals(Set.of("@doc.a.b"), canonicalizeFields(Set.of("a.b")));
    assertEquals(Set.of("@doc"), canonicalizeFields(Set.of("@doc")));
    assertEquals(Set.of("@doc.a"), canonicalizeFields(Set.of("@doc.a")));
    assertEquals(Set.of("@box"), canonicalizeFields(Set.of("@box")));
    assertEquals(Set.of("@box.a"), canonicalizeFields(Set.of("@box.a")));
  }
}
