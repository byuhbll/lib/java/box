package edu.byu.hbll.box.client;

import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * A client for interacting with a Box server.
 *
 * @author Charles Draper
 */
public interface BoxClient {

  /**
   * Collects and returns all documents found according to the given query.
   *
   * @param query the query
   * @return the results
   */
  default QueryResult collect(BoxQuery query) {
    QueryResult result = new QueryResult();
    find(query).forEach(d -> result.add(d));
    result.updateNextCursor(query);
    return result;
  }

  /**
   * Returns documents found according to the given query in the form of a {@link Stream}.
   *
   * @param query the query
   * @return the results
   */
  default Stream<BoxDocument> stream(BoxQuery query) {
    return StreamSupport.stream(find(query).spliterator(), false);
  }

  /**
   * Returns documents found according to the given query in the form of an {@link Iterable}.
   *
   * @param query the query
   * @return the results
   */
  Iterable<BoxDocument> find(BoxQuery query);
}
