package edu.byu.hbll.box;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;

/**
 * An immutable representation of a box document id which includes the source name of the document
 * and its id.
 */
public final class DocumentId implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The name of the source the document belongs to. */
  @JsonProperty private String sourceName;

  /** The document id. */
  @JsonProperty private String id;

  /** Constructor for serializers/deserializers. */
  @SuppressWarnings("unused")
  private DocumentId() {}

  /**
   * Creates a new {@link DocumentId} with the given source name and id.
   *
   * @param sourceName the source name of the document.
   * @param id the id of the document.
   * @throws NullPointerException if either sourceName or id is null
   */
  public DocumentId(String sourceName, String id) {
    Objects.requireNonNull(sourceName);
    Objects.requireNonNull(id);
    this.sourceName = sourceName;
    this.id = id;
  }

  /**
   * Returns the source name.
   *
   * @return the sourceName
   */
  public String getSourceName() {
    return sourceName;
  }

  /**
   * Returns the id.
   *
   * @return the id
   */
  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(sourceName, id);
  }

  /**
   * Checks equality of the source name and ID.
   *
   * @param o the object to be compared.
   * @return whether the given object is equal to this.
   */
  @Override
  public boolean equals(Object o) {

    if (this == o) {
      return true;
    }

    if (o == null) {
      return false;
    }

    if (!(o instanceof DocumentId)) {
      return false;
    }

    DocumentId other = (DocumentId) o;

    return Objects.equals(sourceName, other.sourceName) && Objects.equals(id, other.id);
  }

  /**
   * Returns a string representation of this. Specifically `{sourceName}.{id}`.
   *
   * @return sourceName.id
   */
  @Override
  public String toString() {
    return sourceName + "." + id;
  }
}
