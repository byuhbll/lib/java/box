package edu.byu.hbll.box.internal.core;

import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxQuery;
import edu.byu.hbll.box.QueryResult;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.function.Function;

/**
 * An iterable for retrieving Box documents. Under the hood it requests documents in small batches
 * as needed until the query is fulfilled.
 *
 * @author Charles Draper
 */
public class DocumentIterable implements Iterable<BoxDocument> {

  private static final int DEFAULT_BATCH_SIZE = 10;

  private final BoxQuery query;
  private final int batchSize;
  private final Function<BoxQuery, QueryResult> retriever;

  /**
   * Creates a new {@link DocumentIterable} with default batch size.
   *
   * @param query the query to perform
   * @param retriever the function that returns all documents according to the query
   */
  public DocumentIterable(BoxQuery query, Function<BoxQuery, QueryResult> retriever) {
    this(query, DEFAULT_BATCH_SIZE, retriever);
  }

  /**
   * Creates a new {@link DocumentIterable}.
   *
   * @param query the query to perform
   * @param batchSize the internal batch size
   * @param retriever the function that returns all documents according to the query
   */
  public DocumentIterable(
      BoxQuery query, int batchSize, Function<BoxQuery, QueryResult> retriever) {
    this.query = new BoxQuery(query);
    this.batchSize = batchSize;
    this.retriever = retriever;
  }

  @Override
  public Iterator<BoxDocument> iterator() {
    return new DocumentIterator();
  }

  private class DocumentIterator implements Iterator<BoxDocument> {

    private Queue<BoxDocument> queue = new LinkedList<>();
    private long total;
    private Queue<String> ids = new LinkedList<>(query.getIds());
    private BoxQuery batchQuery = new BoxQuery(query);
    private boolean isId = query.isIdQuery();
    private long limit =
        query.getLimitOrDefault() == BoxQuery.UNLIMITED
            ? Long.MAX_VALUE
            : query.getLimitOrDefault();

    public DocumentIterator() {
      // initialize queue with first batch of documents
      findNext();
    }

    @Override
    public boolean hasNext() {
      return !queue.isEmpty();
    }

    @Override
    public BoxDocument next() {
      if (queue.isEmpty()) {
        throw new NoSuchElementException();
      }

      BoxDocument next = queue.poll();
      findNext();
      return next;
    }

    /** Add more documents to the queue if there are any left. */
    private void findNext() {
      // if there are documents left in the latest fetch
      if (!queue.isEmpty()) {
        return;
      }

      // if id type query
      if (isId) {
        // get next batch of documents by id
        batchQuery.clearIds();
        batchQuery.addIds(nextIds(ids));
      } else {
        // get next batch of documents
        batchQuery.setLimit(Math.min(limit - total, batchSize));
      }

      // if reached end of limit or end of ids
      if (!isId && batchQuery.getLimitOrDefault() == 0 || isId && batchQuery.getIds().isEmpty()) {
        return;
      }

      // get documents
      QueryResult result = retriever.apply(batchQuery);
      total += result.size();
      batchQuery.setCursor(result.getNextCursor());
      queue.addAll(result);
    }

    /**
     * Get next batch of ids.
     *
     * @param ids get next batch from here
     * @return next batch
     */
    private List<String> nextIds(Queue<String> ids) {
      List<String> nextIds = new ArrayList<>();

      for (int i = 0; i < batchSize && !ids.isEmpty(); i++) {
        nextIds.add(ids.poll());
      }

      return nextIds;
    }
  }
}
