package edu.byu.hbll.box.internal.util;

import edu.byu.hbll.box.BoxQuery;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Set;

/** A URI builder for Box documents endpoint. */
public class BoxUriBuilder {

  /**
   * Builds a final {@link URI} given the base uri and query.
   *
   * @param uri the base box URI
   * @param query the query to use
   * @return the resulting {@link URI}
   */
  public static URI buildQuery(URI uri, BoxQuery query) {

    UriBuilder builder = new UriBuilder(uri);

    builder.path("documents");

    if (query.isHarvestQuery()) {
      builder.queryParam("cursor", query.getCursor());
      builder.queryParam("facet", query.getFacets());
      builder.queryParam("limit", query.getLimit());
      builder.queryParam("status", query.getStatuses());
      builder.queryParam("offset", query.getOffset());
      builder.queryParam("order", query.getOrder());
    } else {
      builder.queryParam("id", query.getIds());
      builder.queryParam("process", query.isProcess());
      builder.queryParam("wait", query.isWait());
    }

    boolean boxField = query.getFields().contains("@box");
    boolean boxFields = query.getFields().stream().anyMatch(f -> f.startsWith("@box."));

    Set<String> fields = new LinkedHashSet<>(query.getFields());

    if (!query.getFields().isEmpty() && !boxField || boxFields) {
      // make sure we get back the core metadata fields
      fields.add(BoxQuery.METADATA_FIELD_ID);
      fields.add(BoxQuery.METADATA_FIELD_CURSOR);
      fields.add(BoxQuery.METADATA_FIELD_STATUS);
    }

    builder.queryParam("field", fields);

    URI queryUri = builder.build();

    return queryUri;
  }
}
