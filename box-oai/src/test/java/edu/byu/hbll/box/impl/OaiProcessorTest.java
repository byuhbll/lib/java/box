package edu.byu.hbll.box.impl;

import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.ConstructConfig;
import edu.byu.hbll.box.Facet;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.xml.XmlUtils;
import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit.dsl.RequestMatcherBuilder;
import io.specto.hoverfly.junit5.HoverflyExtension;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.http.HttpClient;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * @author Charles Draper
 */
@ExtendWith(MockitoExtension.class)
@ExtendWith(HoverflyExtension.class)
public class OaiProcessorTest {

  private OaiProcessor oai;

  /**
   * @throws java.lang.Exception
   */
  @BeforeEach
  public void setUp() throws Exception {
    oai = new OaiProcessor();
    oai.setUri("https://www.example.com/");
    oai.setMetadataPrefix("oai");
    oai.setIdRegex("a(.)b");
    oai.setIdReplacement("$1");
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#OaiProcessor()}. */
  @Test
  public void testOaiProcessor() {
    new OaiProcessor();
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#OaiProcessor(java.lang.String)}. */
  @Test
  public void testOaiProcessorString() {
    OaiProcessor oai = new OaiProcessor("https://www.example.com/");
    assertEquals("https://www.example.com/", oai.getUri());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiProcessor#postConstruct(com.fasterxml.jackson.databind.JsonNode)}.
   */
  @Test
  public void testPostConstruct() {
    ObjectNode config = JsonNodeFactory.instance.objectNode();
    config.put("uri", "https://www.example.com/");
    config.put("metadataPrefix", "oai_dc_2");
    config.put("idRegex", "oai(.*)");
    config.put("idReplacement", "$1");

    OaiProcessor oai = new OaiProcessor();
    oai.postConstruct(new ConstructConfig("test", config));

    assertEquals("https://www.example.com/", oai.getUri());
    assertEquals("oai_dc_2", oai.getMetadataPrefix());
    assertEquals("oai(.*)", oai.getIdRegex());
    assertEquals("$1", oai.getIdReplacement());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#process(java.lang.String)}.
   *
   * @throws IOException
   * @throws SAXException
   * @throws FileNotFoundException
   */
  @Test
  public void testProcess(Hoverfly hoverfly) throws Exception {
    Document doc =
        XmlUtils.parse(
            new FileInputStream("src/test/resources/edu/byu/hbll/box/impl/oai.process.xml"));

    hoverfly.simulate(dsl(getRequestBuilder().willReturn(success().body(XmlUtils.toString(doc)))));

    BoxDocument resultDocument = oai.process(new ProcessContext("a1b")).get(0);

    assertEquals("a1b", resultDocument.getId());

    ObjectNode document = resultDocument.getDocument();
    assertEquals(
        "oai:scholarsarchive.byu.edu:byufamilyhistorian-1040",
        document.path("header").path("identifier").asText());

    Set<Facet> actualFacets = new HashSet<>(resultDocument.getFacets());
    Set<Facet> expectedFacets =
        new HashSet<>(
            Arrays.asList(
                Facet.parse("set:publication:journals"),
                Facet.parse("set:publication:byufamilyhistorian")));
    assertEquals(expectedFacets, actualFacets);
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#process(java.lang.String)} to test
   * deleted records.
   *
   * @throws Exception
   */
  @Test
  public void testProcessDeletedRecord(Hoverfly hoverfly) throws Exception {
    hoverfly.simulate(
        dsl(
            getRequestBuilder()
                .willReturn(
                    success().body("<OAI-PMH><error code=\"idDoesNotExist\"></error></OAI-PMH>"))));

    BoxDocument document = oai.process(new ProcessContext("a1b")).get(0);
    assertTrue(document.isDeleted());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#process(java.lang.String)} to test
   * deleted records returned as errors.
   *
   * @throws Exception
   */
  @Test
  public void testProcessDeletedERror(Hoverfly hoverfly) throws Exception {
    String response =
        "<OAI-PMH><GetRecord><record><header status=\"deleted\"/></record></GetRecord></OAI-PMH>";
    hoverfly.simulate(dsl(getRequestBuilder().willReturn(success().body(response))));

    BoxDocument document = oai.process(new ProcessContext("a1b")).get(0);
    assertTrue(document.isDeleted());
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#getClient()}. */
  @Test
  public void testGetClient() {
    assertNotNull(oai.getClient());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.box.impl.OaiProcessor#setClient(javax.ws.rs.client.Client)}.
   */
  @Test
  public void testSetClient() {
    HttpClient client = HttpClient.newHttpClient();
    oai.setClient(client);
    assertEquals(client, oai.getClient());

    try {
      oai.setClient(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#setUri(java.lang.String)}. */
  @Test
  public void testSetUri() {
    HttpClient client = HttpClient.newHttpClient();
    oai.setClient(client);
    assertEquals(client, oai.getClient());

    try {
      oai.setClient(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#getMetadataPrefix()}. */
  @Test
  public void testGetMetadataPrefix() {
    oai = new OaiProcessor();
    assertEquals("oai_dc", oai.getMetadataPrefix());
  }

  /**
   * Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#setMetadataPrefix(java.lang.String)}.
   */
  @Test
  public void testSetMetadataPrefix() {
    oai.setMetadataPrefix("asdf");
    assertEquals("asdf", oai.getMetadataPrefix());

    try {
      oai.setMetadataPrefix(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /** Test method for {@link edu.byu.hbll.box.impl.OaiProcessor#setSets(java.util.Set)}. */
  @Test
  public void testSetSets() {
    oai.setSets(new HashSet<>(Arrays.asList("a", "b")));
    assertEquals(new HashSet<>(Arrays.asList("a", "b")), oai.getSets());

    try {
      oai.setSets(null);
      fail("should throw exception");
    } catch (Exception e) {
    }
  }

  /**
   * Create the Hoverfly base request builder for each GetRecord call.
   *
   * @return the request builder
   */
  private RequestMatcherBuilder getRequestBuilder() {
    return service("https://www.example.com")
        .get("/")
        .queryParam("verb", "GetRecord")
        .queryParam("identifier", "1")
        .queryParam("metadataPrefix", "oai");
  }
}
