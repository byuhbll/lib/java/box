package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.HarvestContext;
import edu.byu.hbll.box.HarvestResult;
import edu.byu.hbll.box.Harvester;
import edu.byu.hbll.box.internal.util.UriBuilder;
import edu.byu.hbll.xml.XmlJsonConverter;
import edu.byu.hbll.xml.XmlUtils;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * A client used to harvest records using the OAI protocol.
 *
 * @author Charles Draper
 */
public class OaiHarvester implements Harvester {

  static final Logger logger = LoggerFactory.getLogger(OaiHarvester.class);

  /** Default metadata prefix. */
  public static final String DEFAULT_METADATA_PREFIX = "oai_dc";

  private HttpClient client = HttpClient.newBuilder().followRedirects(Redirect.ALWAYS).build();
  private String uri;
  private String metadataPrefix = DEFAULT_METADATA_PREFIX;
  private boolean chunked;
  private boolean idsOnly;
  private String idRegex;
  private String idReplacement;
  private Set<String> sets = new HashSet<>();
  private boolean removeNamespaces;

  @Override
  public HarvestResult harvest(HarvestContext context) {

    // extract cursor information
    ObjectNode cursor = context.getCursor();
    String resumptionToken = cursor.path("resumptionToken").asText(null);
    String fromString = cursor.path("from").asText(null);

    // need to start on day two to get off of the boundary of valid dates and non valid dates
    LocalDate from = LocalDate.ofEpochDay(1);

    // parse from
    if (fromString != null) {
      try {
        from = LocalDate.parse(fromString);
      } catch (Exception e) {
        throw new IllegalArgumentException("Cannot parse from date in cursor : " + e);
      }
    }

    LocalDate until = LocalDate.now();

    // if chunked, set until only one month in the future
    if (chunked && until.isAfter(from.plusMonths(1))) {
      until = from.plusMonths(1);
    }

    // build target
    String verb = idsOnly ? "ListIdentifiers" : "ListRecords";

    UriBuilder uriBuilder = new UriBuilder(this.uri).queryParam("verb", verb);

    // use resumption token if it is not empty
    if (resumptionToken != null && !resumptionToken.isEmpty()) {
      uriBuilder.queryParam("resumptionToken", resumptionToken);
    } else {
      // otherwise use dates
      uriBuilder
          .queryParam("from", from.toString())
          .queryParam("until", until.toString())
          .queryParam("metadataPrefix", metadataPrefix);
    }

    URI uri = uriBuilder.build();
    logger.info(uri.toString());

    Document response;

    // execute request
    try {
      HttpResponse<String> textResponse =
          client.send(HttpRequest.newBuilder(uri).build(), BodyHandlers.ofString());
      response = XmlUtils.parse(textResponse.body());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    } catch (InterruptedException e) {
      return null;
    }

    // extract any errors
    List<Element> errors = XmlUtils.getElements(response, "OAI-PMH", "error");

    // end processing here if there are errors
    if (!errors.isEmpty()) {
      Element error = errors.get(0);

      if (error.getAttribute("code").equals("noRecordsMatch")) {
        // ignore error and continue
      } else {
        throw new RuntimeException(
            error.getAttribute("code") + " : " + error.getTextContent() + " : " + uri);
      }
    }

    // if set to only retrieve ids, rebuild the document to match the structure of the request to
    // retrieve
    // records
    if (idsOnly) {
      rebuild(response);
    }

    HarvestResult result = new HarvestResult();
    result.setCursor(cursor);

    // extract harvest records from the result document
    List<BoxDocument> resultDocuments = extract(response);
    result.addDocuments(resultDocuments);

    // get the resumption token
    resumptionToken = getResumptionToken(response);

    cursor.put("resumptionToken", resumptionToken);
    cursor.put("from", from.toString());

    // if there is a resumption token then there is more
    if (resumptionToken != null && !resumptionToken.isEmpty()) {
      result.setMore(true);
    } else {
      cursor.put("from", until.toString());

      if (until.isBefore(LocalDate.now())) {
        result.setMore(true);
      }
    }

    return result;
  }

  /**
   * In the case of listing identifiers only, this reconstructs the resulting document to mimic the
   * records listings so we don't have to repeat so much code later on.
   *
   * @param doc the resulting document
   */
  private void rebuild(Document doc) {
    Element root = XmlUtils.getElement(doc, "OAI-PMH");
    Element listRecords = doc.createElementNS("dummy", "ListRecords");
    root.appendChild(listRecords);

    logger.debug("LocalName: \"" + listRecords.getLocalName() + "\"");

    for (Node header : XmlUtils.getElements(doc, "OAI-PMH", "ListIdentifiers", "header")) {
      Element record = doc.createElementNS("dummy", "record");
      listRecords.appendChild(record);
      record.appendChild(header);
    }
  }

  /**
   * Creates {@link ProcessRecord}s out of the resulting document. This is what is ultimately sent
   * back to the harvest task.
   *
   * @param doc the resulting document
   * @return a list of {@link ProcessRecord}s to be harvested
   * @throws Exception if something goes wrong
   */
  private List<BoxDocument> extract(Document doc) {
    List<Document> records = new ArrayList<>();
    List<BoxDocument> boxDocuments = new ArrayList<>();
    List<Element> nodes = XmlUtils.getElements(doc, "OAI-PMH", "ListRecords", "record");

    for (Element node : nodes) {

      Document record = XmlUtils.toDocument(node);

      String identifier =
          XmlUtils.getElement(record, "record", "header", "identifier").getTextContent();
      String id = idRegex != null ? identifier.replaceAll(idRegex, idReplacement) : identifier;

      List<Element> setNodes = XmlUtils.getElements(record, "record", "header", "setSpec");
      List<String> sets = new ArrayList<>();

      for (Element setNode : setNodes) {
        sets.add(setNode.getTextContent());
      }

      if (this.sets != null && !this.sets.isEmpty()) {
        // if set is not part of the white list of sets then ignore it
        if (Collections.disjoint(this.sets, sets)) {
          continue;
        }
      }

      BoxDocument resultDocument = new BoxDocument(id);

      // status is an optional attribute in the header that can state that the
      // record is deleted, continue if it is
      if ("deleted".equals(XmlUtils.getElement(node, "header").getAttribute("status"))) {
        resultDocument = new BoxDocument(id, Status.DELETED);
      } else if (!idsOnly) {
        Document recordToConvert = record;

        if (removeNamespaces) {
          recordToConvert = XmlUtils.removeNamespaces(recordToConvert);
        }

        ObjectNode document = XmlJsonConverter.toJsonDoc(recordToConvert);
        resultDocument = new BoxDocument(id, document);

        for (String set : sets) {
          resultDocument.addFacet("set", set);
        }
      }

      records.add(record);
      boxDocuments.add(resultDocument);
    }

    return transform(records, boxDocuments);
  }

  /**
   * Subclasses can optionally perform further transformations on the documents.
   *
   * @param records original records from oai source
   * @param boxDocuments the converted box documents
   * @return transformed box documents
   */
  protected List<BoxDocument> transform(List<Document> records, List<BoxDocument> boxDocuments) {
    return boxDocuments;
  }

  /**
   * Retrieves the resumption token from the result.
   *
   * @param result the oai response
   * @return the resumption token if there is one, null otherwise
   */
  private String getResumptionToken(Document response) {
    Element resumptionToken =
        XmlUtils.getElement(response, "OAI-PMH", "ListRecords", "resumptionToken");
    resumptionToken =
        resumptionToken == null
            ? XmlUtils.getElement(response, "OAI-PMH", "ListIdentifiers", "resumptionToken")
            : resumptionToken;
    return resumptionToken == null ? null : resumptionToken.getTextContent();
  }

  /**
   * Gets whether or not to harvest in small batches.
   *
   * @return the chunked
   */
  public boolean isChunked() {
    return chunked;
  }

  /**
   * Sets whether or not to query by finite date ranges in this case one month at a time.
   *
   * @param chunked the chunked to set
   */
  public void setChunked(boolean chunked) {
    this.chunked = chunked;
  }

  /**
   * Gets whether or not to harvest only identifiers.
   *
   * @return the idsOnly
   */
  public boolean isIdsOnly() {
    return idsOnly;
  }

  /**
   * Sets whether or not to harvest only identifiers.
   *
   * @param idsOnly the idsOnly to set
   */
  public void setIdsOnly(boolean idsOnly) {
    this.idsOnly = idsOnly;
  }

  /**
   * Returns the http client.
   *
   * @return the client
   */
  public HttpClient getClient() {
    return client;
  }

  /**
   * Sets the http client to use.
   *
   * @param client the client to set
   */
  public void setClient(HttpClient client) {
    this.client = Objects.requireNonNull(client);
  }

  /**
   * Returns the base uri.
   *
   * @return the uri
   */
  public String getUri() {
    return uri;
  }

  /**
   * Sets the base uri.
   *
   * @param uri the uri to set
   */
  public void setUri(String uri) {
    this.uri = Objects.requireNonNull(uri);
  }

  /**
   * Returns the metadata prefix.
   *
   * @return the metadataPrefix
   */
  public String getMetadataPrefix() {
    return metadataPrefix;
  }

  /**
   * Sets the metadata prefix to use.
   *
   * @param metadataPrefix the metadataPrefix to set
   */
  public void setMetadataPrefix(String metadataPrefix) {
    this.metadataPrefix = Objects.requireNonNull(metadataPrefix);
  }

  /**
   * Returns the ids regex.
   *
   * @return the idRegex
   */
  public String getIdRegex() {
    return idRegex;
  }

  /**
   * Sets the id regex for converting the id from oai record id to box id.
   *
   * @param idRegex the idRegex to set
   */
  public void setIdRegex(String idRegex) {
    this.idRegex = idRegex;
  }

  /**
   * Returns the id replacement.
   *
   * @return the idReplacement
   */
  public String getIdReplacement() {
    return idReplacement;
  }

  /**
   * Sets the id regex replacement for converting the id from oai record id to box id.
   *
   * @param idReplacement the idReplacement to set
   */
  public void setIdReplacement(String idReplacement) {
    this.idReplacement = idReplacement;
  }

  /**
   * Returns the white list of set specs to harvest.
   *
   * @return the sets
   */
  public Set<String> getSets() {
    return sets;
  }

  /**
   * Sets a white list of set specs to harvest.
   *
   * @param sets the sets to set
   */
  public void setSets(Set<String> sets) {
    this.sets = Objects.requireNonNull(sets);
  }

  /**
   * Returns whether or not to remove name spaces.
   *
   * @return the removeNamespaces
   */
  public boolean isRemoveNamespaces() {
    return removeNamespaces;
  }

  /**
   * Sets whether or not to remove name spaces.
   *
   * @param removeNamespaces the removeNamespaces to set
   */
  public void setRemoveNamespaces(boolean removeNamespaces) {
    this.removeNamespaces = removeNamespaces;
  }
}
