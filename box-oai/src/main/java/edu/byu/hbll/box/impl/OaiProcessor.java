package edu.byu.hbll.box.impl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.byu.hbll.box.BoxDocument;
import edu.byu.hbll.box.BoxDocument.Status;
import edu.byu.hbll.box.ProcessContext;
import edu.byu.hbll.box.ProcessResult;
import edu.byu.hbll.box.SingleProcessor;
import edu.byu.hbll.box.internal.util.UriBuilder;
import edu.byu.hbll.xml.XmlJsonConverter;
import edu.byu.hbll.xml.XmlUtils;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * A processor that implements the OAI protocol.
 *
 * @author Charles Draper
 */
public class OaiProcessor implements SingleProcessor {

  static final Logger logger = LoggerFactory.getLogger(OaiProcessor.class);

  /** Default metadata prefix. */
  public static final String DEFAULT_METADATA_PREFIX = "oai_dc";

  private HttpClient client = HttpClient.newBuilder().followRedirects(Redirect.ALWAYS).build();
  private String uri;
  private String metadataPrefix = DEFAULT_METADATA_PREFIX;
  private String idRegex;
  private String idReplacement;
  private Set<String> sets = new HashSet<>();
  private boolean removeNamespaces;

  /** Creates a new empty {@link OaiProcessor}. */
  protected OaiProcessor() {}

  /**
   * Creates a new {@link OaiProcessor} with the given base uri.
   *
   * @param uri the base uri
   */
  public OaiProcessor(String uri) {
    this.uri = Objects.requireNonNull(uri);
  }

  @Override
  public ProcessResult process(ProcessContext context) {

    String id = context.getId();

    Document record = getRecord(id);

    List<Element> setNodes = XmlUtils.getElements(record, "record", "header", "setSpec");
    List<String> sets = new ArrayList<>();

    for (Element setNode : setNodes) {
      sets.add(setNode.getTextContent());
    }

    BoxDocument boxDocument;

    if (!this.sets.isEmpty() && Collections.disjoint(this.sets, sets)) {
      // if set is not part of the white list of sets then ignore it
      boxDocument = new BoxDocument(id, Status.DELETED);
    } else if ("deleted"
        .equals(XmlUtils.getElement(record, "record", "header").getAttribute("status"))) {
      // status is an optional attribute in the header that can state that the record is deleted
      boxDocument = new BoxDocument(id, Status.DELETED);
    } else {
      Document recordToConvert = record;

      if (removeNamespaces) {
        recordToConvert = XmlUtils.removeNamespaces(recordToConvert);
      }

      ObjectNode document = XmlJsonConverter.toJsonDoc(recordToConvert);
      boxDocument = new BoxDocument(id, document);
      sets.forEach(s -> boxDocument.addFacet("set", s));
    }

    return new ProcessResult(transform(record, boxDocument));
  }

  /**
   * Queries the OAI responder for the record of the given id.
   *
   * @param id the id of the record (prior to regex/replacement)
   * @return the retrieved xml document
   */
  public Document getRecord(String id) {
    String identifier = idRegex != null ? id.replaceAll(idRegex, idReplacement) : id;

    URI uri =
        new UriBuilder(this.uri)
            .queryParam("verb", "GetRecord")
            .queryParam("identifier", identifier)
            .queryParam("metadataPrefix", metadataPrefix)
            .build();

    try {
      HttpResponse<String> textResponse =
          client.send(HttpRequest.newBuilder(uri).build(), BodyHandlers.ofString());

      Document response = XmlUtils.parse(textResponse.body());
      Element recordElement = XmlUtils.getElement(response, "OAI-PMH", "GetRecord", "record");
      Element errorElement = XmlUtils.getElement(response, "OAI-PMH", "error");

      if (recordElement != null) {
        return XmlUtils.toDocument(XmlUtils.getElement(response, "OAI-PMH", "GetRecord", "record"));
      } else if (errorElement != null) {
        String errorCode = errorElement.getAttribute("code");
        errorCode = errorCode == null ? "" : errorCode;

        if (errorCode.equalsIgnoreCase("idDoesNotExist")) {
          return XmlUtils.parse("<record><header status=\"deleted\"/></record>");
        } else {
          throw new RuntimeException(errorCode + " : " + errorElement.getTextContent());
        }
      } else {
        throw new RuntimeException("unrecognized OAI response");
      }
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    } catch (InterruptedException e) {
      return null;
    }
  }

  /**
   * Subclasses can override this to do additional transformations on the document.
   *
   * @param record the record from the oai source
   * @param boxDocument the corresponding box document
   * @return the transformed document
   */
  protected BoxDocument transform(Document record, BoxDocument boxDocument) {
    return boxDocument;
  }

  /**
   * Returns the http client.
   *
   * @return the client
   */
  public HttpClient getClient() {
    return client;
  }

  /**
   * Sets the http client to use.
   *
   * @param client the client to set
   */
  public void setClient(HttpClient client) {
    this.client = Objects.requireNonNull(client);
  }

  /**
   * Returns the base uri.
   *
   * @return the uri
   */
  public String getUri() {
    return uri;
  }

  /**
   * Sets the base uri.
   *
   * @param uri the uri to set
   */
  public void setUri(String uri) {
    this.uri = Objects.requireNonNull(uri);
  }

  /**
   * Returns the metadata prefix.
   *
   * @return the metadataPrefix
   */
  public String getMetadataPrefix() {
    return metadataPrefix;
  }

  /**
   * Sets the metadata prefix to use.
   *
   * @param metadataPrefix the metadataPrefix to set
   */
  public void setMetadataPrefix(String metadataPrefix) {
    this.metadataPrefix = Objects.requireNonNull(metadataPrefix);
  }

  /**
   * Returns the ids regex.
   *
   * @return the idRegex
   */
  public String getIdRegex() {
    return idRegex;
  }

  /**
   * Sets the id regex for converting the id from box id to oai record id.
   *
   * @param idRegex the idRegex to set
   */
  public void setIdRegex(String idRegex) {
    this.idRegex = idRegex;
  }

  /**
   * Returns the id replacement.
   *
   * @return the idReplacement
   */
  public String getIdReplacement() {
    return idReplacement;
  }

  /**
   * Sets the id regex replacement for converting the id from box id to oai record id.
   *
   * @param idReplacement the idReplacement to set
   */
  public void setIdReplacement(String idReplacement) {
    this.idReplacement = idReplacement;
  }

  /**
   * Returns the white list of set specs to harvest.
   *
   * @return the sets
   */
  public Set<String> getSets() {
    return sets;
  }

  /**
   * Sets a white list of set specs to harvest.
   *
   * @param sets the sets to set
   */
  public void setSets(Set<String> sets) {
    this.sets = Objects.requireNonNull(sets);
  }

  /**
   * Returns whether or not to remove name spaces.
   *
   * @return the removeNamespaces
   */
  public boolean isRemoveNamespaces() {
    return removeNamespaces;
  }

  /**
   * Sets whether or not to remove name spaces.
   *
   * @param removeNamespaces the removeNamespaces to set
   */
  public void setRemoveNamespaces(boolean removeNamespaces) {
    this.removeNamespaces = removeNamespaces;
  }
}
